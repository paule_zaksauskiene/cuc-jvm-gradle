package com.brighttalk.qa.utils;

import com.google.common.base.Function;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;

import java.util.concurrent.TimeUnit;

public class SeleniumUtil {

    private static final Logger LOG = Logger.getLogger(SeleniumUtil.class);

    private static Long timeout = Long.valueOf(PropertiesUtil.getProperty("timeout"));
    private static Long poll = Long.valueOf(PropertiesUtil.getProperty("poll"));

    public static WebElement findElement(WebDriver driver, final By locator, long timeoutInSeconds, long pollingInSeconds, final boolean refresh) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(timeoutInSeconds, TimeUnit.SECONDS)
                .pollingEvery(pollingInSeconds, TimeUnit.SECONDS)
                .ignoring(Exception.class);

        return wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                if (refresh) {
                    LOG.info(String.format("Refreshing page <%s>", driver.getCurrentUrl()));
                    driver.navigate().refresh();
                }
                ExpectedConditions.presenceOfElementLocated(locator);
                WebElement element = driver.findElement(locator);
                if (element != null) {
                    if (element.isDisplayed()) {
                        LOG.info(String.format("Element <%s> found", locator.toString()));
                    }
                } else {
                    LOG.info(String.format("Element <%s> not found", locator.toString()));
                }
                return element;
            }
        });
    }

    public static WebElement findElement(WebDriver driver, final By locator, final boolean refresh) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(timeout, TimeUnit.SECONDS)
                .pollingEvery(poll, TimeUnit.SECONDS)
                .ignoring(Exception.class);

        return wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                if (refresh) {
                    LOG.info(String.format("Refreshing page <%s>", driver.getCurrentUrl()));
                    driver.navigate().refresh();
                }
                ExpectedConditions.presenceOfElementLocated(locator);
                WebElement element = driver.findElement(locator);
                if (element != null) {
                    if (element.isDisplayed()) {
                        LOG.info(String.format("Element <%s> found", locator.toString()));
                    }
                } else {
                    LOG.info(String.format("Element <%s> not found", locator.toString()));
                }
                return element;
            }
        });
    }

    public static WebElement findElement(WebDriver driver, final By locator) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(timeout, TimeUnit.SECONDS)
                .pollingEvery(poll, TimeUnit.SECONDS)
                .ignoring(Exception.class);

        return wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                ExpectedConditions.presenceOfElementLocated(locator);
                WebElement element = driver.findElement(locator);
                if (element != null) {
                    if (element.isDisplayed()) {
                        LOG.info(String.format("Element <%s> found", locator.toString()));
                    }
                } else {
                    LOG.info(String.format("Element <%s> not found", locator.toString()));
                }
                return element;
            }
        });
    }

    public static WebElement findElementWithinElement(WebElement element, final By locator) {
        return element.findElement(locator);
    }

    private static Select selectElement(WebDriver driver, final By locator, long timeoutInSeconds, long pollingInSeconds) {
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(timeoutInSeconds, TimeUnit.SECONDS)
                .pollingEvery(pollingInSeconds, TimeUnit.SECONDS)
                .ignoring(Exception.class);

        return wait.until(new Function<WebDriver, Select>() {
            public Select apply(WebDriver driver) {
                ExpectedConditions.presenceOfElementLocated(locator);
                return new Select(driver.findElement(locator));
            }
        });
    }

    public static void sendKeys(WebDriver driver, By locator, long timeoutInSeconds, long pollingInSeconds, String value) {
        LOG.info(String.format("Writing <%s> to element <%s>", value, locator));
        findElement(driver, locator, timeoutInSeconds, pollingInSeconds, false).sendKeys(value);
    }

    public static void sendKeys(WebDriver driver, By locator, String value) {
        WebElement element = findElement(driver, locator);

        if (element != null) {
            if (element.isDisplayed()) {
                element.click();
                LOG.info(String.format("Writing <%s> to element <%s>", value, locator));
                driver.switchTo().activeElement().clear();
                driver.switchTo().activeElement().sendKeys(value);
            }
        }
    }

    public static void selectByVisibleText(WebDriver driver, By locator, String text) {
        LOG.info(String.format("Selecting <%s> from Element <%s>", text, locator));
        selectElement(driver, locator, timeout, poll).selectByVisibleText(text);
    }

    public static void clickElement(WebDriver driver, By locator, long timeoutInSeconds, long pollingInSeconds) {
        LOG.info(String.format("Clicking Element <%s>", locator));
        findElement(driver, locator, timeoutInSeconds, pollingInSeconds, false).click();

        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(pageLoadCondition);
    }

    public static void clickElement(WebDriver driver, By locator) {
        WebElement element = findElement(driver, locator);

        if (element != null && element.isDisplayed()) {
            LOG.info(String.format("Clicking Element <%s>", locator));
            element.click();
        } else {
            LOG.error(String.format("Element <%s> is not visible", locator));
        }
    }

    public static boolean isDisplayed(WebDriver driver, By locator, long timeoutInSeconds, long pollingInSeconds) {
        LOG.info(String.format("Checking if element located by <%s> is displayed", locator.toString()));
        return findElement(driver, locator, timeoutInSeconds, pollingInSeconds, false).isDisplayed();
    }

    public static boolean isDisplayed(WebDriver driver, By locator) {
        LOG.info(String.format("Checking if element located by <%s> is displayed", locator.toString()));
        return findElement(driver, locator, timeout, poll, false).isDisplayed();
    }

    public static String getElementText(WebDriver driver, By locator, long timeoutInSeconds, long pollingInSeconds) {
        return findElement(driver, locator, timeoutInSeconds, pollingInSeconds, false).getText();
    }

    public static String getElementText(WebDriver driver, By locator) {
        return findElement(driver, locator).getText();
    }

    public static void checkPage(WebDriver driver, final String expected, long timeoutInSeconds, long pollingInSeconds) {
        LOG.info(String.format("Checking if <%s> is on the Page", expected));
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(timeoutInSeconds, TimeUnit.SECONDS)
                .pollingEvery(pollingInSeconds, TimeUnit.SECONDS)
                .ignoring(Exception.class);

        wait.until(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                boolean condition = driver.getPageSource().contains(expected);
                if (!condition) {
                    LOG.info(String.format("<%s> is not displayed yet, refreshing page...", expected));
                    driver.navigate().refresh();
                }
                return condition;
            }
        });
    }

    public static Boolean elementExists(WebDriver driver, final By locator, long timeoutInSeconds, long pollingInSeconds) {
        LOG.info(String.format("Checking if element located by <%s> exists in the DOM", locator.toString()));
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(timeoutInSeconds, TimeUnit.SECONDS)
                .pollingEvery(pollingInSeconds, TimeUnit.SECONDS)
                .ignoring(Exception.class);

        return wait.until(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                ExpectedConditions.presenceOfElementLocated(locator);
                return driver.findElements(locator).size() > 0;
            }
        });
    }

    public static Boolean elementExists(WebDriver driver, final By locator) {
        LOG.info(String.format("Checking if element located by <%s> exists in the DOM", locator.toString()));
        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(timeout, TimeUnit.SECONDS)
                .pollingEvery(poll, TimeUnit.SECONDS)
                .ignoring(Exception.class);

        return wait.until(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                ExpectedConditions.presenceOfElementLocated(locator);
                return driver.findElements(locator).size() > 0;
            }
        });
    }

    public static void loadPage(WebDriver driver, final String url) {
        LOG.info(String.format("Navigating to the following url <%s>", url));
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(timeout, TimeUnit.SECONDS);

        wait.until(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                driver.get(url);
                //waitFor(2);
                return ((JavascriptExecutor) driver).executeScript(
                        "return document.readyState").equals("complete");
            }
        });
    }

    public static void clickAndDrag(WebDriver driver, WebElement elementToDrag, WebElement elementToDragTo) {
        new Actions(driver).clickAndHold(elementToDrag).moveToElement(elementToDragTo).release(elementToDragTo).build().perform();

        waitFor(5);
    }

    public static void checkAndClickAlerts(WebDriver driver) {
        try {
            driver.switchTo().alert().accept();
        } catch (NoAlertPresentException nape) {
            LOG.debug("No Alert open");
        }
    }

    public static void clickChannelFeatureCheckBox(WebDriver driver, String checkBoxID) {
        ((JavascriptExecutor) driver).executeScript("document.getElementById('channel-config-form').style.display = 'block';");
        ((JavascriptExecutor) driver).executeScript("document.getElementById('" + checkBoxID + "').style.display = 'block';");
        ((JavascriptExecutor) driver).executeScript("document.getElementById('" + checkBoxID + "').checked = true;");
        clickElement(driver, By.xpath("//*[@id='channel-config-form']/descendant::input[@name='op']"));
    }

    public static void selectChannelFeatureOption(WebDriver driver, By locator, String text) {
        ((JavascriptExecutor) driver).executeScript("document.getElementById('channel-config-form').style.display = 'block';");
        ((JavascriptExecutor) driver).executeScript("document.getElementById('edit-isSearchable').style.display = 'block';");

        selectByVisibleText(driver, locator, text);
        clickElement(driver, By.xpath("//div/input[@name='op']"));
    }

    public static void waitFor(int seconds) {
        LOG.info(String.format("Waiting for %d second(s)", seconds));
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException ie) {
            LOG.debug(ie.getMessage(), ie);
        }
    }

}
