package com.brighttalk.qa.utils;

import org.apache.log4j.Logger;
import java.io.*;
import java.net.URL;
import java.util.Calendar;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.UUID;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Util {

    private static final Logger LOG = Logger.getLogger(Util.class);

    public static final int GET_ID_PATTERN_INDEX_GROUP = 1;

    public static String getIP() {
        URL whatismyip;
        BufferedReader in;
        String ip = null;

        try {
            whatismyip = new URL("http://checkip.amazonaws.com");

            in = new BufferedReader(new InputStreamReader(
                    whatismyip.openStream()));

            ip = in.readLine();
        } catch (IOException ioe) {
            LOG.debug(ioe.getMessage(), ioe);
        }

        return ip;
    }

    public static String getStringByRegex(String pattern, int group, String input) {
        Matcher matcher = Pattern.compile(pattern).matcher(input);
        String str = null;

        if (matcher.find()) {
            str = matcher.group(group);
        }

        return str;
    }

    public static int randomize(int min, int max) {
        return min + (int) (Math.random() * ((max + min) + 1));
    }

    // public static String getRand(int len) {
    //     return UUID.randomUUID().toString().replaceAll("\\-", "").substring(0,len);
    // }

    public static String getRand() {
        return UUID.randomUUID().toString().replaceAll("\\-", "").substring(0,20);
    }

    public static String getRandString(String[] array) {
        return array[new Random().nextInt(array.length)];
    }

    public static String toYamlPropertyString(String str) {

        return toKebabCase(str);
    }

    public static String toKebabCase(String str) {

        return str.toLowerCase().replaceAll("\\s+", "-");
    }

    public static String toSnakeCase(String str) {
        return str.toLowerCase().replaceAll("\\s+", "_");
    }

    public static Object getLatestEntry(List<?> objList) {
        return objList.get(objList.size() - 1);
    }

    public static ArrayList<String> parseList(String str){
        return new ArrayList<String>(Arrays.asList(str.split("\\s*,\\s*")));
    }

    /**
     *
     * This function takes in a Calendar object and returns a long form date string without the time.
     *
     * E.g. "2015-5-30 05:00" is returned as "May 30 2015"
     *
     * @param date The Calendar object representing the date you want to format into a string
     * @return String representation of the date
     */
    public static String getLongFormDateStringNoTime(Calendar date) {

        String returnDate;
        String monthName = date.getTime().toString().substring(4, 7);
        String day = zeroPadLeft(Integer.toString(date.get(Calendar.DAY_OF_MONTH)));
        returnDate = monthName + " " + day + " " + date.get(Calendar.YEAR);
        return returnDate;
    }

    /**
     * Add a zero to the beginning of a string if the length is 1.
     *
     * E.g. "5" becomes "05"
     */
    private static String zeroPadLeft(String input) {
        String output;

        if(input.length()==1) {
            output = "0"+input;
        } else {
            output = input;
        }
        return output;
    }

    public static String createHTMLFile(String embedCode){

        String operatingSystem = System.getProperty("os.name");

        System.out.println("Working Directory = " +
                System.getProperty("user.dir"));

        String embedHtmlFilePath;
        if (operatingSystem.toLowerCase().contains("win")){

            embedHtmlFilePath =  System.getProperty("user.dir") + "\\src\\main\\resources\\embeddedPage.html";

        }  else  embedHtmlFilePath =  System.getProperty("user.dir") + "/src/main/resources/embeddedPage.html";

        File f = new File(embedHtmlFilePath);
        String myFullPath = f.getAbsolutePath();

        String htmlFilePartOne = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<body>\n";
        String htmlFilePartTwo = "</body>\n" +
                "</html>";

        PrintWriter writer = null;
        try {
            writer = new PrintWriter(embedHtmlFilePath, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.print(htmlFilePartOne);
        writer.print(embedCode);
        writer.print(htmlFilePartTwo);

        writer.close();

        //myFullPath = myFullPath.replace("bin", "src");
        System.out.println("My full image path: " + myFullPath);

        return "file://" + myFullPath;

    }

}
