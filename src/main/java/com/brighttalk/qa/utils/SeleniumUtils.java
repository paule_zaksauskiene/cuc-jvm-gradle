package com.brighttalk.qa.utils;

import com.brighttalk.qa.common.Constants;
import com.brighttalk.qa.init.EnvSetup;
import com.google.common.base.Function;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.*;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class SeleniumUtils {

    private static final Logger LOG = Logger.getLogger(SeleniumUtils.class);

    public static WebElement findElement(WebDriver driver, final By locator, final long timeoutInSeconds, long pollingInSeconds, final boolean refresh, String informativeMessage)  {

        // If refresh of the page is not needed
        if (!refresh){

            Wait<WebDriver> wait = new FluentWait<>(driver)
                    .withTimeout(timeoutInSeconds, TimeUnit.SECONDS)
                    .pollingEvery(pollingInSeconds, TimeUnit.SECONDS)

                    .withMessage("\n========================================================================" +
                                 "\n" + informativeMessage +
                                 "\n" + informativeMessage +
                                 "\n------------------------------------------------------------------------" +
                                 "\nELEMENT DID NOT APPEAR !" +
                                 "\nLOCATOR: " + locator  +
                                 "\nPAGE URL: " + driver.getCurrentUrl() +
                                 "\nWAITING TIME: " + timeoutInSeconds + " seconds." +
                                 "\n========================================================================")

                    .ignoring(Exception.class);

            return wait.until(new Function<WebDriver, WebElement>() {
                public WebElement apply(WebDriver driver) {

                    ExpectedConditions.presenceOfElementLocated(locator);
                    WebElement element = driver.findElement(locator);
                    if (element != null) {
                        if (element.isDisplayed()) {
                            LOG.info(String.format("Element <%s> found", locator.toString()));
                        }
                    } else {
                        LOG.info(String.format("Element <%s> not found", locator.toString()));
                    }
                    return element;
                }
            });
        }

    // If refresh is needed
    else {
            WebElement element = null;
            int numberOfTimes = (int)timeoutInSeconds / 60;
            while (numberOfTimes > 0){

                try {

                    element = driver.findElement(locator);
                    break;

                } catch (NoSuchElementException ex) {

                    driver.navigate().refresh();
                    System.out.println("Element not found yet, refreshing the page...");
                    LOG.info("Element not found yet, refreshing the page...");

                  try {
                        Thread.sleep(pollingInSeconds * 1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                }
                numberOfTimes = numberOfTimes - 1;

                if (numberOfTimes == 1) {
                    LOG.info("\n========================================================================" +
                            "\nELEMENT DID NOT APPEAR !" +
                            "\nLOCATOR: " + locator.toString()  +
                            "\nPAGE URL: " + driver.getCurrentUrl() +
                            "\n=========================================================================");
                }
            }
            return element;
        }
    }

    public static WebElement findElement(WebDriver driver, final By locator, final boolean refresh) {
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Constants.SELENIUM_TIMEOUT, TimeUnit.SECONDS)
                .pollingEvery(Constants.SELENIUM_POLL, TimeUnit.SECONDS)
                .withMessage("\n========================================================================" +
                        "\nELEMENT DID NOT APPEAR !" +
                        "\nLOCATOR: " + locator  +
                        "\nPAGE URL: " + driver.getCurrentUrl() +
                        "\n=========================================================================")
                .ignoring(Exception.class);

        return wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                if (refresh) {
                    LOG.info(String.format("Refreshing page <%s>", driver.getCurrentUrl()));
                    driver.navigate().refresh();
                }
                ExpectedConditions.presenceOfElementLocated(locator);
                WebElement element = driver.findElement(locator);
                if (element != null) {
                    if (element.isDisplayed()) {
                        LOG.info(String.format("Element <%s> found", locator.toString()));
                    }
                } else {
                    LOG.info(String.format("Element <%s> not found", locator.toString()));
                }
                return element;
            }
        });
    }

    public static WebElement findElement(WebDriver driver, final By locator, String informativeMessage) {

        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Constants.SELENIUM_TIMEOUT, TimeUnit.SECONDS)
                .pollingEvery(Constants.SELENIUM_POLL, TimeUnit.SECONDS)
                .ignoring(Exception.class)
                .withMessage(
                        "\n========================================================================" +
                        "\n" + informativeMessage +
                        "\n------------------------------------------------------------------------" +
                        "\nELEMENT DID NOT APPEAR !" +
                        "\nLOCATOR: " + locator  +
                        "\nPAGE URL: " + driver.getCurrentUrl() +
                        "\n=========================================================================");

        return wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                ExpectedConditions.presenceOfElementLocated(locator);
                WebElement element = driver.findElement(locator);
                if (element != null) {
                    if (element.isDisplayed()) {
                        LOG.info(String.format("Element <%s> found", locator.toString()));
                    }
                } else {
                    LOG.info(String.format("Element <%s> not found", locator.toString()));
                }
                return element;
            }
        });
    }

    public static WebElement findElementWithinElement(WebElement element, final By locator) {
        return element.findElement(locator);
    }

    private static Select selectElement(WebDriver driver, final By locator, long timeoutInSeconds, long pollingInSeconds) {
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(timeoutInSeconds, TimeUnit.SECONDS)
                .pollingEvery(pollingInSeconds, TimeUnit.SECONDS)
                .withMessage("\n========================================================================" +
                        "\nELEMENT DID NOT APPEAR !" +
                        "\nLOCATOR: " + locator  +
                        "\nPAGE URL: " + driver.getCurrentUrl() +
                        "\nWAITING TIME: " + timeoutInSeconds + " seconds." +
                        "\n=========================================================================")
                .ignoring(Exception.class);

        return wait.until(new Function<WebDriver, Select>() {
            public Select apply(WebDriver driver) {
                ExpectedConditions.presenceOfElementLocated(locator);
                return new Select(driver.findElement(locator));
            }
        });
    }

    public static void sendKeys(WebDriver driver, By locator, long timeoutInSeconds, long pollingInSeconds, String value) {
        LOG.info(String.format("Writing <%s> to element <%s>", value, locator));
        findElement(driver, locator, timeoutInSeconds, pollingInSeconds, false, "").sendKeys(value);
    }

    public static void sendKeys(WebDriver driver, By locator, String value) {
        WebElement element = findElement(driver, locator, "");

        if (element != null) {
            if (element.isDisplayed()) {
                element.click();
                LOG.info(String.format("Writing <%s> to element <%s>", value, locator));
                driver.switchTo().activeElement().clear();
                driver.switchTo().activeElement().sendKeys(value);
            }
        }
    }

    public static void selectByVisibleText(WebDriver driver, By locator, String text) {
        LOG.info(String.format("Selecting <%s> from Element <%s>", text, locator));
        selectElement(driver, locator, Constants.SELENIUM_TIMEOUT, Constants.SELENIUM_POLL).selectByVisibleText(text);
    }

    public static void clickElement(WebDriver driver, By locator, long timeoutInSeconds, long pollingInSeconds) {
        LOG.info(String.format("Clicking Element <%s>", locator));
        findElement(driver, locator, timeoutInSeconds, pollingInSeconds, false, "").click();

        ExpectedCondition<Boolean> pageLoadCondition = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
                    }
                };
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(pageLoadCondition);
    }

    public static void clickElement(WebDriver driver, By locator) {
        WebElement element = findElement(driver, locator, "");

        if (element != null && element.isDisplayed()) {
            isElementClickable(driver, locator);
            LOG.info(String.format("Clicking Element <%s>", locator));
            element.click();
        } else {
            LOG.error(String.format("Element <%s> is not visible", locator));
        }
    }

    public static boolean isDisplayed(WebDriver driver, By locator, long timeoutInSeconds, long pollingInSeconds, String message) {
        LOG.info(String.format("Checking if element located by <%s> is displayed", locator.toString()));
        return findElement(driver, locator, timeoutInSeconds, pollingInSeconds, false, message).isDisplayed();
    }

    public static boolean isDisplayed(WebDriver driver, By locator, String message) {

        LOG.info(String.format("Checking if element located by <%s> is displayed", locator.toString()));
        return findElement(driver, locator, Constants.SELENIUM_TIMEOUT, Constants.SELENIUM_POLL, false, message).isDisplayed();
    }

    public static String getElementText(WebDriver driver, By locator, long timeoutInSeconds, long pollingInSeconds) {
        return findElement(driver, locator, timeoutInSeconds, pollingInSeconds, false, "").getText();
    }

    public static String getElementText(WebDriver driver, By locator) {
        return findElement(driver, locator, "").getText();
    }

    public static String getAttributeValue(WebDriver driver, By locator, String attr) {
        return findElement(driver, locator, "").getAttribute(attr);
    }

    public static void checkPage(WebDriver driver, final String expected, long timeoutInSeconds, long pollingInSeconds) {
        LOG.info(String.format("Checking if <%s> is on the Page", expected));
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(timeoutInSeconds, TimeUnit.SECONDS)
                .pollingEvery(pollingInSeconds, TimeUnit.SECONDS)
                .ignoring(Exception.class);

        wait.until(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                boolean condition = driver.getPageSource().contains(expected);
                if (!condition) {
                    LOG.info(String.format("<%s> is not displayed yet, refreshing page...", expected));
                    driver.navigate().refresh();
                }
                return condition;
            }
        });
    }

    public static Boolean elementExists(WebDriver driver, final By locator, long timeoutInSeconds, long pollingInSeconds) {
        LOG.info(String.format("Checking if element located by <%s> exists in the DOM", locator.toString()));
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(timeoutInSeconds, TimeUnit.SECONDS)
                .pollingEvery(pollingInSeconds, TimeUnit.SECONDS)
                .ignoring(Exception.class);

        return wait.until(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                ExpectedConditions.presenceOfElementLocated(locator);
                return driver.findElements(locator).size() > 0;
            }
        });
    }

    public static Boolean elementExists(WebDriver driver, final By locator) {
        LOG.info(String.format("Checking if element located by <%s> exists in the DOM", locator.toString()));
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Constants.SELENIUM_TIMEOUT, TimeUnit.SECONDS)
                .pollingEvery(Constants.SELENIUM_POLL, TimeUnit.SECONDS)
                .ignoring(Exception.class);

        return wait.until(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                ExpectedConditions.presenceOfElementLocated(locator);
                return driver.findElements(locator).size() > 0;
            }
        });
    }

    public static void loadPage(WebDriver driver, String url, long waitTimeInSeconds) {
        driver.get(url);
        try {
            Thread.sleep(waitTimeInSeconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void loadPage(WebDriver driver, final String url) {
        LOG.info(String.format("Navigating to the following url <%s>", url));
        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Constants.SELENIUM_PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);

        wait.until(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                driver.navigate().to(url);
                waitFor(2);
                return ((JavascriptExecutor) driver).executeScript(
                        "return document.readyState").equals("complete");
            }
        });
    }

    public static void clickAndDrag(WebDriver driver, WebElement elementToDrag, WebElement elementToDragTo) {
        new Actions(driver).clickAndHold(elementToDrag).moveToElement(elementToDragTo).release(elementToDragTo).build().perform();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            LOG.info(e.getMessage(), e);
        }
    }

    public static void checkAndClickAlerts(WebDriver driver) {
        try {
            driver.switchTo().alert().accept();
        } catch (NoAlertPresentException nape) {
            LOG.debug("No Alert open");
        }
    }

    public static void clickChannelFeatureCheckBox(WebDriver driver, String checkBoxID) {
        clickElement(driver, By.xpath("//fieldset[@id='form-channel-config-for']/legend[contains(.,'Channel configuration')]"));

                try {
                    waitFor(5);
                    clickElement(driver, By.cssSelector("input#" + checkBoxID));
                } catch (Exception e) {
                    LOG.info(String.format("Failed to click <%s> - refreshing page", checkBoxID));
                    driver.navigate().refresh();
                    clickElement(driver, By.xpath("//fieldset[@id='form-channel-config-for']/legend[contains(.,'Channel configuration')]"));
                }

        clickElement(driver, By.xpath("//div/input[@name='op']"));
    }

    public static void selectChannelFeatureOption(WebDriver driver, By locator, String text) {
        clickElement(driver, By.xpath("//fieldset[@id='form-channel-config-for']/legend[contains(.,'Channel configuration')]"));

        if(driver.findElement(By.cssSelector("#channel-config-form")).getCssValue("display").equalsIgnoreCase("none")) {
            ((JavascriptExecutor) driver).executeScript("document.getElementById('channel-config-form').style.display = 'block';");

            while(!isDisplayed(driver, locator, "")) {
                driver.navigate().refresh();
                ((JavascriptExecutor) driver).executeScript("document.getElementById('channel-config-form').style.display = 'block';");

                waitFor(5);
            }

            selectByVisibleText(driver, locator, text);
        } else {
            selectByVisibleText(driver, locator, text);
        }

        clickElement(driver, By.xpath("//div/input[@name='op']"));
    }

    public static void waitFor(int seconds) {
        LOG.info(String.format("Waiting for %d second(s)", seconds));
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException ie) {
            LOG.debug(ie.getMessage(), ie);
        }
    }

    public static void waitForUntil(WebDriver driver, ExpectedCondition exc, int wait, int timeout, int poll) {

        new WebDriverWait(driver, timeout, (wait * 1000)).pollingEvery(poll, TimeUnit.SECONDS).until(exc);
    }

    public static void verifyElementNotPresent(WebDriver driver, final By locator, String message){

        try {
            driver.findElement(locator);

            Assert.fail("\n========================================================================" +
                    "\n" + message +
                    "\nLOCATOR: " + locator +
                    "\nPAGE URL: " + driver.getCurrentUrl() +
                    "\n=========================================================================");

        } catch (NoSuchElementException ex) {
            /* do nothing, link is not present, assert is passed */
        }
    }

    public static void verifyElementDisappearsFromPage(WebDriver driver, final By locator, final long timeoutInSeconds, long pollingInSeconds, String message){

        int numberOfTimes = (int)timeoutInSeconds / (int) pollingInSeconds;
        while (numberOfTimes > 0){

        try {
              driver.findElement(locator);
              driver.navigate().refresh();
              System.out.println("Element is shown, refreshing the page...");
              LOG.info("Element ids still shown, refreshing the page...");

              try {
                 Thread.sleep(pollingInSeconds * 1000);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }

        } catch (NoSuchElementException ex) {

            break;
        }

        numberOfTimes = numberOfTimes - 1;

        if (numberOfTimes == 1) {
                Assert.fail("\n========================================================================" +
                            "\nELEMENT IS SHOWN ON THE PAGE WHEN IT SHOULD NOT !" +
                            "\nLOCATOR: " + locator.toString()  +
                            "\nPAGE URL: " + driver.getCurrentUrl() +
                            "\n=========================================================================");
                }
        }
    }

    public static void verifyElementDisappearsFromPageWithoutRefresh(WebDriver driver, final By locator, final long timeoutInSeconds, long pollingInSeconds, String informativeMessage){

        int numberOfTimes = (int)timeoutInSeconds / (int)pollingInSeconds;

        while (numberOfTimes > 0){

            try {
                driver.findElement(locator);
                System.out.println("Element is shown...");
                LOG.info("Element still shown...");

                try {
                    Thread.sleep(pollingInSeconds * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            } catch (NoSuchElementException ex) {

                break;
            }

            numberOfTimes = numberOfTimes - 1;

            if (numberOfTimes == 1) {
                Assert.fail("\n========================================================================" +
                            "\n" + informativeMessage +
                            "\n------------------------------------------------------------------------" +
                            "\nELEMENT IS SHOWN ON THE PAGE WHEN IT SHOULD NOT !" +
                            "\nLOCATOR: " + locator.toString()  +
                            "\nPAGE URL: " + driver.getCurrentUrl() +
                            "\n=========================================================================");

            }
        }
    }

    public static void verifyElementShowsUpWithRefresh(WebDriver driver, final By locator, final long timeoutInSeconds, long pollingInSeconds){

        int numberOfTimes = (int)timeoutInSeconds / (int)pollingInSeconds;

        while (numberOfTimes > 0){

            try {
                driver.findElement(locator);
                System.out.println("Element is found...");
                LOG.info("Element is found...");
                if (driver.findElement(locator).isDisplayed()) {
                    System.out.println("Element is displayed...");
                    LOG.info("Element is displayed..."); 
                    break;                   
                } else {
                    Assert.fail("The element is found, but not showing up on the page");
                }

            } catch (NoSuchElementException ex) {
                try {
                    Thread.sleep(pollingInSeconds * 1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                driver.navigate().refresh();
            }   

            numberOfTimes = numberOfTimes - 1;      
            if (numberOfTimes == 1) {
                Assert.fail("\n========================================================================" +
                            "\n------------------------------------------------------------------------" +
                            "\nELEMENT IS STILL NOT SHOWN ON THE PAGE WHEN IT SHOULD!" +
                            "\nLOCATOR: " + locator.toString()  +
                            "\nPAGE URL: " + driver.getCurrentUrl() +
                            "\n=========================================================================");  
            } 
        }                         
        // while (numberOfTimes > 0){

        //     if (!driver.findElement(locator).isDisplayed()) {
        //         System.out.println("Element is still not shown...");
        //         LOG.info("Element still not shown...");
        //         try {
        //             Thread.sleep(pollingInSeconds * 1000);
        //         } catch (InterruptedException e) {
        //             e.printStackTrace();
        //         }
        //         driver.navigate().refresh();   
        //         numberOfTimes = numberOfTimes - 1;   

        //         if (numberOfTimes == 1) {
        //             Assert.fail("\n========================================================================" +
        //                         "\n------------------------------------------------------------------------" +
        //                         "\nELEMENT IS NOT SHOWN ON THE PAGE WHEN IT SHOULD!" +
        //                         "\nLOCATOR: " + locator.toString()  +
        //                         "\nPAGE URL: " + driver.getCurrentUrl() +
        //                         "\n=========================================================================");

        //         }          
        //     } else {
        //         break;
        //     }

        // }


    }

    public static boolean isElementClickable(WebDriver driver, By locator){

        try
            {
                WebDriverWait wait = new WebDriverWait(driver, 30);
                wait.until(ExpectedConditions.elementToBeClickable(locator));
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
    }

    /**
     * This is to verify if the element is displayed, but not to fail if the element does not exist at all
     * @param driver
     * @param locator
     * @param message
     * @return
     */
    public static boolean isElementDisplayed(WebDriver driver, final By locator, String message){

        boolean isDisplayed;
        try {
                isDisplayed = driver.findElement(locator).isDisplayed();

        } catch (NoSuchElementException ex) {

            // Return "false" if element does not exist at all
               isDisplayed = false;
        }

        return isDisplayed;
    }

    public static void verifyContainsText(WebDriver driver, final By locator, String containsText, String informativeMessage) {

        String message = "\n========================================================================" +
                "\n" + informativeMessage +
                "\n------------------------------------------------------------------------" +
                "\nELEMENT DID NOT APPEAR !" +
                "\nLOCATOR: " + locator  +
                "\nPAGE URL: " + driver.getCurrentUrl() +
                "\n=========================================================================";

        SeleniumUtils.findElement(driver, locator, informativeMessage);
        Assert.assertTrue(message, driver.findElement(locator).getText().contains(containsText));

    }

    public static void clearText(WebDriver driver, final By locator) {

        driver.findElement(locator).clear();

    }

    public static void takeScreenshot(WebDriver driver) {

        // Take screenshot if failure occurs

        String timeStamp = String.valueOf(Calendar.getInstance().getTimeInMillis());

        String filename = EnvSetup.failedTestOutputDirectory
                + timeStamp + "_"
                + ".png";

        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

        try {
            FileUtils.copyFile(scrFile, new File(filename));
            System.out.println("The screenshot has been placed for failed test as : " + filename);
        } catch (IOException e) {
            e.printStackTrace();
        }

        LOG.info("The screenshot has been placed for failed test as : " + filename);

    }

    /*
     Finds the locator of the element from the locators.yml file
     */
    public static String resolveElementLocator(String locatorName){

        String pageProp = Util.toYamlPropertyString(locatorName);
        String locator = PropertiesUtil.getProperty(pageProp);

        return locator;
    }
}
