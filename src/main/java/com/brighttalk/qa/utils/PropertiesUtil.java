package com.brighttalk.qa.utils;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
public class PropertiesUtil {

    private static Environment env;

    @Autowired
    private Environment environment;

    @PostConstruct
    public void init() {
        env = environment;
    }

    public static String getProperty(String name) {

        return env.getProperty(name);
    }

}

