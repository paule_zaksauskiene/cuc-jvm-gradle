package com.brighttalk.qa.init;

import com.brighttalk.qa.helper.DateTimeFormatter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Calendar;
import java.util.logging.Level;

public class EnvSetup {
	
	public static final String HOME = "test01"; // System.getProperty("test_server") // picked this value from build.xml

	/*
	* Selenium test data (on test01)
	*/
	public static final String regressionTestsManagerUserEmail  = "regressionTestsManagerUser@brighttalk.com";
	public static final String regressionTestsManagerUserPassword  = "password";
	public static final String regressionTestsDataChannelID  = "2000128227";
	public static final String regressionTestsDataChannelID2  = "2000136543";
	public static final String regressionTestsDataWebinarID  = "1510146909";
	public static final String regressionTestsDataWebinarID2  = "1510146925";
	public static final String regressionTestsDataWebinarID3  = "1510147387";
	public static final String regressionTestsDataWebinarID4  = "1510155665";

	/*
	* Screenshare join.me link
	*/
	public static final String JOIN_ME_URL  = "https://join.me/240-465-195";

	/*
	* TEST01 DATABASE CREDENTIALS
	*/
	public static final String DB_TEST01_USER  = "pzaksauskiene";
	public static final String DB_TEST01_PASSWORD  = "moh8SahS";

	// SauceLABS credentials and browsers
	
//	public static final String SAUCELABS_ID = "mkulkarni";
//	public static final String SAUCELABS_KEY = "59cb9690-cb8f-4a67-ac17-5eaecda3dc26";
	
//	public static final String SAUCELABS_ID  = "pzaksauskiene";
//	public static final String SAUCELABS_KEY = "de18271b-e015-49aa-a45b-6639b357547a";

    public static final String SAUCELABS_ID  = "rwebb";
    public static final String SAUCELABS_KEY = "06ba079b-4644-4649-b2e2-5e13b71ebc46";
//    public static final String Team_Address = "tech.mex@brighttalk.com";

//    public static final String host = "exchange01.mphd01."+HOME+".brighttalk.net";

    /*
     * Email Recipient
     */
    public static final String Email_Address = SAUCELABS_ID+"@brighttalk.com";

//    public static final String managerUserSession = "383BA0F68AD504347725EA58B62997B4%3Apauletest1%40brighttalk.com%3Axfxcbx%3A1427713008%3AEurope%2FLondon%3Atrue";

    /**
     * Failed tests screenshots directory
     */
     public static final String failedTestOutputDirectory = System.getProperty("user.dir") + "/screenshots/";

    /*
     * rabbitmq Details
     */
    public static final String RabbitMQUser = "brighttalk";
    public static final String RabbitMQQueueName = "AutomationQueue";
//    public static String RabbitTest01Factory = "exchange.mphd01.test01.brighttalk.net";
//    public static String RabbitInt05Factory = "exchange01.mphd01.int05.brighttalk.net";

    /**
     * LES page
     */
    public static final String LiveEventServicePage = "http://liveevent.mphd01." + HOME + ".brighttalk.net/";

    /**
     * Resource service page
     */
    public static final String ResourceServicePage = "http://resource.mphd01." + HOME + ".brighttalk.net/";
	public static final String ResourceServicePageStage = "http://monitoring.stage.brighttalk.net/mphd01-monitor/";
    /**
     *
     * Twillio account sid and token
     *
     */
    public static final String TWILLIO_ACCOUNT_SID  = "AC2eb0e5f44a410291acdc752de5aaea14";
//    public static final String TWILLIO_AUTH_TOKEN = "47841ce255d7d561a086d173ccc20f4e";

	public static final String BLOGGER_URL = "http://www.blogger.com";
	public static final String BLOGGER_EMAIL = "seleniumtestbrighttalk@gmail.com";
	public static final String BLOGGER_PASSWORD = "br1ghttalk";
	public static final String BLOGGER_POST_URL = "http://playertestblog.blogspot.com/2016/02/player-test-post.html";
    /*
	 * Log File Locations
	 */
    public static final String DefaultLog         = "src/com/brighttalk/utilities/logging/log_files/Automation.log";
    public static final String RabbitLog          = "src/com/brighttalk/utilities/logging/log_files/Rabbit.log";
    public static final String RabbitMessageLog   = "src/com/brighttalk/utilities/logging/log_files/Message.log";
    public static final String ErrorLog           = "src/com/brighttalk/utilities/logging/log_files/Error.log";
    // Path for archived log files
    public static final String ArchiveLogPath     = "src/com/brighttalk/utilities/logging/archive/";
//    public static String ArchiveReportPath     = "src/com/brighttalk/utilities/reporting/archive/";

    /*
    * XML Report File Location Path
    */
//    public static String ReportXMLPath         = "test-output/xml/";

	
	public static final String 	HOME_URL = "https://www."+HOME+".brighttalk.net";
    public static final String 	LOGIN_URL = "https://www."+HOME+".brighttalk.net/login?redirect=/";
	/*
	 * My brighttalk pages
	 */
	public static final String KNOWLEDGE_FEED	  = "https://www."+HOME+ ".brighttalk.net/mybrighttalk/knowledge-feed";
	public static final String SUBSCRIBED_CHANNEL = "https://www."+HOME+ ".brighttalk.net/mybrighttalk/channels";
	public static final String CHANNEL_I_OWN  	  = "https://www."+HOME+ ".brighttalk.net/mybrighttalk/own-channels";
	public static final String VIEWING_HISTORY    = "https://www."+HOME+ ".brighttalk.net/mybrighttalk/viewing-history";
	
	public static final String CHANNEL 			= "https://www."+HOME+".brighttalk.net/mybrighttalk/channel";
	public static final String PROFILE_URL		= "https://www."+HOME+".brighttalk.net/mybrighttalk/profile/edit";
	public static final String DELETEME			= "https://www."+HOME +".brighttalk.net/mybrighttalk/deleteme";
	
	/*
	 * Brochureware pages - Products and Services
	 */
	
//	public static final String VIDEO_WEBINAR_PLATFORM = HOME + ".brighttalk.net/products-services/video-webinar-platform";
//	public static final String SPONSORED_WEB_SUMMIT = HOME + ".brighttalk.net/products-services/sponsored-web-summit";
	
	/*
	 * Brochureware pages - pages / customer support
	 */
	
//	public static final String PRICING_URL = "https://www."+ HOME +".brighttalk.net/marketing-solutions/pricing";
	public static final String PLATFORM_OVERVIEW_URL = "https://www."+ HOME +".brighttalk.net/marketing-solutions/marketing-platform";
	public static final String CUSTOMER_QUOTE_URL = "https://www."+ HOME +".brighttalk.net/marketing-solutions/quote";
	
//	public static final String FEATURES_CONTENT = "https://www."+ HOME +".brighttalk.net/marketing-solutions/content-audience-revenue";
//	public static final String FEATURES_AUDIENCE = "https://www."+ HOME +".brighttalk.net/marketing-solutions/content-audience-revenue#audience";
//	public static final String FEATURES_REVENUE = "https://www."+ HOME +".brighttalk.net/marketing-solutions/content-audience-revenue#revenue";
	
	public static final String CONTACTUS_URL =  "https://www."+ HOME  + ".brighttalk.net/pages/about-brighttalk/contact-us";
	
//	public static final String CUSTOMER_SUPPORT_AUDIENCE = HOME + ".brighttalk.net/pages/customer-support/audience";
	
	
	/*
	 * Common pages
	 */
	public static final String WEBCASTS 	= "https://www."+HOME +".brighttalk.net/webcasts";
	public static final String CHANNELS 	= "https://www."+HOME +".brighttalk.net/channels";
	public static final String SUMMITS  	= "https://www."+HOME +".brighttalk.net/summits";
//	public static final String PRESENTING 	= "https://www."+HOME +".brighttalk.net/presenting";
//	public static final String Privacypolicy= "https://www."+HOME +".brighttalk.net/pages/privacy-policy";
	public static final String SEARCH 		= "https://www."+HOME +".brighttalk.net/search";
	/*
	 * Manager pages
	 */
	
	public static final String MANAGER_SUMMITS = "https://www."+ HOME +".brighttalk.net/admin/summits";
	public static final String MANAGER_SUMMIT_MANAGE = "http://www." + HOME +".brighttalk.net/summit/manage";
	public static final String ADMIN_SITE_COMMUNITIES = "https://www."+ HOME +".brighttalk.net/admin/other/community";
	
	// for the following url, you have to add the type of the channel at the end
	// String to be replace by the channel id in the SCHEDULE_WEBCAST_PAGE field
	public static final String CHANNELID = "CHANNELID";

	// then we just replace CHANNELID with the right number
	public static final String SCHEDULE_WEBCAST_PAGE = "http://www."+HOME +".brighttalk.net/mybrighttalk/channel/CHANNELID/webcast/create"; 
//	public static final String CHANNEL_WEBCAST_LISTING = "http://www."+HOME +".brighttalk.net/mybrighttalk/channel/CHANNELID/webcasts";

	//Forgot Password URL
	public static final String FORGOT_PASSWORD = "https://www." +HOME+ ".brighttalk.net/mybrighttalk/forgotpassword" ;
	public static final String CHANGE_PASSWORD = "https://www." +HOME+ ".brighttalk.net/mybrighttalk/changepassword";
	
	public boolean 				webdriver_flag = false;
	public boolean 				user_login_flag	= false;
//	public boolean 				channel_owner_flag = false;
	
	public WebDriver web_driver = null;
	public DesiredCapabilities capability = null;
	
//	public EnvSetup(){
//
//		initialize(browser);
//
//	}
	
	public EnvSetup(String operating_sys, String browser, String browser_version, String run_on, String testJobName) {

		if(run_on.equals("CI"))

			initialize(browser);
		else
			initialize(operating_sys,browser, browser_version, testJobName);

	}

	public EnvSetup() {



	}
	
	public void initialize(String browser) {
		
		File webdriverLogFile = new File(String.format("src/com/brighttalk/utilities/logging/log_files/webdriver_logs/driverLog_%s.txt", String.valueOf(Calendar.getInstance().getTimeInMillis())));
        File firefoxLogFile = new File(String.format("src/com/brighttalk/utilities/logging/log_files/webdriver_logs/firefoxLog_%s.txt", String.valueOf(Calendar.getInstance().getTimeInMillis())));

        if (browser.toLowerCase().contains("safari")){

			web_driver  = new SafariDriver();
			this.clearSafariData();
			web_driver.close();
			web_driver = new SafariDriver();
		}

		if (browser.contains("chrome")){

            File f = new File("chromedriver-7");
			//File f = new File("chromedriver.exe");
            String chromedriverPath = f.getAbsolutePath();
            System.setProperty("webdriver.chrome.driver", chromedriverPath);

			ChromeOptions options = new ChromeOptions();
			//options.addArguments("-incognito");
			options.addArguments("--disable-popup-blocking"); // Its important to enable pop ups as clicking on attachments opens new links

			web_driver = new ChromeDriver(options);

		}

		if (browser.toLowerCase().contains("firefox")) {

			/*
			File pathToBinary = new File("C:\\Program Files\\Mozilla Firefox\\firefox.exe");
			FirefoxBinary ffBinary = new FirefoxBinary(pathToBinary);

			FirefoxProfile profile = new FirefoxProfile();
			profile.setPreference("webdriver.log.file", webdriverLogFile.getAbsolutePath());
			profile.setPreference("webdriver.firefox.logfile", firefoxLogFile.getAbsolutePath());

			web_driver = new FirefoxDriver(ffBinary, profile);
			*/
			//FirefoxProfile profile = new FirefoxProfile();
			//profile.setPreference("webdriver.log.file", webdriverLogFile.getAbsolutePath());
			//profile.setPreference("webdriver.firefox.logfile", firefoxLogFile.getAbsolutePath());

			//web_driver = new FirefoxDriver(profile);

		}

        web_driver.manage().window().maximize();

	}
	
	public void  initialize(String operating_sys, String browser, String browser_version, String testJobName){
		
		System.out.println(" [+] Running Test Against: " + HOME_URL+" with TestNG driver");

		switch (browser) {
			case "chrome":
				capability = DesiredCapabilities.chrome();
				capability.setCapability("platform", operating_sys);
				capability.setCapability("job-name", testJobName);

				break;
			case "firefox":
				capability = DesiredCapabilities.firefox();
				capability.setCapability("platform", operating_sys);
				capability.setCapability("job-name", testJobName);

				LoggingPreferences logs = new LoggingPreferences();
				logs.enable(LogType.BROWSER, Level.ALL);
				logs.enable(LogType.CLIENT, Level.ALL);
				logs.enable(LogType.DRIVER, Level.ALL);
				logs.enable(LogType.PERFORMANCE, Level.ALL);
				logs.enable(LogType.PROFILER, Level.ALL);
				logs.enable(LogType.SERVER, Level.ALL);

				capability.setCapability(CapabilityType.LOGGING_PREFS, logs);

				break;
			case "ie":
				capability = DesiredCapabilities.internetExplorer();
				capability.setCapability("platform", operating_sys);
				capability.setCapability("job-name", testJobName);
				break;
			case "safari":
				capability = DesiredCapabilities.safari();
				capability.setCapability("platform", operating_sys);
				capability.setCapability("job-name", testJobName);
				break;
		}

		try {
			web_driver = new RemoteWebDriver(new URL("http://54.93.101.115:4444/wd/hub"), capability);
		} catch (MalformedURLException e) {
			
			e.printStackTrace();
		}
		
		webdriver_flag = true;
	}
/*
***REVIEW: THIS CODE IS FLAGGED FOR DELETION***
	public boolean isWebdriver_flag() {
		return webdriver_flag;
	}
*/
	public boolean isUser_login_flag() {
		return user_login_flag;
	}
	
	public void quitWebDriver(){
		
		web_driver.quit();
		web_driver = null;
		webdriver_flag = false;
	}

	public static void clearSafariData() {

		// Wait some time for Safari to start
		DateTimeFormatter.waitForSomeTime(5);
		Runtime runtime = Runtime.getRuntime();
		String applescriptCommand = "tell application \"Safari\" to activate\n" +
				" delay 2\n" +
				" tell application \"System Events\" to tell process \"Safari\"\n" +
				" keystroke \",\" using command down\n" +
				" tell window 1\n" +
				" delay 3\n" +
				" click button \"Privacy\" of toolbar 1\n" +
				" delay 3\n" +
				" click button 2 of group 1 of group 1\n" +
				" delay 1\n" +
				" click button \"Remove Now\" of sheet 1\n" +
				" end tell\n" +
				" keystroke \"w\" using command down\n" +
				" end tell";

		System.out.println("applescriptCommand: " + applescriptCommand);
		String[] args = {"osascript", "-e", applescriptCommand};
		try {
			Process process = runtime.exec(args);
		} catch (IOException e) {
			e.printStackTrace();
		}

		// Wait for cookies to be deleted
		DateTimeFormatter.waitForSomeTime(20); // important to wait as otherwise the test begins before the script has finished
	}
}