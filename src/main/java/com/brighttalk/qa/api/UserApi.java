package com.brighttalk.qa.api;

import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.siteElements.User;
import com.brighttalk.qa.scenarios.UserHandler;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.http.*;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;


public class UserApi {
	
	private static final String INSERT_USER_VALUES_TEMPLATE = "<request><embed><url>http://www.mysite.com/mychannel</url><track>uid:123,tid:456</track></embed> "+
    						"<form><formAnswer id='1'>${firstName}</formAnswer>" +
    						"<formAnswer id='2'>${lastName}</formAnswer>" + 
    						"<formAnswer id='3'>${email}</formAnswer>" +
    						"<formAnswer id='4'>${timezone}</formAnswer>" +
    						"<formAnswer id='5'>${password}</formAnswer>" +
    						"<formAnswer id='6'>${password}</formAnswer>" +
    						"<formAnswer id='7'></formAnswer></form></request>";
	

	public static void registerUser(User myUser) {
		
		try{
            CloseableHttpClient httpclient = HttpClientBuilder.create().build();
	        HttpPost post = new HttpPost("https://www." + TestConfig.getEnv() +".brighttalk.net/service/user/xml/register");
	       
	        Map<String, Object> params = new HashMap<>();

	 	    params.put("firstName", myUser.getFirstName());
	 	    params.put("lastName", myUser.getSecondName());
	 	    params.put("email", myUser.getEmail());
	 	    params.put("timezone", myUser.getTimezone());
	 	    params.put("password", myUser.getPassword());
	 	 

	  	    StrSubstitutor strSubstitutor = new StrSubstitutor(params);
	 	    String newUserData =  strSubstitutor.replace(INSERT_USER_VALUES_TEMPLATE);
	  
	    	StringEntity input = new StringEntity(newUserData);
			input.setContentType("application/xml");
			post.setEntity(input);
	    	        	
	    	String response = EntityUtils.toString( httpclient.execute( post ).getEntity(), "UTF-8" );
	    	Assert.assertTrue("The user " + myUser.getEmail() + " was not created via API, the response was: "+ response, response.contains("processed"));
            System.out.println("\n User is created: " + response);
	    	httpclient.close();
		}
		catch(IOException ioe) {System.out.println("IOException: UserApi.RegUser: "+ioe.toString());	}
		catch(ParseException pex){System.out.println("ParseException: UserApi.RegUser: "+pex.toString());}
    	
 
    }
	
	public static String loginUser(User myUser){

        HttpClient httpclient = HttpClientBuilder.create().build();
		HttpPost postLogin = new HttpPost("https://www." + TestConfig.getEnv() +".brighttalk.net/service/user/xml/login");

		String userLoginData = "<request><user><identifier>"+myUser.getEmail()+"</identifier><password>"+myUser.getPassword()+"</password></user></request>";
		StringEntity inputForLogin = null;
		try {
			inputForLogin = new StringEntity(userLoginData);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		inputForLogin.setContentType("application/xml; charset=UTF-8");
		postLogin.setEntity(inputForLogin);

		HttpResponse response = null;
		try {
			response = httpclient.execute( postLogin );
		} catch (IOException e) {
			e.printStackTrace();
		}
		HttpEntity entity = response.getEntity();

        System.out.println("Status line after login on API" + response.getStatusLine());

		System.out.println("The user is logged in on API: user: " + myUser.getEmail() + " password: " + myUser.getPassword());

		Header headers[] = response.getAllHeaders();
		Header cookieHeader = null;
		
		for(Header h:headers){

            System.out.println("My header: " + h.getName() + ": " + h.getValue());
			if (h.getName().contains("Set-Cookie")) {
				
				cookieHeader = h;
			}		
		}
		Assert.assertNotNull("loginUser - Cookie Header not found", cookieHeader);
		try {
			String content = EntityUtils.toString(entity);
		} catch (IOException e) {
			e.printStackTrace();
		}

		HeaderElement cookieHeaderElements[] = cookieHeader.getElements();
    	String sessionID = null;
    	
    	for (HeaderElement elements:cookieHeaderElements){


    		if (elements.getName().contains("BTSESSION")) sessionID = elements.getValue();
    	}

        return sessionID;
    	
	}

    // http://svn.brighttalk.com/wiki/G2Project/API/External/User/DeleteMe
	public static void deleteUser(User myUser){

		try{
	
			String btSession = loginUser (myUser);
			btSession = "BTSESSION=" + btSession;
            CloseableHttpClient httpclient = HttpClientBuilder.create().build();
			
			HttpPost postDeleteMe = new HttpPost("https://www." + TestConfig.getEnv() +".brighttalk.net/service/user/xml/deleteme");
			postDeleteMe.addHeader("Cookie", btSession);
	    	    	
	    	String requestData = "<request />";

	    	StringEntity inputForDeleteMeRequest = new StringEntity(requestData);
	    	inputForDeleteMeRequest.setContentType("application/xml");
	    	postDeleteMe.setEntity(inputForDeleteMeRequest);
	    	
	    	String response = EntityUtils.toString( httpclient.execute( postDeleteMe ).getEntity(), "UTF-8" );
            Assert.assertTrue( "==========================================================" +
                             "\nThe user " + myUser.getEmail() + " was not deleted via API, " +
                             "\nThe response from server: " + response +
                             "\n==========================================================", response.contains("processed"));
	    	System.out.println (" [+] User Deleted cia API: " +  myUser.getEmail());
	    	httpclient.close();
		}
		
		catch(IOException ioe) {System.out.println("IOException: UserApi.DelUser: "+ioe.toString());	}
		catch(ParseException pex){System.out.println("ParseException: UserApi.DelUser: "+pex.toString());}

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
		
	}

    public static void deleteUserNoRestrictions(User myUser){

        String myUserId = new UserHandler().getUserId(myUser);
        try{

            User managerUser = new User();
            managerUser.setEmail(TestConfig.regressionTestsManagerUserEmail);
            String btSession = loginUser (managerUser);
            btSession = "BTSESSION=" + btSession;
            CloseableHttpClient httpclient = HttpClientBuilder.create().build();

            HttpPost postDeleteMe = new HttpPost("http://user." + TestConfig.getEnv() +".brighttalk.net/internal/xml/deleteaccount");
            postDeleteMe.addHeader("Cookie", btSession);

            String requestData = "<request><user id='" + myUserId + "'/></request>";
            System.out.println("The request data: " + requestData);
            System.out.println("Request url: " + postDeleteMe.toString());
            System.out.println("Cookie: " + btSession);

            StringEntity inputForDeleteMeRequest = new StringEntity(requestData);
            inputForDeleteMeRequest.setContentType("application/xml");
            postDeleteMe.setEntity(inputForDeleteMeRequest);

            String response = EntityUtils.toString( httpclient.execute( postDeleteMe ).getEntity(), "UTF-8" );
            Assert.assertTrue( "==========================================================" +
                    "\nThe user " + myUser.getEmail() + " was not deleted via API, " +
                    "\nThe response from server: " + response +
                    "\n==========================================================", response.contains("processed"));
            System.out.println (" [+] User Deleted via API: " +  myUser.getEmail());
            httpclient.close();
        }

        catch(IOException ioe) {System.out.println("IOException: UserApi.DelUser: "+ioe.toString());	}
        catch(ParseException pex){System.out.println("ParseException: UserApi.DelUser: "+pex.toString());}

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /*
      Update user details
      http://svn.brighttalk.com/wiki/G2Project/API/External/User/Profile
     */
	public static void updateUserDetails(User myUser){

		String myUserId = new UserHandler().getUserId(myUser);
		try{

			User managerUser = new User();
			managerUser.setEmail(TestConfig.regressionTestsManagerUserEmail);
			String btSession = loginUser (managerUser);
			btSession = "BTSESSION=" + btSession;
			CloseableHttpClient httpclient = HttpClientBuilder.create().build();

			HttpPost postDeleteMe = new HttpPost("http://user." + TestConfig.getEnv() +".brighttalk.net/internal/xml/deleteaccount");
			postDeleteMe.addHeader("Cookie", btSession);

			String requestData = "<request><user id='" + myUserId + "'/></request>";
			System.out.println("The request data: " + requestData);
			System.out.println("Request url: " + postDeleteMe.toString());
			System.out.println("Cookie: " + btSession);

			StringEntity inputForDeleteMeRequest = new StringEntity(requestData);
			inputForDeleteMeRequest.setContentType("application/xml");
			postDeleteMe.setEntity(inputForDeleteMeRequest);

			String response = EntityUtils.toString( httpclient.execute( postDeleteMe ).getEntity(), "UTF-8" );
			Assert.assertTrue( "==========================================================" +
					"\nThe user " + myUser.getEmail() + " was not deleted via API, " +
					"\nThe response from server: " + response +
					"\n==========================================================", response.contains("processed"));
			System.out.println (" [+] User Deleted via API: " +  myUser.getEmail());
			httpclient.close();
		}

		catch(IOException ioe) {System.out.println("IOException: UserApi.DelUser: "+ioe.toString());	}
		catch(ParseException pex){System.out.println("ParseException: UserApi.DelUser: "+pex.toString());}

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	/*
	http://svn.brighttalk.com/wiki/G2Project/API/External/User/Rest/CurrentUserForSession
	 */
	public static void getUserBySession(String session){

		HttpClient httpclient = HttpClientBuilder.create().build();

		HttpGet httpget = new HttpGet("https://www." + TestConfig.getEnv() + "/service/user/session/user");

		httpget.addHeader("Cookie", session);
		httpget.addHeader("Accept", "application/json");

		HttpResponse response = null;
		try {
			response = httpclient.execute(httpget);
		} catch (IOException e) {
			e.printStackTrace();
		}

		Assert.assertNotNull("getUserbySession - response from http://communication." + TestConfig.getEnv() +".brighttalk.net/internal/xml/get was null", response);
		HttpEntity entity = response.getEntity();
		String myresponse = null;
		try {
			myresponse = EntityUtils.toString(entity);
		} catch (IOException e) {
			e.printStackTrace();
		}
		Assert.assertNotNull("getWebcastPIN - Entity extraced from http://communication." + TestConfig.getEnv() + ".brighttalk.net/internal/xml/get was null", myresponse);
		int frontIndex = myresponse.indexOf("user id=");
		frontIndex = frontIndex + 8;
		int endIndex = myresponse.indexOf("externalId");
		String userID = myresponse.substring(frontIndex, endIndex-1);
		System.out.println("My user ID from API response:" + userID);

	}


}
