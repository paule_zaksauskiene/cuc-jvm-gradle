package com.brighttalk.qa.api;

import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.init.EnvSetup;
import com.brighttalk.qa.siteElements.Channel;
import com.brighttalk.qa.siteElements.User;
import com.brighttalk.qa.siteElements.Vote;
import com.brighttalk.qa.utils.Util;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.commons.lang.StringEscapeUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class VotesApi {

    private static final String AddVoteTemplate =

            "{" +
                    "\"webcast\":" +
                    "{" +
                    "\"id\":${webcastId}" +
                    "}," +
                    "\"question\":\"${Votequestiontext}\"," +
                    "\"options\":  [" +
                    "{" +
                    "\"value\":\"${option1}\"" +
                    "}," +
                    "{" +
                    "\"value\":\"${option2}\"" +
                    "}," +
                    "{" +
                    "\"value\":\"${option3}\"" +
                    "}," +
                    "{" +
                    "\"value\":\"${option4}\"" +
                    "}," +
                    "{" +
                    "\"value\":\"${option5}\"" +
                    "}" +
                    "]" +
                    "}";

    private static final String updateVoteTemplate =
            "{" +
                    "\"id\":${voteId}," +
                    "\"status\":${status}," +
                    "\"question\":\"${Votequestiontext}\"," +
                    "\"options\":  [" +
                    "{" +
                    "\"value\":\"${option1}\"" +
                    "}," +
                    "{" +
                    "\"value\":\"${option2}\"" +
                    "}," +
                    "{" +
                    "\"value\":\"${option3}\"" +
                    "}," +
                    "{" +
                    "\"value\":\"${option4}\"" +
                    "}," +
                    "{" +
                    "\"value\":\"${option5}\"" +
                    "}" +
                    "]" +
                    "}";


    // https://www.stage.brighttalk.net/service/audience/vote
    public static String addVote(Channel myChannel, User channelOwner, String webinarId, Vote myVote) {
        String voteId = "";
        try {

            String sessionID = UserApi.loginUser(channelOwner);
            sessionID = "BTSESSION=" + sessionID;
            System.out.println("My BTSESSION : " + sessionID);

            HttpClient httpclient = HttpClientBuilder.create().build();

            // Channel owner creates channel - INTERNAL CALLS MAY NOT WORK ON PROD AND STAGE
            HttpPost postAddVote = new HttpPost("https://www." + TestConfig.getEnv() + ".brighttalk.net/service/audience/vote");

            Map<String, Object> params = new HashMap<>();

            params.put("webcastId", webinarId);
            StringEscapeUtils.escapeJavaScript(myVote.getVoteQuestion());
            params.put("Votequestiontext", StringEscapeUtils.escapeJavaScript(myVote.getVoteQuestion()));
            params.put("option1", StringEscapeUtils.escapeJavaScript(myVote.getVoteOptions().get(0)));
            params.put("option2", StringEscapeUtils.escapeJavaScript(myVote.getVoteOptions().get(1)));
            params.put("option3", StringEscapeUtils.escapeJavaScript(myVote.getVoteOptions().get(2)));
            params.put("option4", StringEscapeUtils.escapeJavaScript(myVote.getVoteOptions().get(3)));
            params.put("option5", StringEscapeUtils.escapeJavaScript(myVote.getVoteOptions().get(4)));

            StrSubstitutor strSubstitutor = new StrSubstitutor(params);
            String newUserData = strSubstitutor.replace(AddVoteTemplate);

            System.out.println("Add vote request data: " + newUserData);

            StringEntity input = new StringEntity(newUserData);
            System.out.println("New vote data: " + newUserData);

            input.setContentType("application/json");
            postAddVote.setEntity(input);
            postAddVote.addHeader("Cookie", sessionID);
            System.out.println("Session data: " + sessionID);

            String responseFromAddVote = EntityUtils.toString(httpclient.execute(postAddVote).getEntity(), "UTF-8");
            System.out.println("==== Trying to add vote to webinar" + webinarId + ", response from server: " + responseFromAddVote);

            voteId = Util.getStringByRegex("\"id\":(\\d+)", Util.GET_ID_PATTERN_INDEX_GROUP, responseFromAddVote);

            System.out.println("VoteId: " + voteId);


        } catch (IOException ioe) {
            System.out.println("IOException: ChannelApi.createCH: " + ioe.toString());
        } catch (ParseException pex) {
            System.out.println("ParseException: ChannelApi.createCH: " + pex.toString());
        }
        return voteId;
    }

    // https://www.stage.brighttalk.net/service/audience/vote/${voteId}
    public static void updateVote(User channelOwner, String webinarId, Vote myVote) {
        try {

            String sessionID = UserApi.loginUser(channelOwner);
            sessionID = "BTSESSION=" + sessionID;
            System.out.println("My BTSESSION : " + sessionID);

            HttpClient httpclient = HttpClientBuilder.create().build();

            // Channel owner creates channel - INTERNAL CALLS MAY NOT WORK ON PROD AND STAGE
            HttpPut putUpdateVote = new HttpPut("https://www." + EnvSetup.HOME + ".brighttalk.net/service/audience/vote/" + myVote.getVoteId());

            String body = "{";
            body += "\"id\":" + myVote.getVoteId();

            if (myVote.getStatus() != null) body += ", \"status\":\"" + myVote.getStatus() + "\"";
            body += "}";

            StringEntity input = new StringEntity(body);
            input.setContentType("application/json");
            putUpdateVote.setEntity(input);
            putUpdateVote.addHeader("Cookie", sessionID);

            String responseFromAddVote = EntityUtils.toString(httpclient.execute(putUpdateVote).getEntity(), "UTF-8");
            System.out.println("==== Trying to update vote for webinar" + webinarId + ", response from server: " + responseFromAddVote);

        } catch (IOException ioe) {
            System.out.println("IOException: ChannelApi.createCH: " + ioe.toString());
        } catch (ParseException pex) {
            System.out.println("ParseException: ChannelApi.createCH: " + pex.toString());
        }
    }
}
