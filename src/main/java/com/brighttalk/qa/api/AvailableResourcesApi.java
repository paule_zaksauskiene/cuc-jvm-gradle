package com.brighttalk.qa.api;

import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.init.EnvSetup;
import com.brighttalk.qa.helper.DateTimeFormatter;
import junit.framework.Assert;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Mike, pzaksauskiene
 */
public class AvailableResourcesApi {

    /**
     *
     * This call checks the resource status API to get the count of available resources:
     * http://resource.mphd01.int05.brighttalk.net/inservice/status
     *
     * @return integer The count of available resources.
     */

    public static int getAvailableResources() {

        JSONParser parser=new JSONParser();
        HttpClient httpclient = HttpClientBuilder.create().build();

        String httpUrlAddress = "";
        if(TestConfig.env == "int05"){
            httpUrlAddress = "http://resource.mphd01." + TestConfig.getEnv() + ".brighttalk.net:11000/inservice/status";
        }

        if(TestConfig.env == "test01"){
            //httpUrlAddress = "http://monitoring." + TestConfig.getEnv() + ".brighttalk.net:11000/inservice/status";
            httpUrlAddress = "http://resource.mphd01." + TestConfig.getEnv() + ".brighttalk.net/inservice/status";
        }

        HttpGet httpget = new HttpGet(httpUrlAddress);
        System.out.println("Calling API: " + httpUrlAddress);

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpget);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity entity = null;
        if (response != null) {
            entity = response.getEntity();
        } else {
            Assert.fail("Null response from " + httpUrlAddress +
                    " when checking resource availability");
        }
        String myresponse = null;
        try {
            if (entity != null) {
                myresponse = EntityUtils.toString(entity);
                System.out.println("Available resources: " + myresponse);
            } else {
                Assert.fail("Null entity from " + httpUrlAddress  +
                        " when checking resource availability");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject obj;
        try {
            obj = (JSONObject)parser.parse(myresponse);
            //Get the JSON array of unallocated channele.
            JSONArray unallocated = (JSONArray)obj.get("unallocatedchannelsreport");
            //Get the size of the array. This is zero if empty and should not error
            return unallocated.size();
        } catch (ParseException e) {
            System.out.println(" [!] Error parsing resources");
            //If the JSON is invalid, then the old method would also not work.
            return 0;
        }
    }

    /**
     * http://svn.brighttalk.com/g2/services/liveplatform/resourceservice/trunk/conf/routes
     * @param resourceId
     */
    public static void rebootResource(String resourceId){

        System.out.println("Rebooting the resource: " + resourceId);

        try{

            HttpClient httpclient = HttpClientBuilder.create().build();
            String putRequest = "http://resource.mphd01." + TestConfig.getEnv() +".brighttalk.net/resource/"+resourceId+"/reboot";

            HttpPut putRebootResource = new HttpPut(putRequest);

            HttpResponse responseFromApi = httpclient.execute( putRebootResource );

            System.out.println("Response from server (status code): " + responseFromApi.getStatusLine().getStatusCode());

            org.junit.Assert.assertTrue("THE RESPONSE FROM ENCODING RESPONSE WAS NOT 204 AS EXPECTED...", responseFromApi.getStatusLine().getStatusCode() == 200);

            // wait for resource to reboot
            DateTimeFormatter.waitForSomeTime(480);

        } catch (IOException myIOException) {
            org.junit.Assert.fail("IOException : " + myIOException.getMessage());
        }
    }

    public static void takeResourceOutOfService(String resourceId) {

        HttpClient httpclient = HttpClientBuilder.create().build();
        String putRequest = "http://resource.mphd01." + EnvSetup.HOME +".brighttalk.net/resource/"+resourceId+"/status/outofservice";

        HttpPut putRebootResource = new HttpPut(putRequest);

        try{

            HttpResponse responseFromApi = httpclient.execute( putRebootResource );
            System.out.println("Response from server (status code): " + responseFromApi.getStatusLine().getStatusCode());
            org.junit.Assert.assertTrue("THE RESPONSE FROM ENCODING RESPONSE WAS NOT 204 AS EXPECTED...", responseFromApi.getStatusLine().getStatusCode() == 200);

        } catch (IOException myIOException) {

            org.junit.Assert.fail("IOException : " + myIOException.getMessage());
        }

    }

    /**
     * http://svn.brighttalk.com/g2/services/liveplatform/resourceservice/trunk/conf/routes
     * @param resourceId
     */

    public static void putResourceInService(String resourceId){

        try{

            HttpClient httpclient = HttpClientBuilder.create().build();
            String putRequest = "http://resource.mphd01." + EnvSetup.HOME +".brighttalk.net/resource/"+resourceId+"/status/inservice";

            HttpPut putRebootResource = new HttpPut(putRequest);

            HttpResponse responseFromApi = httpclient.execute( putRebootResource );

            System.out.println("Response from server (status code): " + responseFromApi.getStatusLine().getStatusCode());

            org.junit.Assert.assertTrue("THE RESPONSE FROM ENCODING RESPONSE WAS NOT 204 AS EXPECTED...", responseFromApi.getStatusLine().getStatusCode() == 200);
        } catch (IOException myIOException) {

            org.junit.Assert.fail("IOException : " + myIOException.getMessage());
        }
    }

    /**
     * This is to check if the healthstate from 5 adapters is Trusted
     * http://svn.brighttalk.com/g2/services/liveplatform/resourceservice/trunk/conf/routes
     * (wirecast, zoiper, imagecaptureadapter, joinme, powerpoint)
     * Note : media-playin doesn't appear to count
     * @param resourceId
     * @return
     */

    public static boolean isResourceHealthy(String resourceId) {

        JSONParser parser=new JSONParser();
        HttpClient httpclient = HttpClientBuilder.create().build();

        String httpGetString = "http://resource.mphd01." + EnvSetup.HOME + ".brighttalk.net/resource/" + resourceId + "/status";
        System.out.println("http get string: " + httpGetString);
        HttpGet httpget = new HttpGet(httpGetString);

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpget);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity entity = null;
        if (response != null) {
            entity = response.getEntity();
        } else {
            Assert.fail("Null response from http://resource.mphd01." + EnvSetup.HOME + ".brighttalk.net/resource/" + resourceId + "/status" +
                    " when checking resource status");
        }
        String myresponse = null;
        try {
            if (entity != null) {
                myresponse = EntityUtils.toString(entity);
                System.out.println("my response: " + myresponse);
            } else {
                Assert.fail("Null entity from http://resource.mphd01." + EnvSetup.HOME + ".brighttalk.net/resource/" + resourceId + "/status" +
                        " when checking resource status");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONArray obj;
        List<HashMap> list;
        try {

            list = new ArrayList<HashMap>();
            JSONArray array = (JSONArray)parser.parse(myresponse);

            for(int i = 0 ; i < array.size() ; i++){

                list.add((HashMap)array.get(i));
            }

            boolean trusted = true;

            for (int i = 0; i < list.size(); i++){

                System.out.println ("Health status: " + list.get(i).get("healthStatus").toString());

                // We only need to check states of other than mediaplayin-adapter
                if (!list.get(i).get("name").toString().contains("mediaplayin-adapter")) {

                    if (!list.get(i).get("healthStatus").toString().contains("Trusted")) trusted = false;
                }
            }
            return trusted;

        } catch (ParseException e) {

            System.out.println(" [!] Error parsing resources");
            //If the JSON is invalid, then the old method would also not work.
            return false;
        }
    }

    /**
     * http://svn.brighttalk.com/g2/services/liveplatform/resourceservice/trunk/conf/routes
     * PUT /communication/:communicationId/release   controllers.Command.resourceReleaseCommand(communicationId: String)
     * @param communicationId
     */
    public static void deallocateResource(String communicationId){

        try{

            HttpClient httpclient = HttpClientBuilder.create().build();
            String putRequest = "http://resource.mphd01." + EnvSetup.HOME +".brighttalk.net/communication/"+communicationId+"/release";

            HttpPut putRebootResource = new HttpPut(putRequest);

            HttpResponse responseFromApi = httpclient.execute( putRebootResource );

            System.out.println("Response from server- deallocating resource (status code): " + responseFromApi.getStatusLine().getStatusCode());

            org.junit.Assert.assertTrue("THE RESPONSE FROM deallocating resource  WAS NOT 204 AS EXPECTED...", responseFromApi.getStatusLine().getStatusCode() == 200);

        } catch (IOException myIOException) {

            org.junit.Assert.fail("IOException : " + myIOException.getMessage());
        }

        // wait for resource to reboot
        DateTimeFormatter.waitForSomeTime(480);
    }

    /**
     * http://svn.brighttalk.com/g2/services/liveplatform/resourceservice/trunk/conf/routes
     * This call checks the resource status API to get the count of available resources:
     * http://resource.mphd01.int05.brighttalk.net/inservice/status
     *
     * @return integer The count of available resources.
     */
    public static boolean verifyResourceIsAllocatedForCommunication(String communicationId) {

        boolean isResourceAllocated = false;

        JSONParser parser=new JSONParser();
        HttpClient httpclient = HttpClientBuilder.create().build();

        HttpGet httpget = new HttpGet("http://resource.mphd01." + TestConfig.getEnv() + ".brighttalk.net/inservice/status");

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpget);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity entity = null;
        if (response != null) {
            entity = response.getEntity();
        } else {
            Assert.fail("Null response from http://resource.mphd01." + TestConfig.getEnv() + ".brighttalk.net/inservice/status" +
                    " when checking resource availability");
        }
        String myresponse = null;
        try {
            if (entity != null) {
                myresponse = EntityUtils.toString(entity);
                System.out.println("Available resources: " + myresponse);
            } else {
                Assert.fail("Null entity from http://resource.mphd01." + TestConfig.getEnv() + ".brighttalk.net/inservice/status" +
                        " when checking resource availability");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject obj;
        try {
            obj = (JSONObject)parser.parse(myresponse);
            //Get the JSON array of unallocated channele.
            JSONArray allocatedResources = (JSONArray)obj.get("allocatedchannelsreport");

            //Get the size of the array. This is zero if empty and should not error
            for (int i = 0; i < allocatedResources.size(); i++) {

                JSONObject oneResourceInformation = (JSONObject) allocatedResources.get(i);
                if (oneResourceInformation.containsValue(communicationId)) {

                    if (oneResourceInformation.containsValue("ALLOCATED")) {

                        isResourceAllocated = true;
                    }
                }
            }

            //return allocatedResources.size();
        } catch (ParseException e) {
            System.out.println(" [!] Error parsing resources");
            //If the JSON is invalid, then the old method would also not work.
           // return 0;
        }

        System.out.println("Information on is resource allocated for communication " +  communicationId + " : " + isResourceAllocated);
        return isResourceAllocated;
    }

    /**
     * http://svn.brighttalk.com/g2/services/liveplatform/resourceservice/trunk/conf/routes
     * This call checks the resource status API to get the count of available resources:
     * http://resource.mphd01.int05.brighttalk.net/inservice/status
     *
     * @return integer The count of available resources.
     */
    public static boolean verifyResourceIsRebootingForCommunication(String communicationId) {

        boolean isResourceAllocated = false;

        JSONParser parser=new JSONParser();
        HttpClient httpclient = HttpClientBuilder.create().build();

        HttpGet httpget = new HttpGet("http://resource.mphd01." + TestConfig.getEnv() + ".brighttalk.net/inservice/status");

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpget);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity entity = null;
        if (response != null) {
            entity = response.getEntity();
        } else {
            Assert.fail("Null response from http://resource.mphd01." + TestConfig.getEnv() + ".brighttalk.net/inservice/status" +
                    " when checking resource availability");
        }
        String myresponse = null;
        try {
            if (entity != null) {
                myresponse = EntityUtils.toString(entity);
                System.out.println("Available resources: " + myresponse);
            } else {
                Assert.fail("Null entity from http://resource.mphd01." + TestConfig.getEnv() + ".brighttalk.net/inservice/status" +
                        " when checking resource availability");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject obj;
        try {
            obj = (JSONObject)parser.parse(myresponse);
            //Get the JSON array of unallocated channele.
            JSONArray allocatedResources = (JSONArray)obj.get("allocatedchannelsreport");

            //Get the size of the array. This is zero if empty and should not error
            for (int i = 0; i < allocatedResources.size(); i++) {

                JSONObject oneResourceInformation = (JSONObject) allocatedResources.get(i);
                if (oneResourceInformation.containsValue(communicationId)) {

                    if (oneResourceInformation.containsValue("rebooting")) {

                        isResourceAllocated = true;
                    }
                }
            }

            //return allocatedResources.size();
        } catch (ParseException e) {
            System.out.println(" [!] Error parsing resources");
            //If the JSON is invalid, then the old method would also not work.
            // return 0;
        }

        System.out.println("Information on is resource allocated for communication " +  communicationId + " : " + isResourceAllocated);
        return isResourceAllocated;
    }

    /**
     * http://svn.brighttalk.com/g2/services/liveplatform/resourceservice/trunk/conf/routes
     * This call checks the resource status API to get the count of available resources:
     * http://resource.mphd01.int05.brighttalk.net/inservice/status
     *
     * @return integer The count of available resources.
     */
    public static boolean verifyResourceIsTearingDownForCommunication(String communicationId) {

        boolean isResourceAllocated = false;

        JSONParser parser=new JSONParser();
        HttpClient httpclient = HttpClientBuilder.create().build();

        HttpGet httpget = new HttpGet("http://resource.mphd01." + TestConfig.getEnv() + ".brighttalk.net/inservice/status");

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpget);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity entity = null;
        if (response != null) {
            entity = response.getEntity();
        } else {
            Assert.fail("Null response from http://resource.mphd01." + TestConfig.getEnv() + ".brighttalk.net/inservice/status" +
                    " when checking resource availability");
        }
        String myresponse = null;
        try {
            if (entity != null) {
                myresponse = EntityUtils.toString(entity);
                System.out.println("Available resources: " + myresponse);
            } else {
                Assert.fail("Null entity from http://resource.mphd01." + TestConfig.getEnv() + ".brighttalk.net/inservice/status" +
                        " when checking resource availability");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject obj;
        try {
            obj = (JSONObject)parser.parse(myresponse);
            //Get the JSON array of unallocated channele.
            JSONArray allocatedResources = (JSONArray)obj.get("allocatedchannelsreport");

            //Get the size of the array. This is zero if empty and should not error
            for (int i = 0; i < allocatedResources.size(); i++) {

                JSONObject oneResourceInformation = (JSONObject) allocatedResources.get(i);
                if (oneResourceInformation.containsValue(communicationId)) {

                    if (oneResourceInformation.containsValue("tearingdown")) {

                        isResourceAllocated = true;
                    }
                }
            }

            //return allocatedResources.size();
        } catch (ParseException e) {
            System.out.println(" [!] Error parsing resources");
            //If the JSON is invalid, then the old method would also not work.
            // return 0;
        }

        System.out.println("Information on is resource allocated for communication " +  communicationId + " : " + isResourceAllocated);
        return isResourceAllocated;
    }


    /**
     *
     * http://svn.brighttalk.com/g2/services/liveplatform/resourceservice/trunk/conf/routes
     * This call checks the resource status API to get the count of available resources:
     * http://resource.mphd01.int05.brighttalk.net/inservice/status
     *
     * @return integer The count of available resources.
     */
    public static void verifyResourcesAreAllocatedAndDeallocate() {

        boolean isResourceAllocated = false;

        JSONParser parser=new JSONParser();
        HttpClient httpclient = HttpClientBuilder.create().build();

        HttpGet httpget = new HttpGet("http://resource.mphd01." + EnvSetup.HOME + ".brighttalk.net/inservice/status");

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpget);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity entity = null;
        if (response != null) {
            entity = response.getEntity();
        } else {
            Assert.fail("Null response from http://resource.mphd01." + EnvSetup.HOME + ".brighttalk.net/inservice/status" +
                    " when checking resource availability");
        }
        String myresponse = null;
        try {
            if (entity != null) {
                myresponse = EntityUtils.toString(entity);
                System.out.println("Available resources: " + myresponse);
            } else {
                Assert.fail("Null entity from http://resource.mphd01." + EnvSetup.HOME + ".brighttalk.net/inservice/status" +
                        " when checking resource availability");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject obj;
        try {
            obj = (JSONObject)parser.parse(myresponse);
            //Get the JSON array of unallocated channele.
            JSONArray allocatedResources = (JSONArray)obj.get("allocatedchannelsreport");

            System.out.println("Allocated resources: " + allocatedResources);

            //Get the size of the array. This is zero if empty and should not error
            for (int i = 0; i < allocatedResources.size(); i++){

                JSONObject oneResourceInformation = (JSONObject)allocatedResources.get(i);

                System.out.println("One resource information: " + oneResourceInformation);

                if (oneResourceInformation.containsValue("ALLOCATED")){

                    isResourceAllocated = true;
                    String commId = (String)oneResourceInformation.get("communicationId");

                    AvailableResourcesApi.deallocateResource(commId);

                }
            }


            //return allocatedResources.size();
        } catch (ParseException e) {
            System.out.println(" [!] Error parsing resources");
            //If the JSON is invalid, then the old method would also not work.
            // return 0;
        }

       // System.out.println("Information on is resource allocated for communication " +  communicationId + " : " + isResourceAllocated);
    }

    public static void verifyResourseIsAllocatedAndDeallocate(String communicationId){

        System.out.println("Now verifying if the resource is still allocated to communication: " + communicationId );

        if (AvailableResourcesApi.verifyResourceIsAllocatedForCommunication(communicationId)){

            DateTimeFormatter.waitForSomeTime(600);
            AvailableResourcesApi.deallocateResource(communicationId);

        }
    }

    /**
     *
     * http://svn.brighttalk.com/g2/services/liveplatform/resourceservice/trunk/conf/routes
     * This call checks the resource status API to get the count of available resources:
     * http://resource.mphd01.int05.brighttalk.net/inservice/status
     *
     * @return integer The count of available resources.
     */
    public static void verifyResourcesAreErroredAndRepair() {

        boolean isResourceErrored = false;

        JSONParser parser=new JSONParser();
        HttpClient httpclient = HttpClientBuilder.create().build();

        HttpGet httpget = new HttpGet("http://resource.mphd01." + EnvSetup.HOME + ".brighttalk.net/inservice/status");

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpget);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity entity = null;
        if (response != null) {
            entity = response.getEntity();
        } else {
            Assert.fail("Null response from http://resource.mphd01." + EnvSetup.HOME + ".brighttalk.net/inservice/status" +
                    " when checking resource availability");
        }
        String myresponse = null;
        try {
            if (entity != null) {
                myresponse = EntityUtils.toString(entity);
                System.out.println("Available resources: " + myresponse);
            } else {
                Assert.fail("Null entity from http://resource.mphd01." + EnvSetup.HOME + ".brighttalk.net/inservice/status" +
                        " when checking resource availability");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject obj;
        try {
            obj = (JSONObject)parser.parse(myresponse);
            //Get the JSON array of unallocated channele.
            JSONArray allocatedResources = (JSONArray)obj.get("allocatedchannelsreport");

            System.out.println("Allocated resources: " + allocatedResources);

            //Get the size of the array. This is zero if empty and should not error
            for (int i = 0; i < allocatedResources.size(); i++){

                JSONObject oneResourceInformation = (JSONObject)allocatedResources.get(i);

                System.out.println("One resource information: " + oneResourceInformation);

                if (oneResourceInformation.containsValue("Errored")){

                    isResourceErrored = true;
                    String resourceId = (String)oneResourceInformation.get("id");

                    AvailableResourcesApi.takeResourceOutOfService(resourceId);

                    // Reboot resource
                    AvailableResourcesApi.rebootResource(resourceId);

                    // put resource back to service
                    AvailableResourcesApi.putResourceInService(resourceId);

                }
            }


            //return allocatedResources.size();
        } catch (ParseException e) {
            System.out.println(" [!] Error parsing resources");
            //If the JSON is invalid, then the old method would also not work.
            // return 0;
        }

        // System.out.println("Information on is resource allocated for communication " +  communicationId + " : " + isResourceAllocated);
    }

    /**
     *
     * http://svn.brighttalk.com/g2/services/liveplatform/resourceservice/trunk/conf/routes
     * This call checks the resource status API to get the count of available resources:
     * http://resource.mphd01.int05.brighttalk.net/inservice/status
     *
     * @return integer The count of available resources.
     */
    public static void verifyResourceIsErroredForCommunicationAndRepair(String commmunicationId) {

        boolean isResourceErrored = false;

        JSONParser parser=new JSONParser();
        HttpClient httpclient = HttpClientBuilder.create().build();

        HttpGet httpget = new HttpGet("http://resource.mphd01." + TestConfig.getEnv() + ".brighttalk.net/inservice/status");

        HttpResponse response = null;
        try {
            response = httpclient.execute(httpget);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity entity = null;
        if (response != null) {
            entity = response.getEntity();
        } else {
            Assert.fail("Null response from http://resource.mphd01." + TestConfig.getEnv() + ".brighttalk.net/inservice/status" +
                    " when checking resource availability");
        }
        String myresponse = null;
        try {
            if (entity != null) {
                myresponse = EntityUtils.toString(entity);
                System.out.println("Available resources: " + myresponse);
            } else {
                Assert.fail("Null entity from http://resource.mphd01." + TestConfig.getEnv() + ".brighttalk.net/inservice/status" +
                        " when checking resource availability");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject obj;
        try {
            obj = (JSONObject)parser.parse(myresponse);
            //Get the JSON array of unallocated channele.
            JSONArray allocatedResources = (JSONArray)obj.get("allocatedchannelsreport");

            System.out.println("Allocated resources: " + allocatedResources);

            //Get the size of the array. This is zero if empty and should not error
            for (int i = 0; i < allocatedResources.size(); i++){

                JSONObject oneResourceInformation = (JSONObject)allocatedResources.get(i);

                System.out.println("One resource information: " + oneResourceInformation);

                if (oneResourceInformation.containsValue("Errored")){

                    isResourceErrored = true;
                    String resourceId = (String)oneResourceInformation.get("id");
                    String communicationIdOnReport = (String)oneResourceInformation.get("communicationId");

                    if (communicationIdOnReport.trim().equals(commmunicationId))  {

                        System.out.println("Resource" + resourceId + "is errored for the communication" +  commmunicationId);

                        AvailableResourcesApi.takeResourceOutOfService(resourceId);

                        // Reboot resource
                        AvailableResourcesApi.rebootResource(resourceId);

                        // put resource back to service
                        AvailableResourcesApi.putResourceInService(resourceId);
                    }
                }
            }

            //return allocatedResources.size();
        } catch (ParseException e) {
            System.out.println(" [!] Error parsing resources");
            //If the JSON is invalid, then the old method would also not work.
            // return 0;
        }

        // System.out.println("Information on is resource allocated for comm unication " +  communicationId + " : " + isResourceAllocated);
    }

}
