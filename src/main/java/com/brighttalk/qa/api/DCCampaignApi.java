package com.brighttalk.qa.api;

import com.brighttalk.qa.common.World;
import com.brighttalk.qa.siteElements.DCCampaign;
import com.brighttalk.qa.siteElements.Account;
import com.brighttalk.qa.siteElements.ProgramSummary;
import com.brighttalk.qa.siteElements.Channel;
import com.brighttalk.qa.siteElements.WebcastPro;
import java.io.IOException;
import java.util.*;
import java.text.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import com.brighttalk.qa.conf.TestConfig;
import org.apache.http.entity.StringEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.junit.Assert;


@ContextConfiguration(
        classes = TestConfig.class)
public class DCCampaignApi {

    @Autowired
    World world;


	public static void createCampaign(World world, DCCampaign myCampaign, Account myAccount, ProgramSummary myProgramSummary) throws IOException{

    try{

        HttpClient httpclient = HttpClientBuilder.create().build();

        HttpPost postCreateDCCampaign = new HttpPost("https://www." + TestConfig.getEnv() + ".brighttalk.net/service/campaign/account/" + myAccount.getAccountId() + "/campaign?leadType=" + myCampaign.getLeadType());
        String url = "https://www." + TestConfig.getEnv() + ".brighttalk.net/service/campaign/account/" + myAccount.getAccountId() + "/campaign?leadType=" + myCampaign.getLeadType();

		    Channel currentChannel = world.getLocal_channels().get(world.getLocal_channels().size() - 1);
         
        int webcastCount = world.getWebcastsPro().size();

        JSONObject json = new JSONObject();
        json.put("name",myCampaign.getCampaignName());
        json.put("displayName",myCampaign.getDisplayName());
        json.put("client",myCampaign.getClient());
        json.put("programSummaryId",myProgramSummary.getProgramSummarySfId());
        json.put("programSummaryKey",myProgramSummary.getName());
        json.put("status",myCampaign.getStatus());
        json.put("type",myCampaign.getLeadType());
        json.put("budget",myCampaign.getBudget());
        json.put("newBudget",myCampaign.getNewBudget());      
        json.put("startDate",myCampaign.getStartDate());
        json.put("endDate",myCampaign.getEndDate());
        json.put("totalLeadTarget",myCampaign.getTotalLeadTarget());
        json.put("totalContentLeadsFulfilled",myCampaign.getTotalLeadsFulfilled());       
        json.put("totalSummitLeadsFulfilled",myCampaign.getTotalSummitLeadsFulfilled());
        json.put("earlyDeliveryFlag",myCampaign.getEarlyDeliveryFlag());
        json.put("promote",myCampaign.getPromote());
        json.put("cpl",myCampaign.getCpl());
        json.put("currency",myCampaign.getCurrency());
        json.put("jobLevelFilter",myCampaign.getJobLevelFilter());
        json.put("companySizeFilter",myCampaign.getCompanySizeFilter());
        json.put("recipients",myCampaign.getRecipients());
        json.put("csmEmail",myCampaign.getCsmEmail());
        json.put("leadEmail",myCampaign.getLeadEmail());
        json.put("campaignValue",myCampaign.getCampaignValue());    
        json.put("leadDeliveredValue",myCampaign.getLeadDeliveredValue());
        json.put("leadsByMonth",myCampaign.getLeadsByMonth()); 
        json.put("leadFulfillmentPrcnt",myCampaign.getLeadFulfillmentPrcnt());       
        json.put("noFilter",myCampaign.getNoFilterFlag());           
        json.put("firstDeliveryDate",myCampaign.getFirstDeliveryDate());        
        json.put("lastDeliveryDate",myCampaign.getLastDeliveryDate()); 
        json.put("reEngage",myCampaign.getReEngageFlag()); 
        json.put("isActive",myCampaign.getIsActive()); 
        json.put("createBooking",myCampaign.getCreateBookingFlag()); 
        json.put("comments",myCampaign.getComments()); 

        JSONArray regions = new JSONArray();
        for (String region : myCampaign.getRegions()){
          regions.add(region);
        }

        JSONObject regionObject = new JSONObject();
        regionObject.put("countries",regions);
        JSONArray region = new JSONArray();
        region.add(regionObject);
        json.put("regions",region);


        JSONArray companyExclusions = new JSONArray();
        for (String companyExclusion : myCampaign.getCompanyExclusions()){
          companyExclusions.add(companyExclusion);
        }
		    json.put("companyExclusions",companyExclusions);


        JSONArray partnerExclusions = new JSONArray();
        for (String partnerExclusion : myCampaign.getPartnerExclusions()){
          partnerExclusions.add(partnerExclusion);
        }
        json.put("partnerExclusions",partnerExclusions);



        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        JSONArray campaignConfigurations = new JSONArray();
        String scheduled = "";

        for(int i=0;i<webcastCount;i++){

          WebcastPro currentCommunication = world.getWebcastsPro().get(i);
      
          scheduled = format.format(currentCommunication.getStartDate().getTime());

          JSONObject config = new JSONObject();
          config.put("channelId",currentChannel.getChannelID());
          config.put("channelTitle",currentChannel.getChannelTitle());
          config.put("communicationId",currentCommunication.getWebcastID());
          config.put("communicationTitle",currentCommunication.getTitle());   
          config.put("communicationScheduled",scheduled);
          config.put("communicationStatus",currentCommunication.getStatus());       
          config.put("communicationPresenter",currentCommunication.getPresenter());
          config.put("community","Information Technology"); 
          config.put("communicationThumbnail","");
          campaignConfigurations.add(config);
        }

        json.put("campaignConfigurations",campaignConfigurations);


        StringEntity input = new StringEntity(json.toString());
        System.out.println("The create campaign json object is : =====================");
        System.out.println(json.toString());

        input.setContentType("application/json");
        postCreateDCCampaign.addHeader("Authorization","Basic YWRtaW46YWRtaW4=");
        postCreateDCCampaign.setEntity(input);

        HttpResponse responseFromCreateCampaign = httpclient.execute( postCreateDCCampaign );
        Assert.assertTrue("THE RESPONSE WAS NOT 200 AS EXPECTED...", responseFromCreateCampaign.getStatusLine().getStatusCode() == 200);


      }

      catch (IOException myIOException) {

        Assert.fail("IOException in createCampaign: " + myIOException.getMessage());

   	  }


  	}
}