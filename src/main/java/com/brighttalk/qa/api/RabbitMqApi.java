package com.brighttalk.qa.api;

import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.siteElements.Channel;
import com.brighttalk.qa.siteElements.User;
import com.brighttalk.qa.siteElements.WebcastPro;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.http.ParseException;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class RabbitMqApi {

    private static final String SEND_RABBIT_MESSAGE_TEMPLATE =
            "{\n" +
                    "\"header\": {" +
                    "          \"name\":\"${message}\"," +
                    "          \"version\": 1," +
                    "          \"id\": \"b6449451-457f-41f6-b08b-636689b46ecc\"," +
                    "          \"communicationId\":\"${commid}\",\n" +
                    "          \"created\": \"2013-12-18T09:15:00.000Z\"," +
                    "          \"originator\":{ " +
                    "               \"id\": \"1\"," +
                    "               \"system\": \"live-event-service\"," +
                    "               \"host\": \"123.43.43.123\"," +
                    "               \"extraInfo\": \"some extra info\"," +
                    "               \"version\":\"0\"" +
                    "          }\n" +
                    "}\n" +
                    "}";

    /**
     * Allows clients to send a message to the Live Platform from the internet
     * http://svn.brighttalk.com/wiki/G2Project/API/LivePlatform/PresenterAdapterMessage
     * samples: presentation-slidedeck-remove
     */
    public static void sendLivePlatformMessage(User manager, Channel myChannel, WebcastPro myCommunication, String message) throws ParseException, IOException {

        String btSession = UserApi.loginUser(manager);

        HttpClient httpclient = HttpClientBuilder.create().build();
        String putRequest = "https://www." + TestConfig.getEnv() + ".brighttalk.net/service/presenter/channel/" + myChannel.getChannelID() + "/webcast/" + myCommunication.getWebcastID() + "/message";

       // https://www.test01.brighttalk.net/service/presenter/channel/2000176693/webcast/1510197437/presenter

        HttpPut putUpdateCommunication = new HttpPut(putRequest);

        btSession = "BTSESSION=" + btSession;
        putUpdateCommunication.addHeader("Cookie", btSession);

        System.out.println("Cookie header: " + btSession);

        Map<String, Object> params = new HashMap<>();

        params.put("commid", myCommunication.getWebcastID());
        params.put("message", message);

        StrSubstitutor strSubstitutor = new StrSubstitutor(params);
        String newCommunicationData = strSubstitutor.replace(SEND_RABBIT_MESSAGE_TEMPLATE);

        System.out.println("Request URL: " + putRequest);
        System.out.println("New communication data: " + newCommunicationData);

        StringEntity input = new StringEntity(newCommunicationData);

        input.setContentType("application/json");
        putUpdateCommunication.setEntity(input);

        StatusLine myStatusLine = httpclient.execute(putUpdateCommunication).getStatusLine();
        myStatusLine.getStatusCode();

        //String responseFromUpdateCommunication = EntityUtils.toString(httpclient.execute(putUpdateCommunication).getEntity(), "UTF-8");

        //System.out.println(" [+] Trying to update communication " + myCommunication.getTitle() + ", response from server: " + responseFromUpdateCommunication);
        System.out.println("Status line: " + myStatusLine.getStatusCode());

        Assert.assertTrue("The communication " + myCommunication.getTitle() + " was not updated via API", myStatusLine.getStatusCode() == 200);

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
