package com.brighttalk.qa.api;

import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.siteElements.User;
import com.brighttalk.qa.siteElements.WebcastPro;
import org.apache.commons.lang3.text.StrSubstitutor;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by pzaksauskiene on 30/04/2014.
 * Calling: http://svn.brighttalk.com/wiki/G2Project/API/External/Audience/ViewingActivity
 */
public class AttachmentsApi {

    private static final String DOWNLOAD_ATTACHMENT_TEMPLATE =

            "<viewingActivity>" +
                "<viewing id='${viewingId}'>" +
                    "<duration>1</duration>" +
                "</viewing>" +
                "<attachmentAccesses>" +
                     "<attachmentAccess>" +
                        "<attachment id='${attachmentId}'/>" +
                      "</attachmentAccess>" +
                 "</attachmentAccesses>" +
            "</viewingActivity>";

    private static final String ADD_NEW_ATTACHMENT_TEMPLATE =

      "<asset>" +
              "<type>resource-fileupload</type>" +
              "<target id='${communicationId}' type='communication' />" +
              "<region>" +
                 "<name>US West (California)</name>" +
               "</region>" +
                "<source>" +
                    "<fileName>SeleniumTextFile.txt</fileName>" +
                    "<extension>txt</extension>" +
                "</source>" +
      "</asset>";

    public static void downloadAttachment(User myUser, String viewingID, String attachmentId) throws IOException {

        String btSession = UserApi.loginUser(myUser);
        btSession = "BTSESSION=" + btSession;
        HttpClient httpclient = HttpClientBuilder.create().build();

        HttpPost postSendQuestion = new HttpPost("https://www." + TestConfig.getEnv() +".brighttalk.net/service/audience/viewing_activity");
        postSendQuestion.addHeader("Cookie", btSession);
        Map<String, Object> params = new HashMap<>();

        params.put("viewingId", viewingID);
        params.put("attachmentId", attachmentId);

        StrSubstitutor strSubstitutor = new StrSubstitutor(params);
        String newQuestionData =  strSubstitutor.replace(DOWNLOAD_ATTACHMENT_TEMPLATE);
        System.out.println("newCommunication data: " + newQuestionData);

        StringEntity input = new StringEntity(newQuestionData);
        input.setContentType("application/xml");
        postSendQuestion.setEntity(input);

        System.out.println("Post send question: " + postSendQuestion.toString());

        try
        {
            String responseSendQuestion = EntityUtils.toString(httpclient.execute(postSendQuestion).getEntity(), "UTF-8");
            System.out.println("==== Trying to download attachment, response from server: " + responseSendQuestion);
            //Assert.assertTrue("The question has not been sent", responseSendQuestion.contains("204"));

        } catch (IOException myIOException) {}

    }

    /*
         http://svn.brighttalk.com/wiki/G2Project/API/External/AMS/Asset
     */
    public static void uploadFileTypeAttachment(User myUser, WebcastPro proWebinar) {

        String btSession = UserApi.loginUser(myUser);
        btSession = "BTSESSION=" + btSession;
        HttpClient httpclient = HttpClientBuilder.create().build();

        HttpPost postSendQuestion = new HttpPost("https://www." + TestConfig.getEnv() +".brighttalk.net/service/ams/asset");
        postSendQuestion.addHeader("Cookie", btSession);
        Map<String, Object> params = new HashMap<>();

        params.put("communicationId", proWebinar.getWebcastID());

        StrSubstitutor strSubstitutor = new StrSubstitutor(params);
        String newQuestionData =  strSubstitutor.replace(ADD_NEW_ATTACHMENT_TEMPLATE);
        System.out.println("newCommunication data: " + newQuestionData);

        StringEntity input = null;
        try {
            input = new StringEntity(newQuestionData);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        input.setContentType("application/xml");
        postSendQuestion.setEntity(input);

        System.out.println("Post send upload attachment text: " + postSendQuestion.toString());

        try
        {
            String responseSendQuestion = EntityUtils.toString(httpclient.execute(postSendQuestion).getEntity(), "UTF-8");
            System.out.println("==== Trying to upload attachment, response from server: " + responseSendQuestion);
            //Assert.assertTrue("The question has not been sent", responseSendQuestion.contains("204"));

        } catch (IOException myIOException) {}


    }
}
