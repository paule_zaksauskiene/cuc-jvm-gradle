package com.brighttalk.qa.api;

import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.init.EnvSetup;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.scenarios.WebcastHandler;
import com.brighttalk.qa.siteElements.Channel;
import com.brighttalk.qa.siteElements.User;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import com.brighttalk.qa.siteElements.WebcastPro;
import com.brighttalk.qa.utils.Util;

@ContextConfiguration(
        classes = TestConfig.class)
public class WebcastProApi {

    @Autowired
    BrightTALKService service;
    @Autowired
    World world;

	private static final String BOOK_WEBCAST_HD_VALUES_TEMPLATE =

			"<webcast>" +
					"<channel id='${channelId}'/>" +
					"<title>${title}</title>" +
					"<description>${description}</description>" +
					"<keywords>${keywords}</keywords>" +
					"<presenter>${presenter}</presenter>" +
					"<duration>${duration}</duration>" +
					"<scheduled>${startTime}</scheduled>" +
					"<timeZone>Europe/London</timeZone>" +
					"<visibility>${visibility}</visibility>" +
					"<provider name='brighttalkhd' />" +
					"<format>video</format>" +
					"<allowAnonymous>false</allowAnonymous>" +
					"<excludeFromChannelCapacity>false</excludeFromChannelCapacity>" +
					"<showChannelSurvey>false</showChannelSurvey>" +
					"<categories></categories>" +
					"<clientBookingReference>Reference 225584</clientBookingReference>" +
			"</webcast>";

	private static final String BOOK_WEBCAST_AS_MANAGER_TEMPLATE =

			"<webcast>" +
					"<channel id='${channelid}'/>" +
					"<title>${title}</title>" +
					"<description>${description}</description>" +
					"<keywords>${keywords}</keywords>" +
					"<serviced>${serviced}</serviced>" +
					"<presenter>${presenter}</presenter>" +
					"<duration>${duration}</duration>" +
					"<scheduled>${startTime}</scheduled>" +
					"<timeZone>Europe/London</timeZone>" +
					"<visibility>${visibility}</visibility>" +
					"<provider name='brighttalkhd' />" +
					"<format>video</format>" +
					"<allowAnonymous>false</allowAnonymous>" +
					"<excludeFromChannelCapacity>false</excludeFromChannelCapacity>" +
					"<showChannelSurvey>false</showChannelSurvey>" +
					"<categories></categories>" +
					"<clientBookingReference>Reference 225584</clientBookingReference>" +
					"</webcast>";


	private static final String UPDATE_OVERRIDE_COMMUNICATION_VALUES_TEMPLATE1 =

			"<webcast id='${communicationId}'>" +
				"<channel id='${channelid}'/>" +
		        	"<status>${status}</status>" +
		        	"<provider name='brighttalkhd' />" +
                    "<categories>";

	private static final String UPDATE_OVERRIDE_COMMUNICATION_VALUES_TEMPLATE_CATEGORY =

		     "<category id='${id}' term='${category}'/>";

	private static final String UPDATE_OVERRIDE_COMMUNICATION_VALUES_TEMPLATE2 =

		   "</categories>" +
		"</webcast>";

	private static final String UPDATE_OVERRIDE_COMMUNICATION_VALUES_TEMPLATE =

			"<webcast id='${communicationId}'>" +
					"<channel id='${channelid}'/>" +
					"<duration>${duration}</duration>" +
					"<status>${status}</status>" +
					"<scheduled>${scheduled}</scheduled>" +
					"<timezone>Europe/London</timezone>" +
					"<serviced>${serviced}</serviced>" +
					"<visibility>${visibility}</visibility>" +
					"<provider name='brighttalkhd' />" +
					"<allowAnonymous>${allowAnonymous}</allowAnonymous>" +
					"<categories>" +
					"<category id='56561' term='${category}'/>" +
					"</categories>" +
					"<clientBookingReference>${clientBookingReference}</clientBookingReference>" +
					"<customUrl>${customUrl}</customUrl>" +
			"</webcast>";


	private static final String ENCODING_RESPONSE_VALUES_TEMPLATE =

			"<encodingResponse href=\"http://ams.test01.brighttalk.net/internal/encodingresponse\">" +
					"<reference>${assetId}</reference>" +
					"<trackingId>${assetId}</trackingId>" +
					"<status>" +
					"<state>success</state>" +
					"<description>Successful</description>" +
					"</status>" +
					"</encodingResponse>";

	/**
	 * Update communication status overriding business rules by calling API:
	 * http://svn.brighttalk.com/wiki/G2Project/API/External/Channel/WebcastHDCRUD
	 * @throws IOException 
	 * @throws ClientProtocolException
	 * @throws ParseException
	 */
	public void updateWebcastProAddCategorizationTags(World world, WebcastPro webcastPro, List<String> tags) throws ParseException, IOException{

            // Add categories for each webinar
         	String cookie = world.getLocal_users().get(0).getSessionCookie();
			String btSession = cookie.replaceAll(";", "");

			HttpClient httpclient = HttpClientBuilder.create().build();
			String putRequest = "https://www." + TestConfig.getEnv() +".brighttalk.net/service/channel/channel/"+ world.getLocal_channels().get(0).getChannelID() +"/webcast/"+ webcastPro.getWebcastID();

			HttpPut putUpdateCommunication = new HttpPut(putRequest);
			putUpdateCommunication.addHeader("Cookie", btSession);

			System.out.println("-----REQUEST HEADERS" + putUpdateCommunication.getAllHeaders());

			Map<String, Object> params = new HashMap<>();

			// Part1
			params.put("communicationId", webcastPro.getWebcastID());
			params.put("channelid", world.getLocal_channels().get(0).getChannelID());
			params.put("status", webcastPro.getStatus());
			StrSubstitutor strSubstitutor = new StrSubstitutor(params);
			String part1 =  strSubstitutor.replace(UPDATE_OVERRIDE_COMMUNICATION_VALUES_TEMPLATE1);
			System.out.println("Update webinar part1 " + part1);

			// Part2
			String categoriesData = "";
			for (int k=0; k< tags.size(); k++){

				params.put("category", tags.get(k));
				int j = k + 1;
				params.put("id", j);
				String updateChannelDataCategories = strSubstitutor.replace(UPDATE_OVERRIDE_COMMUNICATION_VALUES_TEMPLATE_CATEGORY);
				categoriesData = categoriesData + updateChannelDataCategories;
			}

			System.out.println("Categories data: " + categoriesData);

			// Part2
			String part2 =  strSubstitutor.replace(UPDATE_OVERRIDE_COMMUNICATION_VALUES_TEMPLATE2);
			System.out.println ("Update webinar part2: " + part2);

			String updateChannelData = part1 + categoriesData + part2;

			System.out.println("----- Update webinar data:" + updateChannelData);
			StringEntity input = new StringEntity(updateChannelData);

			input.setContentType("application/xml");
			putUpdateCommunication.setEntity(input);

			String responseFromUpdateCommunication = EntityUtils.toString( httpclient.execute( putUpdateCommunication ).getEntity(), "UTF-8" );
			System.out.println(" [+] Trying to update communication " + webcastPro.getTitle() + ", response from server: " + responseFromUpdateCommunication);
			Assert.assertTrue("The communication " + webcastPro.getTitle() + " was not updated via API", responseFromUpdateCommunication.contains("<webcast id="));

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
    }

	/**
	 * Create communication via API:
	 * http://svn.brighttalk.com/wiki/G2Project/API/Internal/Channel/BookCommunication
	 * @return String of the pro webcast ID
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public static String createUpcomingWebcastHd(World world, WebcastPro myWebcastPro) throws IOException{

		String cookie = world.getLocal_users().get(0).getSessionCookie();
		String btSession = cookie.replaceAll(";", "");

		HttpClient httpclient = HttpClientBuilder.create().build();

		HttpPost postCreateCommunication = new HttpPost("https://www." +  TestConfig.getEnv() +".brighttalk.net/service/channel/channel/" + world.getLocal_channels().get(0).getChannelID() + "/webcast");
		postCreateCommunication.addHeader("Cookie", btSession);
		Map<String, Object> params = new HashMap<>();

		params.put("channelId", world.getLocal_channels().get(0).getChannelID());
		params.put("title", myWebcastPro.getTitle());
		params.put("description", myWebcastPro.getDescription());
		params.put("keywords", myWebcastPro.getTags());
		params.put("presenter", myWebcastPro.getPresenter());
		params.put("duration", String.valueOf(Integer.parseInt(myWebcastPro.getDuration()) * 60)); // duration for API is set to seconds
		params.put("visibility", myWebcastPro.getVisibility()); // duration for API is set to seconds

		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		df.setTimeZone(tz);

		// set second 0 is very important for API
		myWebcastPro.getStartDate().set(Calendar.SECOND, 0);
		String startDate = df.format(myWebcastPro.getStartDate().getTime());

		params.put("startTime", startDate);

		//LogWriter.file("Default", "My start timestamp is: " + startDate, true);

		StrSubstitutor strSubstitutor = new StrSubstitutor(params);
		String newCommunicationData =  strSubstitutor.replace(BOOK_WEBCAST_HD_VALUES_TEMPLATE);

		StringEntity input = new StringEntity(newCommunicationData);
		input.setContentType("application/xml");
		postCreateCommunication.setEntity(input);

		try
		{
			String responseFromCreateCommunucation = EntityUtils.toString( httpclient.execute( postCreateCommunication ).getEntity(), "UTF-8" );
			System.out.println("Response from create documentation: " + responseFromCreateCommunucation);

			if (responseFromCreateCommunucation.contains("CapacityExceeded")){
				Assert.fail("\n========================================================================" +
						"\nTHE PRO WEBINAR WAS NOT CREATED - CAPACITY EXCEEDED ON THE SYSTEM !" +
						"\nTRIED TO BOOK FOR: " + startDate +
						"\n=========================================================================");
			}

			Assert.assertTrue("The communication " + myWebcastPro.getTitle() + " was not created via API" +
					"\n RESPONSE FROM SERVER:" +
					"\n" + responseFromCreateCommunucation, responseFromCreateCommunucation.contains("webcast id="));

			String id = Util.getStringByRegex("<webcast id=\"(\\d+)\">", 1, responseFromCreateCommunucation);
			myWebcastPro.setWebcastID(id);
			System.out.println(" [+] Communication ID from API: " + id);

			String pinNumber = Util.getStringByRegex("<pinNumber>(\\d+)</pinNumber>", 1, responseFromCreateCommunucation);
			myWebcastPro.setPIN(pinNumber);

		} catch (IOException myIOException) {
			Assert.fail("IOException in createUpcomingWebcastHd: " + myIOException.getMessage());
		}

		return myWebcastPro.getWebcastID();

	}

	/**
	 * Update communication status overriding business rules by calling API:
	 * http://svn.dev.brighttalk.net/wiki/G2Project/API/External/Channel/WebcastCRUD
	 * @param myCommunication the communication we are working with
	 * @throws IOException
	 * @throws ClientProtocolException
	 * @throws ParseException
	 */
	public static void updateWebcastProAsManager (World world, WebcastPro myCommunication){

		String btSession = UserApi.loginUser(world.getRegressionTestsManagerUser());

		btSession = "BTSESSION=" + btSession;
		HttpClient httpclient = HttpClientBuilder.create().build();
		String putRequest = "https://www." + TestConfig.getEnv() +".brighttalk.net/service/channel/channel/"+ world.getLocal_channels().get(0).getChannelID() +"/webcast/"+ myCommunication.getWebcastID();
		HttpPut putUpdateCommunication = new HttpPut(putRequest);
		putUpdateCommunication.addHeader("Cookie", btSession);
		Map<String, Object> params = new HashMap<>();
		params.put("communicationId", myCommunication.getWebcastID());
		params.put("channelid", world.getLocal_channels().get(0).getChannelID());
		params.put("status", myCommunication.getStatus());
		params.put("duration", String.valueOf(Integer.parseInt(myCommunication.getDuration()) * 60));
		params.put("customUrl", myCommunication.getWebcastUrl());
		params.put("category", myCommunication.getCategorizationTag());
		params.put("visibility", myCommunication.getVisibility());
		params.put("clientBookingReference", myCommunication.getCampaignReference());
		if (myCommunication.getViewerAccess().contains("No registration"))
			params.put("allowAnonymous", "true");
		else params.put("allowAnonymous", "false");

		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		df.setTimeZone(tz);

		// set second 0 is very important for API
		myCommunication.getStartDate().set(Calendar.SECOND, 0);
		String startDate = df.format(myCommunication.getStartDate().getTime());

		System.out.println("startDate and time to change: " + startDate);
		params.put("scheduled", startDate);

		if (myCommunication.isServiced() == true){ params.put("serviced", true);
			} else params.put("serviced", false);

		org.apache.commons.lang3.text.StrSubstitutor strSubstitutor = new org.apache.commons.lang3.text.StrSubstitutor(params);
		String newCommunicationData =  strSubstitutor.replace(UPDATE_OVERRIDE_COMMUNICATION_VALUES_TEMPLATE);

		System.out.println ("New communication data: " + newCommunicationData);

		StringEntity input = null;
		try {
			input = new StringEntity(newCommunicationData);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		input.setContentType("application/xml");
		putUpdateCommunication.setEntity(input);

		String responseFromUpdateCommunication = null;
		try {
			responseFromUpdateCommunication = EntityUtils.toString( httpclient.execute( putUpdateCommunication ).getEntity(), "UTF-8" );
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(" [+] Trying to update communication " + myCommunication.getTitle() + ", response from server: " + responseFromUpdateCommunication);
		Assert.assertTrue("The communication " + myCommunication.getTitle() + " was not updated via API", responseFromUpdateCommunication.contains("<webcast id="));

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
	}

	/**
	 * Update communication status overriding business rules by calling API:
	 * http://svn.dev.brighttalk.net/wiki/G2Project/API/External/Channel/WebcastCRUD
	 * @param myCommunication the communication we are working with
	 * @throws IOException
	 * @throws ClientProtocolException
	 * @throws ParseException
	 */
	public static String createWebcastProAsManager (World world, WebcastPro myCommunication){

		String btSession = UserApi.loginUser(world.getRegressionTestsManagerUser());

		HttpClient httpclient = HttpClientBuilder.create().build();

		btSession = "BTSESSION=" + btSession;
		HttpPost postRequest = new HttpPost("https://www." + TestConfig.env +".brighttalk.net/service/channel/channel/" + world.getLocal_channels().get(0).getChannelID() + "/webcast");

		postRequest.addHeader("Cookie", btSession);
		Map<String, Object> params = new HashMap<>();

		params.put("channelid", world.getLocal_channels().get(0).getChannelID());
		params.put("title", myCommunication.getTitle());
		params.put("description", myCommunication.getDescription());
		params.put("keywords", myCommunication.getTags());

		if (myCommunication.isServiced() == true){

			params.put("serviced", true);
		} else
			params.put("serviced", false);

		params.put("presenter", myCommunication.getPresenter());
		params.put("duration", String.valueOf(Integer.parseInt(myCommunication.getDuration()) * 60));

		// Set start time
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		df.setTimeZone(tz);

		// set second 0 is very important for API
		myCommunication.getStartDate().set(Calendar.SECOND, 0);
		String startDate = df.format(myCommunication.getStartDate().getTime());

		System.out.println("startDate and time to change: " + startDate);
		params.put("startTime", startDate);

		// Visibility
		params.put("visibility", myCommunication.getVisibility());

		if (myCommunication.getViewerAccess().contains("No registration"))
			params.put("allowAnonymous", "true");
		else params.put("allowAnonymous", "false");

		org.apache.commons.lang3.text.StrSubstitutor strSubstitutor = new org.apache.commons.lang3.text.StrSubstitutor(params);
		String newCommunicationData =  strSubstitutor.replace(BOOK_WEBCAST_AS_MANAGER_TEMPLATE);

		System.out.println ("New communication data: " + newCommunicationData);

		StringEntity input = null;
		try {
			input = new StringEntity(newCommunicationData);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		input.setContentType("application/xml");
		postRequest.setEntity(input);

		String responseFromUpdateCommunication = null;
		try {
			responseFromUpdateCommunication = EntityUtils.toString( httpclient.execute( postRequest ).getEntity(), "UTF-8" );
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println(" [+] Trying to update communication " + myCommunication.getTitle() + ", response from server: " + responseFromUpdateCommunication);
		Assert.assertTrue("The communication " + myCommunication.getTitle() + " was not updated via API", responseFromUpdateCommunication.contains("<webcast id="));

		String id = Util.getStringByRegex("<webcast id=\"(\\d+)\">", 1, responseFromUpdateCommunication);
		System.out.println(" [+] Communication ID from API: " + id);

		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();

		}
		return id;
	}

	// This request is will send a POST request to the encorder which will change pro webinar to "recorded" in
	// order to vacate the environment from uneccessary processes when gig is ended
	public static void sendEncoderResponse(String commId, User myUser){

		String assetID = "0";
		int iterator = 5;
		while (assetID.trim().equals("0")){

			// Wait for 60 seconds
			DateTimeFormatter.waitForSomeTime(60);
			System.out.println("Asset id from database: " + assetID);

			assetID = WebcastHandler.getAssetID(commId, "mp4");

			iterator = iterator - 1;
			if (iterator == 0) {

				System.out.println("The Asset value has been verified 5 times, and is still 0");
				Assert.fail("The communication, " + commId + " asset value for mp4 has been verified 5 times, and is still 0...");
			}
		}

		String btSession = null;
		btSession = UserApi.loginUser(myUser);

		btSession = "BTSESSION=" + btSession;
		HttpClient httpclient = HttpClientBuilder.create().build();

		HttpPost postCreateCommunication = new HttpPost("http://ams." + EnvSetup.HOME +".brighttalk.net/internal/encodingresponse");

		postCreateCommunication.addHeader("Cookie", btSession);
		Map<String, Object> params = new HashMap<>();

		params.put("assetId", assetID);

		// Set second 0 is very important for API
		org.apache.commons.lang3.text.StrSubstitutor strSubstitutor = new org.apache.commons.lang3.text.StrSubstitutor(params);
		String newCommunicationData =  strSubstitutor.replace(ENCODING_RESPONSE_VALUES_TEMPLATE);
		System.out.println("My Data to send: " + newCommunicationData);

		StringEntity input = null;
		try {
			input = new StringEntity(newCommunicationData);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		input.setContentType("application/xml");
		postCreateCommunication.setEntity(input);

		try
		{
			HttpResponse responseFromApi = httpclient.execute( postCreateCommunication );

			//LogWriter.file("Default", " [+] Trying to send request to encoder", true);
			System.out.println("Response from server (status code): " + responseFromApi.getStatusLine().getStatusCode());

			Assert.assertTrue("THE RESPONSE FROM ENCODING RESPONSE WAS NOT 204 AS EXPECTED...", responseFromApi.getStatusLine().getStatusCode() == 204);

		} catch (IOException myIOException) {
			Assert.fail("IOException in createUpcomingWebcastHd: " + myIOException.getMessage());
		}
	}

	/**
	 *  http://svn.brighttalk.com/wiki/G2Project/API/Internal/Channel/GetCommunicationDetails
	 *  Returns all details of the webinar
	 */
	public static StringBuffer getCommunicationDetails(User manager, Channel myChannel, WebcastPro myCommunication){

		String btSession = null;
		StringBuffer result = new StringBuffer();

		btSession = UserApi.loginUser(manager);

		btSession = "BTSESSION=" + btSession;
		HttpClient httpclient = HttpClientBuilder.create().build();

		String getRequest = "http://channel." + TestConfig.getEnv() +".brighttalk.net/internal/channel/"+ myChannel.getChannelID() +"/webcast/"+ myCommunication.getWebcastID();

		HttpGet getCommunication = new HttpGet(getRequest);
		getCommunication.addHeader("Cookie", btSession);

		try {
			HttpResponse response = httpclient.execute(getCommunication);
			System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

			BufferedReader rd = new BufferedReader(
					new InputStreamReader(response.getEntity().getContent()));

			result = new StringBuffer();
			String line;
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}

			System.out.println("Communication data returned: " + result);

		} catch (IOException e) {
			e.printStackTrace();
		}

		return result;
	}

	public static String communicationFeatureImageName(StringBuffer communicationData){

		int startPosition = communicationData.indexOf("preview");
		int endPosition = communicationData.indexOf(".png", startPosition);
		String featureImageName = communicationData.substring(startPosition, endPosition);
		System.out.println("The feature image name is: " + featureImageName);
		return featureImageName;

	}
}
