package com.brighttalk.qa.api;

import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.siteElements.Channel;
import com.brighttalk.qa.siteElements.WebcastPro;
import com.brighttalk.qa.siteElements.Campaign;
import java.io.IOException;
import java.util.*;
import java.text.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;



@ContextConfiguration(
        classes = TestConfig.class)
public class CampaignApi {

    @Autowired
    BrightTALKService service;
    @Autowired
    World world;


	/**
	 * Create campaign via API:"
	 */
	public static Campaign createCampaign(World world, Campaign myCampaign) throws IOException{

    try{
        HttpClient httpclient = HttpClientBuilder.create().build();

        HttpPost postCreateCampaign = new HttpPost("https://www." + TestConfig.getEnv() + ".brighttalk.net/service/campaign/campaign");
        Channel currentChannel = world.getLocal_channels().get(world.getLocal_channels().size() - 1);
        int webcastCount = world.getWebcastsPro().size();

        JSONObject json = new JSONObject();
        json.put("id",myCampaign.getCampaignId());
        json.put("displayName",myCampaign.getDisplayName());
        json.put("status",myCampaign.getStatus());
        json.put("type",myCampaign.getLeadType());
        json.put("startDate",myCampaign.getStartDate());
        json.put("totalLeadTarget",myCampaign.getTotalLeadTarget());
        json.put("totalLeadsFulfilled",myCampaign.getTotalLeadsFulfilled());
        json.put("earlyDeliveryFlag",myCampaign.getEarlyDeliveryFlag());
        json.put("promote",myCampaign.getPromote());
        json.put("leadsByMonth",myCampaign.getLeadsByMonth());
        json.put("cpl",myCampaign.getCpl());
        json.put("currency",myCampaign.getCurrency());
        json.put("jobLevelFilter",myCampaign.getJobLevelFilter());
        json.put("companySizeFilter",myCampaign.getCompanySizeFilter());

        JSONArray regions = new JSONArray();
        for (String region : myCampaign.getRegions()){
          regions.add(region);
        }

        JSONObject regionObject = new JSONObject();
        regionObject.put("countries",regions);
        JSONArray region = new JSONArray();
        region.add(regionObject);
        json.put("regions",region);

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        JSONArray campaignConfigurations = new JSONArray();

        for(int i=0;i<webcastCount;i++){

          WebcastPro currentCommunication = world.getWebcastsPro().get(i);
      
          String scheduled = format.format(currentCommunication.getStartDate().getTime());

          JSONObject config = new JSONObject();
          config.put("channelId",currentChannel.getChannelID());
          config.put("communicationId",currentCommunication.getWebcastID());
          config.put("communicationTitle",currentCommunication.getTitle());   
          config.put("communicationScheduled",scheduled);
          config.put("communicationStatus",currentCommunication.getStatus());       
          config.put("communicationPresenter",currentCommunication.getPresenter());
          config.put("community","Information Technology"); 
          config.put("communicationThumbnail","");
          campaignConfigurations.add(config);
        }

        json.put("campaignConfigurations",campaignConfigurations);
        json.put("campaignSfId",myCampaign.getCampaignSfId());
        json.put("name",myCampaign.getCampaignName());
        json.put("programSummaryId",myCampaign.getProgramSummaryId());
        json.put("client",myCampaign.getClient());
        json.put("recipients",myCampaign.getRecipients());
        json.put("csmEmail",myCampaign.getCsmEmail());
        json.put("leadEmail",myCampaign.getLeadEmail());
        json.put("campaignValue",myCampaign.getCampaignValue());
        json.put("leadDeliveredValue",myCampaign.getLeadDeliveredValue());
        json.put("leadFulfillemtnPrcnt",myCampaign.getLeadFulfillmentPrcnt());    
        json.put("noFilter",myCampaign.getNoFilterFlag());
        json.put("endDate",myCampaign.getEndDate());   
        json.put("firstDeliveryDate",myCampaign.getFirstDeliveryDate());
        json.put("lastDeliveryDate",myCampaign.getLastDeliveryDate());   
        json.put("reEngage",myCampaign.getReEngageFlag());
        json.put("isActive",myCampaign.getIsActive());   

        StringEntity input = new StringEntity(json.toString());
        System.out.println("The create campaign json object is : =====================");
        System.out.println(json.toString());

        input.setContentType("application/json");
        postCreateCampaign.addHeader("Authorization","Basic YWRtaW46YWRtaW4=");
        postCreateCampaign.setEntity(input);

        HttpResponse responseFromCreateCampaign = httpclient.execute( postCreateCampaign );
        Assert.assertTrue("THE RESPONSE WAS NOT 200 AS EXPECTED...", responseFromCreateCampaign.getStatusLine().getStatusCode() == 200);


      }

      catch (IOException myIOException) {

        Assert.fail("IOException in createCampaign: " + myIOException.getMessage());

    }

      return myCampaign;
  }


  /**
   * Update campaign displayed name via API:"
   */
  public static void updateDisplayName(World world, Campaign myCampaign) throws IOException{

    String cookie = world.getLocal_users().get(0).getSessionCookie();
    String btSession = cookie.replaceAll(";", "");

    Channel currentChannel = world.getLocal_channels().get(world.getLocal_channels().size() - 1);
    HttpClient httpclient = HttpClientBuilder.create().build();
    HttpPut putUpdateDisplayName = new HttpPut("https://www." + TestConfig.getEnv() + ".brighttalk.net/service/campaign/channel/" + currentChannel.getChannelID() + "/campaign/" + myCampaign.getCampaignId());
    JSONObject json = new JSONObject();
    json.put("displayName",myCampaign.getDisplayName());

    StringEntity input = new StringEntity(json.toString());
    System.out.println("The update campaign json object is : =====================");
    System.out.println(json.toString());

    input.setContentType("application/json");
    putUpdateDisplayName.addHeader("Cookie", btSession);
    putUpdateDisplayName.setEntity(input);

    try
    {
      HttpResponse responseFromUpdateDisplayName = httpclient.execute( putUpdateDisplayName );

      Assert.assertTrue("THE RESPONSE WAS NOT 202 AS EXPECTED...", responseFromUpdateDisplayName.getStatusLine().getStatusCode() == 202);

    } catch (IOException myIOException) {

      Assert.fail("IOException in updateCampaignDisplayName: " + myIOException.getMessage());

    }

  }

};