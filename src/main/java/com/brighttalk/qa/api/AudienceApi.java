package com.brighttalk.qa.api;

import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import java.io.IOException;
import java.util.List;
import com.brighttalk.qa.siteElements.Audience;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.HttpResponse;
import org.apache.http.entity.StringEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.junit.Assert;

@ContextConfiguration(
        classes = TestConfig.class)
public class AudienceApi {

    @Autowired
    World world;

	/**
	 * Add audiences via API"
	 */
	public static void addAudiences(World world, List<Audience> audiences) throws IOException{


		try{

			HttpClient httpclient = HttpClientBuilder.create().build();
			HttpPost postAddAudiences = new HttpPost("https://www." + TestConfig.getEnv() + ".brighttalk.net/service/campaign/campaign/" + world.getDCCampaigns().get(0).getCampaignId() + "/audience" );
      String url = "https://www." + TestConfig.getEnv() + ".brighttalk.net/service/campaign/campaign/" + world.getDCCampaigns().get(0).getCampaignId() + "/audience";
        	
			JSONObject json = new JSONObject();

			JSONArray audienceArray = new JSONArray();
			for(int i=0;i<audiences.size();i++){

          		Audience aud = audiences.get(i);               	

          		JSONObject config = new JSONObject();
          		config.put("activityDatetime",aud.getActivityDate());
          		config.put("channelId",aud.getChannelId());
          		config.put("communicationId",aud.getCommunicationId());   
          		config.put("userId",aud.getUserId());
          		config.put("jobTitle",aud.getJobTitle());       
          		config.put("companyName",aud.getCompany());
          		config.put("industry",aud.getIndustry()); 
          		config.put("stateRegion",aud.getState());
          		config.put("country",aud.getCountry());
          		config.put("jobLevel",aud.getJobLevel());
          		config.put("companySize",aud.getCompanySize());
          		config.put("activityType",aud.getActivityType());
          		config.put("originalReferal",aud.getOriginalReferal());
          		config.put("autoDelivered",aud.getAutoDelivered());
          		config.put("firstActivity",aud.getFirstActivity());
          		config.put("embedUrl",aud.getEmbedUrl());
          		config.put("source",aud.getSource());
          		config.put("leadType",aud.getLeadType());

          		audienceArray.add(config);

        	}

        json.put("audience",audienceArray);

        StringEntity input = new StringEntity(json.toString());

        input.setContentType("application/json");
        postAddAudiences.addHeader("Authorization","Basic YWRtaW46YWRtaW4=");
        postAddAudiences.setEntity(input);

        HttpResponse responseFromAddAudiences = httpclient.execute( postAddAudiences );
        Assert.assertTrue("THE RESPONSE WAS NOT 200 AS EXPECTED...", responseFromAddAudiences.getStatusLine().getStatusCode() == 200);


		}catch (IOException myIOException)

		{

        	Assert.fail("IOException in addAudiences: " + myIOException.getMessage());

    }

	}
}
