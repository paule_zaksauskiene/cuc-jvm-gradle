package com.brighttalk.qa.api;

import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.siteElements.Channel;
import com.brighttalk.qa.siteElements.WebcastPro;
import org.apache.commons.lang.text.StrSubstitutor;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.brighttalk.qa.siteElements.User;

@ContextConfiguration(
		classes = TestConfig.class)
public class ChannelApi {

	@Autowired
	BrightTALKService service;
	@Autowired
	World world;

    private static final String CHANNEL_CONFIGURE_ENABLE_PRO_WEBINAR_TEMPLATE1 =
            "<request>" +
                    "<channel id='${channelid}' sendNotifications='true'>" +
                        "<categories>";

    private static final String CHANNEL_CONFIGURE_ENABLE_PRO_WEBINAR_TEMPLATE_CATEGORY =

            "<category id='${id}' term='${category}'/>";

    private static final String CHANNEL_CONFIGURE_ENABLE_PRO_WEBINAR_TEMPLATE2 =

                    "</categories>" +
                    "</channel>" +
                    "</request>";

    private static final String CHANNEL_CONFIGURE_ENABLE_PRO_WEBINAR_TEMPLATE =
            "<request>" +
                    "<channel id='${channelid}' sendNotifications='true'>" +
                    "<features>" +
                    "<feature name='${featureName}' enabled='${featureEnabled}'>${featureContent}</feature>" +
                    "</features>" +
                    " </channel>" +
                    "</request>";

    private static final String CHANNEL_CONFIGURE_ENABLE_DISABLE_FEATURE_TEMPLATE =
            "<request>" +
                    "<channel id='${channelid}' sendNotifications='true'>" +
                    "<features>" +
                    "<feature name='${featureName}' enabled='${featureEnabled}'></feature>" +
                    "</features>" +
                    " </channel>" +
                    "</request>";

    private static final String CHANNEL_CONFIGURE_enableCommunicationCustomURL_TEMPLATE =
            "<request>" +
                    "<channel id='${channelid}' sendNotifications='true'>" +
                    "<features>" +
                    "<feature name='${featureName}' enabled='${featureEnabled}'></feature>" +
                    "</features>" +
                    " </channel>" +
                    "</request>";


    private static final String INSERT_CHANNEL_VALUES_TEMPLATE =

            "<request><channel><title>${title}</title>" +
                    "<description>${description}</description>" +
                    "<keywords>${keywords}</keywords>" +
                    "<organisation>${organization}</organisation>" +
                    "<strapline>${strapline}</strapline>" +
                    "<type>${type}</type>" +
                    "<contactDetails>" +
                    "<firstName>${firstName}</firstName>" +
                    "<lastName>${secondname}</lastName>" +
                    "<email>${email}</email>" +
                    "<telephone>${email}</telephone>" +
                    "<jobTitle>${jobtitle}</jobTitle>" +
                    "<company>${company}</company>" +
                    "<address1>${address1}</address1>" +
                    "<address2></address2>" +
                    "<city>${city}</city>" +
                    "<state></state>" +
                    "<postcode>${postcode}</postcode>" +
                    "<country>${country}</country>" +
                    "</contactDetails>" +
                    "</channel>" +
                    "</request>";


    private static final String CHANNEL_CONFIGURE_PORTAL_PUBLISHING_TEMPLATE =
            "<request>" +
                    "<channel id='${channelid}'>" +
                    "<isSearchable>${publishing}</isSearchable>" +
                    "<promoteOnHomepage>true</promoteOnHomepage>" +
                    "</channel>" +
                    "</request>";

    private static final String UPDATE_WEBCAST_STATUS_TEMPLATE =

            "<webcast id='${communicationId}'>" +
                    "<channel id='${channelid}'/>" +
                    "<status>${status}</status>" +
                    "</webcast>";


    private static final String CHANNEL_CONFIGURE_REALM_TEMPLATE =
            "<request>" +
                    "<channel id='${channelid}' sendNotifications='true'>" +
                    "<features>" +
                    "<feature name='allowedRealms' enabled='true'>2,1</feature>" +
                    "</features>" +
                    " </channel>" +
                    "</request>";

    private static final String CHANNEL_CONFIGURE_REQUEST_TEMPLATE =
            "<request>" +
                    "<channel id='${channelid}' sendNotifications='true'>" +
                    " <type>${type}</type>" +
                    "<pendingType></pendingType>" +
                    "<bookingNumber>1235488</bookingNumber>" +
                    " </channel>" +
                    "</request>";


    /**
     * The documentation: http://svn.brighttalk.com/wiki/G2Project/API/Internal/Channel/Configure
     */
    public void addCategorizationTags(World world, List<String> tags){

        try {

        	String cookie = world.getLocal_users().get(0).getSessionCookie();
			String btSession = cookie.replaceAll(";", "");

            HttpClient httpclient = HttpClientBuilder.create().build();

            HttpPost postUpdateChannelType = new HttpPost("http://channel." + TestConfig.getEnv() + ".brighttalk.net/internal/xml/configure");
            postUpdateChannelType.addHeader("Cookie", btSession);
            Map<String, Object> params = new HashMap<>();

            // Part 1
			params.put("channelid", world.getLocal_channels().get(0).getChannelID());
            StrSubstitutor strSubstitutor = new StrSubstitutor(params);
			String updateChannelData1 = strSubstitutor.replace(CHANNEL_CONFIGURE_ENABLE_PRO_WEBINAR_TEMPLATE1);
			System.out.println("Update channel part1 " + updateChannelData1);

            // Part 2
            String categoriesData = "";
            for (int i=0; i< tags.size(); i++){

                params.put("category", tags.get(i));
                int j = i + 1;
                params.put("id", j);
                String updateChannelDataCategories = strSubstitutor.replace(CHANNEL_CONFIGURE_ENABLE_PRO_WEBINAR_TEMPLATE_CATEGORY);
                categoriesData = categoriesData + updateChannelDataCategories;
            }

            System.out.println("Categories data: " + categoriesData);

            // Part 3
            String updateChannelData2 = strSubstitutor.replace(CHANNEL_CONFIGURE_ENABLE_PRO_WEBINAR_TEMPLATE2);
            System.out.println("Update channel part2 " + updateChannelData2);

            String updateChannelData = updateChannelData1 + categoriesData + updateChannelData2;
            StringEntity input = new StringEntity(updateChannelData);
            input.setContentType("application/xml");

            System.out.println("The Post request url: " + postUpdateChannelType.toString());
            System.out.println(" [+] Sending the update channel data: " + updateChannelData);  // + " to the URL: " +  postUpdateChannelType.toString());
            System.out.println(" [+] Header Cookie: " + btSession);

            postUpdateChannelType.setEntity(input);
            HttpResponse response = httpclient.execute(postUpdateChannelType);
            String responseFromUpdateChannel = EntityUtils.toString(response.getEntity(), "UTF-8");

            Assert.assertTrue("The Channel " + world.getLocal_channels().get(0).getChannelTitle() +
                    "\nwas not updated via API (channel feature setting), " +
                    "\nThe request was: " + updateChannelData +
                    "\nresponse from server is: " + responseFromUpdateChannel, responseFromUpdateChannel.contains("processed"));
        }
        catch(IOException ioe) {System.out.println("IOException: ChannelApi.update: "+ioe.toString());	}
        catch(ParseException pex){System.out.println("ParseException: ChannelApi.update: "+pex.toString());}

        // We need to wait here, because the portal shows errors opening the channel data so quickly on the myBrightTALK page
        try {
            System.out.println("Waiting for channel data to be updated on the system.");
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * http://svn.brighttalk.com/wiki/G2Project/API/Internal/Channel/Create
     * @param myUser the current user
     * @param myChannel the current channel
     * @return Channel
     */
    public static Channel createChannel(User myUser, Channel myChannel) {

        try{

            String sessionID = UserApi.loginUser(myUser);
            sessionID = "BTSESSION=" + sessionID;

            CloseableHttpClient httpclient = HttpClientBuilder.create().build();

            // Channel owner creates channel - INTERNAL CALLS MAY NOT WORK ON PROD AND STAGE
            HttpPost postCreateChannel = new HttpPost("http://channel." + TestConfig.getEnv() +".brighttalk.net/internal/xml/create");

            Map<String, Object> params = new HashMap<>();

            params.put("title", myChannel.getChannelTitle());
            params.put("description", myChannel.getChannelDescription());
            params.put("keywords", myChannel.getChannelTags());
            params.put("organization", myChannel.getChannelOrganization());
            params.put("strapline", myChannel.getChannelTagline());
            params.put("type", myChannel.getChannelType());
            params.put("firstName", myChannel.getContactDetails().getContactUser().getFirstName());
            params.put("secondname", myChannel.getContactDetails().getContactUser().getSecondName());
            params.put("email", myChannel.getContactDetails().getContactUser().getEmail());
            params.put("jobtitle", myChannel.getContactDetails().getContactUser().getJobTitle());
            params.put("company", myChannel.getContactDetails().getContactUser().getCompany());
            params.put("address1", myChannel.getContactDetails().getContactAddress().getAdressLine1());
            params.put("city", myChannel.getContactDetails().getContactAddress().getCity());
            params.put("postcode", myChannel.getContactDetails().getContactAddress().getPostcode());
            params.put("country", myChannel.getContactDetails().getContactUser().getCountry());


            StrSubstitutor strSubstitutor = new StrSubstitutor(params);
            String newUserData =  strSubstitutor.replace(INSERT_CHANNEL_VALUES_TEMPLATE);

            System.out.println("Channel values template: " + newUserData);

            StringEntity input = new StringEntity(newUserData);
            input.setContentType("application/xml; charset=UTF-8");

            postCreateChannel.setEntity(input);
            postCreateChannel.addHeader("Cookie", sessionID);
            System.out.println("Cookie: " + sessionID);


            String responseFromCreateChannel = EntityUtils.toString( httpclient.execute( postCreateChannel).getEntity(), "UTF-8" );

            Assert.assertTrue("========================================================" +
                    "\nThe channel " + myChannel.getChannelTitle() + " " + myChannel.getChannelType() + " was not created via API," +
                    "\nResponse from server: " + responseFromCreateChannel +
                    "\n========================================================", responseFromCreateChannel.contains("processed"));

            int beginningOfId = responseFromCreateChannel.indexOf("channel id=");
            beginningOfId = beginningOfId + 12;
            int endOfId = responseFromCreateChannel.indexOf("title");
            endOfId = endOfId - 8;

            myChannel.setChannelID(responseFromCreateChannel.substring(beginningOfId, endOfId));

            httpclient.close();

        }
        catch(IOException ioe) {System.out.println("IOException: ChannelApi.createCH: "+ioe.toString());	}
        catch(ParseException pex){System.out.println("ParseException: ChannelApi.createCH: "+pex.toString());}

        return myChannel;

    }

    /**
     * The documentation: http://svn.brighttalk.com/wiki/G2Project/API/Internal/Channel/Configure
     * @param myManager the Manager user
     * @param myChannel the Channel we are working with
     * @param featureName the name of he feature to set
     * @param featureEnabled the enabled String
     * @param featureContent the content of the feature
     */
    public static void setChannelFeature(User myManager, Channel myChannel, String featureName, String featureEnabled, String featureContent){

        try {

            String btSession = UserApi.loginUser(myManager);
            btSession = "BTSESSION=" + btSession;
            HttpClient httpclient = HttpClientBuilder.create().build();

            HttpPost postUpdateChannelType = new HttpPost("http://channel." + TestConfig.getEnv() + ".brighttalk.net/internal/xml/configure");
            postUpdateChannelType.addHeader("Cookie", btSession);
            Map<String, Object> params = new HashMap<>();

            params.put("channelid", myChannel.getChannelID());
            params.put("featureName", featureName);
            params.put("featureEnabled", featureEnabled);
            params.put("featureContent", featureContent);

            StrSubstitutor strSubstitutor = new StrSubstitutor(params);
            String updateChannelData = strSubstitutor.replace(CHANNEL_CONFIGURE_ENABLE_PRO_WEBINAR_TEMPLATE);
            StringEntity input = new StringEntity(updateChannelData);
            input.setContentType("application/xml");

            System.out.println("The Post request url: " + postUpdateChannelType.toString());
            System.out.println(" [+] Sending the update channel data: " + updateChannelData);  // + " to the URL: " +  postUpdateChannelType.toString());
            System.out.println(" [+] Header Cookie: " + btSession);

            postUpdateChannelType.setEntity(input);
            HttpResponse response = httpclient.execute(postUpdateChannelType);
            String responseFromUpdateChannel = EntityUtils.toString(response.getEntity(), "UTF-8");
            System.out.println("Response from server after updating channel settings:" + responseFromUpdateChannel);

            Assert.assertTrue("The Channel " + myChannel.getChannelTitle() +
                    "\nwas not updated via API (channel feature setting), " +
                    "\nThe request was: " + updateChannelData +
                    "\nresponse from server is: " + responseFromUpdateChannel, responseFromUpdateChannel.contains("processed"));
        }
        catch(IOException ioe) {System.out.println("IOException: ChannelApi.update: "+ioe.toString());	}
        catch(ParseException pex){System.out.println("ParseException: ChannelApi.update: "+pex.toString());}

        // We need to wait here, because the portal shows errors opening the channel data so quickly on the myBrightTALK page
        try {
            System.out.println("Waiting for channel data to be updated on the system.");
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void enableDisableChannelFeature(User myManager, Channel myChannel, String featureName, String featureEnabled){

        try {

            String btSession = UserApi.loginUser(myManager);
            btSession = "BTSESSION=" + btSession;
            HttpClient httpclient = HttpClientBuilder.create().build();

            HttpPost postUpdateChannelType = new HttpPost("http://channel." + TestConfig.getEnv() + ".brighttalk.net/internal/xml/configure");
            postUpdateChannelType.addHeader("Cookie", btSession);
            Map<String, Object> params = new HashMap<>();

            params.put("channelid", myChannel.getChannelID());
            params.put("featureName", featureName);
            params.put("featureEnabled", featureEnabled);

            StrSubstitutor strSubstitutor = new StrSubstitutor(params);
            String updateChannelData = strSubstitutor.replace(CHANNEL_CONFIGURE_ENABLE_DISABLE_FEATURE_TEMPLATE);
            StringEntity input = new StringEntity(updateChannelData);
            input.setContentType("application/xml");

            System.out.println("The Post request url: " + postUpdateChannelType.toString());
            System.out.println(" [+] Sending the update channel data: " + updateChannelData);  // + " to the URL: " +  postUpdateChannelType.toString());
            System.out.println(" [+] Header Cookie: " + btSession);

            postUpdateChannelType.setEntity(input);
            HttpResponse response = httpclient.execute(postUpdateChannelType);
            String responseFromUpdateChannel = EntityUtils.toString(response.getEntity(), "UTF-8");
            System.out.println("Response from server after updating channel settings:" + responseFromUpdateChannel);

            Assert.assertTrue("The Channel " + myChannel.getChannelTitle() +
                    "\nwas not updated via API (channel feature setting), " +
                    "\nThe request was: " + updateChannelData +
                    "\nresponse from server is: " + responseFromUpdateChannel, responseFromUpdateChannel.contains("processed"));
        }
        catch(IOException ioe) {System.out.println("IOException: ChannelApi.update: "+ioe.toString());	}
        catch(ParseException pex){System.out.println("ParseException: ChannelApi.update: "+pex.toString());}

        // We need to wait here, because the portal shows errors opening the channel data so quickly on the myBrightTALK page
        try {
            System.out.println("Waiting for channel data to be updated on the system.");
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void deleteChannel(User myManager, Channel myChannel) {

        try{
            String btSession = UserApi.loginUser(myManager);
            btSession = "BTSESSION=" + btSession;

            CloseableHttpClient httpclient = HttpClientBuilder.create().build();

            HttpDelete postDeleteChannel = new HttpDelete("https://www." + TestConfig.getEnv() +".brighttalk.net/service/channel/channel/" + myChannel.getChannelID());
            postDeleteChannel.addHeader("Cookie", btSession);

            HttpResponse responseAfterDelete = httpclient.execute( postDeleteChannel );
            String responseFromDeleteChannel = responseAfterDelete.getStatusLine().toString();

            Assert.assertTrue("The Channel " + myChannel.getChannelTitle() + " was not deleted via API: " + responseFromDeleteChannel, responseFromDeleteChannel.contains("204"));

            System.out.println(" [+] Channel Deleted via API: " +  myChannel.getChannelTitle());
            httpclient.close();
        }
        catch(IOException ioe) {System.out.println("IOException: ChannelApi.deleteCH: "+ioe.toString());	}
        catch(ParseException pex){System.out.println("ParseException: ChannelApi.deleteCH: "+pex.toString());}

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    /*
    This API deletes from the booking and calendar (the webinar still remains in core and is non cancelled on the channel owner pages).
     */
    public static void cancelAllBookingsOnChannel(User myManager, Channel myChannel){

        try{
            String btSession = UserApi.loginUser(myManager);
            btSession = "BTSESSION=" + btSession;

            CloseableHttpClient httpclient = HttpClientBuilder.create().build();

            HttpDelete deleteAllBookingsOnChannel = new HttpDelete("http://booking.mphd01." + TestConfig.getEnv() +".brighttalk.net/channel/" + myChannel.getChannelID() + "/bookings");
            deleteAllBookingsOnChannel.addHeader("Cookie", btSession);

            System.out.println(" [+] Delete ALL bookings on channnels: " + deleteAllBookingsOnChannel.toString());

            HttpResponse responseAfterDelete = httpclient.execute( deleteAllBookingsOnChannel );
            String responseFromDeleteChannel = responseAfterDelete.getStatusLine().toString();

            System.out.println(" [+] Delete Response: " + responseFromDeleteChannel);
            //Assert.assertTrue("The Channel " + myChannel.getChannelTitle() + " was not deleted via API: " + responseFromDeleteChannel, responseFromDeleteChannel.contains("204"));
            //LogWriter.file("Default", "The Channel " + myChannel.getChannelTitle() + " has been deleted via API: " + responseFromDeleteChannel, false);
            httpclient.close();
        }
        catch(IOException ioe) {System.out.println("IOException: ChannelApi.deleteCH: "+ioe.toString());	}
        catch(ParseException pex){System.out.println("ParseException: ChannelApi.deleteCH: "+pex.toString());}

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    /**
     * The documentation: http://svn.brighttalk.com/wiki/G2Project/API/Internal/Channel/Configure
     *
     * @param myManager the manager User
     * @param myChannel the channel we are working with
     * @param publishing : One of : included, excluded, rule_based

     */
    public static void updatePortalPublishing(User myManager, Channel myChannel, String publishing){

        try {

            String btSession = UserApi.loginUser(myManager);
            btSession = "BTSESSION=" + btSession;
            HttpClient httpclient = HttpClientBuilder.create().build();

            HttpPost postUpdateChannelType = new HttpPost("http://channel." + TestConfig.getEnv() + ".brighttalk.net/internal/xml/configure");
            postUpdateChannelType.addHeader("Cookie", btSession);
            Map<String, Object> params = new HashMap<>();

            params.put("channelid", myChannel.getChannelID());
            params.put("publishing", publishing);


            StrSubstitutor strSubstitutor = new StrSubstitutor(params);
            String updateChannelData = strSubstitutor.replace(CHANNEL_CONFIGURE_PORTAL_PUBLISHING_TEMPLATE);
            StringEntity input = new StringEntity(updateChannelData);
            input.setContentType("application/xml");

            System.out.println("I am sending the portal publishing data: " + updateChannelData + " to the URL: " +  postUpdateChannelType.toString());
            System.out.print("My header Cookie: " + btSession);

            postUpdateChannelType.setEntity(input);
            HttpResponse response = httpclient.execute(postUpdateChannelType);
            String responseFromUpdateChannel = EntityUtils.toString(response.getEntity(), "UTF-8");
            System.out.println("Response from update channel portal publishing API: " + responseFromUpdateChannel);

            Assert.assertTrue("The Channel " + myChannel.getChannelTitle() + " was not updated via API: " + responseFromUpdateChannel, responseFromUpdateChannel.contains("200"));
        }
        catch(IOException ioe) {System.out.println("IOException: ChannelApi.update: "+ioe.toString());	}
        catch(ParseException pex){System.out.println("ParseException: ChannelApi.update: "+pex.toString());}

        // We need to wait because it takes a bit of time until the webinar is enabled.
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }


    /**
     *  http://svn.brighttalk.com/wiki/G2Project/API/External/Channel/SyndicationCreate
     *  url http://www.brighttalk.com/service/channel/channel/{id}/communication/{id}/syndication
     */
    public static void syndicateProWebinar (User manager, Channel myChannel, WebcastPro myCommunication) throws ParseException, IOException{

        String btSession = UserApi.loginUser(manager);
        btSession = "BTSESSION=" + btSession;
        HttpClient httpclient = HttpClientBuilder.create().build();
        String putRequest = "https://www." + TestConfig.getEnv() +".brighttalk.net/service/channel/channel/"+ myChannel.getChannelID() +"/communication/"+ myCommunication.getWebcastID() + "/syndication";

        HttpPut putUpdateCommunication = new HttpPut(putRequest);
        putUpdateCommunication.addHeader("Cookie", btSession);
       // Map<String, Object> params = new HashMap<>();
       // params.put("communicationId", myCommunication.getWebcastID());
       // params.put("channelid", myChannel.getChannelID());
        //params.put("status", myCommunication.getStatus());

        //StrSubstitutor strSubstitutor = new StrSubstitutor(params);
       // String newCommunicationData =  strSubstitutor.replace(UPDATE_WEBCAST_STATUS_TEMPLATE);
        //System.out.println ("New communication data: " + newCommunicationData);

         String newData = "<syndication excludeFromChannelCapacity='true' />";
         StringEntity input = new StringEntity(newData);
         input.setContentType("application/xml");
         putUpdateCommunication.setEntity(input);

        try
        {
            HttpResponse responseFromApi = httpclient.execute( putUpdateCommunication );

            System.out.println("Response from server (status code): " + responseFromApi.getStatusLine().getStatusCode());

            Assert.assertTrue("THE RESPONSE FROM ENCODING RESPONSE WAS NOT 201 AS EXPECTED...", responseFromApi.getStatusLine().getStatusCode() == 201);

        } catch (IOException myIOException) {

            Assert.fail("IOException in updateWebcastProStatusAsManager: " + myIOException.getMessage());
        }
    }

    /**
     *  http://svn.brighttalk.com/wiki/G2Project/API/External/Channel/SyndicationUpdate
     *  url http://www.brighttalk.com/service/channel/channel/{id}/communication/{id}/syndication
     */
    public static void approveSyndication (User manager, Channel myChannel, WebcastPro myCommunication) throws ParseException, IOException{

        String btSession = UserApi.loginUser(manager);
        btSession = "BTSESSION=" + btSession;
        HttpClient httpclient = HttpClientBuilder.create().build();
        String putRequest = "https://www." + TestConfig.getEnv() +".brighttalk.net/service/channel/channel/"+ myChannel.getChannelID() +"/communication/"+ myCommunication.getWebcastID() + "/syndication";

        HttpPut putUpdateCommunication = new HttpPut(putRequest);
        putUpdateCommunication.addHeader("Cookie", btSession);
       // Map<String, Object> params = new HashMap<>();
       // params.put("communicationId", myCommunication.getWebcastID());
       // params.put("channelid", myChannel.getChannelID());
       // params.put("status", myCommunication.getStatus());

        //StrSubstitutor strSubstitutor = new StrSubstitutor(params);

        String updateData = "<syndication><authorisation type='master' >approved</authorisation></syndication>";

        //String newCommunicationData =  strSubstitutor.replace(UPDATE_WEBCAST_STATUS_TEMPLATE);
        System.out.println ("New communication data: " + updateData);
        System.out.println("btSession: " + btSession);

        StringEntity input = new StringEntity(updateData);
        input.setContentType("application/xml");
        putUpdateCommunication.setEntity(input);

        try
        {
            HttpResponse responseFromApi = httpclient.execute( putUpdateCommunication );

            System.out.println("Response from server (status code): " + responseFromApi.getStatusLine().getStatusCode());

            Assert.assertTrue("THE RESPONSE FROM ENCODING RESPONSE WAS NOT 200 AS EXPECTED...", responseFromApi.getStatusLine().getStatusCode() == 200);

        } catch (IOException myIOException) {

            Assert.fail("IOException in updateWebcastProStatusAsManager: " + myIOException.getMessage());
        }
    }

    public static void addAnonymousRealmToChannel (User myManager, Channel myChannel) throws ParseException, IOException{

        String btSession = UserApi.loginUser(myManager);
        btSession = "BTSESSION=" + btSession;
        HttpClient httpclient = HttpClientBuilder.create().build();

        HttpPost postUpdateChannelType = new HttpPost("http://channel." + TestConfig.getEnv() +".brighttalk.net/internal/xml/configure");
        postUpdateChannelType.addHeader("Cookie", btSession);
        Map<String, Object> params = new HashMap<>();

        params.put("channelid", myChannel.getChannelID());

        org.apache.commons.lang3.text.StrSubstitutor strSubstitutor = new org.apache.commons.lang3.text.StrSubstitutor(params);
        String updateChannelData =  strSubstitutor.replace(CHANNEL_CONFIGURE_REALM_TEMPLATE);
        StringEntity input = new StringEntity(updateChannelData);
        input.setContentType("application/xml");

        postUpdateChannelType.setEntity(input);
        HttpResponse response = httpclient.execute(postUpdateChannelType);
        String responseFromUpdateChannel = EntityUtils.toString(response.getEntity(), "UTF-8");
        Assert.assertTrue("The Channel " + myChannel.getChannelTitle() + " was not updated via API: " + responseFromUpdateChannel, responseFromUpdateChannel.contains("200"));

    }

    public static void updateChannelType (User myManager, Channel myChannel) throws IOException {

        String btSession = UserApi.loginUser(myManager);
        btSession = "BTSESSION=" + btSession;
        HttpClient httpclient = HttpClientBuilder.create().build();

        HttpPost postUpdateChannelType = new HttpPost("http://channel." + TestConfig.getEnv() +".brighttalk.net/internal/xml/configure");
        postUpdateChannelType.addHeader("Cookie", btSession);
        Map<String, Object> params = new HashMap<>();

        params.put("channelid", myChannel.getChannelID());
        params.put("type", myChannel.getChannelType());

        org.apache.commons.lang3.text.StrSubstitutor strSubstitutor = new org.apache.commons.lang3.text.StrSubstitutor(params);
        String updateChannelData =  strSubstitutor.replace(CHANNEL_CONFIGURE_REQUEST_TEMPLATE);
        StringEntity input = new StringEntity(updateChannelData);
        input.setContentType("application/xml");

        System.out.println("Update channel manager request data: " + updateChannelData);

        postUpdateChannelType.setEntity(input);
        HttpResponse response = httpclient.execute(postUpdateChannelType);
        String responseFromUpdateChannel = EntityUtils.toString(response.getEntity(), "UTF-8");
        System.out.println("==== Response from update channel: " + responseFromUpdateChannel);
        Assert.assertTrue("The Channel " + myChannel.getChannelTitle() + " was not updated via API: " + responseFromUpdateChannel, responseFromUpdateChannel.contains("200"));

    }
}
