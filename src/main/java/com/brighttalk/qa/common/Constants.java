package com.brighttalk.qa.common;

import com.brighttalk.qa.init.EnvSetup;

public class Constants {

    //misc.
    public static final int ID_REGEX_GROUP = 1;
    public static final int SELENIUM_TIMEOUT = 60;
    public static final int SELENIUM_PAGE_LOAD_TIMEOUT = 30;
    public static final int SELENIUM_POLL = 1;

    //Environments
    public static final String INT01 = "int01";
    public static final String INT02 = "int02";
    public static final String INT03 = "int03";
    public static final String INT04 = "int04";
    public static final String INT05 = "int05";
    public static final String TEST01 = "test01";

    //browsers
    public static final String CHROME = "chrome";
    public static final String FIREFOX = "firefox";
    public static final String IE = "ie";
    public static final String SAFARI = "safari";

    //General
    public static final String ALL_BRIGHT_TALK = "All BrightTALK";
    public static final String DEFAULT_COMMUNITY_DROPDOWN_SELECTION = "Webinars and videos";
    public static final int NUMBER_OF_COMMUNITY_DROPDOWN_OPTIONS = 3;

    public static final String NO_CONTENT_BUTTON_TEXT = "Explore BrightTALK";

    //Webcasts
    public static final String WEBCASTS = "Webinars and videos";
    public static final String RECORDED = "Recorded";
    public static final String UPCOMING = "Upcoming";
    public static final String FEATURED_RECORDED_WEBCAST = "Last:";
    public static final String FEATURED_UPCOMING_WEBCAST = "Next:";
    public static final String NO_FEATURE_WEBCAST = "Currently no content in this channel";
    public static final String ATTEND_BUTTON = "Attend";
    public static final String PLAY_BUTTON = "Play";
    public static final String PROCESSING_BUTTON = "Processing. Try again later.";
    public static final String PRE_REG_COUNT = "Pre-reg";
    public static final String VIEWS_COUNT = "views";

    public static final String TEST_WEBCAST_PREFIX = "Test_webcast_";

    //Summits
    public static final String SUMMITS = "Summits";

    //Summit Page
    public static final String SUMMIT_SUBTITLE = "About this summit";
    public static final String SUMMIT_TAGS_TITLE = "Tags: ";
    public static final String SUMMIT_WEBCASTS_HEADING = "Coming up on this summit";

    //Channels
    public static final String CHANNELS = "Channels";
    public static final String CHANNELS_I_OWN_TITLE = "Channels I own";

    //Communities
    public static final String COMMUNITY = "Community";

    //Configuration
    public static final String PUBLISH = "Publish";

    //Channel Types
    public static final String STANDARD = "Standard";
    public static final String PROFESSIONAL = "Professional";
    public static final String ENTERPRISE = "Enterprise";


    //Date format patterns
    public static final String SUMMIT_DATE = "MMM dd yyyy";
    public static final String SUMMIT_WEBCAST_DATE = "MMM dd yyyy";

    //Status Codes
    public static final String OK = "200";

    //invokeWatcher jmx urls
    public static final String INVOKE_WEBCAST_WATCHER = "http://core.jboss01.core." + EnvSetup.HOME + ".brighttalk.net:8080/jmx-console/HtmlAdaptor?action=invokeOpByName&name=brighttalk.search.config:name=webcastWatcher&methodName=watchWebcasts";
    public static final String INVOKE_WEBCAST_BY_CHANNELS_WATCHER = "http://core.jboss01.core." + EnvSetup.HOME + ".brighttalk.net:8080/jmx-console/HtmlAdaptor?action=invokeOpByName&name=brighttalk.search.config:name=webcastWatcher&methodName=watchWebcastsByChannels";
    public static final String INVOKE_WEBCAST_BY_COMMUNITIES_WATCHER = "http://core.jboss01.core." + EnvSetup.HOME + ".brighttalk.net:8080/jmx-console/HtmlAdaptor?action=invokeOpByName&name=brighttalk.search.config:name=webcastWatcher&methodName=watchWebcastsByCommunities";
    public static final String INVOKE_WEBCAST_BY_SUMMARIES_WATCHER = "http://core.jboss01.core." + EnvSetup.HOME + ".brighttalk.net:8080/jmx-console/HtmlAdaptor?action=invokeOpByName&name=brighttalk.search.config:name=webcastWatcher&methodName=watchWebcastsBySummaries";

    public static final String INVOKE_CHANNEL_WATCHER = "http://core.jboss01.core." + EnvSetup.HOME + ".brighttalk.net:8080/jmx-console/HtmlAdaptor?action=invokeOpByName&name=brighttalk.search.config:name=channelWatcher&methodName=watchChannels";
    public static final String INVOKE_CHANNEL_BY_COMMUNICATIONS_WATCHER = "http://core.jboss01.core." + EnvSetup.HOME + ".brighttalk.net:8080/jmx-console/HtmlAdaptor?action=invokeOpByName&name=brighttalk.search.config:name=channelWatcher&methodName=watchChannelsByCommunications";
    public static final String INVOKE_CHANNEL_BY_COMMUNITIES_WATCHER = "http://core.jboss01.core." + EnvSetup.HOME + ".brighttalk.net:8080/jmx-console/HtmlAdaptor?action=invokeOpByName&name=brighttalk.search.config:name=channelWatcher&methodName=watchChannelsByCommunities";
    public static final String INVOKE_CHANNEL_BY_SUMMARIES_WATCHER = "http://core.jboss01.core." + EnvSetup.HOME + ".brighttalk.net:8080/jmx-console/HtmlAdaptor?action=invokeOpByName&name=brighttalk.search.config:name=channelWatcher&methodName=watchChannelsBySummaries";


    public static final String INVOKE_SUMMIT_WATCHER = "http://core.jboss01.core." + EnvSetup.HOME + ".brighttalk.net:8080/jmx-console/HtmlAdaptor?action=invokeOpByName&name=brighttalk.search.config:name=summitWatcher&methodName=watchSummits";
    public static final String INVOKE_SUMMIT_BY_COMMUNICATIONS_WATCHER = "http://core.jboss01.core." + EnvSetup.HOME + ".brighttalk.net:8080/jmx-console/HtmlAdaptor?action=invokeOpByName&name=brighttalk.search.config:name=summitWatcher&methodName=watchSummitsByCommunications";
    public static final String INVOKE_SUMMIT_BY_COMMUNITIES_WATCHER = "http://core.jboss01.core." + EnvSetup.HOME + ".brighttalk.net:8080/jmx-console/HtmlAdaptor?action=invokeOpByName&name=brighttalk.search.config:name=summitWatcher&methodName=watchSummitsByCommunities";
    public static final String INVOKE_SUMMIT_BY_SPONSORS_WATCHER = "http://core.jboss01.core." + EnvSetup.HOME + ".brighttalk.net:8080/jmx-console/HtmlAdaptor?action=invokeOpByName&name=brighttalk.search.config:name=summitWatcher&methodName=watchSummitsBySponsors";

    //invokeDocumentPusher jmx urls
    public static final String INVOKE_WEBCAST_PUSHER = "http://core.jboss01.core." + EnvSetup.HOME + ".brighttalk.net:8080/jmx-console/HtmlAdaptor?action=invokeOpByName&name=brighttalk.search.config:name=webcastDocumentsPusher&methodName=indexWebcastDocuments";
    public static final String INVOKE_CHANNEL_PUSHER = "http://core.jboss01.core." + EnvSetup.HOME + ".brighttalk.net:8080/jmx-console/HtmlAdaptor?action=invokeOpByName&name=brighttalk.search.config:name=channelDocumentsPusher&methodName=indexChannelDocuments";
    public static final String INVOKE_SUMMIT_PUSHER = "http://core.jboss01.core." + EnvSetup.HOME + ".brighttalk.net:8080/jmx-console/HtmlAdaptor?action=invokeOpByName&name=brighttalk.search.config:name=summitDocumentsPusher&methodName=indexSummitDocuments";

    //Placeholders
    public static final String CHANNEL_ID = "CHANNEL_ID";
    public static final String WEBCAST_ID = "WEBCAST_ID";
    public static final String SUMMIT_ID = "SUMMIT_ID";
    public static final String COMMUNITY_ALIAS = "COMMUNITY_ALIAS";


    //Service request templates
    //Channel
    public static final String INSERT_CHANNEL_VALUES_TEMPLATE =
            "<request>" +
                    "<channel>" +
                    "<title>${title}</title>" +
                    "<description>${description}</description>" +
                    "<keywords>${keywords}</keywords>" +
                    "<organisation>${organization}</organisation>" +
                    "<strapline>${strapline}</strapline>" +
                    "<type>${type}</type>" +
                    "<contactDetails>" +
                    "<firstName>${firstName}</firstName>" +
                    "<lastName>${secondname}</lastName>" +
                    "<email>${email}</email>" +
                    "<telephone>${email}</telephone>" +
                    "<jobTitle>${jobtitle}</jobTitle>" +
                    "<company>${company}</company>" +
                    "<address1>${address1}</address1>" +
                    "<address2></address2>" +
                    "<city>${city}</city>" +
                    "<state></state>" +
                    "<postcode>${postcode}</postcode>" +
                    "<country>${country}</country>" +
                    "</contactDetails>" +
                    "</channel>" +
                    "</request>";

    public static final String CHANNEL_TYPE_TEMPLATE =
            "<channelType href='http://channel.brighttalk.com/internal/channeltype/{id}'>" +
                    "<name>${type}</name>" +
                    "<enabled role='user'>true</enabled>" +
                    "<default>true</default>" +
                    "<features/>" +
                    "</channelType>";

    public static final String CHANNEL_CONFIGURE_REQUEST_TEMPLATE =
            "<request>" +
                    "<channel id='${channelid}' sendNotifications='true'>" +
                    "<type>premium</type>" +
                    "<pendingType></pendingType>" +
                    "<bookingNumber>1235488</bookingNumber>" +
                    "</channel>" +
                    "</request>";


    public static final String CHANNEL_CONFIGURE_REALM_TEMPLATE =
            "<request>" +
                    "<channel id='${channelid}' sendNotifications='true'>" +
                    "<features>" +
                    "<feature name='allowedRealms' enabled='true'>2,1</feature>" +
                    "</features>" +
                    " </channel>" +
                    "</request>";

    public static final String DELETE_CHANNEL =
            "<request>" +
                    "<channel id='${channelid}'" +
                    "</request>";

    //Communication
    public static final String BOOK_COMMUNICATION_VALUES_TEMPLATE =
            "<request>" +
                    "<channel id='${channelId}'>" +
                    "<communication>" +
                    "<title>${title}</title>" +
                    "<description>${description}</description>" +
                    "<keywords>${keywords}</keywords>" +
                    "<author><name>${presenter}</name></author>" +
                    "<format>audio</format>" +
                    "<duration>${duration}</duration>" +
                    "<start>${startTime}</start>" +
                    "<timezone>Europe/London</timezone>" +
                    "<status>upcoming</status>" +
                    "<visibility>public</visibility>" +
                    "<publishStatus>published</publishStatus>" +
                    "<featureImage href='...'/>" +
                    "<provider name='brighttalk'>" +
                    "<resource type='webcast' id='12345'/>" +
                    "</provider>" +
                    "<allowAnonymous>false</allowAnonymous>" +
                    "<excludeFromChannelCapacity>false</excludeFromChannelCapacity>" +
                    "<showChannelSurvey>true</showChannelSurvey>" +
                    "<customUrl>http://www.customurl.com</customUrl>" +
                    "<categories></categories>" +
                    "<clientBookingReference>Reference 225584</clientBookingReference>" +
                    "</communication>" +
                    "</channel>" +
                    "</request>";

    public static final String SET_COMMUNICATION_RECORDED_TEMPLATE =
            "<request>" +
                    "<communication id='${communicationId}'>" +
                    "<status>recorded</status>" +
                    "<start>${startTime}</start>" +
                    "<timezone>Europe/London</timezone>" +
                    "<media url='communication/1/audio.mp3' length='750'/>" +
                    "<timeline></timeline>" +
                    "</communication>" +
                    "</request>";

    public static final String COMMUNICATION_ID_REGEX_PATTERN = "<communication id=\"(\\d+)\"/>";


    //Community
    public static final String CREATE_COMMUNITY_TEMPLATE =
            "<category>" +
                    "<title>${title}</title>" +
                    "<description>${description}</description>" +
                    "<alias>${alias}</alias>" +
                    "<enabled>true</enabled>" +
                    "</category>";

    //Summit
    public static final String CREATE_SUMMIT_TEMPLATE =
            "<summit>" +
                    "<title>${title}</title>" +
                    "<alias>${alias}</alias>" +
                    "<description>${description}</description>" +
                    "<keywords>${keywords}</keywords>" +
                    "<visibility>public</visibility>" +
                    "<scheduled>${scheduled}</scheduled>" +
                    "<enableSponsorASummit>false</enableSponsorASummit>" +
                    "</summit>";

    //User
    public static final String REGISTER_USER_TEMPLATE =
            "<request><embed><url>http://www.mysite.com/mychannel</url><track>uid:123,tid:456</track></embed> "+
                    "<form>" +
                    "<formAnswer id='1'>${firstName}</formAnswer>" +
                    "<formAnswer id='2'>${lastName}</formAnswer>" +
                    "<formAnswer id='3'>${email}</formAnswer>" +
                    "<formAnswer id='4'>${timezone}</formAnswer>" +
                    "<formAnswer id='5'>${password}</formAnswer>" +
                    "<formAnswer id='6'>${password}</formAnswer>" +
                    "<formAnswer id='7'></formAnswer>" +
                    "</form>" +
                    "</request>";

    public static final String LOGIN_USER_TEMPLATE =
            "<request>" +
                    "<user>" +
                    "<identifier>${email}</identifier>" +
                    "<password>${password}</password>" +
                    "</user>" +
                    "</request>";

    public static final String DELETE_USER_TEMPLATE =
            "<request />";

}

