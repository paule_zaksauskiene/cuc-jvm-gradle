package com.brighttalk.qa.common;

import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.utils.SeleniumUtils;
import cucumber.api.Scenario;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.util.StringUtils;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * sourced from:
 * https://github.com/cucumber/cucumber-jvm/blob/master/examples/java-webbit-websockets-selenium/src/test/java/cucumber/examples/java/websockets/SharedDriver.java
 */
public class SharedChromeDriver extends EventFiringWebDriver {

    private static Logger LOG = Logger.getLogger(SharedChromeDriver.class);

    public static WebDriver DRIVER = init(); //Initialises webDriver characteristics

    public SharedChromeDriver() {

        super(DRIVER);
    }


    public static WebDriver init() {

        String runMode = System.getProperty("RUN_MODE");

        if (StringUtils.hasLength(runMode)) {

            if (runMode.equalsIgnoreCase("grid")) {

                /*String browser = System.getProperty("TEST_BROWSER");

                DesiredCapabilities capability;

                if (browser.equalsIgnoreCase("chrome")) {
                    capability = DesiredCapabilities.chrome();
                } else {
                    LOG.debug("Unsupported browser! Using firefox");

                    //capability = DesiredCapabilities.firefox();
                    capability = DesiredCapabilities.chrome();
                }

                String browserVersion = System.getProperty("TEST_BROWSER_VERSION");

                if (StringUtils.hasLength(browserVersion)) {
                    capability.setCapability("version", System.getProperty("TEST_BROWSER_VERSION"));
                }

                try {
                    DRIVER = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capability);
                } catch (MalformedURLException mue) {
                    LOG.debug(mue.getMessage(), mue);
                }

                LOG.info(String.format("initiating remote <%s> driver on http://%s:4444", capability.getBrowserName(), Util.getIP())); */

                    DesiredCapabilities capabilities = DesiredCapabilities.chrome();
                    capabilities.setBrowserName("chrome");
                    capabilities.setPlatform(Platform.LINUX);
                    try {
                       DRIVER = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capabilities);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                     }

                     DRIVER.get("http://the-internet.herokuapp.com/login");

                     DRIVER.findElement(By.xpath("//h2[contains(.,'Login page')]"));

                     org.junit.Assert.assertTrue("FAILED !", DRIVER.getTitle().contains("The Internet"));

            } else {

                initiateDriver();
            }
        } else {

            initiateDriver();
        }

        DRIVER.manage().window().maximize();
        DRIVER.manage().deleteAllCookies();

        return DRIVER;
    }


    public static void initiateDriver(){

        LOG.info("run mode set to %s - defaulting to chrome driver");

        File f = null;
        final String OS = System.getProperty("os.name");
        if (OS.contains("Win")) {

            f  = new File("chromedriver.exe");
        }  else {

            f  = new File("chromedriver");
        }

        String chromedriverPath = f.getAbsolutePath();
        System.out.println("Chromedriver path: " + chromedriverPath);
        LOG.info("Chromedriver path: " + chromedriverPath);

        String userDir = System.getProperty("user.dir");
        System.out.println("User dir: " + userDir);
        LOG.info("User dir: " + userDir);

        System.setProperty("webdriver.chrome.driver", chromedriverPath);
        ChromeOptions options = new ChromeOptions();

        options.addArguments("--no-sandbox");
        options.addArguments("--disable-popup-blocking"); // Its important to enable pop ups as clicking on attachments opens new links



        // OPTIONS TO enable Flash (we load profile that has Flash enabled)

        // This code is used to create the profile
        //options.addArguments("user-data-dir=SeleniumTestProfile1");
       // WebDriver driver= new ChromeDriver(options);
        //driver.get("chrome://settings/content"); // Now set Allow for your websites manually on the settings page
        //DateTimeFormatter.waitForSomeTime(180);
        //options.addArguments("user-data-dir=SeleniumTestProfile1");

        // These options are required for the BrightTALK screenshare scenarios as the Chrome will be opening the Share Your Screen dialog
        options.addArguments("--use-fake-ui-for-media-stream");
        options.addArguments("--enable-usermedia-screen-capturing");

        String machineName = System.getProperty("user.name").trim();
        if (machineName.contains("macbookpro")) {

            options.addArguments("--auto-select-desktop-capture-source=Screen 2");

        } else {

            options.addArguments("--auto-select-desktop-capture-source=Entire screen");
        }

        System.out.println("Machine name " + machineName);
        options.addArguments("load-extension=/Users/" + machineName + "/Library/Application Support/Google/Chrome/Default/Extensions/mhhlcfengggleddnhpjokkmdhhpgclmh/0.3_0"); //BrightTALK screenshare

        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        capabilities.setCapability(CapabilityType.SUPPORTS_APPLICATION_CACHE,false);

        DRIVER = new ChromeDriver(capabilities);
        //DRIVER.manage().window().maximize();
        DRIVER.manage().deleteAllCookies();

    }

    public void deleteAllCookies() {

        DRIVER.manage().deleteAllCookies();
    }

    public void embedScreenshot(Scenario scenario) {

        try {

            WebDriver driver = (RemoteWebDriver) new Augmenter().augment(DRIVER);

            // Iterate throught all browser windows and capture a screenshot
            Set<String> windowHandles = driver.getWindowHandles();

            for (String winHandle : windowHandles) {

                driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
                byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            }

        } catch (WebDriverException somePlatformsDontSupportScreenshots) {

            LOG.error(somePlatformsDontSupportScreenshots.getMessage());
        }
    }

    public static WebDriver get() {

        return DRIVER;

    }
}


