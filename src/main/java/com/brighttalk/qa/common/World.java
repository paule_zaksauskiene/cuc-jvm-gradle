package com.brighttalk.qa.common;

import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.siteElements.CategorizationTag;
import com.brighttalk.qa.siteElements.OnDemandVideo;
import com.brighttalk.qa.siteElements.Account;
import com.brighttalk.qa.siteElements.ProgramSummary;
import com.brighttalk.qa.siteElements.WebcastPro;
import com.brighttalk.qa.siteElements.Campaign;
import com.brighttalk.qa.siteElements.DCCampaign;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class World {

    @Autowired
    EntityAttributeResolver eaResolver;

    //List<User> users;
    List<com.brighttalk.qa.siteElements.User> local_users;

    com.brighttalk.qa.siteElements.User regressionTestsManagerUser;

    com.brighttalk.qa.siteElements.User demandCentralTestsManagerUser;

    com.brighttalk.qa.siteElements.User demandCentralTestsManagerUserNA;

    com.brighttalk.qa.siteElements.User demandCentralTestsOwnerUser;

    com.brighttalk.qa.siteElements.User demandCentralTestsOwnerUserNA; 

    com.brighttalk.qa.siteElements.User connectorManagerUser;           
    //List<Channel> channels;

    List<com.brighttalk.qa.siteElements.Channel> local_channels;

    List<Object> communications;

    List<WebcastPro> webcastsPro;

    List<OnDemandVideo> onDemandVideos;

    List<Campaign> campaigns;

    List<DCCampaign> dcCampaigns;

    List<Account> accounts;

    List<ProgramSummary> programSummaries;

    String communicationEmbedCode;

    List<CategorizationTag> categorizationTags;

    List<String> windowHandles;

    String browserUsed = "";

    public World() {

        regressionTestsManagerUser = new com.brighttalk.qa.siteElements.User();
        regressionTestsManagerUser.setEmail(TestConfig.regressionTestsManagerUserEmail);
        regressionTestsManagerUser.setPassword(TestConfig.regressionTestsManagerUserPassword);
        demandCentralTestsManagerUser = new com.brighttalk.qa.siteElements.User();
        demandCentralTestsManagerUser.setEmail(TestConfig.demandCentralTestsManagerUserEmail);
        demandCentralTestsManagerUser.setPassword(TestConfig.demandCentralTestsManagerUserPassword);   
        demandCentralTestsManagerUserNA = new com.brighttalk.qa.siteElements.User();
        demandCentralTestsManagerUserNA.setEmail(TestConfig.demandCentralTestsManagerUserNAEmail);
        demandCentralTestsManagerUserNA.setPassword(TestConfig.demandCentralTestsManagerUserNAPassword);  
        demandCentralTestsOwnerUser = new com.brighttalk.qa.siteElements.User();
        demandCentralTestsOwnerUser.setEmail(TestConfig.demandCentralTestsOwnerUserEmail);
        demandCentralTestsOwnerUser.setPassword(TestConfig.demandCentralTestsOwnerUserPassword);  
        demandCentralTestsOwnerUserNA = new com.brighttalk.qa.siteElements.User();
        demandCentralTestsOwnerUserNA.setEmail(TestConfig.demandCentralTestsOwnerUserNAEmail);
        demandCentralTestsOwnerUserNA.setPassword(TestConfig.demandCentralTestsOwnerUserNAPassword);   
        connectorManagerUser = new com.brighttalk.qa.siteElements.User();
        connectorManagerUser.setEmail(TestConfig.connectorManagerUserEmail);
        connectorManagerUser.setPassword(TestConfig.connectorManagerUserPassword);                                      
        //users = new CopyOnWriteArrayList<>();
        //channels = new CopyOnWriteArrayList<>();

        local_users = new CopyOnWriteArrayList<>();
        local_channels = new CopyOnWriteArrayList<>();

        communications = new CopyOnWriteArrayList<>();

        webcastsPro = new CopyOnWriteArrayList<>();

        onDemandVideos = new CopyOnWriteArrayList<>();

        campaigns = new CopyOnWriteArrayList<>();

        dcCampaigns = new CopyOnWriteArrayList<>();

        accounts = new CopyOnWriteArrayList<>();

        programSummaries = new CopyOnWriteArrayList<>();

        windowHandles = new ArrayList<String>();

        setBrowserUsed("chrome");

    }

    //public List<User> getUsers() {
     //   return users;
    //}

    //public void setUsers(List<User> users) {
     //   this.users = users;
   // }

   // public List<Channel> getChannels() {
    //    return channels;
  //  }

   // public void setChannels(List<Channel> channels) {
     //   this.channels = channels;
   // }

    public List<Object> getCommunications() {

        return communications;
    }

    public com.brighttalk.qa.siteElements.Channel getConsumerChannel() {
        return consumerChannel;
    }

    com.brighttalk.qa.siteElements.Channel consumerChannel;

    public void setCommunications(List<Object> communications) {
        this.communications = communications;
    }

    public void setConsumerChannel(com.brighttalk.qa.siteElements.Channel consumerChannel) {

        if (consumerChannel != null) {
            eaResolver.add("{consumer_channel_title}", consumerChannel.getChannelTitle());
        }

        this.consumerChannel = consumerChannel;
    }

    public String getCommunicationEmbedCode() {

        return communicationEmbedCode;
    }

    public void setCommunicationEmbedCode(String communicationEmbedCode) {

        this.communicationEmbedCode = communicationEmbedCode;
    }

    public List<CategorizationTag> getCategorizationTags() {

        return categorizationTags;
    }

    public void setCategorizationTags(List<CategorizationTag> categorizationTags) {
        this.categorizationTags = categorizationTags;
    }

    public List<com.brighttalk.qa.siteElements.User> getLocal_users() {
        return local_users;
    }

    public void setLocal_users(List<com.brighttalk.qa.siteElements.User> local_users) {
        this.local_users = local_users;
    }

    public List<com.brighttalk.qa.siteElements.Channel> getLocal_channels() {
        return local_channels;
    }

    public void setLocal_channels(List<com.brighttalk.qa.siteElements.Channel> local_channels) {
        this.local_channels = local_channels;
    }

    public List<WebcastPro> getWebcastsPro() {
        return webcastsPro;
    }

    public void setWebcastsPro(List<WebcastPro> webcastsPro) {
        this.webcastsPro = webcastsPro;
    }

    public List<Campaign> getCampaigns() {
        return campaigns;
    }

    public void setCampaigns(List<Campaign> campaigns) {
        this.campaigns = campaigns;
    }

    public List<DCCampaign> getDCCampaigns() {
        return dcCampaigns;
    }

    public void setDCCampaigns(List<DCCampaign> dcCampaigns) {
        this.dcCampaigns = dcCampaigns;
    }

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public List<ProgramSummary> getProgramSummaries() {
        return programSummaries;
    }

    public void setProgramSummaries(List<ProgramSummary> programSummaries) {
        this.programSummaries = programSummaries;
    }

    public com.brighttalk.qa.siteElements.User getRegressionTestsManagerUser() {
        return regressionTestsManagerUser;
    }

    public void setRegressionTestsManagerUser(com.brighttalk.qa.siteElements.User regressionTestsManagerUser) {
        this.regressionTestsManagerUser = regressionTestsManagerUser;
    }

    public com.brighttalk.qa.siteElements.User getDemandCentralTestsManagerUser() {
        return demandCentralTestsManagerUser;
    }

    public void setDemandCentralTestsManagerUser(com.brighttalk.qa.siteElements.User demandCentralTestsManagerUser) {
        this.demandCentralTestsManagerUser = demandCentralTestsManagerUser;
    }

    public com.brighttalk.qa.siteElements.User getDemandCentralTestsManagerUserNA() {
        return demandCentralTestsManagerUserNA;
    }

    public void setDemandCentralTestsManagerUserNA(com.brighttalk.qa.siteElements.User demandCentralTestsManagerUserNA) {
        this.demandCentralTestsManagerUserNA = demandCentralTestsManagerUserNA;
    }

    public com.brighttalk.qa.siteElements.User getDemandCentralTestsOwnerUser() {
        return demandCentralTestsOwnerUser;
    }

    public void setDemandCentralTestsOwnerUser(com.brighttalk.qa.siteElements.User demandCentralTestsOwnerUser) {
        this.demandCentralTestsOwnerUser = demandCentralTestsOwnerUser;
    }

    public com.brighttalk.qa.siteElements.User getDemandCentralTestsOwnerUserNA() {
        return demandCentralTestsOwnerUserNA;
    }

    public void setDemandCentralTestsOwnerUserNA(com.brighttalk.qa.siteElements.User demandCentralTestsOwnerUserNA) {
        this.demandCentralTestsOwnerUserNA = demandCentralTestsOwnerUserNA;
    }

    public com.brighttalk.qa.siteElements.User getConnectorManagerUser() {
        return connectorManagerUser;
    }

    public void setConnectorManagerUser(com.brighttalk.qa.siteElements.User connectorManagerUser) {
        this.connectorManagerUser = connectorManagerUser;
    }

    public List<String> getWindowHandles() {
        return windowHandles;
    }

    public void setWindowHandles(List<String> windowHandles) {
        this.windowHandles = windowHandles;
    }

    public String getBrowserUsed() {
        return browserUsed;
    }

    public void setBrowserUsed(String browserUsed) {
        this.browserUsed = browserUsed;
    }

    public List<OnDemandVideo> getOnDemandVideos() {
        return onDemandVideos;
    }

    public void setOnDemandVideos(List<OnDemandVideo> onDemandVideos) {
        this.onDemandVideos = onDemandVideos;
    }

}
