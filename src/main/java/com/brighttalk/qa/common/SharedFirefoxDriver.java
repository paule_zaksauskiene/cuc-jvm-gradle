package com.brighttalk.qa.common;

import cucumber.api.Scenario;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.firefox.internal.ProfilesIni;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.springframework.util.StringUtils;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;

/**
 * sourced from:
 * https://github.com/cucumber/cucumber-jvm/blob/master/examples/java-webbit-websockets-selenium/src/test/java/cucumber/examples/java/websockets/SharedDriver.java
 */
public class SharedFirefoxDriver extends EventFiringWebDriver{

    private static Logger LOG = Logger.getLogger(SharedFirefoxDriver.class);

    public static WebDriver DRIVER = initFirefox();

    public SharedFirefoxDriver() {

        super(DRIVER);
    }


    public static WebDriver initFirefox() {

        System.setProperty("webdriver.gecko.driver","geckodriver4");

        FirefoxProfile profile;

        String machineName = System.getProperty("user.name").trim();
        if (machineName.contains("macbookpro")) {

            //ProfilesIni profileDefault = new ProfilesIni();
            //profile = profileDefault.getProfile("default-1501580016917");
            profile = new FirefoxProfile();

        } else {

            profile = new FirefoxProfile();
            //profile.setPreference("plugin.state.flash", 2);

        }

        DesiredCapabilities dc = DesiredCapabilities.firefox();
        dc.setCapability(FirefoxDriver.PROFILE, profile);

        DRIVER = new FirefoxDriver(dc);

        DRIVER.manage().window().maximize();
        return DRIVER;
    }


    public void deleteAllCookies() {
        DRIVER.manage().deleteAllCookies();
    }

    public void embedScreenshot(Scenario scenario) {

        try {

            WebDriver driver = (RemoteWebDriver) new Augmenter().augment(DRIVER);

            // Iterate throught all browser windows and capture a screenshot
            Set<String> windowHandles = driver.getWindowHandles();

            for (String winHandle : windowHandles) {

                driver.switchTo().window(winHandle); // switch focus of WebDriver to the next found window handle (that's your newly opened window)
                byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                scenario.embed(screenshot, "image/png");
            }

        } catch (WebDriverException somePlatformsDontSupportScreenshots) {

            LOG.error(somePlatformsDontSupportScreenshots.getMessage());
        }
    }

    public static WebDriver get() {

        return DRIVER;

    }
}


