package com.brighttalk.qa.common;


import java.util.Calendar;

/**
 * Created by Mike on 04/06/2015
 *
 * This class houses some basic functions, like date operations
 */
public class GenericFunctions {
    private GenericFunctions(){

    }

    /**
     *
     * This function takes in a Calendar object and returns a long form date string.
     *
     * E.g.
     * "2015-5-30 05:00" is returned as "May 30 2015 05:00 am"
     *
     * @param date The Calendar object representing the date you want to format into a string
     * @return String representation of the date
     */
    public static String getLongFormDateString(Calendar date) {
        String returnDate;

        String monthName = date.getTime().toString().substring(4, 7);

        String day = zeroPadLeft(Integer.toString(date.get(Calendar.DAY_OF_MONTH)));

        String hour = zeroAs12(Integer.toString(date.get(Calendar.HOUR)));

        String minute = zeroPadLeft(Integer.toString(date.get(Calendar.MINUTE)));

        String ampm;
        if (date.get(Calendar.AM_PM) == 0) { ampm = "am"; } else { ampm = "pm"; }


        returnDate = monthName + " " + day + " " + date.get(Calendar.YEAR) +
                " " + hour + ":" + minute + " " + ampm;


        return returnDate;
    }
    /**
     *
     * This function takes in a Calendar object and returns a long form date string without the time.
     *
     * E.g.
     * "2015-5-30 05:00" is returned as "May 30 2015"
     *
     * @param date The Calendar object representing the date you want to format into a string
     * @return String representation of the date
     */
    public static String getLongFormDateStringNoTime(Calendar date) {
        String returnDate;

        String monthName = date.getTime().toString().substring(4, 7);

        String day = zeroPadLeft(Integer.toString(date.get(Calendar.DAY_OF_MONTH)));

        returnDate = monthName + " " + day + " " + date.get(Calendar.YEAR);


        return returnDate;
    }    /**
     *
     * This function takes in a Calendar object and returns a String with the hour and AM/PM
     *
     * E.g.
     * "2015-5-30 05:00" is returned as "05am"
     *
     * @param date The Calendar object representing the date you want to format into a string
     * @return String representation of the date
     */
    public static String getHourWithAMPM(Calendar date) {
        String returnDate;

        String hour = zeroAs12(Integer.toString(date.get(Calendar.HOUR)));


        String ampm;
        if (date.get(Calendar.AM_PM) == 0) { ampm = "am"; } else { ampm = "pm"; }


        returnDate = hour + ampm;


        return returnDate;
    }

    /**
     * Add a zero to the beginning of a string if the length is 1.
     *
     * E.g. "5" becomes "05"
     */
    private static String zeroPadLeft(String input) {
        String output;

        if(input.length()==1) {
            output = "0"+input;
        } else {
            output = input;
        }
        return output;
    }
    /**
     * If a zero is passed in, it is passed out as a 12
     */
    private static String zeroAs12(String input) {
        String output;

        if(input.equals("0")) {
            output = "12";
        } else {
            output = input;
        }
        return output;
    }
}
