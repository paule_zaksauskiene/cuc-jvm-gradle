package com.brighttalk.qa.common;


import java.util.HashMap;
import java.util.Map;

public class UrlResolver {

    Map<String, String> urlMap;

    public UrlResolver() {

        if (this.urlMap == null) {
            this.urlMap = new HashMap<>();
        }
    }

    public void add(String key, String value) {

        urlMap.put(key, value);
    }

    public String resolve(String url) {

        for (String str : urlMap.keySet()) {

            System.out.println("String to replace: " + str);
            System.out.println("New value: " + urlMap.get(str));
            url = url.replace(str, urlMap.get(str));
        }

        return url;
    }

    public Map<String, String> getMap() {
        return urlMap;
    }

    public void setUrlMap(Map<String, String> urlMap) {
        this.urlMap = urlMap;
    }
}
