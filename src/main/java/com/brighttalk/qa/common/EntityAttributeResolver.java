package com.brighttalk.qa.common;


import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
/**
 * TODO make this better so that we can handle multiple objects of the same type (channel, webcast, users, etc)
 * TODO and be able to use it safely without tests getting muddled up on which data to pull from the map
 *
 * an idea
 * add another layer of object identification Map<String, Map<String, Map<String, String>>>
 * so that we can somewhat query it
 * For example:
 * object_name, (scenario title/number|channel_owner_id), entity in field/value map form
 * here we need to decide what we can do to further split objects.
 * it has to be something that will work even if tests are being ran in parallel
 * so scenario title/number
 * channel_owner ids given that scenarios always create their own data - this begs the question on whether we need to
 * further split it up to become something like this:
 * scenario_title, object_name, channel_owner_id, entity in field/value form. This seems best but might be overkill
 * should i think of creating a data db?
**/
public class EntityAttributeResolver {

    Map<String, Map<String, Object>> eaMap;

    public EntityAttributeResolver() {
        if (this.eaMap == null) {
            this.eaMap = new HashMap<>();
        }
    }

    public void add(String key, Object obj) {

        Map<String, Object> map = new HashMap<>();
        for (Field field : obj.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            try {
                map.put(field.getName(), field.get(obj));
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }


        eaMap.put(key, map);
    }

    public String resolve(String attr) {

        attr = attr.replace("{", "");
        attr = attr.replace("}", "");

        String[] attrKeys = attr.split("\\.");
        String key = attrKeys[0];
        String fieldName = attrKeys[1];

        for (String str : eaMap.keySet()) {
            if(str.equalsIgnoreCase(key)) {
                for(String str2 : eaMap.get(str).keySet()) {
                    if(str2.equalsIgnoreCase(fieldName))
                    attr = String.valueOf(eaMap.get(str).get(str2));
                }
            }
        }

        return attr;
    }

    public Map<String, Map<String, Object>> getMap() {
        return eaMap;
    }

    public void setMap(Map<String, Map<String, Object>> eaMap) {
        this.eaMap = eaMap;
    }
}
