package com.brighttalk.qa.page_objects.channelowner;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.BrighttalkPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(
        classes = TestConfig.class)
public class WebcastProSyndicationPage extends BrighttalkPage {

   @Autowired
   SharedChromeDriver driver;

   @Autowired
   UrlResolver urlResolver;

   @Autowired
   BrightTALKService service;

    public WebcastProSyndicationPage(SharedChromeDriver driver) {
        
    	super(driver);
    }

    @Override
    public void openPage(SharedChromeDriver driver) {

    }

    public void openPage(SharedChromeDriver driver, String channelID, String webcastID){

        driver.get("https://www." + TestConfig.getEnv() + ".brighttalk.net" + "/mybrighttalk/channel/" + channelID + "/webcast/" + webcastID + "/syndication");
    }

    @Override
    public void pageIsShown(SharedChromeDriver driver) {

    }
 }
