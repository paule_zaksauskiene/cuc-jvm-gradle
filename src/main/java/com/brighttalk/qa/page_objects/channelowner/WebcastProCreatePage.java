package com.brighttalk.qa.page_objects.channelowner;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.BrighttalkPage;
import com.brighttalk.qa.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(
        classes = TestConfig.class)
public class WebcastProCreatePage extends BrighttalkPage {

    @Autowired
    SharedChromeDriver driver;
    @Autowired
    UrlResolver urlResolver;
    @Autowired
    BrightTALKService service;

    private final By WEBCAST_PRO_CREATE_PAGE = By.xpath("//meta[@content='mybrighttalk-channel-webcast-brighttalkhd-create']");

    public WebcastProCreatePage(SharedChromeDriver driver) {
        
    	super(driver);
	}

	@Override
	public void openPage(SharedChromeDriver driver) {
	    		
	}
    
	public void openPage(WebDriver driver, String channelID) {
		     
    	driver.get("https://www." + TestConfig.getEnv() + ".brighttalk.net" +"/mybrighttalk/channel/"+ channelID + "/webcast/brighttalkhd/create");
    }
	
    @Override
    public void pageIsShown(SharedChromeDriver driver) {

        SeleniumUtils.findElement(driver, WEBCAST_PRO_CREATE_PAGE, "THE CREATE PRO WEBINAR PAGE IS NOT SHOWN!");

    }

}
