package com.brighttalk.qa.page_objects.audience;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.BrighttalkPage;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(
        classes = TestConfig.class)
public class ProPlayerTestPage extends BrighttalkPage {

    @Autowired
    SharedChromeDriver driver;
    @Autowired
    UrlResolver urlResolver;
    @Autowired
    BrightTALKService service;
    @Autowired
    Pages pages;

    /*
        Locators
     */
    private final String pageUrl = "https://www.int05.brighttalk.net/clients/js/embed-tester/index.html"; // Same for all environments
    private final By PLAYER_TEST_PAGE = By.xpath("//h1[contains(.,'PRO Player Test Page')]");
    private final By CHANNEL_ID = By.name("channelid");
    private final By ENVIRONMENT = By.name("environment");
    private final By COMMUNICATION_ID = By.name("commid");
    private final By CATEGORIES = By.id("categories");
    private final By LANGUAGE = By.className("jslanguage");
    private final By SHOW_PLAYER_BUTTON = By.cssSelector("button.jsShowPlayer");
    private final By PLAYER_FRAME =  By.xpath("//iframe[@class='jsPlayer']");
    private final By EMBED_CODE =  By.xpath("//div[contains(@class, 'BTEmbedCodeText')]");


    public ProPlayerTestPage(SharedChromeDriver driver) {

        super(driver);
    }



    @Override
    public void openPage(SharedChromeDriver driver) {

        driver.get(pageUrl);
    }


    @Override
    public void pageIsShown(SharedChromeDriver driver) {

        SeleniumUtils.findElement(driver, PLAYER_TEST_PAGE, "THE PRO PLAYER TEST PAGE DID NOT OPEN!");
        DateTimeFormatter.waitForSomeTime(10); // its important to wait here as the player page loads longer

    }

    public void enterChannelID(WebDriver driver, String channelId){

        SeleniumUtils.findElement(driver, CHANNEL_ID, "THE CHANNEL ID ENTER FIELD IS NOT FOUND ON THE PRO PLAYER TEST PAGE!").clear();
        SeleniumUtils.findElement(driver, CHANNEL_ID, "THE CHANNEL ID ENTER FIELD IS NOT FOUND ON THE PRO PLAYER TEST PAGE!").sendKeys(channelId);

    }

    public void selectEnvironment(WebDriver driver){

        new Select(SeleniumUtils.findElement(driver, ENVIRONMENT, "THE ENVIRONMENT DROP DOWN BOX IS NOT FOUND ON THE PRO PLAYER TEST PAGE!")).selectByVisibleText(TestConfig.env);

    }

    public void enterCommunicationID(WebDriver driver, String channelId){

        SeleniumUtils.findElement(driver, COMMUNICATION_ID, "THE COMMUNICATION ID ENTER FIELD IS NOT FOUND ON THE PRO PLAYER TEST PAGE!").clear();
        SeleniumUtils.findElement(driver, COMMUNICATION_ID, "THE COMMUNICATION ID ENTER FIELD IS NOT FOUND ON THE PRO PLAYER TEST PAGE!").sendKeys(channelId);

    }

    public void enterCategories(SharedChromeDriver driver, java.util.List<String> categories){

        String categoriesToEnter = "";

        for (int i = 0; i < categories.size(); i++){

            if (i > 0 ){

                categoriesToEnter = categoriesToEnter + "," + categories.get(i);

            } else {
                categoriesToEnter = categoriesToEnter + categories.get(i);
            }
        }

        driver.findElement(CATEGORIES).clear();
        driver.findElement(CATEGORIES).sendKeys(categoriesToEnter);

    }

    // The language can be selected as the following values: en-US, de-DE, es-ES, fr-FR, it-IT, ja-JP, ko-KR, pt-BR, zh-CN, ru-RU
    public void selectLanguage(WebDriver driver, String language){

        new Select(SeleniumUtils.findElement(driver, LANGUAGE, "THE ENVIRONMENT DROP DOWN BOX IS NOT FOUND ON THE PRO PLAYER TEST PAGE!")).selectByVisibleText(language);

        // Its important to wait here as the form does not pick up newly created value if the next button will be clicked too fast
        DateTimeFormatter.waitForSomeTime(2);
    }

    public void clickTheShowPlayerButton(WebDriver driver){

        SeleniumUtils.findElement(driver, SHOW_PLAYER_BUTTON, "THE SHOW PLAYER BUTTON IS NOT FOUND ON THE PRO PLAYER TEST PAGE!").click();

        // Its important to wait here as the player needs to set up a new language
        DateTimeFormatter.waitForSomeTime(4);
    }

    public void switchToPlayerIframe(SharedChromeDriver driver){

        driver.switchTo().defaultContent();
        driver.switchTo().frame(SeleniumUtils.findElement(driver, PLAYER_FRAME, "THE jsPLAYER IFRAME IS NOT FOUND ON THE PRO PLAYER TEST PAGE!"));

    }

    public String copyEmbedCode(WebDriver driver){

        DateTimeFormatter.waitForSomeTime(5); // wait untill code appears
        String embedCode = SeleniumUtils.findElement(driver, EMBED_CODE, "THE EMBED CODE IS NOT FOUND ON THE PLAYER TEST PAGE!").getText();
        embedCode = embedCode.replace("height\" : \"100%", "height\" : \"1000");
        System.out.println("THE EMBED CODE: " + embedCode);
        return embedCode;
    }
}
