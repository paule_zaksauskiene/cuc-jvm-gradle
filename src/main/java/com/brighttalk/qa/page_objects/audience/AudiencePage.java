package com.brighttalk.qa.page_objects.audience;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.BrighttalkPage;
import com.brighttalk.qa.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import java.util.concurrent.TimeUnit;
//import static org.seleniumhq.jetty9.servlets.gzip.GzipHttpOutput.LOG;

@ContextConfiguration( classes = TestConfig.class)
public class AudiencePage extends BrighttalkPage {

	@Autowired
    SharedChromeDriver driver;
	@Autowired
	UrlResolver urlResolver;
	@Autowired
	BrightTALKService service;

	private final By AUDIENCE_PAGE = By.xpath("//meta[@content='webcast']");
	
	 public AudiencePage(SharedChromeDriver driver) {
		   
	       super(driver);
	 }
	  
	   @Override
	    public void openPage(SharedChromeDriver driver) {

	    	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS); // wait to load the page
	    }
	   
	   public void openPage(SharedChromeDriver driver, String channel_id, String webcast_id){

		   driver.get("https://www." + TestConfig.getEnv() +".brighttalk.net"  + "/webcast/" + channel_id + "/" + webcast_id);
		   driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS); // wait to load the page

	   }

	public void openAttendPage(WebDriver driver, String channel_id, String webcast_id){

		// Sample : https://www.int05.brighttalk.net/service/player/en-US/theme/default/channel/5008/webcast/7/attend
		driver.get("https://www." + TestConfig.getEnv() +".brighttalk.net"  + "/service/player/en-US/theme/default/channel/" + channel_id + "/webcast/" + webcast_id +  "/play");
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS); // wait to load the page

	}
	    @Override
	    public void pageIsShown(SharedChromeDriver driver){

            SeleniumUtils.findElement(driver, AUDIENCE_PAGE, "THE AUDIENCE PAGE IS NOT OPENED!");

	    }

	    public void verifyifYouAreConfirmedOverlayIsShownAndCloseIt(SharedChromeDriver driver){

			// Wait for element to exist
			SeleniumUtils.findElement(driver, By.xpath("//div[contains(@class, 'onboarding-panel')]"),  "THE USER REGISTRATION CONFIRMATION OVERLAY IS NOT SHOWN");

            // Verify the element is displayed
			if (SeleniumUtils.isDisplayed(driver, By.xpath("//div[contains(@class, 'onboarding-panel')]"), "THE USER REGISTRATION CONFIRMATION OVERLAY IS NOT SHOWN")) {

				//LOG.info("The \"You are confirmed to attend \" overlay is displayed and will be closed..");
				SeleniumUtils.findElement(driver, By.xpath("//button[contains(@class, 'skip-button')]"), "").click();

				if (SeleniumUtils.isDisplayed(driver, By.xpath("//div[contains(@class, 'share-with-your-team-panel')]"), "")){

					//LOG.info("The \"You are confirmed to attend \" overlay next page is displayed and will be closed..");
					SeleniumUtils.findElement(driver, By.xpath("//a[contains(@class, 'skip-button')]"), "").click();
				}

			}
		}
}
