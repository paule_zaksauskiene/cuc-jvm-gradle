package com.brighttalk.qa.page_objects.common;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.utils.SeleniumUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertEquals;

@ContextConfiguration(
        classes = TestConfig.class)
public abstract class BrighttalkPage {

    @Autowired
    SharedChromeDriver driver;
    @Autowired
    UrlResolver urlResolver;
    @Autowired
    BrightTALKService service;
	
    private static final Logger LOG = Logger.getLogger(SeleniumUtils.class);

    public String getWindowHandle() {

       return driver.getWindowHandle();
    }

    public BrighttalkPage(SharedChromeDriver driver){
    	

    }
    
    public abstract void openPage(SharedChromeDriver driver);
    
    public abstract void pageIsShown(SharedChromeDriver driver);
    
    public void openPage(String custom_url){
    	
    	driver.get(custom_url);
    	
    	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
    }

}
