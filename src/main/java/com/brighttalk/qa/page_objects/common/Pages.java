package com.brighttalk.qa.page_objects.common;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.audience.AudiencePage;
import com.brighttalk.qa.page_objects.audience.ProPlayerTestPage;
import com.brighttalk.qa.page_objects.blogger.BloggerPage;
import com.brighttalk.qa.page_objects.blogger.BloggerPostPage;
import com.brighttalk.qa.page_objects.channelowner.WebcastProCreatePage;
import com.brighttalk.qa.page_objects.channelowner.WebcastProSyndicationPage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import com.brighttalk.qa.page_objects.loginregister.LoginPage;

@ContextConfiguration(
        classes = TestConfig.class)
public class Pages {

    @Autowired
    SharedChromeDriver driver;
    @Autowired
    UrlResolver urlResolver;
    @Autowired
    BrightTALKService service;

    private AudiencePage audiencePage;
    private ProPlayerTestPage proPlayerTestPage;
    private SearchPage searchPage;
    private BloggerPostPage bloggerPostPage;
    private BloggerPage bloggerPage;
    private LoginPage loginPage;
    private Homepage homepage;
    private WebcastProCreatePage webcastProCreatePage;
    private WebcastProSyndicationPage webcastProSyndicationPage;

    public Pages() {

    }

    public AudiencePage audiencePage() {

        if (audiencePage == null) {
            audiencePage = new AudiencePage(driver);
        }
        return audiencePage;
    }

    public ProPlayerTestPage proPlayerTestPage(){
        if ( proPlayerTestPage == null ){
            proPlayerTestPage = new ProPlayerTestPage (driver);
        }
        return proPlayerTestPage;
    }

    public SearchPage searchPage(){
        if ( searchPage == null ){
            searchPage = new SearchPage(driver);
        }
        return searchPage;
    }

    public BloggerPostPage bloggerPostPage(){
        if ( bloggerPostPage == null ){
            bloggerPostPage = new BloggerPostPage(driver);
        }
        return bloggerPostPage;
    }

    public BloggerPage bloggerPage(){
        if ( bloggerPage == null ){
            bloggerPage = new BloggerPage (driver);
        }
        return bloggerPage;
    }

    public LoginPage loginPage() {
        if (loginPage == null) {
            loginPage = new LoginPage(driver);
        }
        return loginPage;
    }

    public Homepage home() {
        if (homepage == null) {
            homepage = new Homepage(driver);
        }
        return homepage;
    }

    public WebcastProCreatePage createWebcastHdPage() {
        if (webcastProCreatePage == null) {
            webcastProCreatePage = new WebcastProCreatePage(driver);
        }
        return webcastProCreatePage;
    }

    public WebcastProSyndicationPage webcastProSyndicationPage() {
        if (webcastProSyndicationPage == null) {
            webcastProSyndicationPage = new WebcastProSyndicationPage(driver);
        }
        return webcastProSyndicationPage;
    }  
}