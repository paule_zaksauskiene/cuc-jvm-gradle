package com.brighttalk.qa.page_objects.common;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.init.EnvSetup;
import com.brighttalk.qa.utils.SeleniumUtils;
import org.openqa.selenium.By;

public class SearchPage extends BrighttalkPage {

    private final By SEARCH_PAGE = By.xpath("//meta[@content='search']");

	public SearchPage(SharedChromeDriver driver) {
		   
	    super(driver);
	}

    @Override
    public void openPage(SharedChromeDriver driver) {
    	
    	driver.get(EnvSetup.SEARCH);

    }
    
    public void pageIsShown(SharedChromeDriver driver) {

        SeleniumUtils.findElement(driver, SEARCH_PAGE, "");

    }
  }

