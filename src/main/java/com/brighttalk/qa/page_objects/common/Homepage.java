package com.brighttalk.qa.page_objects.common;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.utils.SeleniumUtils;
import org.openqa.selenium.By;

public class Homepage extends BrighttalkPage {

    private final By HOMEPAGE = By.xpath("//meta[@content='home']");

    public static final By joinNowLocator = By.cssSelector("a.sign-up-link");

	   public Homepage(SharedChromeDriver driver) {
		   
	       super(driver);
	    }

	@Override
	public void openPage(SharedChromeDriver driver) {

		//driver.get(TestConfig.HOME_URL);

	}

    public void pageIsShown(SharedChromeDriver driver) {

        SeleniumUtils.findElement(driver, HOMEPAGE, "THE BrightTALK HOMEPAGE DID NOT OPEN!");
        
    }

    public void clickLogin() {

		driver.findElement(By.linkText("Log in")).click();

    }
  }

