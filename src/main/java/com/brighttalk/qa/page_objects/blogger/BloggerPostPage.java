package com.brighttalk.qa.page_objects.blogger;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.init.EnvSetup;
import com.brighttalk.qa.page_objects.common.BrighttalkPage;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.siteElements.User;
import com.brighttalk.qa.utils.SeleniumUtils;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;

public class BloggerPostPage extends BrighttalkPage {

	@Autowired
	World world;

	private final By PLAYER_TEST_POST = By.cssSelector("h3.post-title.entry-title");
	private final By PLAY_BUTTON = By.xpath("//*[contains(@class, 'jsPlayButton')]");
	public final By ATTACHMENTS_TAB = By.xpath("//*[contains(@class, 'jsView')]/div[2][contains(@data-tab-target, 'Attachments')]");
	private final By PLAYER_FRAME =  By.xpath("//iframe[@class='jsPlayer']");
	private final By LOGIN_BUTTON = By.xpath("//a[contains(@class, 'login-button')]");
	private final By EMAIL = By.name("email");
	private final By PASSWORD = By.name("password");
	private final By LOGIN_BUTTON_BELOW_USER_DETAILS = By.xpath("//button[contains(@class, 'jsSubmitForm')]");
	private final By ATTENDANCE_CONFIRMED = By.xpath("//section[contains(@class, 'jsAttendanceConfirmed')]");
	private final By ATTENDANCE_CONFIRMED_GERMAN = By.xpath("//section[contains(@class, 'jsAttendanceConfirmed')][contains(., 'Ihre Teilnahme ist bestätigt')]");


	public BloggerPostPage(SharedChromeDriver driver) {

	   super(driver);
	}


	public void openPage() {

		// Open the blogger post page as viewer
	   //driver.get(EnvSetup.BLOGGER_POST_URL);

	}

	public void openPage(SharedChromeDriver driver) {

		// Open the blogger post page as viewer
		driver.get(EnvSetup.BLOGGER_POST_URL);

	}


	@Override
	public void pageIsShown(SharedChromeDriver driver) {

		Assert.assertEquals("Player test post", SeleniumUtils.findElement(driver, PLAYER_TEST_POST, "THE TITLE \"Player test post\" IS NOT FOUND ON THE BLOGGER PAGE!").getText(), "THE TITLE \"Player test post\" IS NOT FOUND ON THE BLOGGER PAGE!");

	}

	public void clickPlayButton(SharedChromeDriver driver){

		SeleniumUtils.findElement(driver, PLAY_BUTTON, "THE \"PLAY or ENTER\" BUTTON IS NOT FOUND ").click();
	}

	public void switchToPlayerIframe(SharedChromeDriver driver){

		driver.switchTo().frame(SeleniumUtils.findElement(driver, PLAYER_FRAME, "THE jsPLAYER IFRAME IS NOT FOUND ON THE PRO PLAYER TEST PAGE!"));

	}

	public void clickLoginButton(SharedChromeDriver driver){

		WebDriverWait wait = new WebDriverWait(driver, 15);
		//WebDriverWait wait = new Function<? super WebDriver,V>
		wait.until(ExpectedConditions.elementToBeClickable(LOGIN_BUTTON));

		// Its important to wait here as even the "elementToBeClickable" does not wait enough
		DateTimeFormatter.waitForSomeTime(2);

		WebElement element= SeleniumUtils.findElement(driver, LOGIN_BUTTON, "THE LOGIN BUTTON IS NOT FOUND !");

		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);


	}

	public void enterLoginDetails(SharedChromeDriver driver, User user){

		SeleniumUtils.findElement(driver, EMAIL, "THE EMAIL FIELD IS NOT FOUND ON THE BLOGGER POST PAGE!").clear();
		SeleniumUtils.findElement(driver, EMAIL, "THE EMAIL FIELD IS NOT FOUND ON THE BLOGGER POST PAGE!").sendKeys(user.getEmail());

		SeleniumUtils.findElement(driver, PASSWORD, "THE PASSWORD FIELD IS NOT FOUND ON THE BLOGGER POST PAGE!").clear();

		if  (user.getPassword()!= null) {

			SeleniumUtils.findElement(driver, PASSWORD, "THE PASSWORD FIELD IS NOT FOUND ON THE BLOGGER POST PAGE!").sendKeys(user.getPassword());
		}
	}

	public void clickLoginButtonBelowUserDetails(SharedChromeDriver driver){

		driver.switchTo().defaultContent();
		switchToPlayerIframe(driver);

		// Find Login button element below the user details
		WebElement element= SeleniumUtils.findElement(driver, LOGIN_BUTTON_BELOW_USER_DETAILS, "THE LOGIN BUTTON IS NOT FOUND !");

		// Generate click on the Login button
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", element);

	}

	/**
	 * Verify the Your Place is Confirmed text is shown when user registers to the upcoming webinar
	 * @param driver
	 */
	public void verifyWebcastConfirmedPanel(SharedChromeDriver driver) {

		driver.switchTo().defaultContent();
	    switchToPlayerIframe(driver);
        SeleniumUtils.findElement(driver, ATTENDANCE_CONFIRMED, 60, 1, false, "THE TEXT \"Your place is confirmed\" IS NOT FOUND ON THE WEBINAR AUDIENCE PAGE!");

	}

	/**
	 * Verify the Your Place is Confirmed text is shown when user registers to the upcoming webinar in german language
	 * @param driver
	 */
	public void verifyWebcastConfirmedPanelInGerman(SharedChromeDriver driver) {

		driver.switchTo().defaultContent();
		switchToPlayerIframe(driver);
		SeleniumUtils.findElement(driver, ATTENDANCE_CONFIRMED_GERMAN , 60, 1, false, "THE TEXT \"Your place is confirmed\" IS NOT FOUND ON THE WEBINAR AUDIENCE PAGE!");

	}
}
