package com.brighttalk.qa.page_objects.loginregister;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.page_objects.common.BrighttalkPage;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.siteElements.User;
import com.brighttalk.qa.utils.SeleniumUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.*;
import java.util.concurrent.TimeUnit;

public class LoginPage extends BrighttalkPage {

    private final By LOGIN_PAGE = By.xpath("//meta[@content='login']");
    private final By LOGIN_PAGE_HEADER = By.xpath("//h1[@class='first-level-headline'][contains(., 'Log in to BrightTALK')]");
    private final By USER_LOGIN_EMAIL = By.id("user_login_email");
    private final By USER_LOGIN_PASSWORD = By.id("user_login_password");
    private final By LOGIN_BUTTON = By.xpath("//input[@value='Log in']");

	public LoginPage(SharedChromeDriver driver) {
    	
    	super(driver);
    }
     
    @Override
    public void openPage(SharedChromeDriver driver) {
    	
    	driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.navigate().refresh();

    }

    @Override
    public void pageIsShown(SharedChromeDriver driver) {

        SeleniumUtils.findElement(driver, LOGIN_PAGE, "");
    }


    public void loginUserTemp(SharedChromeDriver driver, User testUser) {

        // Waiting is needed for Firefox to render all input fields before entering values into them
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        SeleniumUtils.findElement(driver, LOGIN_PAGE_HEADER, 60, 1, false, "THE \"Log in to BrightTALK\" IS NOT FOUND ON THE LOGIN PAGE!" );

        // its very important to wait here as safari fails on entering values if added too quickly
        DateTimeFormatter.waitForSomeTime(3);

        // This line is essential to verify if input field is visible
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(USER_LOGIN_EMAIL));

        System.out.println("My test user is: " + testUser.getEmail());

        SeleniumUtils.findElement(driver, USER_LOGIN_EMAIL, "THE USER LOGIN EMAIL FIELD IS NOT FOUND ON THE LOGIN PAGE!").sendKeys(testUser.getEmail());
        SeleniumUtils.findElement(driver, USER_LOGIN_EMAIL, "THE USER LOGIN EMAIL FIELD IS NOT FOUND ON THE LOGIN PAGE!").clear();
        SeleniumUtils.findElement(driver, USER_LOGIN_EMAIL, "THE USER LOGIN EMAIL FIELD IS NOT FOUND ON THE LOGIN PAGE!").sendKeys(testUser.getEmail());
        SeleniumUtils.findElement(driver, USER_LOGIN_PASSWORD, "THE USER LOGIN PASSWORD FIELD IS NOT FOUND ON THE LOGIN PAGE!").sendKeys(testUser.getPassword());
        SeleniumUtils.findElement(driver, LOGIN_BUTTON , "THE LOGIN IN BUTTON IS NOT FOUND ON THE LOGIN PAGE!").click();

        TestConfig.user_login_flag = true;
    }
}
