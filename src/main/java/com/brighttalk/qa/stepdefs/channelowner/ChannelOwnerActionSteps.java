package com.brighttalk.qa.stepdefs.channelowner;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.io.File;

@ContextConfiguration(
        classes = TestConfig.class)
public class ChannelOwnerActionSteps {

    @Autowired
    SharedChromeDriver driver;

    @Autowired
    UrlResolver urlResolver;

    @Autowired
    BrightTALKService service;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Autowired
    BrowserAssertionSteps browserAssertionSteps;

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    @Autowired
    World world;

    @Autowired
    Pages pages;

    @When("^I click Proceed button on the Edit booking form$")
    public void i_click_proceed_button_on_the_edit_booking_form() throws Throwable {

        String pageProp = Util.toYamlPropertyString("pro-webinar-booking-form-proceed-button");
        String locator = PropertiesUtil.getProperty(pageProp);

        By elementToClick;

        // Verifies if the locator is xpath or css
        if(locator.contains("//")){

            elementToClick = By.xpath(locator);

        } else {

            elementToClick = By.cssSelector(locator);
        }

        DateTimeFormatter.waitForSomeTime(1); // We need to wait for some of the buttons, as the click is not performed

        // For some locators its important to wait for element to be clickable
        SeleniumUtils.waitForUntil(driver, ExpectedConditions.elementToBeClickable(elementToClick), 2, 120, 1);
        SeleniumUtils.waitForUntil(driver, ExpectedConditions.visibilityOfElementLocated(elementToClick), 2, 120, 1);

        // Generate click on the  button
        WebElement element= SeleniumUtils.findElement(driver, elementToClick, "THE LOGIN BUTTON IS NOT FOUND !");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
        //SeleniumUtils.findElement(driver, elementToClick, "").click();

        DateTimeFormatter.waitForSomeTime(1);
    }

    @When("^I cancel booking$")
    public void i_cancel_booking() throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();
        DateTimeFormatter.waitForSomeTime(1);
        ((JavascriptExecutor)driver).executeScript("confirm = function(message){return true;};");
        ((JavascriptExecutor)driver).executeScript("alert = function(message){return true;};");
        ((JavascriptExecutor)driver).executeScript("prompt = function(message){return true;}");
        DateTimeFormatter.waitForSomeTime(1);

        browserActionSteps.i_click("edit-booking-page-cancel-button","","");

        if (world.getBrowserUsed().contains("firefox")){

            try {

                WebDriverWait wait = new WebDriverWait(driver, 10);
                wait.until(ExpectedConditions.alertIsPresent());
                Alert alert = driver.switchTo().alert();
                String alertText = alert.getText();
                alert.accept();


            } catch (Exception e) {

            }
        }

        browserAssertionSteps.the_element_exists("edit-booking-page-webcast-cancelled-message");
    }

    @Given("^I select jpeg feature image")
    public void i_select_feature_image_jpeg(){

        i_select_feature_image("feature_image.JPEG");
    }

    @Given("^I select png feature image")
    public void i_select_feature_image_png(){

        i_select_feature_image("png_image.png");
    }

    public void i_select_feature_image(String featureImage){

        File f = new File("src/main/resources/" + featureImage);
        String myFullPath = f.getAbsolutePath();

        System.out.println("My full image path: " + myFullPath);

        browserActionSteps.i_enter_value("pro-webinar-booking-form-edit-featureimage", myFullPath);

    }

    @When("^I cancel booking schedule$")
    public void i_cancel_booking_schedule() throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver(); //define webdriver
        DateTimeFormatter.waitForSomeTime(1);
        ((JavascriptExecutor)driver).executeScript("confirm = function(message){return true;};");
        ((JavascriptExecutor)driver).executeScript("alert = function(message){return true;};");
        ((JavascriptExecutor)driver).executeScript("prompt = function(message){return true;}");
        DateTimeFormatter.waitForSomeTime(1);

        browserActionSteps.i_click2("edit-booking-page-cancel-button","","");

        if (world.getBrowserUsed().contains("firefox")){

            try {

                WebDriverWait wait = new WebDriverWait(driver, 10);
                wait.until(ExpectedConditions.alertIsPresent());
                Alert alert = driver.switchTo().alert();
                String alertText = alert.getText();
                alert.accept();


            } catch (Exception e) {

            }
        }

        browserAssertionSteps.the_element_exists("edit-booking-page-webcast-cancelled-message");
    }
}
