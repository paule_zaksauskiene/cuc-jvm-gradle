package com.brighttalk.qa.stepdefs.channelowner;

import com.brighttalk.qa.api.WebcastProApi;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.siteElements.User;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(
        classes = TestConfig.class)
public class ChannelOwnerAssertionSteps {

    @Autowired
    SharedChromeDriver driver;

    @Autowired
    World world;

    @Autowired
    BrightTALKService service;

    @Autowired
    BrowserAssertionSteps browserAssertionSteps;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Autowired
    UrlResolver urlResolver;

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    /**
     * This is used to add the new booked pro webinar (on the front end to the World object to be deleted later
     * @throws Throwable
     */

     // Verifies the image appears on the view booking page
    @Then("^first feature image is uploaded")
    public void featureImageIsShown(){

        world.getWebcastsPro().get(0).setFeatureImageName(this.verifyFeatureImage());

    }

    @Then("^feature image is uploaded$")
    public String verifyFeatureImage(){

        String featureImage = "";
        for (int i = 0; i < 15; i++){

            try {

                String pageProp = Util.toYamlPropertyString("view-booking-page-feature-image");
                String locator = PropertiesUtil.getProperty(pageProp);
                featureImage =  driver.findElement(By.xpath(locator)).getAttribute("href");

                break;

            } catch (NoSuchElementException ex) {

                System.out.println("Could not find feature image on the View booking page, will wait 60 secs...");
                DateTimeFormatter.waitForSomeTime(60);
                this.driver.navigate().refresh();
                try {
                    browserAssertionSteps.the_page_opens("view-booking-page-metatag");
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }

            }
            if (i==11) Assert.fail("Could not find the feature image on the PRO WEBINAR VIEW booking page...");
        }

        if (featureImage != "") {

            int startPosition = featureImage.indexOf("preview");
            int endPosition = featureImage.indexOf(".png", startPosition);
            featureImage = featureImage.substring(startPosition, endPosition);
        }

        StringBuffer communicationDetails = WebcastProApi.getCommunicationDetails(world.getRegressionTestsManagerUser(), world.getLocal_channels().get(0), world.getWebcastsPro().get(0));

        String featureImageName = WebcastProApi.communicationFeatureImageName(communicationDetails);

        this.verifyAutoCapturedFeatureImage(featureImageName);

        return featureImage;
    }


    //To verify the feature image has been captured and displays the correct png file
    public void verifyAutoCapturedFeatureImage(String featureImageName) {

        for (int i = 0; i < 12; i++) {

            try {
                driver.findElement(By.xpath("//a[@class='preview'][contains(@href, '" + featureImageName + "')]"));
                break;

            } catch (NoSuchElementException ex) {

                try {
                    Thread.sleep(60000);
                    this.driver.navigate().refresh();

                    try {
                        browserAssertionSteps.the_page_opens("view-booking-page-metatag");
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (i == 4) Assert.fail("Could not find the feature image on the PRO WEBINAR VIEW booking page...");
        }
    }

    @Then("^feature image is replaced$")
    public void feature_image_is_replaced(){

        // Retrieves the preview image from API and compares to the image shown on the Edit booking page
        StringBuffer communicationDetails = WebcastProApi.getCommunicationDetails(world.getRegressionTestsManagerUser(), world.getLocal_channels().get(0), world.getWebcastsPro().get(0));
        String featureImageName2 = WebcastProApi.communicationFeatureImageName(communicationDetails);

        verifyAutoCapturedFeatureImage(featureImageName2);

        Assert.assertTrue("The image was not replaced with new one..", !featureImageName2.trim().contains(world.getWebcastsPro().get(0).getFeatureImageName().trim()));

    }

    // Verifies the image appears on the view booking page
    @Then("^the feature image appears on the view booking page$")
    public String featureImageAppearsOnBookingPage(){

        String featureImage = "";
        for (int i = 0; i < 12; i++){

            try {

                String pageProp = Util.toYamlPropertyString("view-booking-page-feature-image");
                String locator = PropertiesUtil.getProperty(pageProp);
                featureImage =  driver.findElement(By.xpath(locator)).getAttribute("href");
                break;

            } catch (NoSuchElementException ex) {

                System.out.println("Could not find feature image on the View booking page, will wait 60 secs...");
                DateTimeFormatter.waitForSomeTime(60);
                this.driver.navigate().refresh();
                try {
                    browserAssertionSteps.the_page_opens("view-booking-page-metatag");
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }

            }
            if (i==11) Assert.fail("Could not find the feature image on the PRO WEBINAR VIEW booking page...");
        }

        if (featureImage != "") {

            int startPosition = featureImage.indexOf("preview");
            int endPosition = featureImage.indexOf(".png", startPosition);
            featureImage = featureImage.substring(startPosition, endPosition);
        }
        return featureImage;
    }

    @Then("^anonymous viewer details are shown on viewings report$")
    public void verifyViewerDetails(){

        String userDetailsLocator = SeleniumUtils.resolveElementLocator("webcast-reports-viewings-report-viewer-details");

        User anonymousUser = new User();
        anonymousUser.setFirstName("Not Subscribed");
        anonymousUser.setSecondName("");
        anonymousUser.setJobTitle("Withheld");
        anonymousUser.setCompany("Withheld");

        SeleniumUtils.findElement(driver, By.xpath(userDetailsLocator + "[contains(.,'" + world.getLocal_users().get(0).getFirstName() + "')]"), 1200, 60, true, "THE VIEWER FIRST NAME IS NOT FOUND ON THE WEBCAST VIEWINGS PAGE!");
        SeleniumUtils.findElement(driver, By.xpath(userDetailsLocator + "[contains(.,'" + world.getLocal_users().get(0).getSecondName() + "')]"), 60, 6, true, "THE VIEWER SECOND NAME IS NOT FOUND ON THE WEBCAST VIEWINGS PAGE!");

        SeleniumUtils.findElement(driver, By.xpath(userDetailsLocator + "[contains(.,'" + world.getLocal_users().get(0).getJobTitle() + "')]"), 60, 6, true, "THE VIEWER JOB TITLE IS NOT FOUND ON THE WEBCAST VIEWINGS PAGE!");
        SeleniumUtils.findElement(driver, By.xpath(userDetailsLocator + "[contains(.,'" + world.getLocal_users().get(0).getCompany() + "')]"), 60, 6, true, "THE VIEWER COMPANY IS NOT FOUND ON THE WEBCAST VIEWINGS PAGE!");

    }

    @Then("^viewer first name (.*), last name (.*), job title (.*), company (.*) appears on viewings report$")
    public void viewerDetailsAppearOnTheReport(String firstName, String lastName, String jobTitle, String company){

        String userDetailsLocator = SeleniumUtils.resolveElementLocator("webcast-reports-viewings-report-viewer-details");

        User user = new User();
        user.setFirstName(firstName.trim());
        user.setSecondName(lastName.trim());
        user.setJobTitle(jobTitle.trim());
        user.setCompany(company.trim());

        SeleniumUtils.findElement(driver, By.xpath(userDetailsLocator + "[contains(.,'" + user.getFirstName() + "')]"), 1200, 60, true, "THE VIEWER FIRST NAME IS NOT FOUND ON THE WEBCAST VIEWINGS PAGE!");
        SeleniumUtils.findElement(driver, By.xpath(userDetailsLocator + "[contains(.,'" + user.getSecondName() + "')]"), 60, 6, true, "THE VIEWER SECOND NAME IS NOT FOUND ON THE WEBCAST VIEWINGS PAGE!");

        SeleniumUtils.findElement(driver, By.xpath(userDetailsLocator + "[contains(.,'" + user.getJobTitle() + "')]"), 60, 6, true, "THE VIEWER JOB TITLE IS NOT FOUND ON THE WEBCAST VIEWINGS PAGE!");
        SeleniumUtils.findElement(driver, By.xpath(userDetailsLocator + "[contains(.,'" + user.getCompany() + "')]"), 60, 6, true, "THE VIEWER COMPANY IS NOT FOUND ON THE WEBCAST VIEWINGS PAGE!");

    }

    @Then("^pro webinar custom url can not be changed on the edit booking page$")
    public void verifyWebcastUrlCanNotBeChanged(){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        try {
            driver.findElement(By.xpath("//span[@id='changeWebcastUrl'][contains(.,'Change')]"));
            Assert.fail("WEBCAST URL Change link is present for webcast URL, when it should not on the PRO WEBINAR Edit booking page.");
        } catch (NoSuchElementException ex) {
        /* do nothing, link is not present, assert is passed */
        }
    }


}
