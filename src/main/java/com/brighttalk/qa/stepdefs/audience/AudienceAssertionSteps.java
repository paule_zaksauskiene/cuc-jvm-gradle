package com.brighttalk.qa.stepdefs.audience;

import com.brighttalk.qa.common.EntityAttributeResolver;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(
        classes = TestConfig.class)
public class AudienceAssertionSteps {

    @Autowired
    BrightTALKService brightTALKService;
    @Autowired
    EntityAttributeResolver eaResolver;
    @Autowired
    World world;
    @Autowired
    SharedChromeDriver driver;
    @Autowired
    UrlResolver urlResolver;
    @Autowired
    Pages pages;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Autowired
    BrowserAssertionSteps browserAssertionSteps;

    @Then("^the user is confirmed to attend webinar$")
    public void the_user_is_confirmed_to_attend_webinar() throws Throwable {

        pages.bloggerPostPage().verifyWebcastConfirmedPanel(driver);
    }

    @Then("^the user is confirmed to attend webinar in german language$")
    public void the_user_is_confirmed_to_attend_webinar_in_german_language() throws Throwable {

        pages.bloggerPostPage().verifyWebcastConfirmedPanelInGerman(driver);
    }

    @Then("^vote is shown on audience page$")
    public void vote_is_shown_on_audience_page() throws Throwable {

        driver.switchTo().defaultContent();
        this.switchToTabsFrame();

        String pageProp = Util.toYamlPropertyString("vote-question-audience-page");
        String locator = PropertiesUtil.getProperty(pageProp);

        // The search with text and waiting is required here as the test was failing due to the vote question not updating quick enough
        SeleniumUtils.findElement(driver, By.xpath(locator + "[contains(.,'" + world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteQuestion() + "')]"), "THE VOTE QUESTION " + world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteQuestion() + " IS NOT FOUND ON THE AUDIENCE PAGE!");

        pageProp = Util.toYamlPropertyString("question-options-audience-page");
        locator = PropertiesUtil.getProperty(pageProp);

        for(int i = 0; i < world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteOptions().size(); i++) {

            //assertTrue(answers.get(i).getText().contains(vote.getVoteOptions().get(i)), "THE VOTE OPTION " + vote.getVoteOptions().get(i) + " IS NOT FOUND ON THE PAGE, INSTEAD: " + answers.get(i).getText() + " IS FOUND!");
            // We need to use xpath here with search text as the DOM is changing dynamically and the following line fails often: List<WebElement> answers = driver.findElements(By.cssSelector(".question-options.jsQuestionOptions > li"));
            int j = i + 1;
            SeleniumUtils.findElement(driver, By.xpath(locator + "/li[" + j + "][contains(.,'" + world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteOptions().get(i) + "')]"), "THE VOTE OPTION " + world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteOptions().get(i) + " IS NOT FOUND ON THE AUDIENCE PAGE!");
        }
    }

    public void switchToTabsFrame(){

        SeleniumUtils.findElement(driver, By.xpath("//iframe[contains(@class, 'embedded-player')]"), 30, 1, false, "THE TABS FRAME IS NOT FOUND ON THE AUDIENCE PAGE !");
        driver.switchTo().frame(SeleniumUtils.findElement(driver, By.xpath("//iframe[contains(@class, 'embedded-player')]"), 90, 1, false, "THE EMBEDDED PLAYER IFRAME IS NOT FOUND ON THE AUDIENCE PAGE!"));
    }

    @Then("^vote question is shown on audience page$")
    public void vote_question_is_shown_on_audience_page() throws Throwable {

        driver.switchTo().defaultContent();
        this.switchToTabsFrame();

        String pageProp = Util.toYamlPropertyString("closed-vote-question");
        String locator = PropertiesUtil.getProperty(pageProp);

        // The search with text and waiting is required here as the test was failing due to the vote question not updating quick enough
        SeleniumUtils.findElement(driver, By.xpath(locator + "[contains(.,'" + world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteQuestion() + "')]"), "THE VOTE QUESTION " + world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteQuestion() + " IS NOT FOUND ON THE AUDIENCE PAGE!");

    }

    @Then("^vote is shown on recorded webinar audience page$")
    public void vote_is_shown_on_recorded_webinar_audience_page() throws Throwable {

        browserActionSteps.i_visit_page("audience");
        browserAssertionSteps.the_element_exists("manage-channel-page-metatag");

        switchToTabsFrame();

        browserActionSteps.i_click("on-demand-webinar-votes-tab", "", "");

        String pageProp = Util.toYamlPropertyString("on-demand-webinar-vote-question");
        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.findElement(driver, By.xpath(locator + "[contains(.,'" + world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteQuestion()+ "')]"), "");
        vote_is_shown_on_audience_page();

    }

    @Then("^vote with question (.*) is shown on audience page$")
    public void vote_with_question_is_shown_on_audience_page(String voteQuestion) throws Throwable {

        driver.switchTo().defaultContent();
        browserAssertionSteps.the_element_exists("audience-page-metatag");

        switchToTabsFrame();
        browserActionSteps.i_click("on-demand-webinar-votes-tab", "", "");
        switchToTabsFrame();

        String pageProp = Util.toYamlPropertyString("on-demand-webinar-vote-question");
        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.findElement(driver, By.xpath(locator + "[contains(.,'" + voteQuestion.trim() + "')]"), "");

    }

    @Then("^vote with question (.*) is not shown on audience page$")
    public void vote_with_question_is_not_shown_on_audience_page(String voteQuestion) throws Throwable {

        driver.switchTo().defaultContent();
        browserAssertionSteps.the_element_exists("audience-page-metatag");

        switchToTabsFrame();
        browserActionSteps.i_click("on-demand-webinar-votes-tab", "", "");
        switchToTabsFrame();

        String pageProp = Util.toYamlPropertyString("closed-vote-question");
        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.verifyElementNotPresent(driver, By.xpath(locator + "[contains(.,'" + voteQuestion + "')]"), "THE VOTE QUESTION " + voteQuestion + " IS FOUND ON THE AUDIENCE PAGE WHEN IT SHOULD NOT BE SHOWN!");

    }

    @Then("^I should see the flash player$")
    public void i_should_see_the_flash_player() throws Throwable {

        String pageProp = Util.toYamlPropertyString("flash-player");
        String locator = PropertiesUtil.getProperty(pageProp);
        String channelID = world.getLocal_channels().get(0).getChannelID();
        String webinarId = world.getWebcastsPro().get(0).getWebcastID();

        SeleniumUtils.findElement(driver, By.xpath(locator + "[@data-webcast-channel-id='" + channelID + "'][@data-webcast-id='" + webinarId + "']"), 180, 1, false, "THE FLASH PLAYER IS NOT FOUND ON THE PLAYER PAGE!");

    }

    /*
         The feature image appears on the audience page
     */
    @Then("^the feature image appears on the audience page$")
    public String featureImageAppearsOnTheAudiencePage(){

        String featureImageName = "";
        for (int i = 0; i < 15; i++){

            DateTimeFormatter.waitForSomeTime(5); // We need to wait here as even if the page loads the image is not appearing at the same moment

            try {

                String pageProp = Util.toYamlPropertyString("audience-page-feature-image");
                String featureImageXpath = PropertiesUtil.getProperty(pageProp);
                driver.findElement(By.xpath(featureImageXpath));
                featureImageName = driver.findElement(By.xpath(featureImageXpath)).getAttribute("src");
                break;

            } catch (NoSuchElementException ex) {

                try {

                    System.out.println("Waiting for one more minute for feature page to appear.. ");
                    Thread.sleep(60000);
                    this.driver.navigate().refresh();
                    try {
                        browserAssertionSteps.the_page_opens("audience-page-metatag");
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (i == 9) Assert.fail("Could not find the feature image on the Audience page...");
        }

        if (featureImageName != "") {

            int startPosition = featureImageName.indexOf("preview");
            int endPosition = featureImageName.indexOf(".png", startPosition);
            featureImageName = featureImageName.substring(startPosition, endPosition);
        }
        return featureImageName;
    }
}
