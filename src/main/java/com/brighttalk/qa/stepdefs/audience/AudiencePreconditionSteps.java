package com.brighttalk.qa.stepdefs.audience;


import com.brighttalk.qa.common.EntityAttributeResolver;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Given;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(
        classes = TestConfig.class)
public class AudiencePreconditionSteps {

    @Autowired
    BrightTALKService brightTALKService;
    @Autowired
    EntityAttributeResolver eaResolver;
    @Autowired
    World world;
    @Autowired
    SharedChromeDriver driver;
    @Autowired
    UrlResolver urlResolver;
    @Autowired
    Pages pages;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Autowired
    BrowserAssertionSteps browserAssertionSteps;

    /*
    The method selects the values on the player test page and generates embed code for the given language
    The language samples: english, german, spanish
     */
    @Given("^audience has voted$")
    public void audience_has_voted() throws Throwable {

        // open audience page
        browserActionSteps.i_visit_page("audience");
        browserAssertionSteps.the_element_exists("audience-page-metatag");

        driver.switchTo().defaultContent();
        browserAssertionSteps.switchToElement("audience-tabs-frame");

        String pageProp = Util.toYamlPropertyString("question-options-audience-page");
        String locator = PropertiesUtil.getProperty(pageProp);

        // The search with text and waiting is required here as the test was failing due to the vote question not updating quick enough
        WebElement webElement = SeleniumUtils.findElement(driver, By.xpath(locator + "/li[contains(.,'" + world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteOptions().get(0) + "')]/span"), "THE VOTE QUESTION " + world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteQuestion() + " IS NOT FOUND ON THE AUDIENCE PAGE!");
        SeleniumUtils.waitForUntil(driver, ExpectedConditions.elementToBeClickable(webElement), 2, 30, 1);
        webElement.click();

        DateTimeFormatter.waitForSomeTime(3); // its important to wait here as browser fails to send data if new page is opened too quickly

    }
}
