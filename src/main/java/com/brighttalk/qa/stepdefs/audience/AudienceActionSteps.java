package com.brighttalk.qa.stepdefs.audience;

import com.brighttalk.qa.api.AttachmentsApi;
import com.brighttalk.qa.common.EntityAttributeResolver;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.scenarios.AttachmentHandler;
import com.brighttalk.qa.scenarios.ViewingHandler;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.When;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

@ContextConfiguration(
        classes = TestConfig.class)
public class AudienceActionSteps {

    @Autowired
    BrightTALKService brightTALKService;

    @Autowired
    EntityAttributeResolver eaResolver;

    @Autowired
    World world;

    @Autowired
    SharedChromeDriver driver;

    @Autowired
    UrlResolver urlResolver;

    @Autowired
    Pages pages;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    @When("^user opens the embed page for the webinar$")
    public void user_opens_embed_page_for_webinar() throws Throwable {

        String communicationEmbedCode = world.getCommunicationEmbedCode();
        driver.get(Util.createHTMLFile(communicationEmbedCode));
    }

    @When("^I visit webinar audience page$")
    public void i_visit_webinar_audience_page() throws Throwable {

        pages.audiencePage().openPage(driver, world.getLocal_channels().get(0).getChannelID(), world.getWebcastsPro().get(0).getWebcastID());
    }

    @When("^I visit webinar audience play page$")
    public void i_visit_webinar_audience_attend_page() throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        pages.audiencePage().openAttendPage(driver, world.getLocal_channels().get(0).getChannelID(), world.getWebcastsPro().get(0).getWebcastID());
    }

    @When("^clicks the Attend button$")
    public void clicks_the_Attend_button() throws Throwable {

        pages.bloggerPostPage().switchToPlayerIframe(driver);
        pages.bloggerPostPage().clickPlayButton(driver);
    }

    @When("^enters login details$")
    public void enters_login_details() throws Throwable {

        //world.getLocal_users().get(0).setPassword("123456789");
        driver.switchTo().defaultContent();
        pages.bloggerPostPage().switchToPlayerIframe(driver);

        pages.bloggerPostPage().clickLoginButton(driver);

        driver.switchTo().defaultContent();
        pages.bloggerPostPage().switchToPlayerIframe(driver);

        pages.bloggerPostPage().enterLoginDetails(driver, world.getLocal_users().get(0));
        pages.bloggerPostPage().clickLoginButtonBelowUserDetails(driver);
    }

    @When("^I generate attachments downloads$")
    public void i_generate_attachments_download(){

        // Retrieve the viewing ID from the database (required to generate attachment downloads)
        ViewingHandler myViewingHandler = new ViewingHandler();
        String viewingID = myViewingHandler.getViewingId(world.getWebcastsPro().get(0).getWebcastID());

        // Generate attachment download
        AttachmentHandler myAttachmentHandler = new AttachmentHandler();
        ArrayList<String> myAttachmentIds = myAttachmentHandler.getAttachmentIds(world.getWebcastsPro().get(0).getWebcastID());
        try {
            AttachmentsApi.downloadAttachment(world.getRegressionTestsManagerUser(), viewingID, myAttachmentIds.get(0));
            AttachmentsApi.downloadAttachment(world.getRegressionTestsManagerUser(), viewingID, myAttachmentIds.get(1));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @When("^I switch to audience page tab frame$")
    public void switchToAudienceTabFrame(){

        String parentLocator = PropertiesUtil.getProperty(Util.toYamlPropertyString("audience-tabs-frame"));

        By parentElementToClick;
        if (parentLocator.contains("//")){

            parentElementToClick = By.xpath(parentLocator);

        } else {

            parentElementToClick = By.cssSelector(parentLocator);
        }

        driver.switchTo().defaultContent();
        DateTimeFormatter.waitForSomeTime(5);
        driver.switchTo().frame(SeleniumUtil.findElement(driver, parentElementToClick));
    }

    @When("^I select (.*) rating stars$")
    public void selectRatingStars(String numberOfStars){

        DateTimeFormatter.waitForSomeTime(2);

        switchToAudienceTabFrame();

        String pageProp = Util.toYamlPropertyString("rating-stars");
        String locator = PropertiesUtil.getProperty(pageProp);

        By numberOfStarsLocator = By.xpath(locator+ "[" + numberOfStars + "]");

        SeleniumUtils.isDisplayed(driver, numberOfStarsLocator, "");
        WebElement numberOfStartsElement = SeleniumUtils.findElement(driver, numberOfStarsLocator , "THE STARS ICON IS NOT FOUND ON THE PLAYER TEST PAGE RATINGS TAB!");

        new Actions(driver).moveToElement(numberOfStartsElement).perform();

        numberOfStartsElement.click();
        DateTimeFormatter.waitForSomeTime(2);
    }

    @When("^click \"Send Rating\" button$")
    public void clickSendRatingButton(){

        // We need to wait here for the button to be fully clickable as it takes time for it to enable after entering the text
        DateTimeFormatter.waitForSomeTime(5);

        String pageProp = Util.toYamlPropertyString("audience-page-send-rating-button");
        String locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.isElementClickable(driver, By.xpath(locator));

        WebElement element = SeleniumUtils.findElement(driver, By.xpath(locator), "THE \"SEND RATING\" BUTTON IS NOT FOUND ON THE PLAYER TEST PAGE!");

        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);

    }

    // Default text Selenium Feedback Text
    @When("^I enter rating text \"(.*)\"$")
    public void enterRatingText(String feedbackText){

        switchToAudienceTabFrame();

        browserActionSteps.i_enter_value("audience-page-rating-text", "");
        DateTimeFormatter.waitForSomeTime(2); // its important to wait as sometimes text area does not catch the new text if done too quickly

        switchToAudienceTabFrame();

        try {
            browserActionSteps.i_click("audience-page-rating-text", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        DateTimeFormatter.waitForSomeTime(2);

        switchToAudienceTabFrame();

        browserActionSteps.i_enter_value("audience-page-rating-text", feedbackText.trim());
        DateTimeFormatter.waitForSomeTime(10); // its important to wait as sometimes text area does not catch the new text if done too quickly
    }

    @When("^I drag and drop$")
    public void dragndrop(){

        String startPosition = "//div[contains(@data-reactid, '273')]";
        String endPosition = "//div[contains(@data-reactid, '226')]";

        WebElement startElement = driver.findElement(By.xpath(startPosition));
        WebElement endElement = driver.findElement(By.xpath(endPosition));

        Actions builder = new Actions(driver);

        // PREPARATION: This is to prepare the page to scroll down if element not visible so the position is get correctly by Robot class
        Actions dragAndDrop = builder.clickAndHold(startElement);
        dragAndDrop.perform();
        //Actions dragAndDrop1 = builder.moveToElement(endElement);
        //dragAndDrop1.perform();
        //DateTimeFormatter.waitForSomeTime(2);

        // PREPARATION: Move physical mouse
        Robot robot = null;
        try {
            robot = new Robot();
        } catch (AWTException e) {
            e.printStackTrace();
        }

        endElement.getLocation();
        int xcoordinate = endElement.getLocation().getX();
        xcoordinate = xcoordinate + 50;

        int ycoordinate = endElement.getLocation().getY();
        ycoordinate = ycoordinate + 100;

        robot.mouseMove(xcoordinate ,ycoordinate);
        DateTimeFormatter.waitForSomeTime(5);

        // REAL ACTION: CLICK and HOLD element
        builder.clickAndHold(startElement).perform();

        // REAL ACTION: move to element
        builder.moveToElement(endElement).perform();

        // REAL ACTION: release
        builder.release(endElement).perform();

        DateTimeFormatter.waitForSomeTime(5);

    }
}
