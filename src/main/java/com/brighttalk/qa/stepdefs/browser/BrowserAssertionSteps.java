package com.brighttalk.qa.stepdefs.browser;

import com.brighttalk.qa.common.EntityAttributeResolver;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.stepdefs.common.dto.Field;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import com.google.common.base.Function;
import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

@ContextConfiguration(
        classes = TestConfig.class)
public class BrowserAssertionSteps {

    @Autowired
    SharedChromeDriver driver;
    @Autowired
    EntityAttributeResolver eaResolver;
    @Autowired
    UrlResolver urlResolver;

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    @Then("^I should be on the \"([^\"]*)\" page$")
    public void i_should_be_on_the_page(String page) throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();
        String url = "";
        if (page.contains(" ")) {
            String pageProp = Util.toYamlPropertyString(page);
            url = PropertiesUtil.getProperty(pageProp);            
        } else {
            url = Util.toYamlPropertyString(page);
        }

        //url = urlResolver.resolve(url);

        String currentUrl = driver.getCurrentUrl();

        assertTrue(currentUrl.contains(url));
    }

    @Then("^I should see \"([^\"]*)\" is \"([^\"]*)\"$")
    public void i_should_see_element_checked(String element, String checked) throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String pageProp = Util.toYamlPropertyString(element);
        String locator = PropertiesUtil.getProperty(pageProp);

        By elementBy;

        if(locator.contains("//")){

            elementBy = By.xpath(locator);

        } else {

            elementBy = By.cssSelector(locator);
        }

        if (checked.equalsIgnoreCase("checked") | checked.equalsIgnoreCase("selected")) {
            assertTrue(driver.findElement(elementBy).isSelected());           
        } else {
            assertFalse(driver.findElement(elementBy).isSelected()); 
        }

    }

    @Then("^I should see (\\d+|a|an|the) \"([^\"]*)\"(?: (?:in the \"([^\"]*)\"(?: (iframe|element))?))?$")
    public void i_should_see_element(String count, String element, String parentElement, String parentType) throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        WebElement wElement;

        if (StringUtils.hasLength(parentElement)) {
            String parentLocator = PropertiesUtil.getProperty(Util.toYamlPropertyString(parentElement));
            wElement = SeleniumUtil.findElement(driver, By.cssSelector(parentLocator));

            if (parentType.equalsIgnoreCase("iframe")) {
                driver.switchTo().frame(wElement);
            }
        }

        wElement = SeleniumUtil.findElement(driver, By.tagName("body"));

        String pageProp = Util.toYamlPropertyString(element);
        String locator = PropertiesUtil.getProperty(pageProp);

        By elementBy;

        if(locator.contains("//")){

            elementBy = By.xpath(locator);

        } else {

            elementBy = By.cssSelector(locator);
        }

        if (count.equalsIgnoreCase("a") | count.equalsIgnoreCase("the")) {

            //assertTrue(wElement.findElement(elementBy).isDisplayed());
            WebDriverWait wait = new WebDriverWait(driver, 15);
            wait.until(ExpectedConditions.visibilityOfElementLocated(elementBy));


        } else {

            int eCount = Integer.valueOf(count);
            assertEquals(eCount, wElement.findElements(elementBy).size());
        }

        driver.switchTo().defaultContent();
    }

    @Then("^I should see (?:a|the) \"([^\"]*)\" with text matching \"([^\"]*)\"$")
    public void i_should_see_message(String element, String text) {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String pageProp = Util.toYamlPropertyString(element);
        String locator = PropertiesUtil.getProperty(pageProp);

        By messageElementLocator;

        if(locator.contains("//")){

            messageElementLocator = By.xpath(locator);

        } else {

            messageElementLocator = By.cssSelector(locator);
        }

        new WebDriverWait(driver, 120)
                .until(ExpectedConditions.visibilityOfElementLocated(messageElementLocator));

        assertTrue(SeleniumUtil.isDisplayed(driver, messageElementLocator));

        // Verify the text is displayed as expected
        By messageElementLocatorText;

        if (StringUtils.hasLength(text)) {

            if (locator.contains("//")) {

                messageElementLocatorText = By.xpath(locator + "[contains(.,'" + text.trim() + "')]");
                SeleniumUtils.findElement(driver, messageElementLocatorText, 2, 180, false, "THE MESSAGE TEXT " + text + "IS NOT FOUND THE PAGE.");

            } else {

                assertTrue(SeleniumUtil.getElementText(driver, messageElementLocator).contains(text));
            }
        }
    }

    @Then("^I should see \"([^\"]*)\" with data \"([^\"]*)\" show up$")
    public void i_should_see_data_show_up(String element, String text) {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String pageProp = Util.toYamlPropertyString(element);
        String locator = PropertiesUtil.getProperty(pageProp);

        By messageElementLocator;

        if(locator.contains("//")){ 

            messageElementLocator = By.xpath(locator);

        } else {

            messageElementLocator = By.cssSelector(locator);
        }


        SeleniumUtils.verifyElementShowsUpWithRefresh(driver, messageElementLocator, 60, 5);
        //assertTrue(SeleniumUtil.isDisplayed(driver, messageElementLocator));
        assertTrue(SeleniumUtil.getElementText(driver, messageElementLocator).contains(text));
        
    }    


    @Then("^I should see (?:a|the) \"([^\"]*)\" field empty$")
    public void i_should_see_field_empty(String element) {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String pageProp = Util.toYamlPropertyString(element);
        String locator = PropertiesUtil.getProperty(pageProp);

        By messageElementLocator;

        if(locator.contains("//")){

            messageElementLocator = By.xpath(locator);

        } else {

            messageElementLocator = By.cssSelector(locator);
        }

        new WebDriverWait(driver, 30)
                .until(ExpectedConditions.visibilityOfElementLocated(messageElementLocator));

        assertTrue(SeleniumUtil.isDisplayed(driver, messageElementLocator));

        // Verify the text is displayed as expected

         String elementText = SeleniumUtils.findElement(driver, messageElementLocator, 2, 30, false, "THE ELEMENT " + messageElementLocator + "IS NOT FOUND THE PAGE.").getText();
         assertTrue(elementText.trim().equals(""));

    }

    @Then("^I should see the following fields(?: (?:in the \"([^\"]*)\"(?: (iframe|element))))?$")
    public void i_should_see_elements(String parentElement, String parentType, List<Field> table) throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        WebElement element;

        if (StringUtils.hasLength(parentElement)) {
            String parentLocator = PropertiesUtil.getProperty(Util.toYamlPropertyString(parentElement));
            element = SeleniumUtil.findElement(driver, By.cssSelector(parentLocator));

            if (parentType.equalsIgnoreCase("iframe")) {
                driver.switchTo().frame(element);
            }
        }

        element = SeleniumUtil.findElement(driver, By.tagName("body"));

        for (Field ff : table) {
            String pageProp = Util.toYamlPropertyString(ff.getField());
            String locator = PropertiesUtil.getProperty(pageProp);

            assertTrue(element.findElement(By.cssSelector(locator)).isDisplayed());
        }

        driver.switchTo().defaultContent();
    }

    @Then("^I should see the following rows in \"([^\"]*)\"$")
    public void i_should_see_table(String tableElement, DataTable table) {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        WebElement tableWebElement;
        List<WebElement> tableRowsWebElement;
        String locator;

        locator = PropertiesUtil.getProperty(Util.toYamlPropertyString(tableElement));
        tableWebElement = SeleniumUtil.findElement(driver, By.cssSelector(locator));

        assertTrue(tableWebElement.isDisplayed());

        List<Map<String, String>> tableRaw = table.asMaps(String.class, String.class);

        locator += " > tbody > tr";
        tableRowsWebElement = tableWebElement.findElements(By.cssSelector(locator));
        assertTrue(tableRaw.size() == tableRowsWebElement.size());

        for (int i = 0; i > tableRaw.size(); i++) {
            assertTrue(tableRowsWebElement.get(i).isDisplayed());

            for (Map.Entry<String, String> entry : tableRaw.get(i).entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();

                locator = PropertiesUtil.getProperty(Util.toYamlPropertyString(key));
                String actual = tableRowsWebElement.get(i).findElement(By.cssSelector(locator)).getText();

                Matcher m = Pattern.compile("\\{.*\\}").matcher(value);

                String expected = m.find() ? eaResolver.resolve(value) : value;

                assertTrue(actual.contains(expected));
            }
        }
    }

    @Then("^the \"([^\\\"]*)\" opens$")
    public void the_page_opens(String page) throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String pageProp = Util.toYamlPropertyString(page);
        String locator = PropertiesUtil.getProperty(pageProp);

        By messageElementLocator;

        if(locator.contains("//")){

            messageElementLocator = By.xpath(locator);

        } else {

            messageElementLocator = By.cssSelector(locator);
        }

        WebDriverWait wait = new WebDriverWait(driver, 30);

        wait.until(ExpectedConditions.presenceOfElementLocated(messageElementLocator));

    }

    @Then("^the element \"([^\\\"]*)\" is present")
    public void the_element_exists(String element){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String pageProp = Util.toYamlPropertyString(element);
        String locator = PropertiesUtil.getProperty(pageProp);

        By elementLocator;

        if(locator.contains("//")){

            elementLocator = By.xpath(locator);

        } else {

            elementLocator = By.cssSelector(locator);
        }

        SeleniumUtils.findElement(driver, elementLocator, 120, 1, false, "The " + element + " is not found on the page!");

        if (element.contains("audience-page-metatag")){

            DateTimeFormatter.waitForSomeTime(5); // Wait for the jwicon container to load, no way to check it
        }
    }

    @Then("^the element \"([^\\\"]*)\" appears in \"([^\\\"]*)\" secs")
    public void the_element_exists(String element, String secs){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String pageProp = Util.toYamlPropertyString(element);
        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.findElement(driver, By.xpath(locator), Integer.parseInt(secs), 1, false, "The " + element + " is not found on the page!");

        if (element.contains("audience-page-metatag")){

            DateTimeFormatter.waitForSomeTime(5); // Wait for the jwicon container to load, no way to check it
        }

    }

    public void switchToElement(String element){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String pageProp = Util.toYamlPropertyString(element);
        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.findElement(driver, By.xpath(locator), "The " + element + " is not found on the page!");
        driver.switchTo().frame(SeleniumUtils.findElement(driver, By.xpath(locator), 90, 1, false,  locator + " iframe is not found on the page!"));
    }

    @Then("^the \"(.*)\" is not displayed on the page$")
    public void elementIsNotDisplayed(String element){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String pageProp = Util.toYamlPropertyString(element);
        String locator = PropertiesUtil.getProperty(pageProp);

        By elementBy;
        if (locator.contains("//")) {

             elementBy = By.xpath(locator);

        } else

            elementBy = By.cssSelector(locator);

        Assert.assertFalse(SeleniumUtils.isDisplayed(driver, elementBy, ""));

    }

    @Then("^the \"(.*)\" is not present$")
    public void elementDoesNotExist(String element){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String pageProp = Util.toYamlPropertyString(element);
        String locator = PropertiesUtil.getProperty(pageProp);

        By elementBy;
        if (locator.contains("//")) {

            elementBy = By.xpath(locator);

        } else

            elementBy = By.cssSelector(locator);

        try {

            driver.findElement(elementBy);
            Assert.fail("Element <" + locator + "> is present, when it should not be on the page.");

        } catch (NoSuchElementException ex) {
        /* do nothing, link is not present, assert is passed */
        }
    }

    @Then("^the element \"(.*)\" disappears from page$")
    public void elementDisappearsFromPage(String element){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String pageProp = Util.toYamlPropertyString(element);
        String locator = PropertiesUtil.getProperty(pageProp);

        By elementBy;
        if (locator.contains("//")) {

            elementBy = By.xpath(locator);

        } else

            elementBy = By.cssSelector(locator);


        SeleniumUtils.verifyElementDisappearsFromPageWithoutRefresh(driver, elementBy, 60, 2, "THE ELEMENT " + locator + " DID NOT DISAPPEAR FROM PAGE AS EXPECTED!");
    }

    @Then("^temporal step$")
    public void temporal_step(){

      // driver.deleteAllCookies();
       System.out.println("TEST");
       System.out.println("TEST");
    }

    @Then(("^the page is loaded$"))
    public void waitForPageLoad() {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        Wait<WebDriver> wait = new WebDriverWait(driver, 30);
        wait.until(new Function<WebDriver, Boolean>() {
            public Boolean apply(WebDriver driver) {
                System.out.println("Current Window State       : "
                        + String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState")));
                return String
                        .valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
                        .equals("complete");
            }
        });
    }

}
