package com.brighttalk.qa.stepdefs.browser;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.SharedFirefoxDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.scenarios.UserHandler;
import com.brighttalk.qa.siteElements.User;
import com.brighttalk.qa.stepdefs.common.dto.FieldDetail;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.When;
import junit.framework.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

@ContextConfiguration(
        classes = TestConfig.class)
public class BrowserActionSteps {

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    @Autowired
    SharedChromeDriver sharedChromeDriver;

    @Autowired
    SharedFirefoxDriver sharedFirefoxDriver;

    @Autowired
    UrlResolver urlResolver;

    @Autowired
    BrightTALKService service;

    @Autowired
    World world;

    @Autowired
    Pages pages;

    @When("^I visit(?: the)? \"([^\"]*)\"(?: page)?$")
    public void i_visit_page(String page) throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        //String envProp = "brighttalk-home";

        String envProp = page.toLowerCase().contains("demand central") ? "demandcentral-home" : "brighttalk-home";
        String pageProp = Util.toYamlPropertyString(page);


        String url;

        if (!(PropertiesUtil.getProperty(pageProp).contains("https") || PropertiesUtil.getProperty(pageProp).contains("http"))) {

            url = PropertiesUtil.getProperty(envProp) + PropertiesUtil.getProperty(pageProp);

        } else {

            url = PropertiesUtil.getProperty(pageProp);
        }

        url = urlResolver.resolve(url);

        SeleniumUtil.loadPage(driver, url);

        if (url.contains("/presenter")){

            DateTimeFormatter.waitForSomeTime(35);

            // This code does not work when pro webinar is less than 15 mins to live, therefore has been replaced with waits only

            // We need to wait second here, as the loading pop up does not appear immediatelly when page opens, it takes about half of a second
            // therefore checking it immediatelly may return wrong result
            //DateTimeFormatter.waitForSomeTime(1);

            //pageProp = Util.toYamlPropertyString("presenter-screen-loading-overlay-disappeared");
            //String locator = PropertiesUtil.getProperty(pageProp);

            //SeleniumUtils.findElement(driver, By.xpath(locator), 180, 1, false, "THE PRESENTER SCREEN LOADING POP UP DID NOT DISAPPEAR AFTER 3 MINUTES");
        }

    }

    @When("^I click(?: on)?(?: the)? \"([^\"]*)\"(?: (?:in the \"([^\"]*)\"(?: (iframe|element))?))?$")
    public void i_click(String element, String parentElement, String parentType) throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        WebElement wElement;

        if (StringUtils.hasLength(parentElement)) {

            String parentLocator = PropertiesUtil.getProperty(Util.toYamlPropertyString(parentElement));

            By parentElementToClick;
            if(parentLocator.contains("//")){

                parentElementToClick = By.xpath(parentLocator);

            } else {

                parentElementToClick = By.cssSelector(parentLocator);
            }
            wElement = SeleniumUtil.findElement(driver, parentElementToClick);

            if (parentType.equalsIgnoreCase("iframe")) {

                driver.switchTo().frame(wElement);
            }
        }

        wElement = SeleniumUtil.findElement(driver, By.tagName("body"));

        String pageProp = Util.toYamlPropertyString(element);
        String locator = PropertiesUtil.getProperty(pageProp);

        By elementToClick;

        // Verifies if the locator is xpath or css
        if(locator.contains("//")){

            elementToClick = By.xpath(locator);

        } else {

            elementToClick = By.cssSelector(locator);
        }

        DateTimeFormatter.waitForSomeTime(1); // We need to wait for some of the buttons, as the click is not performed

        // For some locators its important to wait for element to be clickable
        SeleniumUtils.waitForUntil(driver, ExpectedConditions.elementToBeClickable(elementToClick), 2, 120, 1);
        SeleniumUtils.waitForUntil(driver, ExpectedConditions.visibilityOfElementLocated(elementToClick), 2, 120, 1);

        // Generate click on the Login button
        WebElement webElement= SeleniumUtils.findElement(driver, elementToClick, "THE LOGIN BUTTON IS NOT FOUND !");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", webElement);
        //SeleniumUtils.findElement(driver, elementToClick, "").click();

        driver.switchTo().defaultContent();

        DateTimeFormatter.waitForSomeTime(3);
    }

    @When("^I fill in the following(?: form)? details(?: (?:in the \"([^\"]*)\"(?: (iframe|element))))?$")
    public void i_fill_in(String parentElement, String parentType, List<FieldDetail> table) throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        DateTimeFormatter.waitForSomeTime(4);  // its important to wait here, due to the stale element reference: element is not attached to the page document

        WebElement element;
        User newUser = new User();
        newUser.setRegistered(false);
        world.getLocal_users().add(newUser);

        if (StringUtils.hasLength(parentElement)) {

            String parentLocator = PropertiesUtil.getProperty(Util.toYamlPropertyString(parentElement));

            new WebDriverWait(driver, 30)
                    .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(parentLocator)));

            element = SeleniumUtil.findElement(driver, By.cssSelector(parentLocator));

            if (parentType.equalsIgnoreCase("iframe")) {

                driver.switchTo().frame(element);
            }
        }

        element = SeleniumUtil.findElement(driver, By.tagName("body"));

        for (FieldDetail ffv : table) {

            String pageProp = Util.toYamlPropertyString(ffv.getField());
            String locator = PropertiesUtil.getProperty(pageProp);

            SeleniumUtils.waitForUntil(driver, ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locator)), 2, 120, 1);
            WebElement innerElement = element.findElement(By.cssSelector(locator));

            if (innerElement.getTagName().equalsIgnoreCase("select")) {

                SeleniumUtils.waitForUntil(driver, ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locator)), 2, 120, 1);
                new Select(innerElement).selectByValue(ffv.getValue());

            } else {

                if (innerElement.getAttribute("class").contains("Date")) {

                    // Recalculate date
                    Calendar webinarStartDate = Calendar.getInstance();

                    // Add the number of minutes specified to the current time
                    webinarStartDate.add(Calendar.MINUTE, Integer.parseInt(ffv.getValue()));

                    // Get start date in format required in the booking form
                    String startDate = Util.getLongFormDateStringNoTime(webinarStartDate);

                    // Clear the default value on the form
                    innerElement.clear();

                    // Enter date into the form field
                    innerElement.sendKeys(startDate);

                } else {

                    if (ffv.getValue().contains("random")){

                        String userEmail = "SeleniumUser"+System.currentTimeMillis()+"@brighttalk-"+ System.currentTimeMillis()+ ".com";
                        innerElement.sendKeys(userEmail);
                        newUser.setEmail(userEmail);

                    } else

                        try {

                            innerElement.sendKeys(ffv.getValue());
                         }
                            catch(StaleElementReferenceException ex)
                        {

                            System.out.println("test");
                            DateTimeFormatter.waitForSomeTime(2);
                            innerElement.sendKeys(ffv.getValue());
                        }
                }

                // if the field is Tags, then we need to click the "Add TAG" button
                try {

                    innerElement.getAttribute("id");
                    if (innerElement.getAttribute("id").contains("edit-keywords")) {

                        i_click("pro-webinar-booking-form-add-tags-button", "","");

                    }
                }
                catch(StaleElementReferenceException ex)
                {
                    DateTimeFormatter.waitForSomeTime(2);
                    innerElement.getAttribute("id");
                    if (innerElement.getAttribute("id").contains("edit-keywords")) {

                        i_click("pro-webinar-booking-form-add-tags-button", "","");

                    }
                }
            }
        }

        driver.switchTo().defaultContent();
    }

    @When("^I visit(?: the)? \"([^\"]*)\"(?: page)? for webinar (.*)$")
    public void i_visit_page(String page, String webinarID) throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String envProp = "brighttalk-home";
        String pageProp = Util.toYamlPropertyString(page);

        String url;

        if (!PropertiesUtil.getProperty(pageProp).contains("https")) {

            url = PropertiesUtil.getProperty(envProp) + PropertiesUtil.getProperty(pageProp);

        } else {

            url = PropertiesUtil.getProperty(pageProp);
        }

        urlResolver.add("{comm_id}", webinarID);
        url = urlResolver.resolve(url);

        SeleniumUtil.loadPage(driver, url);

        if (url.contains("/presenter")){

            // We need to wait second here, as the loading pop up does not appear immediatelly when page opens, it takes about half of a second
            // therefore checking it immediatelly may return wrong result
            DateTimeFormatter.waitForSomeTime(1);

            pageProp = Util.toYamlPropertyString("presenter-screen-loading-overlay-disappeared");
            String locator = PropertiesUtil.getProperty(pageProp);

            SeleniumUtils.findElement(driver, By.xpath(locator), 180, 1, false, "THE PRESENTER SCREEN LOADING POP UP DID NOT DISAPPEAR AFTER 3 MINUTES");
        }
    }

    @When("^I visit(?: the)? \"([^\"]*)\"(?: page)? for pro webinar (.*) in channel (.*)$")
    public void i_visit_page_for_webinar_in_channel(String page, String webinarID, String channelID) throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String envProp = "brighttalk-home";
        String pageProp = Util.toYamlPropertyString(page);

        String url;

        if (!PropertiesUtil.getProperty(pageProp).contains("https")) {

            url = PropertiesUtil.getProperty(envProp) + PropertiesUtil.getProperty(pageProp);

        } else {

            url = PropertiesUtil.getProperty(pageProp);
        }

        urlResolver.add("{comm_id}", webinarID);
        urlResolver.add("{channel_id}", channelID);

        url = urlResolver.resolve(url);

        SeleniumUtil.loadPage(driver, url);

        if (url.contains("/presenter")){

            // We need to wait second here, as the loading pop up does not appear immediatelly when page opens, it takes about half of a second
            // therefore checking it immediatelly may return wrong result
            DateTimeFormatter.waitForSomeTime(1);

            pageProp = Util.toYamlPropertyString("presenter-screen-loading-overlay-disappeared");
            String locator = PropertiesUtil.getProperty(pageProp);

            SeleniumUtils.findElement(driver, By.xpath(locator), 180, 1, false, "THE PRESENTER SCREEN LOADING POP UP DID NOT DISAPPEAR AFTER 3 MINUTES");
        }
    }

    @When("I wait for (.*) secs$")
    public void i_wait_for(String secs) throws Throwable {

       DateTimeFormatter.waitForSomeTime(Integer.parseInt(secs));
    }

    @When("for element \"(.*)\" I enter value \"(.*)\"$")
    public void i_enter_value(String elementName, String elementValue){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();
        DateTimeFormatter.waitForSomeTime(3); // we need to wait for the form to settle

        String pageProp = Util.toYamlPropertyString(elementName);
        String locator = PropertiesUtil.getProperty(pageProp);

        By elementToClick;

        if(locator.contains("//")){

            try {

                elementToClick = By.xpath(locator);

                }

            catch(StaleElementReferenceException ex)
            {

                elementToClick = By.xpath(locator);
            }

        } else {

            try {

                elementToClick = By.cssSelector(locator);
            }

            catch(StaleElementReferenceException ex)

            {
                elementToClick = By.cssSelector(locator);
            }
        }

        SeleniumUtils.waitForUntil(driver, ExpectedConditions.elementToBeClickable(elementToClick), 2, 120, 1);
        SeleniumUtils.waitForUntil(driver, ExpectedConditions.visibilityOfElementLocated(elementToClick), 2, 120, 1);
        SeleniumUtils.findElement(driver, elementToClick, "").clear();

        try {

            driver.findElement(elementToClick).sendKeys(elementValue);
        }

        catch(StaleElementReferenceException ex)

        {
            driver.findElement(elementToClick).sendKeys(elementValue);
        }
    }

    @When("for element \"(.*)\" I select value \"(.*)\"$")
    public void i_select_value(String elementName, String elementValue){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String pageProp = Util.toYamlPropertyString(elementName);
        String locator = PropertiesUtil.getProperty(pageProp);

        By elementToClick;

        if(locator.contains("//")){

            elementToClick = By.xpath(locator);

        } else {

            elementToClick = By.cssSelector(locator);
        }

        //Dimension
        Dimension defaultDimension = driver.manage().window().getSize();
        Dimension newDimension = new Dimension(1000, 1000);
        //driver.manage().window().setSize(newDimension);
        DateTimeFormatter.waitForSomeTime(3);

        Select elementTimezone = new Select(driver.findElement(elementToClick));
        elementTimezone.selectByValue(elementValue);
        //driver.manage().window().setSize(defaultDimension);
        DateTimeFormatter.waitForSomeTime(3);
    }

    @When("for element \"(.*)\" I select text \"(.*)\"$")
    public void i_select_by_visible_text(String elementName, String elementValue){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String pageProp = Util.toYamlPropertyString(elementName);
        String locator = PropertiesUtil.getProperty(pageProp);

        By elementToClick;

        if(locator.contains("//")){

            elementToClick = By.xpath(locator);

        } else {

            elementToClick = By.cssSelector(locator);
        }

        Select elementTimezone = new Select(driver.findElement(elementToClick));
        elementTimezone.selectByVisibleText(elementValue);
    }


    @When("^I open new browser window (.*)$")
    public void i_open_browser_window(String browserWindowNumber) throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("window.open()");

        Set<String> handles = driver.getWindowHandles();
        ArrayList<String> handlesList = new ArrayList<>();
        handlesList.addAll(handles);

        String handle = handlesList.get(handlesList.size() - 1);
        driver.switchTo().window(handle);

        world.getWindowHandles().add(handle);
        i_visit_page("view-booking-page");

    }

    @When("^I activate browser window (.*)$")
    public void i_activate_browser_window (String browserWindowNumber) throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        driver.switchTo().window(world.getWindowHandles().get(Integer.parseInt(browserWindowNumber.trim()) - 1));
        DateTimeFormatter.waitForSomeTime(10);

    }

    @When("^I switch to the new browser window$")
    public void i_switch_a_new_browser_window() throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }

        DateTimeFormatter.waitForSomeTime(10);

    }

    @When("^I refresh the page$")
    public void i_refresh_the_page() throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        driver.navigate().refresh();

    }

    @When("^I switch to a new tab$")
    public void i_switch_to_a_new_tab() throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        Set<String> windowHandles = driver.getWindowHandles();

        if (windowHandles.size() < 2)
            Assert.fail("NEW TAB IS NOT OPENED FOR LINK ATTACHMENT ON THE PLAYER PAGE!"); // get all windows

        for (String windowHandle : windowHandles) {

            if (!windowHandle.equals(world.getWindowHandles().get(0))) {

                driver.switchTo().window(windowHandle);
                break;
            }
        }
    }

    @When("^I accept alert message$")
    public void acceptTheLessThank15MinsAlert(){

        try {

            WebDriver driver = browserPreconditionSteps.defineWebDriver();

            WebDriverWait wait = new WebDriverWait(driver, 10);
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = driver.switchTo().alert();
            alert.accept();

        } catch (Exception e) {System.err.println("FAILED ON: Accepting the LESS THAN 15 MINS alert on Edit Booking page: " + e.getMessage());}
    }

    @When("^I switch to \"(.*)\" frame$")
    public void switchToFrame(String frame){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String parentLocator = PropertiesUtil.getProperty(Util.toYamlPropertyString(frame.trim()));

        By parentElementToClick;
        if (parentLocator.contains("//")){

            parentElementToClick = By.xpath(parentLocator);

        } else {

            parentElementToClick = By.cssSelector(parentLocator);
        }

        driver.switchTo().defaultContent();
        DateTimeFormatter.waitForSomeTime(6);
        driver.switchTo().frame(SeleniumUtil.findElement(driver, parentElementToClick));
    }

    @When("^I delete all cookies$")
    public void deleteAllCookies(){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        driver.manage().deleteAllCookies();

    }

    @When("^I login new user$")
    public void i_create_new_user() {

        UserHandler myUserHandler = new UserHandler();
        User user = myUserHandler.createUser();

        user.setRegistered(true);
        world.getLocal_users().add(user);

        i_enter_value("login-page-email", user.getEmail());
        i_enter_value("login-page-password", user.getPassword());

        try {
            i_click("login-page-login-button", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @When("^I click2(?: on)?(?: the)? \"([^\"]*)\"(?: (?:in the \"([^\"]*)\"(?: (iframe|element))?))?$")
    public void i_click2(String element, String parentElement, String parentType) throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        WebElement wElement;

        if (StringUtils.hasLength(parentElement)) {

            String parentLocator = PropertiesUtil.getProperty(Util.toYamlPropertyString(parentElement));

            By parentElementToClick;
            if(parentLocator.contains("//")){

                parentElementToClick = By.xpath(parentLocator);

            } else {

                parentElementToClick = By.cssSelector(parentLocator);
            }
            wElement = SeleniumUtil.findElement(driver, parentElementToClick);

            if (parentType.equalsIgnoreCase("iframe")) {

                driver.switchTo().frame(wElement);
            }
        }

        wElement = SeleniumUtil.findElement(driver, By.tagName("body"));

        String pageProp = Util.toYamlPropertyString(element);
        String locator = PropertiesUtil.getProperty(pageProp);

        By elementToClick;

        // Verifies if the locator is xpath or css
        if(locator.contains("//")){

            elementToClick = By.xpath(locator);

        } else {

            elementToClick = By.cssSelector(locator);
        }

        DateTimeFormatter.waitForSomeTime(1); // We need to wait for some of the buttons, as the click is not performed

        // For some locators its important to wait for element to be clickable
        SeleniumUtils.waitForUntil(driver, ExpectedConditions.elementToBeClickable(elementToClick), 2, 120, 1);
        SeleniumUtils.waitForUntil(driver, ExpectedConditions.visibilityOfElementLocated(elementToClick), 2, 120, 1);

        // Generate click on the Login button
        WebElement webElement= SeleniumUtils.findElement(driver, elementToClick, "THE LOGIN BUTTON IS NOT FOUND !");
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", webElement);
        //SeleniumUtils.findElement(driver, elementToClick, "").click();


        DateTimeFormatter.waitForSomeTime(3);
    }
}
