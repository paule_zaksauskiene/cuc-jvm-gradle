package com.brighttalk.qa.stepdefs.browser;


import com.brighttalk.qa.common.*;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.scenarios.UserHandler;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtil;
import cucumber.api.java.en.Given;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(
        classes = TestConfig.class)
public class BrowserPreconditionSteps {

    @Autowired
    SharedFirefoxDriver sharedFirefoxDriver;

    @Autowired
    SharedChromeDriver sharedChromeDriver;

    @Autowired
    UrlResolver urlResolver;

    @Autowired
    BrightTALKService brightTALKService;

    @Autowired
    World world;

    public WebDriver defineWebDriver(){

        if (world.getBrowserUsed().contains("firefox")){

            return sharedFirefoxDriver;
        } else {
            return sharedChromeDriver;
        }
    }

    @Given("^I am logged in as BrightTALK manager$")
    public void i_am_logged_in_as_BrightTALK_manager() {

        WebDriver driver = defineWebDriver();

        com.brighttalk.qa.siteElements.User managerUser;
        UserHandler myUserHandler = new UserHandler();
        managerUser = world.getRegressionTestsManagerUser();
        String sessionCookie = myUserHandler.loginUserAPI(managerUser);
        managerUser.setLoggedin(true);
        managerUser.setRegistered(true);

        String cookie =  sessionCookie;
        String replaceCookie = cookie.replaceAll(";", "");
        cookie = replaceCookie.replace("BTSESSION=", "");

        SeleniumUtil.loadPage(driver, urlResolver.resolve(PropertiesUtil.getProperty("brighttalk-home")));

        DateTimeFormatter.waitForSomeTime(5); //It is important to wait here as browser can not proceed cookie data so quickly

        System.out.println("cookie: " + cookie);
        driver.manage().addCookie(new Cookie("BTSESSION", cookie));

        DateTimeFormatter.waitForSomeTime(5); //It is important to wait here as browser can not proceed cookie data so quickly
    }


    @Given("^I am (?:authenticated|logged in)$")
    public void i_am_authenticated() {

        WebDriver driver = defineWebDriver();

        com.brighttalk.qa.siteElements.User managerUser;
        UserHandler myUserHandler = new UserHandler();
        managerUser = myUserHandler.createManageOpsUser();
        world.getLocal_users().add(managerUser);
        String sessionCookie = myUserHandler.loginUserAPI(managerUser);
        world.getLocal_users().get(0).setSessionCookie(myUserHandler.loginUserAPI(managerUser));
        managerUser.setLoggedin(true);
        managerUser.setRegistered(true);

        String cookie =  world.getLocal_users().get(0).getSessionCookie();
        String replaceCookie = cookie.replaceAll(";", "");
        cookie = replaceCookie.replace("BTSESSION=", "");

        SeleniumUtil.loadPage(driver, urlResolver.resolve(PropertiesUtil.getProperty("brighttalk-home")));

        DateTimeFormatter.waitForSomeTime(5); //It is important to wait here as browser can not proceed cookie data so quickly

        System.out.println("cookie: " + cookie);
        driver.manage().addCookie(new Cookie("BTSESSION", cookie));

        DateTimeFormatter.waitForSomeTime(5); //It is important to wait here as browser can not proceed cookie data so quickly
    }

    @Given("^I am (?:authenticated|logged in) as manager$")
    public void i_am_authenticated_as_manager() {

        System.out.println("test");
        i_am_authenticated();
    }

    @Given("^I am (?:authenticated|logged in) as channel owner")
    public void i_am_authenticated_as_channel_owner() {

        com.brighttalk.qa.siteElements.User channelOwner;
        UserHandler myUserHandler = new UserHandler();
        channelOwner = myUserHandler.createUser();
        world.getLocal_users().add(channelOwner);
        world.getLocal_users().get(0).setSessionCookie(myUserHandler.loginUserAPI(channelOwner));
        channelOwner.setLoggedin(true);
        channelOwner.setRegistered(true);

        String cookie =  world.getLocal_users().get(0).getSessionCookie();
        String replaceCookie = cookie.replaceAll(";", "");
        cookie = replaceCookie.replace("BTSESSION=", "");

        WebDriver driver = defineWebDriver();
        SeleniumUtil.loadPage(driver, urlResolver.resolve(PropertiesUtil.getProperty("brighttalk-home")));
        driver.manage().addCookie(new Cookie("BTSESSION", cookie));
    }

    @Given("^I am (?:authenticated|logged in) as new user")
    public void i_am_authenticated_as_new_user() {

        com.brighttalk.qa.siteElements.User newUser;

        UserHandler myUserHandler = new UserHandler();

        newUser = myUserHandler.createUser();

        world.getLocal_users().add(newUser);
        world.getLocal_users().get(world.getLocal_users().size() - 1).setSessionCookie(myUserHandler.loginUserAPI(newUser));

        newUser.setLoggedin(true);
        newUser.setRegistered(true);

        String cookie = world.getLocal_users().get(world.getLocal_users().size() - 1).getSessionCookie();
        String replaceCookie = cookie.replaceAll(";", "");
        cookie = replaceCookie.replace("BTSESSION=", "");

        WebDriver driver = defineWebDriver();
        SeleniumUtil.loadPage(driver, urlResolver.resolve(PropertiesUtil.getProperty("brighttalk-home")));
        driver.manage().addCookie(new Cookie("BTSESSION", cookie));

        DateTimeFormatter.waitForSomeTime(5); //It is important to wait here as browser can not proceed cookie data so quickly
    }

    @Given("^new user containing specific characters is logged in")
    public void user_specific_chars_logged_in() {

        com.brighttalk.qa.siteElements.User newUser;

        UserHandler myUserHandler = new UserHandler();

        newUser = myUserHandler.createUser("èàùéâêîôûëïçäöüß€áéíñóúü¿¡思想的指導者の毎日何千人も積極생각", "매일수천àÀáÁãÃçÇéÉêÊÍíóÓúÚüÜёъяшертыуиопющэасдфгчйкл",  "ьжзхцвбнм每天都有思想領袖正在積極分享他們的見解!@£$%^&*()", "_+±{}|?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆");

        world.getLocal_users().add(newUser);
        world.getLocal_users().get(world.getLocal_users().size() - 1).setSessionCookie(myUserHandler.loginUserAPI(newUser));

        newUser.setLoggedin(true);
        newUser.setRegistered(true);

        String cookie = world.getLocal_users().get(world.getLocal_users().size() - 1).getSessionCookie();
        String replaceCookie = cookie.replaceAll(";", "");
        cookie = replaceCookie.replace("BTSESSION=", "");

        WebDriver driver = defineWebDriver();
        SeleniumUtil.loadPage(driver, urlResolver.resolve(PropertiesUtil.getProperty("brighttalk-home")));
        driver.manage().addCookie(new Cookie("BTSESSION", cookie));

        DateTimeFormatter.waitForSomeTime(5); //It is important to wait here as browser can not proceed cookie data so quickly
    }

    @Given("^I am logged in as new user containing specific characters")
    public void i_am_logged_in_as_user_containing_specific_characters() {

        com.brighttalk.qa.siteElements.User newUser;

        UserHandler myUserHandler = new UserHandler();

        newUser = myUserHandler.createUser("èàùéâêîôûëïçäöüß€áéíñóúü¿¡思想的指導者の毎日何千人も積極생각", "매일 수천àÀáÁãÃçÇéÉêÊÍíóÓúÚüÜёъяшертыуиопющэасдфгчйкл",  "ьжзхцвбнм每天都有思想領袖正在積極分享他們的見解!@£$%^&*()", "_+±{}|?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆");

        world.getLocal_users().add(newUser);
        world.getLocal_users().get(world.getLocal_users().size() - 1).setSessionCookie(myUserHandler.loginUserAPI(newUser));

        newUser.setLoggedin(true);
        newUser.setRegistered(true);

        String cookie = world.getLocal_users().get(world.getLocal_users().size() - 1).getSessionCookie();
        String replaceCookie = cookie.replaceAll(";", "");
        cookie = replaceCookie.replace("BTSESSION=", "");

        WebDriver driver = defineWebDriver();
        SeleniumUtil.loadPage(driver, urlResolver.resolve(PropertiesUtil.getProperty("brighttalk-home")));
        driver.manage().addCookie(new Cookie("BTSESSION", cookie));

        DateTimeFormatter.waitForSomeTime(5); //It is important to wait here as browser can not proceed cookie data so quickly
    }

    @Given("^I am (?:authenticated|logged in) as manager not ops$")
    public void i_am_authenticated_as_manager_not_ops() {

        com.brighttalk.qa.siteElements.User managerUser;
        UserHandler myUserHandler = new UserHandler();
        managerUser = myUserHandler.createManagerUser();

        world.getLocal_users().add(managerUser);
        world.getLocal_users().get(world.getLocal_users().size() - 1).setSessionCookie(myUserHandler.loginUserAPI(managerUser));

        managerUser.setLoggedin(true);
        managerUser.setRegistered(true);

        String cookie =  world.getLocal_users().get(world.getLocal_users().size() - 1).getSessionCookie();
        String replaceCookie = cookie.replaceAll(";", "");
        cookie = replaceCookie.replace("BTSESSION=", "");

        WebDriver driver = defineWebDriver();
        SeleniumUtil.loadPage(driver, urlResolver.resolve(PropertiesUtil.getProperty("brighttalk-home")));
        driver.manage().addCookie(new Cookie("BTSESSION", cookie));

        DateTimeFormatter.waitForSomeTime(5); //It is important to wait here as browser can not proceed cookie data so quickly
    }

    @Given("^BrightTALK cookie is added$")
    public void brightTALK_cookie_is_added() {

        WebDriver driver = defineWebDriver();
        Cookie ck = new Cookie("BTSESSION", "B0DA89CD77AD6D4E08F3A7B74A3862A0%3APaule%3AZaksauskiene%3A1517320756%3AEurope%2FLondon%3Atrue");
        driver.manage().addCookie(ck);
    }

}
