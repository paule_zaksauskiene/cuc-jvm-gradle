package com.brighttalk.qa.stepdefs.webcast;


import brighttalktest.messagebus.FakeDialIn;
import com.brighttalk.qa.api.ChannelApi;
import com.brighttalk.qa.common.EntityAttributeResolver;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.scenarios.UserHandler;
import com.brighttalk.qa.siteElements.WebcastPro;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import com.brighttalk.qa.stepdefs.ondemandvideo.OndemandVideoPreconditionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

@ContextConfiguration(
        classes = TestConfig.class)

public class WebcastActionSteps {

    @Autowired
    BrightTALKService service;

    @Autowired
    EntityAttributeResolver eaResolver;

    @Autowired
    World world;

    @Autowired
    Pages pages;

    @Autowired
    SharedChromeDriver driver;

    @Autowired
    BrowserAssertionSteps browserAssertionSteps;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    @Autowired
    OndemandVideoPreconditionSteps ondemandVideoPreconditionSteps;

    @When("^I syndicate (that|(?:a|an) (?: (upcoming|live|recorded))?(?: (audio&slides|pro|ondemand)))? (?:webcast|webinar) into ((?:a|another) channel|(?:a|the|another) channel I own)$")
    public void syndicate_webcast(String webcastTitle, String webcastType, String webastStatus, String c) {

        com.brighttalk.qa.siteElements.User user;
        com.brighttalk.qa.siteElements.Channel channel;
        WebcastPro comm;
        Long commId;

        if (c.contains("channel I own")) {
            user = (com.brighttalk.qa.siteElements.User) Util.getLatestEntry(world.getLocal_users());
        } else {

            UserHandler myUserHandler = new UserHandler();
            com.brighttalk.qa.siteElements.User managerUser = myUserHandler.createManageOpsUser();
            world.getLocal_users().add(managerUser);
            world.getLocal_users().get(0).setSessionCookie(myUserHandler.loginUserAPI(managerUser));
            managerUser.setLoggedin(true);
            managerUser.setRegistered(true);

        }

        service.getHandler().createChannel();
        channel = service.getHandler().createChannel();
        world.setConsumerChannel(channel);
        eaResolver.add("consumer_channel", channel);

        if (webcastTitle.equalsIgnoreCase("that")) {

            comm = (WebcastPro) Util.getLatestEntry(world.getWebcastsPro());

        } else {

            String json = "{\"status\":\"" + (webastStatus != null ? webastStatus : "upcoming") + "\"}";
            WebcastPro myWebcastPro = new WebcastPro();
            myWebcastPro.setDuration("15");
            myWebcastPro.setStatus("upcoming");
            myWebcastPro.setStartDate(Calendar.getInstance());
            myWebcastPro.getStartDate().add(Calendar.YEAR, 1);
            comm = service.getHandler().createPro(myWebcastPro);
        }

        try {

            ChannelApi.syndicateProWebinar(world.getRegressionTestsManagerUser(), channel, comm);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @When("^the syndication is(?: (not))? approved(?: by the (master|conumer) channel owner)?")
    public void auth_syndication(String flag, String channelOwnerType) {

        com.brighttalk.qa.siteElements.Channel c_channel = world.getConsumerChannel();
        com.brighttalk.qa.siteElements.Channel m_channel = (com.brighttalk.qa.siteElements.Channel) Util.getLatestEntry(world.getLocal_channels());
        Long commId;

        WebcastPro comm = (WebcastPro)Util.getLatestEntry(world.getWebcastsPro());

        try {
            ChannelApi.approveSyndication(world.getLocal_users().get(0), c_channel, comm);
        } catch (IOException e) {
            e.printStackTrace();
        }

       /* if (!StringUtils.hasLength(flag) || !flag.equalsIgnoreCase("not")) {
            if (StringUtils.hasLength(channelOwnerType)) {
                if (channelOwnerType.equalsIgnoreCase("master")) {

                    service.getCommunicationService().authoriseMasterSyndication(m_channel.getUser(), c_channel.getChannelID(), commId);

                } else if (channelOwnerType.equalsIgnoreCase("consumer")) {
                    service.getCommunicationService().authoriseConsumerSyndication(c_channel.getUser(), c_channel.getId(), commId);
                }
            } else {
                service.getCommunicationService().authoriseMasterSyndication(m_channel.getUser(), c_channel.getId(), commId);
                service.getCommunicationService().authoriseConsumerSyndication(c_channel.getUser(), c_channel.getId(), commId);
            }
        } */
    }

    @When("^I visit the pro webinar booking page$")
    public void i_visit_the_pro_webinar_booking_page() throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();
        pages.createWebcastHdPage().openPage(driver, world.getLocal_channels().get(0).getChannelID());

    }

    @When("^(.*) attachments views is generated$")
    public void attachments_views_is_generated(String numberoOfViews) throws Throwable {

        ArrayList<String> attachmentViews = new ArrayList<>();
        attachmentViews.add("1");
        attachmentViews.add("1");

        int numberOfTimes = 20;
        while (numberOfTimes > 0) {

            boolean isFound;

            try {
                driver.findElement(By.xpath("//div[@class='webcast-summary-reports-wrapper']/div[5]/div/div[2]/div/div/div[3][contains(.,'" + Integer.parseInt(numberoOfViews.trim()) + "')]"));
                for (int i = 0; i < attachmentViews.size(); i++) {
                    int rowNumber = i + 2;
                    String xpathAttachmentViews = "//div[@class='webcast-summary-reports-wrapper']/div[5]/div/div[2]/div/div[" + rowNumber + "]/div[3][contains(.,'" + attachmentViews.get(i) + "')]";
                    System.out.println("Xpath to verify: " + xpathAttachmentViews);
                    driver.findElement(By.xpath(xpathAttachmentViews));

                }

                isFound = true;

            } catch (org.openqa.selenium.NoSuchElementException e) {
                isFound = false;
            }
            if (isFound) {

                break;
            } else {
                System.out.println("Waiting for one more minute for attachments board to update..");

                try {
                    Thread.sleep(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                driver.navigate().refresh();
                browserAssertionSteps.the_page_opens("webinar-reports-page-metatag");
                numberOfTimes--;
                if (numberOfTimes == 0)
                    org.junit.Assert.fail("Attachments did not appear on the PRO WEBINAR reports page.");

            }

        }
    }

    @When("^feedback number (.*) is generated$")
    public void verifyFeedbackBoard(String numberOfFeedback){

            int numberOfTimes = 20;
            while (numberOfTimes > 0){

                boolean isFound;

                try {
                    driver.findElement(By.xpath("//div[@class='webcast-summary-reports-wrapper']/div[6]/div/div[2]/div/div/div[3][contains(.,'" + numberOfFeedback.trim() + "')]"));
                    isFound = true;

                } catch (org.openqa.selenium.NoSuchElementException e) {
                    isFound = false;
                }
                if (isFound){

                    break;
                }
                else
                {
                    System.out.println("Waiting for one more minute for Feedback board to update..");

                    try {
                        Thread.sleep(60000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    driver.navigate().refresh();
                    try {
                        browserAssertionSteps.the_page_opens("webinar-reports-page-metatag");
                    } catch (Throwable throwable) {
                        throwable.printStackTrace();
                    }
                    numberOfTimes--;
                    if (numberOfTimes == 0) org.junit.Assert.fail("Feedback did not appear on the PRO WEBINAR reports page.");

                }

            }
    }

    @When("^questions number (.*) is generated on the questions board$")
    public void questionsNumberIsGeneratedOnQuestionsBoard(String numberOfQuestions){

        int numberOfTimes = 6;
        while (numberOfTimes > 0){

            boolean isFound;

            try {
                driver.findElement(By.xpath("//div[@class='webcast-summary-reports-wrapper']/div[3]/div/div[2]/div/div/div[3][contains(.,'" + numberOfQuestions + "')]"));
                isFound = true;

            } catch (org.openqa.selenium.NoSuchElementException e) {
                isFound = false;
            }
            if (isFound){

                break;
            }
            else
            {
                System.out.println("Waiting for one more minute for Questions board to update..");

                try {
                    Thread.sleep(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                driver.navigate().refresh();

                try {
                    browserAssertionSteps.the_page_opens("webinar-reports-page-metatag");
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }

                numberOfTimes--;
                if (numberOfTimes == 0) org.junit.Assert.fail("Question(s) did not appear on the PRO WEBINAR reports page.");

            }

        }
    }

    @When("^question \"(.*)\" appears on the report$")
    public void questionAppearsOnTheReport(String questionText) {

        SeleniumUtils.findElement(driver, By.xpath("//td[contains(@class,'col col-name')][contains(.,'" + world.getRegressionTestsManagerUser().getFirstName() + "')]"), 600, 60, false, "THE VIEWER FIRST NAME IS NOT FOUND ON THE WEBCAST QUESTIONS REPORT AFTER 20 MINUTES!");

        SeleniumUtils.findElement(driver, By.xpath("//td[contains(@class,'col col-name')][contains(.,'" + world.getRegressionTestsManagerUser().getSecondName() + "')]"), 600, 60, false, "THE VIEWER SECOND NAME IS NOT FOUND ON THE WEBCAST QUESTIONS REPORT AFTER 20 MINUTES!");

        SeleniumUtils.findElement(driver, By.xpath("//div[contains(@class,'cell-container')][contains(.,'" + questionText + "')]"), 600, 60, false, "THE VIEWER QUESTION IS NOT FOUND ON THE WEBCAST QUESTIONS REPORT AFTER 20 MINUTES!");

        SeleniumUtils.findElement(driver, By.xpath("//td[contains(@class,'col col-answered')][contains(.,'No')]"), "THE VIEWER ANSWER IS NOT FOUND ON THE WEBCAST QUESTIONS REPORT AFTER 20 MINUTES");
    }

    @When("^viewers total number (.*) and (.*) live and (.*) recorded is generated on the viewings board$")
    public void verifyViewingsBoard(String total, String live, String recorded){

        int numberOfTimes = 8;
        while (numberOfTimes > 0){

            boolean isFound;

            try {

                String elementLocator = SeleniumUtils.resolveElementLocator("webcast-reports-viewings-board-total") + "[contains(.,'" + total + "')]";
                driver.findElement(By.xpath(elementLocator));

                elementLocator = SeleniumUtils.resolveElementLocator("webcast-reports-viewings-board-live") + "[contains(.,'" + live + "')]";
                driver.findElement(By.xpath(elementLocator));

                elementLocator = SeleniumUtils.resolveElementLocator("webcast-reports-viewings-board-recorded") + "[contains(.,'" + recorded + "')]";
                driver.findElement(By.xpath(elementLocator));

                isFound = true;

            } catch (org.openqa.selenium.NoSuchElementException e) {

                isFound = false;
            }
            if (isFound){

                break;
            }
            else
            {
                System.out.println("Waiting for one more minute for viewings board to update..");

                try {
                    Thread.sleep(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                driver.navigate().refresh();
                try {
                    browserAssertionSteps.the_page_opens("webinar-reports-page-metatag");
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
                numberOfTimes--;
                if (numberOfTimes == 0) org.junit.Assert.fail("Viewings did not appear on the PRO WEBINAR reports page.");

            }

        }
    }

    @When("^I click on attachment \"(.*)\"$")
    public void clickOnAttachment(String attachmentTitle){

        // First add the window handle id to the world object as new tab will be opened
        world.getWindowHandles().add(driver.getWindowHandle());

        DateTimeFormatter.waitForSomeTime(10); // its is necesaary to wait for all elements to load

        WebElement element = SeleniumUtils.findElement(driver, By.xpath("//a[contains(@class,'jsAttachment')]/div/h3[contains(.,'" + attachmentTitle + "')]"), 120, 1, false, "THE ATTACHMENT IS NOT FOUND !");

        System.out.println("Text: " + element.getText());
        System.out.println("location: " + element.getLocation().toString());

        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("arguments[0].focus();", element);

        String javaScript = "var evObj = document.createEvent('MouseEvents');" +
                "evObj.initMouseEvent(\"mouseover\",true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);" +
                "arguments[0].dispatchEvent(evObj);";


        ((JavascriptExecutor)driver).executeScript(javaScript, element);

        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);

        //  element.sendKeys(Keys.ENTER);
        // element.sendKeys(Keys.RETURN);
    }

    @When("^I end pro webinar$")
    public void iEndProWebinar(){

        try {

            browserActionSteps.i_click("end-presenting-button", "", "");
            browserActionSteps.i_click("end-presentation-confirmation-button", "", "");

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        String pageProp = Util.toYamlPropertyString("message-thank-for-for-presenting-on-brighttalk");
        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.findElement(driver, By.xpath(locator), "The \"Thank you for presenting on BrightTalk\" message is not shown");
        FakeDialIn.sendFakeDialIn( "exchange.mphd01." + TestConfig.getEnv() + ".brighttalk.net", world.getWebcastsPro().get(0).getWebcastID(), 0);
    }

    @When("^I start reuploading \"(.*)\" video file$")
    public void iStartReuploadingVideo(String fileName){

        try {
            browserActionSteps.i_visit_page("pro-webinar-prepare-and-present");
            browserActionSteps.i_click("pro-webinar-prepare-and-present-reupload-button", "", "");

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        ondemandVideoPreconditionSteps.i_have_selected_a_video(fileName);

        browserActionSteps.i_select_by_visible_text("upload-video-page-upload-location", "Europe");
        try {
            browserActionSteps.i_click("on-demand-video-upload-button", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @When("^I start reuploading invalid video file$")
    public void iStartReuploadingInvalidVideo(){

        try {
            browserActionSteps.i_visit_page("pro-webinar-prepare-and-present");
            browserActionSteps.i_click("pro-webinar-prepare-and-present-reupload-button", "", "");

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        ondemandVideoPreconditionSteps.i_have_selected_a_video("SampleVideo_1280x720_5mb.mp3");

        browserActionSteps.i_select_by_visible_text("upload-video-page-upload-location", "Europe");
        try {
            browserActionSteps.i_click("on-demand-video-upload-button", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Given("^I select a \"(.*)\" video file$")
    public void i_have_selected_a_video(String filePath) {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        // Get absolute path of the slides file
        File f = new File(filePath);
        String presentationPath = f.getAbsolutePath();

        String pageProp = Util.toYamlPropertyString("on-demand-video-select-video");
        String locator = PropertiesUtil.getProperty(pageProp);

        browserAssertionSteps.the_element_exists("on-demand-video-select-video");

        DateTimeFormatter.waitForSomeTime(2);

        SeleniumUtils.findElement(driver, By.xpath(locator), "THE PPT FILE UPLOAD INPUT ELEMENT IS NOT FOUND ON THE PRESENTER SCREEN!").sendKeys(presentationPath);

        System.out.println("Presentation file path: " + presentationPath);

    }

}

