package com.brighttalk.qa.stepdefs.webcast;

import com.brighttalk.qa.api.AttachmentsApi;
import com.brighttalk.qa.api.ChannelApi;
import com.brighttalk.qa.api.VotesApi;
import com.brighttalk.qa.api.WebcastProApi;
import com.brighttalk.qa.common.EntityAttributeResolver;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.helper.ResourceServiceHandler;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.scenarios.UserHandler;
import com.brighttalk.qa.siteElements.*;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import com.brighttalk.qa.stepdefs.ondemandvideo.OndemandVideoPreconditionSteps;
import com.brighttalk.qa.stepdefs.presenter.PresenterActionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.*;
import brighttalktest.messagebus.FakeDialIn;

@ContextConfiguration(
        classes = TestConfig.class)
public class WebcastPreconditionSteps {

    @Autowired
    BrightTALKService brightTALKService;

    @Autowired
    EntityAttributeResolver eaResolver;

    @Autowired
    World world;

    @Autowired
    SharedChromeDriver driver;

    @Autowired
    Pages pages;

    @Autowired
    UrlResolver urlResolver;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Autowired
    BrowserAssertionSteps browserAssertionSteps;

    @Autowired
    PresenterActionSteps presenterActionSteps;

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    @Autowired
    OndemandVideoPreconditionSteps ondemandVideoPreconditionSteps;

    User managerUser;

    @Given("^(?:\\s?(\\d+|a|an|no|several))?(?: (upcoming|live|recorded))?(?: (audio&slides|pro|ondemand))? (?:webcast|webinar) exists$")
    public void a_webcast_exists(String count, String type, String webcastStatus) {

        Channel channel;
        WebcastPro comm = null;
        String channelId;
        int webinarCount;

        if (world.getLocal_channels().isEmpty()) {

            if (world.getLocal_users().isEmpty()) {

                //user = brightTALKService.getHandler().registerUser();

                UserHandler myUserHandler = new UserHandler();
                managerUser = myUserHandler.createManageOpsUser();
                world.getLocal_users().add(managerUser);
                world.getLocal_users().get(0).setSessionCookie(myUserHandler.loginUserAPI(managerUser));
                managerUser.setLoggedin(true);
                managerUser.setRegistered(true);

            } else {
                managerUser = world.getLocal_users().get(world.getLocal_users().size() - 1);
            }

            System.out.println("My new user: " + managerUser.getEmail());
            System.out.println("My new user password: " + managerUser.getPassword());

            brightTALKService.getHandler().createChannel();
        }

        channel = world.getLocal_channels().get(world.getLocal_channels().size() - 1);
        ChannelApi.setChannelFeature(world.getRegressionTestsManagerUser(), channel, "customRegistrationTemplate", "true", "minimum");

        urlResolver.add("{channel_id}", channel.getChannelID());


        if (count.equalsIgnoreCase("a") | count.equalsIgnoreCase("an")) {
            webinarCount = 1;
        } else if (count.equalsIgnoreCase("several")) {
            webinarCount = Util.randomize(3, 9);
        } else if (count.equalsIgnoreCase("no")) {
            webinarCount = 0;
        } else {
            webinarCount = Integer.valueOf(count);
        }

        if (webinarCount > 0) {
            String json = "{\"status\":\"" + (webcastStatus != null ? webcastStatus : "upcoming") + "\"}";

            while (webinarCount > 0) {
                if (type != null) {
                    if (type.equalsIgnoreCase("a+s")) {

                        //comm = brightTALKService.getHandler().createAudioSlides(channelId, json);
                        //comm = brightTALKService.getHandler().createAudioSlides(channelId, json);

                    } else if (type.equalsIgnoreCase("ondemand")) {
                        //comm = brightTALKService.getHandler().createVideo(channelId, json);
                    } else {

                        WebcastPro myWebcastPro = new WebcastPro();
                        myWebcastPro.setDuration("15");
                        myWebcastPro.setStatus("upcoming");
                        myWebcastPro.setStartDate(Calendar.getInstance());
                        myWebcastPro.getStartDate().add(Calendar.YEAR, 1);
                        comm = brightTALKService.getHandler().createPro(myWebcastPro);
                        urlResolver.add("{comm_id}", comm.getWebcastID());
                    }
                } else {

                    WebcastPro myWebcastPro = new WebcastPro();
                    myWebcastPro.setDuration("15");
                    myWebcastPro.setStatus("upcoming");
                    myWebcastPro.setStartDate(Calendar.getInstance());
                    myWebcastPro.getStartDate().add(Calendar.YEAR, 1);
                    comm = brightTALKService.getHandler().createPro(myWebcastPro);
                    urlResolver.add("{comm_id}", comm.getWebcastID());
                }
                --webinarCount;
            }
        }

//      temporary - we need to be able to store all test data created and identify which ones are to be
//      utilised as part of the tests.

        if (webinarCount == 1) {

            eaResolver.add("webcast", comm);

        }
    }

    @Given("^\"(.*)\" more upcoming webinars are added to a channel$")
    public void more_webinars_is_added_to_a_channel(String count) {

        WebcastPro comm = null;
        int webinarCount;

        managerUser = world.getLocal_users().get(world.getLocal_users().size() - 1);

        webinarCount = Integer.valueOf(count);

        for (int i = 0; i < webinarCount; i++){

            int webinarNumber = i + 2;

            WebcastPro myWebcastPro = new WebcastPro();
            myWebcastPro.setDuration("15");
            myWebcastPro.setStatus("upcoming");
            myWebcastPro.setStartDate(Calendar.getInstance());
            myWebcastPro.getStartDate().add(Calendar.YEAR, webinarNumber);

            comm = brightTALKService.getHandler().createPro(myWebcastPro);
            urlResolver.add("{comm_id" + webinarNumber + "}", comm.getWebcastID());
            eaResolver.add("webcast" + webinarNumber, comm);

            --webinarCount;
        }
    }

    @Given("^a serviced webcast exists$")
    public void a_serviced_webcast_exists() {

        Channel channel;
        WebcastPro comm = null;

        if (world.getLocal_channels().isEmpty()) {

            if (world.getLocal_users().isEmpty()) {

                UserHandler myUserHandler = new UserHandler();
                managerUser = myUserHandler.createManageOpsUser();
                world.getLocal_users().add(managerUser);
                world.getLocal_users().get(0).setSessionCookie(myUserHandler.loginUserAPI(managerUser));
                managerUser.setLoggedin(true);
                managerUser.setRegistered(true);

            } else {

                managerUser = world.getLocal_users().get(world.getLocal_users().size() - 1);
            }

            System.out.println("My new user: " + managerUser.getEmail());
            System.out.println("My new user password: " + managerUser.getPassword());

            brightTALKService.getHandler().createChannel();
        }

        channel = world.getLocal_channels().get(world.getLocal_channels().size() - 1);
        ChannelApi.setChannelFeature(world.getRegressionTestsManagerUser(), channel, "customRegistrationTemplate", "true", "minimum");

        urlResolver.add("{channel_id}", channel.getChannelID());

        comm = brightTALKService.getHandler().createServicedPro();
        urlResolver.add("{comm_id}", comm.getWebcastID());

    }

    @Given("^a webcast with categorization tags exists$")
    public void a_webcast_with_categorization_tags_exists(List<String> tags) {

        this.a_webcast_exists("1", "pro", "upcoming");

        WebcastProApi webcastProApi = new WebcastProApi();
        try {
            webcastProApi.updateWebcastProAddCategorizationTags(world, world.getWebcastsPro().get(0), tags);
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Channel categorization tags added");

        List<CategorizationTag> categorizationTags = new ArrayList<CategorizationTag>();

        for (int i = 0; i < tags.size(); i++) {

            CategorizationTag category = new CategorizationTag();
            category.setId(Integer.toString(i));
            System.out.println("Category: " + tags.get(i));
            System.out.println("The position of slash" + tags.get(i).indexOf("/"));
            category.setCategory(tags.get(i).substring(0, tags.get(i).indexOf("/")));
            category.setTagValue(tags.get(i).substring(tags.get(i).indexOf("/") + 1));
            categorizationTags.add(category);
        }

        world.setCategorizationTags(categorizationTags);

    }

    @Given("^the following webcasts exist$")
    public void the_following_webcasts_exist(List<String> webinarNames) {

        // Create pro webinar for each name
        for (int i = 0; i < webinarNames.size(); i++) {

            if (webinarNames.get(i).contains("from now")) {

                int hoursFromNow = Integer.parseInt(webinarNames.get(i).substring(0, 1));

                WebcastPro myWebcastPro = new WebcastPro();
                myWebcastPro.setDuration("20");
                myWebcastPro.setStatus("upcoming");
                myWebcastPro.setStartDate(Calendar.getInstance());
                myWebcastPro.getStartDate().add(Calendar.HOUR, hoursFromNow);

                try {
                    myWebcastPro.setWebcastID(WebcastProApi.createUpcomingWebcastHd(world, myWebcastPro));
                } catch (IOException e) {
                    e.printStackTrace();
                }

                world.getWebcastsPro().add(myWebcastPro);

            }
        }
    }

    @Given("^categorization tag is set for the first two webcasts$")
    public void categorization_tag_is_set_for_the_first_two_webcasts(List<String> tags) {

        for (int i = 0; i < world.getWebcastsPro().size() - 1; i++) {

            WebcastProApi webcastProApi = new WebcastProApi();
            try {
                webcastProApi.updateWebcastProAddCategorizationTags(world, world.getWebcastsPro().get(i), tags);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Given("^pro webinar contains vote$")
    public void pro_webinar_contains_vote() {

        Vote vote = new Vote();
        String voteId = VotesApi.addVote(world.getLocal_channels().get(0), world.getRegressionTestsManagerUser(), world.getWebcastsPro().get(0).getWebcastID(), vote);

        vote.setVoteId(voteId);
        world.getWebcastsPro().get(0).getWebcastVotes().add(vote);

    }

    @Given("^pro webinar contains vote with the following details$")
    public void pro_webinar_contains_vote_with_the_following_details(List<String> tags) {

        Vote vote = new Vote();

        // Set vote question
        vote.setVoteQuestion(tags.get(0));

        // Set vote answers
        for (int i = 1; i < tags.size(); i++) {

            int j = i - 1;
            vote.getVoteOptions().set(j, tags.get(i));

        }

        String voteId = VotesApi.addVote(world.getLocal_channels().get(0), world.getRegressionTestsManagerUser(), world.getWebcastsPro().get(0).getWebcastID(), vote);

        vote.setVoteId(voteId);
        world.getWebcastsPro().get(0).getWebcastVotes().add(vote);

    }

    @Given("^pro webinar is less than 5 mins before live$")
    public void pro_webinar_is_less_than_5_mins_before_live() {

        world.getWebcastsPro().get(0).setStartDate(Calendar.getInstance());
        world.getWebcastsPro().get(0).getStartDate().add(Calendar.MINUTE, 3);
        WebcastProApi.updateWebcastProAsManager(world, world.getWebcastsPro().get(0));
    }

    @Given("^pro webinar is live$")
    public void pro_webinar_is_live() {

        // To verify if there is at least one available resource
        new ResourceServiceHandler().verifyAvailableResources();

        world.getWebcastsPro().get(0).setStartDate(Calendar.getInstance());
        world.getWebcastsPro().get(0).getStartDate().add(Calendar.MINUTE, 3);
        WebcastProApi.updateWebcastProAsManager(world, world.getWebcastsPro().get(0));

        try {

            // Call to presenters line
            if (TestConfig.getEnv().contains("test01")){

                FakeDialIn.sendFakeDialIn("exchange.mphd01." + TestConfig.getEnv() + ".brighttalk.net", world.getWebcastsPro().get(0).getWebcastID(), 1);

            } else

                FakeDialIn.sendFakeDialIn("exchange01.mphd01." + TestConfig.getEnv() + ".brighttalk.net", world.getWebcastsPro().get(0).getWebcastID(), 1);


            browserActionSteps.i_visit_page("presenter");

        } catch (Throwable throwable) {

            throwable.printStackTrace();
        }

        waitForStartOrPastTime();

        waitForPresenterCountBeMoreThanZero();

        if (!isTheStartPresentingPopupIsShown()) {

            clickStartPresentingButton();
        }

        startPresenting();

        world.getWebcastsPro().get(0).setStatus("live");
        waitforAnyLoadingOverlayToDisappear();

        //try {

        //  newBrowserActionSteps.i_click("add-vote-button", "", "");

        //} catch (Throwable throwable) {
        //   throwable.printStackTrace();
        //}

        System.out.println("Add vote button....");
    }

    @Given("^serviced pro webinar is live$")
    public void serviced_pro_webinar_is_live() {

        // To verify if there is at least one available resource
        new ResourceServiceHandler().verifyAvailableResources();

        world.getWebcastsPro().get(0).setStartDate(Calendar.getInstance());
        world.getWebcastsPro().get(0).getStartDate().add(Calendar.MINUTE, 3);
        WebcastProApi.updateWebcastProAsManager(world, world.getWebcastsPro().get(0));

        try {

            // Call to presenters line
            FakeDialIn.sendFakeDialIn("exchange.mphd01." + TestConfig.getEnv() + ".brighttalk.net", world.getWebcastsPro().get(0).getWebcastID(), 1);

            browserActionSteps.i_visit_page("presenter-serviced-page");

        } catch (Throwable throwable) {

            throwable.printStackTrace();
        }

        waitForStartOrPastTime();

        waitForPresenterCountBeMoreThanZero();

        if (!isTheStartPresentingPopupIsShown()) {

            clickStartPresentingButton();
        }

        startPresenting();

        world.getWebcastsPro().get(0).setStatus("live");
        waitforAnyLoadingOverlayToDisappear();

        //try {

        //  newBrowserActionSteps.i_click("add-vote-button", "", "");

        //} catch (Throwable throwable) {
        //   throwable.printStackTrace();
        //}

        System.out.println("Add vote button....");
    }


    /*
      We are waiting here untill the time is less than 10 secs before live
      There is a problem to wait for exact number of seconds to live as Selenium may not capture the location at the same second and fails intermitently
      Therefore we specify time to wait '00:00:0' on the counter which can be '00:00:09', '00:00:08, ... etc..
    */
    @Given("^time to start is after T-0 on the presenter screen$")
    public void waitForStartOrPastTime() {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        By locator = new By.ByXPath("//li[contains(@class,'jsTimer')][contains(.,'00:00:0')]");

        String timeShows = driver.findElement(By.xpath("//li[contains(@class,'jsTimer')]")).getText();
        System.out.println("Time remaining is shown : " + timeShows);
        if (timeShows.contains("00:00:0") || timeShows.contains("-00:0")) {

        } else {

            SeleniumUtils.findElement(driver, locator, 240, 1, false, "THE START ATIME 00:00:00 WAS NOT FOUND ON THE PRESENTER SCREEN AFTER 6 MINUTES..");
        }

        // Its important to wait here 10 secs, as the commands above are waiting for '00:00:0', which could mean '00:00:09'
        DateTimeFormatter.waitForSomeTime(10);
    }

    public void waitForPresenterCountBeMoreThanZero() {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        for (int i = 0; i < 5; i++)

            try {
                driver.findElement(By.xpath("//span[@class='jsPresenterCount'][contains(.,'0')]"));
                if (i == 4)
                    Assert.fail("THE PRESENTER COUNT IS STILL 0 ON THE PRESENTERS LIME AFTER WAITING FOR 4 MINUTES TO PRESENTER TO JOIN ON THE PRESENTER SCREEN!");
                DateTimeFormatter.waitForSomeTime(60);

            } catch (NoSuchElementException ex) {

                break;
            }
    }

    public boolean isTheStartPresentingPopupIsShown() {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        try {
            driver.findElement(By.xpath("//aside[contains(@class, 'jsView')][contains(@style, 'display: block')]/div/div/button[contains(.,'Start Presenting')]"));
            return true;
        } catch (NoSuchElementException ex) {

            return false;
        }
    }

    public void clickStartPresentingButton() {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (!this.isTheStartPresentingPopupIsShown()) {

            if (this.isPopupDisplayedOnPresenterPage())
                System.out.println("There is an unexpected pop up displayed on the presenter page!");

            // Wait for Start presenting button to be clickable on the top of the Presenting screen
            WebDriverWait wait = new WebDriverWait(driver, 30);
            By startPresentingButtonLocator = By.xpath("//button[contains(@class, 'jsActionButton')][contains(.,'Start Presenting')]");
            wait.until(ExpectedConditions.elementToBeClickable(startPresentingButtonLocator));

            // Click the Start presenting button on the top of the screen
            WebElement element = SeleniumUtils.findElement(driver, startPresentingButtonLocator, "THE \"START PRESENTING\" BUTTON IS NOT FOUND ON THE PRESENTER SCREEN");

            Actions actions = new Actions(driver);
            actions.moveToElement(element).click().perform();
        }

    }

    public boolean isPopupDisplayedOnPresenterPage() {

        return SeleniumUtils.isElementDisplayed(driver, By.xpath("//div[contains(@style, 'display: block;')][contains(@class, 'jsMask')][contains(@class, 'jsView')]"), "");
    }

    public String startPresenting() {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Wait for Start Presenting to be clickable on pop up
        WebDriverWait wait = new WebDriverWait(driver, 30);
        By startPresentingPopupButtonElementLocator = By.xpath("//button[contains(@class, 'jsGoLive')][contains(.,'Start Presenting')]");
        wait.until(ExpectedConditions.elementToBeClickable(startPresentingPopupButtonElementLocator));

        // Click on Start presenting button on the pop up
        SeleniumUtils.findElement(driver, By.xpath("//button[contains(@class, 'jsGoLive')][contains(.,'Start Presenting')]"), 60, 2, false, "THE START PRESENTING BUTTON WAS NOT FOUND ON THE START PRESENTING POP UP AFTER 60 SECS").click();

        // Verify the button is changed to End Presenting
        By locator = new By.ByXPath("//button[contains(@class, 'jsActionButton')][contains(.,'End presenting')]");
        SeleniumUtils.findElement(driver, locator, 120, 1, false, "THE BUTTON DID NOT CHANGE TO END PRESENTING AFTER WEBINAR HAS STARTED ON THE PRESENTER SCREEN.");
        return "live";
    }

    /**
     * This method is used to wait untill the loading pop up disappears
     * It can be presenter screen loading or starting the presenteration overlay
     */
    public void waitforAnyLoadingOverlayToDisappear() {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        SeleniumUtils.findElement(driver, By.xpath("//aside[contains(@class, 'jsView')][contains(@style, 'display: none')]"), 240, 1, false, "THE LOADING POP UP STILL REMAINS OPEN!");

        DateTimeFormatter.waitForSomeTime(3); // wait 3 seconds so the pop up os definitely closed

    }

    @Given("^the vote is open$")
    public void the_vote_is_open() {

        try {
            browserActionSteps.i_click("Votes from audience section", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        presenterActionSteps.i_click_the_Vote_view_button();

        try {
            browserActionSteps.i_click("start voting button", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

    }

    @When("^vote has been run$")
    public void vote_has_been_run() throws Throwable {

        the_vote_is_open();
        DateTimeFormatter.waitForSomeTime(60);
        browserActionSteps.i_click("Close vote button", "", "");

    }

    @When("^pro webinar is recorded$")
    public void pro_webinar_is_recorded() {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        if (world.getWebcastsPro().get(0).getStatus().contains("live")) {

            try {

                if (world.getWebcastsPro().get(0).isServiced() == true){

                    browserActionSteps.i_visit_page("presenter-serviced-page");

                } else {

                    browserActionSteps.i_visit_page("presenter");
                }

                browserActionSteps.i_click("end-presenting-button", "", "");
                browserActionSteps.i_click("end-presentation-confirmation-button", "", "");
                String pageProp = Util.toYamlPropertyString("message-thank-for-for-presenting-on-brighttalk");
                String locator = PropertiesUtil.getProperty(pageProp);
                SeleniumUtils.findElement(driver, By.xpath(locator), "The \"Thank you for presenting on BrightTalk\" message is not shown");
                FakeDialIn.sendFakeDialIn("exchange.mphd01." + TestConfig.getEnv() + ".brighttalk.net", world.getWebcastsPro().get(0).getWebcastID(), 0);
                world.getWebcastsPro().get(0).setStatus("Processing");
                ResourceServiceHandler.releaseResourceAfterTest(world.getWebcastsPro().get(0).getWebcastID());


            } catch (Throwable throwable) {

                throwable.printStackTrace();
            }
        }

        try {
            browserActionSteps.i_visit_page("manage-channel-page");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        waitForWebinarToBeInState(world.getWebcastsPro().get(0).getWebcastID(), "recorded", 120);
        world.getWebcastsPro().get(0).setStatus("recorded");

    }

    public void waitForWebinarToBeInState(String webcastId, String webinarStatus, int howManyMinutesToWait) {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        int numberOfTimes = howManyMinutesToWait;
        String recordedLocator = "//*[@id='listItem_" + webcastId + "']/td[5]/div[contains(.,'" + webinarStatus + "')]";
        while (numberOfTimes > 0) {

            boolean isFound;

            try {
                driver.findElement(By.xpath(recordedLocator));
                isFound = true;

            } catch (NoSuchElementException e) {
                isFound = false;
            }
            if (isFound) {

                break;
            } else {
                System.out.println("Waiting for one more minute for channel owner webcast listing page to update..");

                try {
                    Thread.sleep(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                driver.navigate().refresh();


                String pageProp = Util.toYamlPropertyString("manage-channel-page-metatag");
                String locator = PropertiesUtil.getProperty(pageProp);
                SeleniumUtils.findElement(driver, By.xpath(locator), "");
                numberOfTimes--;
                if (numberOfTimes == 0)
                    Assert.fail("WEBINAR " + webcastId + " STATE HAS NOT CHANGED TO " + webinarStatus + " AFTER " + howManyMinutesToWait + " MINUTES ON THE MANAGE CHANNEL PAGE!");

            }

        }
    }

    @Given("^pro webinar contains (.*) votes$")
    public void pro_webinar_contains_no_of_votes(String numberOfVotes) {

        for (int i = 0; i < Integer.parseInt(numberOfVotes); i++) {

            Vote vote = new Vote();
            String voteId = VotesApi.addVote(world.getLocal_channels().get(0), world.getRegressionTestsManagerUser(), world.getWebcastsPro().get(0).getWebcastID(), vote);
            vote.setVoteId(voteId);
            world.getWebcastsPro().get(0).getWebcastVotes().add(vote);

        }
    }

    @Given("^pro webinar duration is (.*) mins")
    public void pro_webinar_duration_is_mins(String mins) {

        world.getWebcastsPro().get(0).setDuration(mins.trim());
        WebcastProApi.updateWebcastProAsManager(world, world.getWebcastsPro().get(0));
        System.out.println("Webinar duration is now: " + world.getWebcastsPro().get(0).getDuration());

    }

    @Given("^a pro webinar is private")
    public void a_pro_webinar_is_private() {

        world.getWebcastsPro().get(0).setVisibility("private");
        WebcastProApi.updateWebcastProAsManager(world, world.getWebcastsPro().get(0));

    }

    @Given("^a pro webinar contains campaign reference \"(.*)\"")
    public void a_pro_webinar_contains_campaign_reference(String campaignReference) {

        world.getWebcastsPro().get(0).setCampaignReference(campaignReference);
        WebcastProApi.updateWebcastProAsManager(world, world.getWebcastsPro().get(0));

    }

    @Given("^a pro webinar viewer access is \"(.*)\"")
    public void a_pro_webinar_viewer_access_is(String viewerAccess) {

        world.getWebcastsPro().get(0).setViewerAccess(viewerAccess.trim());
        WebcastProApi.updateWebcastProAsManager(world, world.getWebcastsPro().get(0));
    }

    @Given("^a pro webinar starts in (.*) minutes")
    public void a_pro_webinar_starts_in_minutes(String minutes) {

        world.getWebcastsPro().get(0).setStartDate(Calendar.getInstance());
        world.getWebcastsPro().get(0).getStartDate().add(Calendar.MINUTE, Integer.parseInt(minutes.trim()));

        WebcastProApi.updateWebcastProAsManager(world, world.getWebcastsPro().get(0));

        DateTimeFormatter.waitForSomeTime(30);
    }

    @Given("^no resource is needed")
    public void no_resource_is_needed() {

        world.getWebcastsPro().get(0).setResourceNeeded(false);
    }

    @Given("^pro webinar contains \"(.*)\" attachment")
    public void webinar_contains_attachment(String attachmentType) {

        try {
            browserActionSteps.i_visit_page("webcast-attachments-page");
            browserAssertionSteps.the_page_opens("webcast-attachments-page-metatag");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        if (attachmentType.trim().contains("link to a file")) {

            Attachment myFileDocumentAttachment = new Attachment("link to a file", "http://www.brighttalk.com");
            new Select(SeleniumUtils.findElement(driver, By.id("newAttachmentType"), "THE NEW ATTACHMENT TYPE MENU IS NOT FOUND ON THE WEBINAR ATTACHMENTS PAGE!")).selectByVisibleText("File or link to a file");

            try {
                browserActionSteps.i_click("add-attachment-button", "", "");
                browserActionSteps.i_click("add-attachment-button", "", "");

            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

            browserAssertionSteps.the_element_exists("attachment-link-title");
            browserActionSteps.i_enter_value("attachment-file-title-field", myFileDocumentAttachment.getAttachmentTitle());
            browserActionSteps.i_enter_value("attachment-file-description-field", myFileDocumentAttachment.getAttachmentDescription());

            if (!myFileDocumentAttachment.getFileDocumentType().contains("upload")) {

                try {
                    browserActionSteps.i_click("attachment-file-choice-link", "", "");
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }

                browserAssertionSteps.the_element_exists("attachment-file-choice-link-fields");

                browserActionSteps.i_enter_value("attachment-document-url-field", myFileDocumentAttachment.getAttachmentUrl());

                browserActionSteps.i_enter_value("attachment-link-file-size-field", "100");

                browserActionSteps.i_select_by_visible_text("attachment-link-file-format-field", myFileDocumentAttachment.getFileFormat());

                try {
                    browserActionSteps.i_click("attachment-add-dialog-button", "", "");
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }


            }

            world.getWebcastsPro().get(0).getWebcastAttachments().add(myFileDocumentAttachment);
        }

        // link to a webpage attachment

        if (attachmentType.trim().contains("link to a web page")){

            Attachment myLinkToWebpageAttachment = new Attachment("link to a web page", "http://www.brighttalk.com");

            browserAssertionSteps.the_element_exists("attachment-type-locator");

            new Select(SeleniumUtils.findElement(driver, By.id("newAttachmentType"), "THE NEW ATTACHMENT TYPE MENU IS NOT FOUND ON THE WEBINAR ATTACHMENTS PAGE!")).selectByVisibleText("Link to a Web page");

            try {
                browserActionSteps.i_click("add-attachment-button", "", "");
                browserActionSteps.i_click("add-attachment-button", "", "");

            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            browserActionSteps.i_enter_value("attachment-link-title", myLinkToWebpageAttachment.getAttachmentTitle());

            browserActionSteps.i_enter_value("attachment-link-description", myLinkToWebpageAttachment.getAttachmentDescription());

            try {

                try {
                    browserAssertionSteps.i_should_see_element("1", "attachment-link-url-locator", "", "");
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }


                browserActionSteps.i_enter_value("attachment-link-url-locator", myLinkToWebpageAttachment.getAttachmentUrl());

            } catch (ElementNotVisibleException myException){

                System.out.println("Element linkURL is not displayed");
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            // Wait for the button "Add" to be clickable
            try {
                browserAssertionSteps.i_should_see_element("1", "attachment-add-link-url-add-button", "", "");
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

            // Click the Add button to add the attachment
            try {
                browserActionSteps.i_click("attachment-add-link-url-add-button", "", "");
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            world.getWebcastsPro().get(0).getWebcastAttachments().add(myLinkToWebpageAttachment);
        }
    }

    @Given("^pro webinar contains file type attachment")
    public void pro_webinar_contains_file_type_attachment(){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        // AttachmentsApi.uploadFileTypeAttachment(world.getRegressionTestsManagerUser(), world.getWebcastsPro().get(0));
        Attachment myFileTypeAttachment = new Attachment("file", "SeleniumTextFile.txt");
        myFileTypeAttachment.setAttachmentTitle("SeleniumTextFile.txt");

        try {
            browserActionSteps.i_visit_page("webcast-attachments-page");
            browserAssertionSteps.the_page_opens("webcast-attachments-page-metatag");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        try {

            browserActionSteps.i_select_by_visible_text("attachment-type-locator", "File or link to a file");
            browserActionSteps.i_click("add-attachment-button", "", "");

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        browserAssertionSteps.the_element_exists("attachment-link-title");
        browserActionSteps.i_enter_value("attachment-file-title-field", myFileTypeAttachment.getAttachmentTitle());
        browserActionSteps.i_enter_value("attachment-file-description-field", myFileTypeAttachment.getAttachmentDescription());

        // Get absolute path of the slides file
        File f = new File("SeleniumTextFile.txt");
        String presentationPath = f.getAbsolutePath();

        String pageProp = Util.toYamlPropertyString("channel-owner-attachments-page-file-attachment-select");
        String locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.findElement(driver, By.xpath(locator), "THE PPT FILE UPLOAD INPUT ELEMENT IS NOT FOUND ON THE PRESENTER SCREEN!").sendKeys(presentationPath);

        browserActionSteps.i_select_by_visible_text("attachment-upload-location", "Europe");

        world.getWebcastsPro().get(0).getWebcastAttachments().add(myFileTypeAttachment);

        try {
            browserActionSteps.i_click("attachment-upload-button", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        pageProp = Util.toYamlPropertyString("attachment-checking-file-progress-spinner");
        locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.verifyElementDisappearsFromPageWithoutRefresh(driver, By.xpath(locator), 240, 2,"The attachment \"Checking file...\" spinner did not disappear in 4 minutes...");

        browserAssertionSteps.i_should_see_message("added-attachment-title", myFileTypeAttachment.getAttachmentTitle());

    }

    @Given("^I close presenter screen overlay")
    public void closePopUp(){

        if (this.isPopupDisplayedOnPresenterPage())

            System.out.println("There is an unexpected pop up displayed on the presenter page!");

        try {
            browserActionSteps.i_click("presenter-page-popup-close-button", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Given("^a webcast with id (.*) exists")
    public void a_webcast_with_id_exists(String webcastId){

        WebcastPro webcastPro = new WebcastPro();
        webcastPro.setWebcastID(webcastId);
        webcastPro.setShouldBeDeleted(false);

        world.getWebcastsPro().add(webcastPro);

        urlResolver.add("{comm_id}", String.valueOf(webcastPro.getWebcastID()));

    }

    @Given("^webcast already contains \"(.*)\" attachment with url \"(.*)\"")
    public void webcast_already_contains_link_a_file_attachment_with_url(String attachmentTitle, String attachmentUrl){

        Attachment myFileDocumentAttachment = new Attachment(attachmentTitle, attachmentUrl);
        myFileDocumentAttachment.setAttachmentTitle(attachmentTitle);
        myFileDocumentAttachment.setAttachmentUrl(attachmentUrl);

        world.getWebcastsPro().get(0).getWebcastAttachments().add(myFileDocumentAttachment);
    }

    @Given("^I select feature image that is too large")
    public void i_select_feature_image_that_is_too_large(){

        File f = new File("src/main/resources/flowers-5.jpg");
        String myFullPath = f.getAbsolutePath();

        //myFullPath = myFullPath.replace("bin", "src");
        System.out.println("My full image path: " + myFullPath);

        browserActionSteps.i_enter_value("pro-webinar-booking-form-edit-featureimage", myFullPath);

    }

    @Given("^I select (.*) file for attachment upload")
     public void i_select_file(String fileName){

        // Get absolute path of the slides file
        File f = new File(fileName);
        String presentationPath = f.getAbsolutePath();

        String pageProp = Util.toYamlPropertyString("channel-owner-attachments-page-file-attachment-select");
        String locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.findElement(driver, By.xpath(locator), "THE PPT FILE UPLOAD INPUT ELEMENT IS NOT FOUND ON THE PRESENTER SCREEN!").sendKeys(presentationPath);
    }

    @Given("^the \"(.*)\" video file is selected for existing pro webinar on the prepare and present page$")
    public void i_have_selected_a_video(String filePath) {

        browserPreconditionSteps.i_am_authenticated_as_manager();

        try {

            browserActionSteps.i_visit_page_for_webinar_in_channel("pro-webinar-prepare-and-present", "1510272071", "2000248877");
            browserActionSteps.i_click("pro-webinar-prepare-and-present-reupload-button", "", "");

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        ondemandVideoPreconditionSteps.i_have_selected_a_video("SampleVideo_1280x720_5mb.mp4");
        browserAssertionSteps.i_should_see_message("upload-video-page-selected-file", "SampleVideo_1280x720_5mb.mp4");


    }

}
