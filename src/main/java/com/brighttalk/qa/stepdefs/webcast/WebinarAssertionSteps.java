package com.brighttalk.qa.stepdefs.webcast;

import com.brighttalk.qa.api.WebcastProApi;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.helper.WebElementWaiter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.siteElements.WebcastPro;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

@ContextConfiguration(
        classes = TestConfig.class)
public class WebinarAssertionSteps {

    @Autowired
    SharedChromeDriver driver;

    @Autowired
    World world;

    @Autowired
    BrightTALKService service;

    @Autowired
    BrowserAssertionSteps browserAssertionSteps;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    @Autowired
    UrlResolver urlResolver;

    /**
     * This is used to add the new booked pro webinar (on the front end to the World object to be deleted later
     * @throws Throwable
     */
    @Then("^new pro webinar is booked$")
    public void new_pro_webinar_is_booked() throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String webcastId;
        String current_url = driver.getCurrentUrl();

        String urlParts [];
        urlParts = current_url.split("/");
        webcastId = urlParts[7];
        System.out.println(" [+] Pro webinar id: " + webcastId);

        //Communication proWebinar = service.getCommunicationService().getCommunicationById(Long.parseLong(webcastId), world.getChannels().get(0).getId());
        WebcastPro newProWebinar = new WebcastPro();
        newProWebinar.setWebcastID(webcastId);

        world.getWebcastsPro().add(newProWebinar);

        urlResolver.add("{channel_id}", world.getLocal_channels().get(0).getChannelID());
        urlResolver.add("{comm_id}", webcastId);

    }

    @Then("^I see the vote on the vote board with the following details$")
    public void i_see_the_vote_on_the_vote_board_with_the_following_details(List<String> voteDetails) throws Throwable {

        int numberOfTimes = 6;
        while (numberOfTimes > 0){

            boolean isFound;

            String pageProp = Util.toYamlPropertyString("webcast-reports-page-votes-board-vote-question");
            String locator = PropertiesUtil.getProperty(pageProp);

            pageProp = Util.toYamlPropertyString("webcast-reports-page-votes-board-no-of-votes");
            String locator2 = PropertiesUtil.getProperty(pageProp);

            try {

                driver.findElement(By.xpath(locator + "[contains(.,'" + voteDetails.get(0).trim() + "')]"));
                driver.findElement(By.xpath(locator2 + "[contains(.,'" + voteDetails.get(1).trim() + " Votes')]"));
                isFound = true;

            } catch (NoSuchElementException e) {
                isFound = false;
            }
            if (isFound){

                break;
            }
            else
            {
                System.out.println("Waiting for one more minute for Votes board to update..");

                try {
                    Thread.sleep(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                driver.navigate().refresh();
                browserAssertionSteps.the_element_exists("webinar-reports-page-metatag");
                numberOfTimes--;
                if (numberOfTimes == 0) Assert.fail("Votes did not appear on the channel owner reports page.");

            }
        }
    }

    @Then("^I see the vote report for the vote \"(.*)\"$")
    public void i_see_the_vote_report_for_the_vote(String voteQuestion, List<String> reportDetails) throws Throwable {

        browserActionSteps.i_visit_page("webcast-votes-report");

        String pageProp = Util.toYamlPropertyString("webcast-votes-report-question");
        String locator = PropertiesUtil.getProperty(pageProp);

        // Votes question
        SeleniumUtils.findElement(driver, By.xpath(locator + "[contains(.,'" + voteQuestion + "')]"), "THE VOTE " + voteQuestion + " IS NOT FOUND ON THE VOTES REPORT PAGE!");

        int j = 1;
        for (int i = 0; i < reportDetails.size() / 2; i ++){

            verifyVoteOptionResult(j, reportDetails.get(i), Integer.parseInt(reportDetails.get(i + 1)), Integer.parseInt(reportDetails.get(i+2)));
            i = i + 2;
            j = j + 1;
        }
    }

    public void verifyVoteOptionResult(int optionNumber, String optionTitle, int numberOfVotes, int resultPercentage) {

        String pageProp = Util.toYamlPropertyString("webcast-votes-report-options");
        String locator = PropertiesUtil.getProperty(pageProp);

        // Verify option title
        String locatorOptionTitle = locator + "/li[" + optionNumber + "][contains(.,'" + optionTitle + "')]";
        SeleniumUtils.findElement(driver, By.xpath(locatorOptionTitle), "");

        // Verify number of votes
        int votesPosition = 0;
        if (optionNumber == 1) votesPosition = 1;
        if (optionNumber == 2) votesPosition = 3;
        if (optionNumber == 3) votesPosition = 5;
        if (optionNumber == 4) votesPosition = 7;
        if (optionNumber == 5) votesPosition = 9;

        SeleniumUtils.findElement(driver, By.xpath(locator + "/div[" + votesPosition + "][contains(.,'" + numberOfVotes + " Votes')]"), "THE VOTE ANSWER FOR THE FIRST OPTION IS NOT FOUND ON THE VOTES REPORT PAGE!");

        // Verify the vote percentage is shown on the vote reports page

        int votesPercentPosition = 0;
        if (optionNumber == 1) votesPercentPosition = 2;
        if (optionNumber == 2) votesPercentPosition = 4;
        if (optionNumber == 3) votesPercentPosition = 6;
        if (optionNumber == 4) votesPercentPosition = 8;
        if (optionNumber == 5) votesPercentPosition = 10;

        SeleniumUtils.findElement(driver, By.xpath(locator + "/div[" + votesPercentPosition + "]/div/span[contains(.,'" + resultPercentage + "%')]"), "THE VOTE ANSWER PERCENTAGE FOR THE FIRST OPTION IS NOT FOUND ON THE VOTES REPORT PAGE!");

    }

    @Then("^I see the feature image$")
    public void i_see_feature_image(){

        // Retrieves the preview image from API and compares to the image shown on the Edit booking page
        String featureImageName = WebcastProApi.communicationFeatureImageName(WebcastProApi.getCommunicationDetails(world.getRegressionTestsManagerUser(), world.getLocal_channels().get(0), world.getWebcastsPro().get(0)));

        for (int i = 0; i < 12; i++) {

            try {
                driver.findElement(By.xpath("//a[@class='preview'][contains(@href, '" + featureImageName + "')]"));
                break;

            } catch (NoSuchElementException ex) {

                try {
                    Thread.sleep(60000);
                    this.driver.navigate().refresh();
                    browserAssertionSteps.the_element_exists("view-booking-page-metatag");

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if (i == 4) Assert.fail("Could not find the feature image on the PRO WEBINAR VIEW booking page...");
        }
    }

    @Then("^the attachment viewers details are shown$")
    public void the_attachment_viewers_details_are_shown(){

        ArrayList<String> attachmentViewer = new ArrayList<>();
        attachmentViewer.add(world.getRegressionTestsManagerUser().getFirstName());
        attachmentViewer.add(world.getRegressionTestsManagerUser().getSecondName());
        attachmentViewer.add(world.getRegressionTestsManagerUser().getJobTitle());
        attachmentViewer.add(world.getRegressionTestsManagerUser().getCompany());
        attachmentViewer.add(world.getWebcastsPro().get(0).getWebcastAttachments().get(0).getAttachmentTitle());

        verifyViewerDetails(attachmentViewer);

    }

    public void verifyViewerDetails(ArrayList<String> attachmentViewer){

        for (int i = 0; i < attachmentViewer.size(); i ++){

            SeleniumUtils.findElement(driver, By.xpath("//div[contains(@class, 'report')]/table/tbody/tr[contains(.,'" + attachmentViewer.get(0) + "')]"), 60, 1, false, "THE VIEWER DETAILS " + attachmentViewer.get(0) + " ARE NOT FOUND ON THE WEBCAST ATTACHMENTS REPORT PAGE!");
            SeleniumUtils.findElement(driver, By.xpath("//div[contains(@class, 'report')]/table/tbody/tr[contains(.,'" + attachmentViewer.get(1) + "')]"), 60, 1, false, "THE VIEWER DETAILS " + attachmentViewer.get(1) + " ARE NOT FOUND ON THE WEBCAST ATTACHMENTS REPORT PAGE!");
          //  SeleniumUtils.findElement(driver, By.xpath("//div[contains(@class, 'report')]/table/tbody/tr[contains(.,'" + attachmentViewer.get(2) + "')]"), 60, 1, false, "THE VIEWER DETAILS " + attachmentViewer.get(2) + " ARE NOT FOUND ON THE WEBCAST ATTACHMENTS REPORT PAGE!");
          //  SeleniumUtils.findElement(driver, By.xpath("//div[contains(@class, 'report')]/table/tbody/tr[contains(.,'" + attachmentViewer.get(3) + "')]"), 60, 1, false, "THE VIEWER DETAILS " + attachmentViewer.get(3) + " ARE NOT FOUND ON THE WEBCAST ATTACHMENTS REPORT PAGE!");
            SeleniumUtils.findElement(driver, By.xpath("//div[contains(@class, 'report')]/table/tbody/tr[contains(.,'" + attachmentViewer.get(4) + "')]"), 60, 1, false, "THE VIEWER DETAILS " + attachmentViewer.get(4) + " ARE NOT FOUND ON THE WEBCAST ATTACHMENTS REPORT PAGE!");
        }
    }


    @Then("^webinar details are shown on the webcasts listing page$")
    public void webinar_details_are_shown_on_the_webcasts_listing_page(){

        String[] webcast_info_on_the_page;
        webcast_info_on_the_page = getAllInfoAboutCertainWebcast(world.getWebcastsPro().get(0).getWebcastID());

        verifyDataOfTheWebcast(webcast_info_on_the_page, world.getWebcastsPro().get(0));
        verifyWebcastRating(world.getWebcastsPro().get(0), "0");
    }

    public String[] getAllInfoAboutCertainWebcast(String webcast_id){

        String [] webcast_info = {"date-time","webcast-title","presenter","status","pre-reg","views","viewed-for"};
        String webcastID = world.getWebcastsPro().get(0).getWebcastID();

        webcast_info[0] = new WebElementWaiter().waitAndFindByXpath(driver, "//*[@id='listItem_"+ webcastID +"\']/td[1]/div/span/abbr", 600, 60, false).getText(); //checking webcast date & time
        webcast_info[1] = new WebElementWaiter().waitAndFindByXpath(driver, "//*[@id='listItem_"+ webcastID +"\']/td[3]/div", 600, 60, false).getText(); //checking webcast title
        webcast_info[2] = new WebElementWaiter().waitAndFindByXpath(driver, "//*[@id='listItem_"+ webcastID +"\']/td[4]/div", 600, 60, false).getText(); //checking webcast presenter
        webcast_info[3] = new WebElementWaiter().waitAndFindByXpath(driver, "//*[@id='listItem_"+ webcastID +"\']/td[5]/div", 600, 60, false).getText(); //checking webcast status
        webcast_info[4] = new WebElementWaiter().waitAndFindByXpath(driver, "//*[@id='listItem_"+ webcastID +"\']/td[6]/div", 600, 60, false).getText(); //checking webcast pre-reg
        webcast_info[5] = new WebElementWaiter().waitAndFindByXpath(driver, "//*[@id='listItem_"+ webcastID +"\']/td[7]/div", 600, 60, false).getText(); //checking webcast views
        webcast_info[6] = new WebElementWaiter().waitAndFindByXpath(driver, "//*[@id='listItem_"+ webcastID +"\']/td[8]/div", 600, 60, false).getText(); //checking webcast viewed-for

        return webcast_info;
    }

    public void verifyDataOfTheWebcast(String [] webcast_info_on_the_page, WebcastPro myWebcastPro){

        String webinarDateTime = DateTimeFormatter.webinarPageDateTimeFormat(myWebcastPro);

        System.out.println("Webcast info on the page: " + webcast_info_on_the_page[0].trim());

        System.out.println("Actual webinar date and time: " + webinarDateTime.trim());

        Assert.assertTrue("THE DATE OF THE WEBCAST IS NOT AS EXPECTED ON THE CHANNEL OWNER WEBCAST LISTING PAGE, EXPECTED: " + webinarDateTime.trim() + ", ACTUAL: " + webcast_info_on_the_page[0].trim(), webinarDateTime.trim().contains(webcast_info_on_the_page[0].trim()));

        Assert.assertTrue("The webinar title I expect to see: " + myWebcastPro.getTitle() + ", is not the same as shown on the page: " + webcast_info_on_the_page[1].trim(), myWebcastPro.getTitle().contains(webcast_info_on_the_page[1].trim()));

        Assert.assertTrue(myWebcastPro.getPresenter().contains(webcast_info_on_the_page[2]));
        // TRANSCODING NOT VERIFIED FOR NOW, therefore the status is not checked
        //Assert.assertTrue(myWebcastPro.getStatus().contains(webcast_info_on_the_page[3].trim()), "The webinar status we expect is: " + myWebcastPro.getStatus() + ", the status shown on the page is: "  + webcast_info_on_the_page[3].trim());
        Assert.assertTrue((webcast_info_on_the_page[4]).contains("0"));

        // Verify number of viewings
        // org.testng.Assert.assertTrue(webcast_info_on_the_page[5].contains("1"));
        Assert.assertTrue((webcast_info_on_the_page[6]).contains("< 2 min"));

    }

    public void verifyWebcastRating(WebcastPro myWebcastPro, String webcastRating){

        By locator = new By.ByXPath("//*[@id='listItem_" + myWebcastPro.getWebcastID() + "']/td[9]/div/div/div[contains(@style, '" + webcastRating + "')]");
        SeleniumUtils.findElement(this.driver, locator, 240, 60, true, "");
    }

    @Then("^the number of viewers on report is (.*)$")
    public void verifyNumberOfViewers(String numberOfViewers){

        SeleniumUtils.findElement(driver, By.xpath(SeleniumUtils.resolveElementLocator("webcast-reports-viewings-report-number-of-viewers") + "[contains(.,'" + numberOfViewers + "')]"), 600, 60, false, "THE NUMBER OF VIEWERS " + numberOfViewers + " IS NOT FOUND IN THE WEBCAST VIEWINGS PAGE!");

    }

    @Then("^viewer details on viewing report are shown$")
    public void verifyViewerDetails(){

        String userDetailsLocator = SeleniumUtils.resolveElementLocator("webcast-reports-viewings-report-viewer-details");

        SeleniumUtils.findElement(driver, By.xpath(userDetailsLocator + "[contains(.,'" + world.getLocal_users().get(0).getFirstName() + "')]"), 1200, 60, true, "THE VIEWER FIRST NAME IS NOT FOUND ON THE WEBCAST VIEWINGS PAGE!");
        SeleniumUtils.findElement(driver, By.xpath(userDetailsLocator + "[contains(.,'" + world.getLocal_users().get(0).getSecondName() + "')]"), 600, 60, false, "THE VIEWER SECOND NAME IS NOT FOUND ON THE WEBCAST VIEWINGS PAGE!");

        // NOT DONE YET (its empty for current user)
        //SeleniumUtils.findElement(driver, By.xpath(userDetailsLocator + "[contains(.,'" + world.getLocal_users().get(0).getJobTitle() + "')]"), 600, 60, false, "THE VIEWER JOB TITLE IS NOT FOUND ON THE WEBCAST VIEWINGS PAGE!");
        //SeleniumUtils.findElement(driver, By.xpath(userDetailsLocator + "[contains(.,'" + world.getLocal_users().get(0).getCompany() + "')]"), 600, 60, false, "THE VIEWER COMPANY IS NOT FOUND ON THE WEBCAST VIEWINGS PAGE!");

    }

    @Then("^CSV report link is present$")
    public void verifyTheGenerateCSVReportsLink(){

        By userDetailsElement = By.xpath(SeleniumUtils.resolveElementLocator("webcast-reports-viewings-reports-csv-all-viewers"));
        SeleniumUtils.findElement(driver, userDetailsElement, "THE LINK \"Download all viewers - Generate\" IS NOT FOUND ON THE WEBCAST VIWINGS REPORT PAGE!");

        userDetailsElement = By.xpath(SeleniumUtils.resolveElementLocator("webcast-reports-viewings-reports-csv-live-viewers"));
        SeleniumUtils.findElement(driver, userDetailsElement, "THE LINK \"Download all live viewers - Generate\" IS NOT FOUND ON THE WEBCAST VIWINGS REPORT PAGE!");

        userDetailsElement = By.xpath(SeleniumUtils.resolveElementLocator("webcast-reports-viewings-reports-csv-recorded-viewers"));
        SeleniumUtils.findElement(driver, userDetailsElement, "THE LINK \"Download all recorded viewers - Generate\" IS NOT FOUND ON THE WEBCAST VIWINGS REPORT PAGE!");

        userDetailsElement = By.xpath(SeleniumUtils.resolveElementLocator("webcast-reports-viewings-reports-csv-log-viewers"));
        SeleniumUtils.findElement(driver, userDetailsElement, "THE LINK \"Download all webcasts viewings log - Generate\" IS NOT FOUND ON THE WEBCAST VIWINGS REPORT PAGE!");

    }

    @Then("^webinar details are correct on webcast listing page$")
    public void webinarDetailsAreCorrectOnWebcastListingPage(){

        String webcast_id = world.getWebcastsPro().get(0).getWebcastID();

        String [] webcast_info = {"date-time","webcast-title","presenter","status","pre-reg","views","viewed-for"};
        webcast_info[0] = new WebElementWaiter().waitAndFindByXpath(driver, "//*[@id='listItem_"+webcast_id+"\']/td[1]/div/span/abbr", 600, 60, false).getText(); //checking webcast date & time
        webcast_info[1] = new WebElementWaiter().waitAndFindByXpath(driver, "//*[@id='listItem_"+webcast_id+"\']/td[3]/div", 600, 60, false).getText(); //checking webcast title
        webcast_info[2] = new WebElementWaiter().waitAndFindByXpath(driver, "//*[@id='listItem_"+webcast_id+"\']/td[4]/div", 600, 60, false).getText(); //checking webcast presenter
        webcast_info[3] = new WebElementWaiter().waitAndFindByXpath(driver, "//*[@id='listItem_"+webcast_id+"\']/td[5]/div", 600, 60, false).getText(); //checking webcast status
        webcast_info[4] = new WebElementWaiter().waitAndFindByXpath(driver, "//*[@id='listItem_"+webcast_id+"\']/td[6]/div", 600, 60, false).getText(); //checking webcast pre-reg
        webcast_info[5] = new WebElementWaiter().waitAndFindByXpath(driver, "//*[@id='listItem_"+webcast_id+"\']/td[7]/div", 600, 60, false).getText(); //checking webcast views
        webcast_info[6] = new WebElementWaiter().waitAndFindByXpath(driver, "//*[@id='listItem_"+webcast_id+"\']/td[8]/div", 600, 60, false).getText(); //checking webcast viewed-for

        String webinarDateTime = DateTimeFormatter.webinarPageDateTimeFormat(world.getWebcastsPro().get(0));

        System.out.println("Webcast info on the page: " + webcast_info[0].trim());

        System.out.println("Actual webinar date and time: " + webinarDateTime.trim());

        Assert.assertTrue("THE DATE OF THE WEBCAST IS NOT AS EXPECTED ON THE CHANNEL OWNER WEBCAST LISTING PAGE, EXPECTED: " + webinarDateTime.trim() + ", ACTUAL: " + webcast_info[0].trim(), webinarDateTime.trim().contains(webcast_info[0].trim()));

        Assert.assertTrue("The webinar title I expect to see: " + world.getWebcastsPro().get(0).getTitle() + ", is not the same as shown on the page: " + webcast_info[1].trim(), world.getWebcastsPro().get(0).getTitle().contains(webcast_info[1].trim()));

        Assert.assertTrue(world.getWebcastsPro().get(0).getPresenter().contains(webcast_info[2]));

        // TRANSCODING NOT VERIFIED FOR NOW, therefore the status is not checked
        //Assert.assertTrue(myWebcastPro.getStatus().contains(webcast_info_on_the_page[3].trim()), "The webinar status we expect is: " + myWebcastPro.getStatus() + ", the status shown on the page is: "  + webcast_info_on_the_page[3].trim());
        Assert.assertTrue((webcast_info[4]).contains("0"));

        // Verify number of viewings
        // org.testng.Assert.assertTrue(webcast_info_on_the_page[5].contains("1"));

        Assert.assertTrue((webcast_info[6]).contains("< 2 min"));;
    }

    @Then("^webinar rating is \"(.*)\" on the webcast listing page$")
    public void webinarRatingIsOnTheWebcastListingPage(String webinarRating){

        By locator = new By.ByXPath("//*[@id='listItem_" + world.getWebcastsPro().get(0).getWebcastID() + "']/td[9]/div/div/div[contains(@style, '" + webinarRating + "')]");
        SeleniumUtils.findElement(this.driver, locator, 240, 60, true, "");

    }
}
