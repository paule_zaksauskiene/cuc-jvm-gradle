package com.brighttalk.qa.stepdefs.ondemandvideo;

import com.brighttalk.qa.api.WebcastProApi;
import com.brighttalk.qa.common.EntityAttributeResolver;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.siteElements.OnDemandVideo;
import com.brighttalk.qa.siteElements.WebcastPro;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

@ContextConfiguration(
        classes = TestConfig.class)
public class OndemandVideoPreconditionSteps {

    @Autowired
    BrightTALKService brightTALKService;

    @Autowired
    EntityAttributeResolver eaResolver;

    @Autowired
    World world;

    @Autowired
    SharedChromeDriver driver;

    @Autowired
    Pages pages;

    @Autowired
    BrowserAssertionSteps browserAssertionSteps;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    @Autowired
    UrlResolver urlResolver;

    com.brighttalk.qa.siteElements.User managerUser;

    @Given("^new on demand video exists$")
    public void new_on_demand_video_exists() {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        try {

            browserActionSteps.i_click("manage-channel-page-add-content-button", "", "");
            browserActionSteps.i_click("manage-channel-page-upload-video", "", "");

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        OnDemandVideo myOnDemandVideo = new OnDemandVideo();

        browserActionSteps.i_enter_value("on-demand-video-title", myOnDemandVideo.getTitle());
        browserActionSteps.i_enter_value("on-demand-video-description", myOnDemandVideo.getDescription());
        browserActionSteps.i_enter_value("on-demand-video-presenter", myOnDemandVideo.getPresenter());

        browserActionSteps.i_enter_value("on-demand-video-tags", myOnDemandVideo.getTags());
        try {
            browserActionSteps.i_click("on-demand-video-tags-add-button", "", "");
            browserActionSteps.i_click("on-demand-video-proceed-button", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        browserAssertionSteps.the_element_exists("on-demand-video-prepare-and-preview-page", "30");

        String onDemandVideoId = driver.getCurrentUrl();
        int beginning = onDemandVideoId.indexOf("webcast/");
        beginning = beginning + 8;

        int ending = onDemandVideoId.indexOf("/prepare-upload");

        onDemandVideoId = onDemandVideoId.substring(beginning, ending);
        System.out.println("on demand video id: " + onDemandVideoId);

        myOnDemandVideo.setOnDemandVideoID(onDemandVideoId);

        world.getOnDemandVideos().add(myOnDemandVideo);
        urlResolver.add("{ondemandvideo_id}", world.getOnDemandVideos().get(0).getOnDemandVideoID());
    }

    @Given("^I have selected a \"(.*)\" video file$")
    public void i_have_selected_a_video(String filePath) {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        // Get absolute path of the slides file
        File f = new File(filePath);
        String presentationPath = f.getAbsolutePath();

        String pageProp = Util.toYamlPropertyString("on-demand-video-select-video");
        String locator = PropertiesUtil.getProperty(pageProp);

        browserAssertionSteps.the_element_exists("on-demand-video-select-video");

        DateTimeFormatter.waitForSomeTime(2);

        SeleniumUtils.findElement(driver, By.xpath(locator), "THE PPT FILE UPLOAD INPUT ELEMENT IS NOT FOUND ON THE PRESENTER SCREEN!").sendKeys(presentationPath);

        System.out.println("Presentation file path: " + presentationPath);

    }

    @Given("^I have started to upload a video$")
    public void i_have_started_to_upload_a_video() {

        //i_have_selected_a_video("SampleVideo_1280x720_20mb.mp4");
        i_have_selected_a_video("20minutes.mp4");

        browserAssertionSteps.the_element_exists("on-demand-video-auto-capture-image-selected");

        browserActionSteps.i_select_by_visible_text("on-demand-video-upload-location", "Europe");

        try {
            browserActionSteps.i_click("on-demand-video-upload-button", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Given("^I have selected a \"(.*)\" feature image file for video upload$")
    public void i_have_selected_a_feature_image_file_for_video_upload(String filePath) {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        // Get absolute path of the slides file
        //File f = new File(filePath);
        //String presentationPath = f.getAbsolutePath();

        String presentationPath = System.getProperty("user.dir") + "/src/main/resources/" + filePath;

        String pageProp = Util.toYamlPropertyString("on-demand-video-feature-image-select");
        String locator = PropertiesUtil.getProperty(pageProp);

        browserAssertionSteps.the_element_exists("on-demand-video-feature-image-select");

        DateTimeFormatter.waitForSomeTime(2);

        SeleniumUtils.findElement(driver, By.xpath(locator), "THE FEATURE IMAGE FILE UPLOAD INPUT ELEMENT IS NOT FOUND ON THE PRESENTER SCREEN!").sendKeys(presentationPath);

        System.out.println("FEATURE IMAGE file path: " + presentationPath);

    }
}
