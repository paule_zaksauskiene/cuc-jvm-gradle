package com.brighttalk.qa.stepdefs.ondemandvideo;

import brighttalktest.messagebus.FakeDialIn;
import com.brighttalk.qa.api.ChannelApi;
import com.brighttalk.qa.common.EntityAttributeResolver;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.scenarios.UserHandler;
import com.brighttalk.qa.siteElements.WebcastPro;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

@ContextConfiguration(
        classes = TestConfig.class)

public class OndemandVideoActionSteps {

    @Autowired
    BrightTALKService service;

    @Autowired
    EntityAttributeResolver eaResolver;

    @Autowired
    World world;

    @Autowired
    Pages pages;

    @Autowired
    SharedChromeDriver driver;

    @Autowired
    BrowserAssertionSteps browserAssertionSteps;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    @When("^I click button to remove feature image$")
    public void clickSendRatingButton(){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        // We need to wait here for the button to be fully clickable as it takes time for it to enable after entering the text
        DateTimeFormatter.waitForSomeTime(5);

        String pageProp = Util.toYamlPropertyString("on-demand-video-feature-image-remove");
        String locator = PropertiesUtil.getProperty(pageProp);

        WebElement element = SeleniumUtils.findElement(driver, By.xpath(locator), "THE \"SEND RATING\" BUTTON IS NOT FOUND ON THE PLAYER TEST PAGE!");

        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);

        //Actions act = new Actions(driver);
        //act.moveToElement(element).click().build().perform();

    }
}

