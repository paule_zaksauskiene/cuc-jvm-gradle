package com.brighttalk.qa.stepdefs.presenter;

import com.brighttalk.qa.common.EntityAttributeResolver;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(
        classes = TestConfig.class)
public class PresenterPreconditionSteps {

    @Autowired
    BrightTALKService brightTALKService;

    @Autowired
    EntityAttributeResolver eaResolver;

    @Autowired
    World world;

    @Autowired
    SharedChromeDriver driver;

    @Autowired
    Pages pages;

    com.brighttalk.qa.siteElements.User managerUser;

    public String readWebcastPin(){

        SeleniumUtils.findElement(driver, By.linkText("Presenter instructions"), "THE LINK TO PRESENTER INSTRUCTIONS IS NOT FOUND ON THE PREPARE AND PRESENT PAGE!").click();

        String myPinText = SeleniumUtils.findElement(driver, By.xpath("//div[@id='presenterInstructionsContent']/p[contains(.,'PIN:')]"), "THE PRESENTER INSTRUCTIONS WITH PIN IS NOT FOUND ON THE PREPARE AND PRESENT PAGE!").getText();;

        int startOfPin = myPinText.indexOf("PIN:");
        String myPin = myPinText.substring(startOfPin + 5, startOfPin + 13);
        System.out.println (" [+] Webcast Pin: " + myPin);
        return myPin;

    }

    @Then("^resource is allocated")
    public void resourceIsAllocated(){

        DateTimeFormatter.waitForSomeTime(10); // The red error is not immediatelly present, so 10 secs is recommended time to wait

        String pageProp = Util.toYamlPropertyString("presenter-screen-red-error");
        String locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.verifyElementNotPresent(driver, By.xpath(locator), "THE RED \"SYSTEM ERROR\" MESSAGE IS SHOWN ON THE PRESENTER SCREEN, THE TEST WILL STOP!");


    }
}
