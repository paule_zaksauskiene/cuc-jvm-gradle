package com.brighttalk.qa.stepdefs.presenter;

import com.brighttalk.qa.common.EntityAttributeResolver;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.helper.WebElementWaiter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.scenarios.WebcastHandler;
import com.brighttalk.qa.siteElements.Timezone;
import com.brighttalk.qa.siteElements.Vote;
import com.brighttalk.qa.siteElements.WebcastPro;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import static junit.framework.TestCase.assertTrue;

@ContextConfiguration(
        classes = TestConfig.class)
public class PresenterAssertionSteps {

    @Autowired
    BrightTALKService brightTALKService;

    @Autowired
    EntityAttributeResolver eaResolver;

    @Autowired
    World world;

    @Autowired
    SharedChromeDriver driver;

    @Autowired
    Pages pages;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Autowired
    BrowserAssertionSteps browserAssertionSteps;

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    com.brighttalk.qa.siteElements.User managerUser;

    @Then("^the vote appears on the presenter screen$")
    public void the_vote_appears_on_the_presenter_screen() {

        String pageProp = Util.toYamlPropertyString("vote-item");
        String locator = PropertiesUtil.getProperty(pageProp);

        List<WebElement> votes = driver.findElements(By.cssSelector(locator));

        assertTrue(votes.size() == new Vote[]{world.getWebcastsPro().get(0).getWebcastVotes().get(0)}.length);


        for (int i = 0; i < votes.size(); i++) {

            WebElement vote = votes.get(i);

            pageProp = Util.toYamlPropertyString("vote-question-title-item");
            locator = PropertiesUtil.getProperty(pageProp);

            String voteQuestionOnPage = vote.findElement(By.cssSelector(locator)).getText();
            voteQuestionOnPage = vote.findElement(By.cssSelector(locator)).getText();
            String questionToCompare = new Vote[]{world.getWebcastsPro().get(0).getWebcastVotes().get(0)}[i].getVoteQuestion();

            assertTrue(
                    voteQuestionOnPage.contains(questionToCompare)
            );

            assertTrue(vote.findElement(By.cssSelector(".icon-bar-chart")).isDisplayed());
        }

    }

    @Then("^the vote details appear on the presenter screen$")
    public void the_vote_details_appear_on_the_presenter_screen() {

        // Verify the vote details are as expected on the presenter screen (vote question)
        // Important to wait, as the immediate check may cause "org.openqa.selenium.StaleElementReferenceException: Element is no longer attached to the DOM" issue

        String pageProp = Util.toYamlPropertyString("vote-details-question-title");
        String locator = PropertiesUtil.getProperty(pageProp);

        String questionOnScreen = SeleniumUtils.findElement(driver, By.xpath(locator), "").getText();
        Assert.assertTrue(questionOnScreen.trim().contains(world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteQuestion()));

        List<WebElement> answers = driver.findElements(By.cssSelector(".question-options.jsQuestionOptions > li"));

        Assert.assertEquals(answers.size(), world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteOptions().size());

        int j;
        for(int i = 0; i < world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteOptions().size(); i ++) {

            j = i + 1;
            SeleniumUtils.findElement(driver, By.xpath("//ul[contains(@class, 'jsQuestionOptions')]/li[" + j + "][contains(@class, 'jsVoteOption')]/p[contains(.,'" + world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteOptions().get(i) + "')]"), "THE VOTE OPTION " + world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteOptions().get(i) + " IS NOT FOUND ON THE WEBCAST PRESENTER PAGE!");
        }

        pageProp = Util.toYamlPropertyString("back-to-vote-list-button");
        locator = PropertiesUtil.getProperty(pageProp);

        assertTrue(SeleniumUtils.isDisplayed(driver, By.cssSelector(locator), ""));

        pageProp = Util.toYamlPropertyString("delete-vote-button");
        locator = PropertiesUtil.getProperty(pageProp);

        assertTrue(SeleniumUtils.isDisplayed(driver, By.cssSelector(locator), ""));

        pageProp = Util.toYamlPropertyString("edit-vote-button");
        locator = PropertiesUtil.getProperty(pageProp);

        assertTrue(SeleniumUtils.isDisplayed(driver, By.cssSelector(locator), ""));
    }

    @Then("^the following vote results are shown on the presenter page$")
    public void the_following_vote_results_are_shown_on_the_presenter_page(List<String> fields) {

        // Verify total number of voters
        String pageProp = Util.toYamlPropertyString("presenter-screen-total-number-of-voters");
        String locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.findElement(driver, By.xpath(locator + "[contains(.,'" + fields.get(0) + "')]"), "THE TOTAL VOTES NUMBER " + fields.get(0) + " IS NOT FOUND ON THE PRESENTER PAGE!");

        // I open the Votes section
        try {
            browserActionSteps.i_click("Votes from audience section", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        // Verify option result (only for option1)
        pageProp = Util.toYamlPropertyString("presenter-screen-vote-option-result");
        locator = PropertiesUtil.getProperty(pageProp);
        String textIseeOnScreen =  SeleniumUtils.getElementText(driver, By.xpath(locator));

        Assert.assertEquals(textIseeOnScreen.trim(), fields.get(2).trim());

    }

    @Then("^I see the Presentation ended pop up$")
    public void i_see_the_Presentation_ended_pop_up() {

        // Verify total number of voters
        String pageProp = Util.toYamlPropertyString("thank-you-for-presenting-on-brighttalk-overlay");
        String locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.findElement(driver, By.xpath(locator), 600, 60, false, "\"THE THANK YOU FOR PRESENTING ON BRIGHTTALK\" DID NOT APPEAR AFTER 10 MINUTES ON THE PRESENTER SCREEN!");

        world.getWebcastsPro().get(0).setStatus("processing");

    }

    @Then("^PPT slides are uploaded$")
    public void ppt_slides_are_uploaded() {

        // Wait untill "hidden" is removed from jsSideControls panel
        String pageProp = Util.toYamlPropertyString("powerpoint-preview-slides-hidden-element");
        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.verifyElementDisappearsFromPageWithoutRefresh(driver, By.xpath(locator), 3000, 5, "THE PPT SLIDES UPLOAD DID NOT FINISH AFTER 20 MINUTES ON THE PRESENTER SCREEN!");

        // Wait until Preview your slides is shown
        browserAssertionSteps.the_element_exists("preview-slides-element");

        // Verify the icon shows the slides are added
        browserAssertionSteps.the_element_exists("powerpoint-added-slides-icon");

    }

    @Then("^the number of slides is (.*)$")
    public void the_number_of_slides_is(String slideNumber) {

        // Wait untill "hidden" is removed from jsSideControls panel
        String pageProp = Util.toYamlPropertyString("powerpoint-slides-number");
        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.findElement(driver, By.xpath(locator + "[contains(.,'" + slideNumber +  "')]"), "");

    }

    @Then("^I should see a webinar title$")
    public void i_should_see_a_webinar_element_on_presenter_screen() {

        // Wait untill "hidden" is removed from jsSideControls panel
        String pageProp = Util.toYamlPropertyString("presenter-screen-webinar-title");
        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.findElement(driver, By.xpath(locator + "[contains(.,'" + world.getWebcastsPro().get(0).getTitle() +  "')]"), "");

    }

    @Then("^I should see a webinar presenter$")
    public void i_should_see_a_webinar_presenter_screen() {

        // Wait untill "hidden" is removed from jsSideControls panel
        String pageProp = Util.toYamlPropertyString("presenter-screen-webinar-presenter");
        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.findElement(driver, By.xpath(locator + "[contains(.,'" + world.getWebcastsPro().get(0).getPresenter() +  "')]"), "");

    }

    @Then("^I should see a webinar duration")
    public void i_should_see_a_webinar_duration() {

        // Wait untill "hidden" is removed from jsSideControls panel
        String pageProp = Util.toYamlPropertyString("presenter-screen-webinar-duration");
        String locator = PropertiesUtil.getProperty(pageProp);

        String durationText = SeleniumUtils.findElement(driver, By.xpath(locator), "").getText();
        Assert.assertTrue("THE DURATION DISPLAYED ON THE PAGE: " + durationText + " IS NOT THE SAME AS EXPECTED :" + world.getWebcastsPro().get(0).getDuration().toString(), durationText.contains(world.getWebcastsPro().get(0).getDuration()));

    }

    @Then("^I should see a webinar description")
    public void i_should_see_a_webinar_description() {

        // Wait untill "hidden" is removed from jsSideControls panel
        String pageProp = Util.toYamlPropertyString("presenter-screen-webinar-duration");
        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.findElement(driver, By.xpath(locator + "[contains(.,'" + world.getWebcastsPro().get(0).getDescription() +  "')]"), "");

    }

    @Then("^I should see the time left before live")
    public void i_should_see_the_time_left_before_live() {

        // Wait untill "hidden" is removed from jsSideControls panel
        String pageProp = Util.toYamlPropertyString("presenter-screen-webinar-duration");
        String locator = PropertiesUtil.getProperty(pageProp);

        for (int i = 0; i < 4; i ++){

            // verifies time left
            Calendar myCurrentTime = Calendar.getInstance();
            String myWebcastTimezoneName = Timezone.timezoneList.get(world.getWebcastsPro().get(0).getTimezone());
            myCurrentTime.setTimeZone(TimeZone.getTimeZone(myWebcastTimezoneName));

            long myCurrentTimeInMillis = myCurrentTime.getTimeInMillis();
            long timeDifferenceInMillis =  world.getWebcastsPro().get(0).getStartDate().getTimeInMillis() - myCurrentTimeInMillis;
            int howManyDaysLeft = (int) (timeDifferenceInMillis / 86400000);
            int howManyHoursLeft = (int) (timeDifferenceInMillis - (howManyDaysLeft * 86400000)) / 3600000;
            String dateTimeDisplay = "LIVE in: " + howManyDaysLeft + "d " + howManyHoursLeft + "h";
            //System.out.println("I Expect to see: " + dateTimeDisplay);

            String dateTimeOnPage = SeleniumUtils.findElement(driver, By.xpath("//li[@class='jsTimer last communication-timer']"), "").getText();

            if (dateTimeOnPage.contains(dateTimeDisplay)) {

                break;

            } else {

                if (i == 3) Assert.fail("Wrong time IS SHOWN on the presenter screen.");

                try {
                    Thread.sleep(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                this.driver.navigate().refresh();
                try {
                    browserAssertionSteps.the_page_opens("presenter-page");
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }

                DateTimeFormatter.waitForSomeTime(1);

                pageProp = Util.toYamlPropertyString("presenter-screen-loading-overlay-disappeared");
                String locator_presenter_screen_loading_overlay_disappeared = PropertiesUtil.getProperty(pageProp);

                SeleniumUtils.findElement(driver, By.xpath(locator_presenter_screen_loading_overlay_disappeared), 180, 1, false, "THE PRESENTER SCREEN LOADING POP UP DID NOT DISAPPEAR AFTER 3 MINUTES");
            }

            i ++;
        }
    }


    @Then("^the presenter screen is not showing content changing status")
    public void the_presenter_screen_is_not_showing_content_changing_status() {

        browserAssertionSteps.the_element_exists("presenter-screen-bridge-preparing-screen");

    }

    @Then("^inactive slides icon becomes grey")
    public void inactive_slides_icon_becomes_grey() {

        String pageProp = Util.toYamlPropertyString("presenter-screen-powerpoint-slides-tab-icon-color");
        String locator = PropertiesUtil.getProperty(pageProp);

        // Wait until the inactive slides icon becomes grey (it does not happen immediatelly)
        int n = 6;
        while (n > 0) {

             String color =  SeleniumUtils.findElement(driver, By.xpath(locator), "THE SLIDES TAB ICON IT NOT FOUND ON THE PRESENTER SCREEN").getCssValue("color");

             System.out.println("The inactive icons tab color is : " + color.trim());

             if (color.trim().contains("rgba(129, 129, 129, 1)")) {
             break;

        } else {

            try {
                    Thread.sleep(20000);
              } catch (InterruptedException e) {
                    e.printStackTrace();
              }

         }

        n = n - 1;
        if (n == 1) Assert.fail("Slides icon is not grey");

        }
    }

    @Then("^attachment (.*) is shown on the page")
    public void attachment_is_shown_on_the_page(String attachmentNumber){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String pageProp = Util.toYamlPropertyString("presenter-page-attachments-list");
        String locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.findElement(driver, By.xpath(locator + "[contains(.,'" + world.getWebcastsPro().get(0).getWebcastAttachments().get(Integer.parseInt(attachmentNumber) - 1).getAttachmentTitle() + "')]"), "");

    }

    @Then("^I should see the rating count (.*)")
    public void i_should_see_rating_count(String ratingCount){

        String pageProp = Util.toYamlPropertyString("presenter-page-rating-count");
        String locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.findElement(driver, By.xpath(locator + "[contains(text(), '" + ratingCount.trim() + "')]"), "");

    }

    // The ratingCount is the number of the rating which needs to be verified (for example, if there is 3 ratings on the screen,
    // the second rating will be verified by specifying parameter ratingCount as "2".
    @Then("^I should see the (.*) rating with (.*) stars")
    public void verifyRatingStarsOnPresentingScreen(String ratingCount, String howManyStars){

        String startPercentage = "";
        if (howManyStars.equals("1")) startPercentage = "22";
        if (howManyStars.equals("2")) startPercentage = "40";
        if (howManyStars.equals("3")) startPercentage = "59";
        if (howManyStars.equals("4")) startPercentage = "77";
        if (howManyStars.equals("5")) startPercentage = "96";

        System.out.println("The xpath: " + "//ul[contains(@class, 'jsFeedbackList')]/li[" + ratingCount + "]/div/div/div[contains(@class, 'stars rating')][contains(@style, '" + startPercentage + ".')]");
        SeleniumUtils.findElement(driver, By.xpath("//ul[contains(@class, 'jsFeedbackList')]/li[" + ratingCount + "]/div/div/div[contains(@class, 'stars rating')][contains(@style, '" + startPercentage + "')]"), "THE RATING VALUE IS NOT AS EXPECTED ON THE PRESENTER SCREEN");
    }

    @Then("^the live support is enabled on support hours")
    public void the_live_support_is_enabled_on_support_hours(){

        //If webinar is within support hours verify the live support dialog
        boolean isWebinarWithinSupportHours = new WebcastHandler().verifyWebinarStartsWithinLiveSupportWorkingHours(world.getWebcastsPro().get(0));

        if (isWebinarWithinSupportHours) {

            String agentsAvailable = SeleniumUtils.findElement(driver, By.xpath("//*[@class='lsd-no-agents-msg']"), "THE ELEMENT TO IDENTIFY IF SUPPORT AGENTS ARE AVAILABLE IS NOT FOUND ON THE PRESENTER SCREEN!").getCssValue("display");

            if (agentsAvailable.contains("none")) {

                SeleniumUtils.findElement(driver, By.xpath("//section[contains(@class, 'live-chat-support')]/button[contains(.,'Get live support now')]"), 600, 1, false, "LIVE SUPPORT WAS NOT ENABLED AFTER WAITING FOR 10 MINUTES ON THE PRESENTER SCREEN.");

                String supportButtonXpath = "//button[contains(@class, 'get-support-btn')]";

                WebDriverWait wait = new WebDriverWait(driver, 30);
                wait.until(ExpectedConditions.elementToBeClickable(By.xpath(supportButtonXpath)));

                SeleniumUtils.findElement(driver, By.xpath(supportButtonXpath), 60, 1, false, "THE LIVE SUPPORT BUTTON IS NOT FOUND ON THE PRESENTER SCREEN").click();

                // Verify support dialog
                SeleniumUtils.findElement(driver, By.xpath("//div[@id='SnapABug_CL']/div[2][contains(.,'Hello, how can I help you today?')]"), 60, 1, false, "THE \"HELLO, HOW CAN I HELP YOU TODAY\" IS NOT SHOWN ON THE PRESENTER SCREEN LIVE SUPPORT DIALOG BOX.");

                driver.findElement(By.xpath("//li[contains(@class, 'live-support-disabled')]/h2/span[contains(text(), 'Live presenter support')]"));
                String textOnStapABugElement = SeleniumUtils.findElement(driver, By.id("SnapABug_CE"), "").getText();
                Assert.assertTrue("THE LIVE SUPPORT DIALO BOX IS NOT OPENED EMPTY AS EXPECTED ON THE PRESENTER SCREEN!", "".contains(textOnStapABugElement));

                // Wait for Close button be clickable
                DateTimeFormatter.waitForSomeTime(1);
                //  WebDriverWait wait = new WebDriverWait(driver, 15);
                // wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("img[alt=\"Close\"]")));
                // SeleniumUtils.findElement(driver, By.cssSelector("img[alt=\"Close\"]"), "THE CLOSE LIVE SUPPORT DIALOG BUTTON IS NOT FOUND ON THE PRESENTER PAGE").click();

            }
        }
    }

    // Verify the details of the pro webinar is correctly displayed on the page
    @Then("^webinar details are shown on the presenting page")
    public void verifyWebcastProDetailsOnThePage(){

        WebcastPro myWebcastPro = world.getWebcastsPro().get(0);

        //Verify title
        driver.findElement(By.xpath("//h2[contains(.,'" + myWebcastPro.getTitle() + "')]"));

        // Verify time left
        for (int i = 0; i < 4; i ++){

            driver.findElement(By.xpath("//div[@class='BTWecastTimer upcoming']/p[contains(.,'LIVE IN:')]"));

            Calendar myCurrentTime = Calendar.getInstance();
            String myWebcastTimezoneName = Timezone.timezoneList.get(myWebcastPro.getTimezone());
            myCurrentTime.setTimeZone(TimeZone.getTimeZone(myWebcastTimezoneName));

            long myCurrentTimeInMillis = myCurrentTime.getTimeInMillis();
            long timeDifferenceInMillis =  myWebcastPro.getStartDate().getTimeInMillis() - myCurrentTimeInMillis;
            int howManyDaysLeft = (int) (timeDifferenceInMillis / 86400000);
            int howManyHoursLeft = (int) (timeDifferenceInMillis - (howManyDaysLeft * 86400000)) / 3600000;

            String dateTimeDisplay = howManyDaysLeft + "d " + howManyHoursLeft + "h";
            System.out.println("I Expect to see: " + dateTimeDisplay);
            String dateTimeOnPage = driver.findElement(By.xpath("//div[@class='BTWecastTimer upcoming']/p[2]")).getText();
            System.out.println("I see on the page: " + dateTimeOnPage);

            if (dateTimeOnPage.contains(dateTimeDisplay)) {

                break;

            } else {

                if (i == 3) Assert.fail("Wrong time found on the presenter screen.");

                try {
                    Thread.sleep(60000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                this.driver.navigate().refresh();

                try {
                    browserAssertionSteps.the_page_opens("presenting-page-metatag");
                } catch (Throwable throwable) {
                    throwable.printStackTrace();
                }
            }

            i++;
        }

        // Verify presenter instructions tab
        driver.findElement(By.linkText("Presenter instructions")).click();
        new WebElementWaiter().waitAndFindByXpath(driver, "//div[@id='presenterInstructions'][@style='display: block;']", 60, 1, false);

        // Verify title on Presenter Instructions tab
        driver.findElement(By.xpath("//div[@id='presenterInstructions']/div/div/p[contains(.,'" + myWebcastPro.getTitle() + "')]"));

        // Verify date
        String dateTimeExpectToSee = DateTimeFormatter.presenterInstructionsDateTimeFormat(myWebcastPro);

        String dateTimeISeeOnThePage = SeleniumUtils.findElement(driver, By.xpath("//div[@id='presenterInstructionsContent']"), "THE PRESENTER INSTRUCTIONS CONTENT IS NOT FOUND ON THE PRESENTING SCREEN PAGE!").getText();

        System.out.println("I see on the page: " + dateTimeISeeOnThePage);
        System.out.println("I expect to see: " + dateTimeExpectToSee);

        //This line fails intermittently
        SeleniumUtils.findElement(driver, By.xpath("//*[@id='presenterInstructionsContent'][contains(.,'" + dateTimeExpectToSee + "')]"), "THE DATE AND TIME " + dateTimeExpectToSee +  "IS NOT SHOWN ON THE PRESENTING SCREEN PAGE, SEEING " + dateTimeISeeOnThePage + " INSTEAD.");

        SeleniumUtils.findElement(driver, By.xpath("//*[@id='webcastTimezone'][contains(.,'" + myWebcastPro.getTimezone() + "')]"), "THE TIMEZONE IS NOT FOUND ON THE PRESENTING SCREEN PAGE!");

        // Verify duration
        driver.findElement(By.xpath("//*[contains(.,'" + myWebcastPro.getDuration() + "')]"));

    }

}
