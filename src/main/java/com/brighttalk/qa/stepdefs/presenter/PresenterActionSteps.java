package com.brighttalk.qa.stepdefs.presenter;

import com.brighttalk.qa.api.WebcastProApi;
import com.brighttalk.qa.common.EntityAttributeResolver;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.siteElements.Vote;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Calendar;

@ContextConfiguration(
        classes = TestConfig.class)
public class PresenterActionSteps {

    @Autowired
    BrightTALKService brightTALKService;

    @Autowired
    EntityAttributeResolver eaResolver;

    @Autowired
    World world;

    @Autowired
    SharedChromeDriver driver;

    @Autowired
    Pages pages;

    @Autowired
    UrlResolver urlResolver;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Autowired
    BrowserAssertionSteps browserAssertionSteps;

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    com.brighttalk.qa.siteElements.User managerUser;

    @When("^I enter all vote answers$")
    public void i_enter_vote_answers() {

       Vote vote = new Vote();
       world.getWebcastsPro().get(0).getWebcastVotes().add(vote);

       String pageProp = Util.toYamlPropertyString("add-vote-answer-one");
       String locator = PropertiesUtil.getProperty(pageProp);
       SeleniumUtils.clickElement(driver, By.cssSelector(locator));
       SeleniumUtils.sendKeys(driver, By.cssSelector(locator), vote.getVoteOptions().get(0));

       pageProp = Util.toYamlPropertyString("add-vote-answer-two");
       locator = PropertiesUtil.getProperty(pageProp);
       SeleniumUtils.clickElement(driver, By.cssSelector(locator));
       SeleniumUtils.sendKeys(driver, By.cssSelector(locator), vote.getVoteOptions().get(1));

       pageProp = Util.toYamlPropertyString("add-vote-answer-three");
       locator = PropertiesUtil.getProperty(pageProp);
       SeleniumUtils.clickElement(driver, By.cssSelector(locator));
       SeleniumUtils.sendKeys(driver, By.cssSelector(locator), vote.getVoteOptions().get(2));

       pageProp = Util.toYamlPropertyString("add-vote-answer-four");
       locator = PropertiesUtil.getProperty(pageProp);
       SeleniumUtils.clickElement(driver, By.cssSelector(locator));
       SeleniumUtils.sendKeys(driver, By.cssSelector(locator), vote.getVoteOptions().get(3));

       pageProp = Util.toYamlPropertyString("add-vote-answer-five");
       locator = PropertiesUtil.getProperty(pageProp);
       SeleniumUtils.clickElement(driver, By.cssSelector(locator));
       SeleniumUtils.sendKeys(driver, By.cssSelector(locator), vote.getVoteOptions().get(4));

    }

    @When("^I enter vote question$")
    public void i_enter_vote_question() {

        Vote vote = new Vote();

        String pageProp = Util.toYamlPropertyString("add-vote-question");
        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.clickElement(driver, By.cssSelector(locator));
        SeleniumUtils.sendKeys(driver, By.cssSelector(locator), vote.getVoteQuestion());

        world.getWebcastsPro().get(0).getWebcastVotes().add(vote);

    }

    @When("^I enter two duplicate vote answers$")
    public void i_enter_two_duplicate_vote_answers() {

        String pageProp = Util.toYamlPropertyString("add-vote-answer-one");
        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.clickElement(driver, By.cssSelector(locator));
        SeleniumUtils.sendKeys(driver, By.cssSelector(locator), "test_answer_duplicate");

        pageProp = Util.toYamlPropertyString("add-vote-answer-two");
        locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.clickElement(driver, By.cssSelector(locator));
        SeleniumUtils.sendKeys(driver, By.cssSelector(locator), "test_answer_duplicate");

    }

    @When("^I enter \"([^\"]*)\" vote answer$")
    public void i_enter_vote_answer(String answerNumber) {

        if (answerNumber.contains("first")) {

            String pageProp = Util.toYamlPropertyString("add-vote-answer-one");
            String locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.clickElement(driver, By.cssSelector(locator));
            SeleniumUtils.sendKeys(driver, By.cssSelector(locator), world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteOptions().get(0));
        }

        if (answerNumber.contains("second")) {

            String pageProp = Util.toYamlPropertyString("add-vote-answer-two");
            String locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.clickElement(driver, By.cssSelector(locator));
            SeleniumUtils.sendKeys(driver, By.cssSelector(locator), world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteOptions().get(1));
        }

        if (answerNumber.contains("third")) {

            String pageProp = Util.toYamlPropertyString("add-vote-answer-three");
            String locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.clickElement(driver, By.cssSelector(locator));
            SeleniumUtils.sendKeys(driver, By.cssSelector(locator), world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteOptions().get(2));
        }

        if (answerNumber.contains("fourth")) {

            String pageProp = Util.toYamlPropertyString("add-vote-answer-four");
            String locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.clickElement(driver, By.cssSelector(locator));
            SeleniumUtils.sendKeys(driver, By.cssSelector(locator), world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteOptions().get(3));
        }

        if (answerNumber.contains("fifth")) {

            String pageProp = Util.toYamlPropertyString("add-vote-answer-five");
            String locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.clickElement(driver, By.cssSelector(locator));
            SeleniumUtils.sendKeys(driver, By.cssSelector(locator), world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteOptions().get(4));
        }

    }

    @When("^I click the Vote view button$")
    public void i_click_the_Vote_view_button() {

        String pageProp = Util.toYamlPropertyString("vote-view-button");
        String locator = PropertiesUtil.getProperty(pageProp);

        //locator = locator + "[data-id='" + world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteId() +"'] > span";

        SeleniumUtils.clickElement(driver, By.cssSelector(locator));

        DateTimeFormatter.waitForSomeTime(2);


    }

    @When("^I edit vote question as \"([^\"]*)\"$")
    public void i_edit_vote_question_as(String voteQuestion) {

        String pageProp = Util.toYamlPropertyString("add-vote-question");
        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.clickElement(driver, By.cssSelector(locator));
        SeleniumUtils.sendKeys(driver, By.cssSelector(locator), voteQuestion);

        world.getWebcastsPro().get(0).getWebcastVotes().get(0).setVoteQuestion(voteQuestion);

    }

    @When("^I enter vote question as \"([^\"]*)\"$")
    public void i_enter_vote_question_as(String voteQuestion) {

        Vote vote = new Vote();
        vote.setVoteQuestion(voteQuestion);

        String pageProp = Util.toYamlPropertyString("add-vote-question");

        String locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.clickElement(driver, By.cssSelector(locator));

        SeleniumUtils.sendKeys(driver, By.cssSelector(locator), vote.getVoteQuestion());

        world.getWebcastsPro().get(0).getWebcastVotes().add(vote);

    }

    @When("^I edit vote answer \"([^\"]*)\" as \"([^\"]*)\"$")
    public void i_edit_vote_answer_as(String voteAnswerNumber, String voteAnswer) {

        String pageProp = "";

        if (voteAnswerNumber.contains("1")) {

            pageProp = Util.toYamlPropertyString("add-vote-answer-one");

        }

        if (voteAnswerNumber.contains("2")) {

            pageProp = Util.toYamlPropertyString("add-vote-answer-two");

        }

        if (voteAnswerNumber.contains("3")) {

            pageProp = Util.toYamlPropertyString("add-vote-answer-three");

        }

        if (voteAnswerNumber.contains("4")) {

            pageProp = Util.toYamlPropertyString("add-vote-answer-four");

        }

        if (voteAnswerNumber.contains("5")) {

            pageProp = Util.toYamlPropertyString("add-vote-answer-five");

        }

        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.clickElement(driver, By.cssSelector(locator));
        SeleniumUtils.sendKeys(driver, By.cssSelector(locator), voteAnswer);

        int voteAnswerNumberInt = Integer.parseInt(voteAnswerNumber.trim()) - 1;

        world.getWebcastsPro().get(0).getWebcastVotes().get(0).getVoteOptions().set(voteAnswerNumberInt, voteAnswer);

    }


    @When("^I see the (.*) minutes overrun pop up with (.*) mins left$")
    public void i_see_the_overrun_pop_up_at_2_minutes_1_seconds(String numberOfOverrunMinutes, String numberOfEndInMinutes) {

        String pageProp = Util.toYamlPropertyString("presentation-overruning-overlay");
        String locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.findElement(driver, By.xpath(locator), 180, 1, false, "THE TEXT \"This presentation is overrunning by\" IS NOT FOUND ON THE PRESENTER PAGE!");

        pageProp = Util.toYamlPropertyString("presentation-overrun-timer");
        locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.findElement(driver, By.xpath(locator + "[contains(text(), '0" + numberOfOverrunMinutes + ":')]"), "THE NUMBER OF OVERRUN MINUTES IS NOT AS EXPECTED ON THE PRESENTER SCREEN OVERRUN POP UP SCREEN.");

        String pageProp2 = Util.toYamlPropertyString("presentation-overrun-overlay-we-will-automatically-end-in");
        String locator2 = PropertiesUtil.getProperty(pageProp2);

        SeleniumUtils.findElement(driver, By.xpath(locator2), "THE TEXT \"WE WILL AUTOMATICALLY END IN\" IS NOT SHOWN ON THE PRESENTER SCREEN OVERRUN POP UP.");

        String pageProp3 = Util.toYamlPropertyString("presentation-overrun-end-in-timer");
        String locator3 = PropertiesUtil.getProperty(pageProp3);

        SeleniumUtils.findElement(driver, By.xpath(locator3 + "[contains(text(), '0" + numberOfEndInMinutes + ":')]"), "THE NUMBER OF \"END IN MINUTES\" IS NOT SHOWN ON THE PRESENTER SCREEN OVERRUN POP UP DIALOG BOX!");

    }

    public void verifyPresentationOverruningPopupAndCloseIt(){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String pageProp = Util.toYamlPropertyString("presentation-overruning-overlay");
        String locator = PropertiesUtil.getProperty(pageProp);

        try {

            driver.findElement(By.xpath(locator));

            System.out.println("The presentation is showing overruning pop up, which need to be closed...");

            // Close the pop up
            try {
                browserActionSteps.i_click("close-overrun-pop-up-button", "", "");
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

        } catch (NoSuchElementException ex) {
        /* do nothing, link is not present, assert is passed */
        }
    }

    /**
     * Warning ! this method works on chrome / safari only, as the /input element does not exist on Firefox because the Flash button is shown
     * for PPT upload
     * @param filePath
     */
    @When("^I select PPT file \"(.*)\" to begin upload$")
    public void uploadPowerpointSlides(String filePath) {

        DateTimeFormatter.waitForSomeTime(5); // its important to wait here as if the slides are uploaded the second time, the upload process fails if done too quickly

        // Verify the file upload element is present (but don't click on it)
        try {
            browserAssertionSteps.i_should_see_element("1", "powerpoint-upload-button", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        // Get absolute path of the slides file
        File f = new File(filePath);
        String presentationPath = f.getAbsolutePath();

        String pageProp = Util.toYamlPropertyString("powerpoint-upload-input-element");
        String locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.findElement(driver, By.xpath(locator), "THE PPT FILE UPLOAD INPUT ELEMENT IS NOT FOUND ON THE PRESENTER SCREEN!").sendKeys(presentationPath);

        System.out.println("Presentation file path: " + presentationPath);
    }

    @Given("^pro webinar is less than 30 mins before live$")
    public void pro_webinar_is_less_than_30_mins_before_live() {

        world.getWebcastsPro().get(0).setStartDate(Calendar.getInstance());
        world.getWebcastsPro().get(0).getStartDate().add(Calendar.MINUTE, 28);
        WebcastProApi.updateWebcastProAsManager(world, world.getWebcastsPro().get(0));

        DateTimeFormatter.waitForSomeTime(30); // Its important to wait here, as presenter screen may have some delay
    }

    @When("^I remove slides from presenter screen$")
    public void i_remove_slides_from_presenter_screen() {

        try {

            browserActionSteps.i_click("presenter-screen-remove-slides", "", "");
            browserActionSteps.i_click("presenter-screen-remove-slides-confirmation-button", "", "");

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        // Wait for the Remove slides button to be clickable
       //WebDriverWait wait = new WebDriverWait(driver, 30);
        //wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("p.remove-slides.jsRemoveSlides")));

        //WebElement element = driver.findElement(By.cssSelector("p.remove-slides.jsRemoveSlides"));
        //Actions actions = new Actions(driver);
        //actions.moveToElement(element).click().perform();

        // Wait for "Yes remove my slides" button to be clickable on the pop up
        //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(@class, 'jsRemoveSlides')][contains(.,'Yes remove my slides')]")));

        //SeleniumUtils.findElement(driver, By.xpath("//button[contains(@class, 'jsRemoveSlides')][contains(.,'Yes remove my slides')]"), "THE \"Yes remove my slides\" BUTTON IS NOT SHOWN WHEN EXPECTED ON THE PRESENTER SCREEN!").click();


    }

    @When("^PPT slides are removed$")
    public void pPT_slides_are_removed(){

        String pageProp = Util.toYamlPropertyString("presenter-screen-add-powerpoint-hidden-element");
        String locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.verifyElementDisappearsFromPageWithoutRefresh(driver, By.xpath(locator), 30, 1, "THE \"UPLOAD POWERPOINT\" BUTTON IS STILL HIDDEN WHEN IT SHOULD NOT ON THE PRESENTER SCREEN!");

    }

    @When ("^I close the presenters please dial in now pop up$")
    public void closePresentersPleaseDialInNowPopup(){

        DateTimeFormatter.waitForSomeTime(2); // wait a little bit before closing the pop up, because it reappears if closed too quick

        System.out.println("Waiting now for the \"Presenters please dial in now pop up\" ");

        String pageProp = Util.toYamlPropertyString("presenter-page-presenters-please-dial-in");
        String locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.findElement(driver, By.xpath(locator), 240, 1, false, "THE \"PRESENTERS PLEASE DIAL IN NOW\" POP UP DIDN'T APPEAR FOR 6 MINUTES!" );
        DateTimeFormatter.waitForSomeTime(2); // it is necessary to wait here as if the presenter screen goes blank the  pop up closes too quickly

        pageProp = Util.toYamlPropertyString("presenters-page-presenters-plase-dial-in-pop-up-close-button");
        locator = PropertiesUtil.getProperty(pageProp);

        SeleniumUtils.findElement(driver, By.xpath(locator), 30, 1, false, "THE CLOSE BUTTON FOR \"PRESENTERS PLEASE DIAL IN NOW\" POP UP DIDN'T APPEAR !" ).click();

        SeleniumUtils.findElement(driver, By.xpath("//aside[contains(@class, 'jsView')][contains(@style, 'none')]"), "PRESENTERS PLEASE DIAL POP UP IS NOT CLOSED ON THE PRESENTERS SCREEN!");
    }

    @When ("^I click a button to remove slides$")
    public void clickRemoveSlides(){

        WebElement element = SeleniumUtils.findElement(driver, By.cssSelector("p.remove-slides.jsRemoveSlides"), "THE REMOVE SLIDES BUTTON IS NOT FOUND ON THE PRESENTER SCREEN!");
        Actions actions = new Actions(driver);
        actions.moveToElement(element).click().perform();

    }

    @When ("^I click the stop presenter announcement displaying button$")
    public void clickStopAnnouncement(){

        String pageProp = Util.toYamlPropertyString("presenter-page-stop-display-message-button");
        String locator = PropertiesUtil.getProperty(pageProp);

        WebElement element = SeleniumUtils.findElement(driver, By.xpath(locator), "THE \"STOP DISPLAYING\" BUTTON IS NOT FOUND ON THE PLAYER TEST PAGE!");

        new Actions(driver).moveToElement(element).click().perform();

        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);

    }

    @When ("^I enter webinar pin$")
    public void iEnterWebinarPin(){

        browserActionSteps.i_enter_value("presenter-access-page-pin-number", world.getWebcastsPro().get(0).getPIN());

    }

    @When ("^I send the message \"(.*)\" on presenter chat$")
    public void iSendMessageOnPresenterChat(String message){

        // Expand Presenter chat
        WebDriverWait wait = new WebDriverWait(driver, 30);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[2][contains(@class, 'presenter-chat')]/h2")));

        SeleniumUtils.findElement(driver, By.xpath("//li[2][contains(@class, 'presenter-chat')]/h2"), "THE PRESENTER CHAT IS NOT FOUND ON THE PRESENTER SCREEN!").click();

        //Click on Presenter chat input, enter test message and press Enter
        By chatInputLocator = By.cssSelector(".jsChatInputBox");

        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".jsChatInputBox")));

        SeleniumUtils.findElement(driver, chatInputLocator, "THE CHAT INPUT BOX IS NOT FOUND ON THE PRESENTER SCREEN!").click();

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        SeleniumUtils.findElement(driver, chatInputLocator, "THE CHAT INPUT BOX IS NOT FOUND ON THE PRESENTER SCREEN!").sendKeys(message);

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        SeleniumUtils.findElement(driver, chatInputLocator, "THE CHAT INPUT BOX IS NOT FOUND ON THE PRESENTER SCREEN!").sendKeys(Keys.RETURN);

    }

    @When ("^share the main screen$")
    public void share_the_main_screen(){

        DateTimeFormatter.waitForSomeTime(10); // i

        Runtime runtime = Runtime.getRuntime();
        String applescriptCommand =

                "tell application \"Screen Sharing\" to activate\n" +
                        " delay 2\n" +
                        " tell application \"System Events\"\n" +
                        " tell process \"Screen Sharing\"" +
                        " click button \"Share\"\n" +
                        " delay 1\n" +
                " end tell";


        System.out.println("applescriptCommand: " + applescriptCommand);
        String[] args = {"osascript", "-e", applescriptCommand};
        try {
            Process process = runtime.exec(args);

            String line;
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(process.getInputStream()) );
            while ((line = in.readLine()) != null) {
                System.out.println(line);
            }
            in.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        // Wait for cookies to be deleted
        DateTimeFormatter.waitForSomeTime(20); // i

    }

    @When ("^I add BrightTALK screenshare$")
    public void i_add_brighttalk_screenshare(){

       try {
            browserActionSteps.i_click("presenter-screen-joinme-tab","","");
            browserActionSteps.i_click("presenter-screen-add-brighttalk-screenshare-button","","");
            DateTimeFormatter.waitForSomeTime(5);
            browserActionSteps.i_click("brighttalk-screenshare-one-computer-one-screen-button","","");
            DateTimeFormatter.waitForSomeTime(5);
            browserActionSteps.i_click("brighttalk-screenshare-start-sharing-button","","");
            DateTimeFormatter.waitForSomeTime(10);

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        if (world.getWebcastsPro().get(0).getStatus().contains("live")){

            browserAssertionSteps.the_element_exists("brighttalk-screenshare-sharing-your-screen-text");

        } else {

            browserAssertionSteps.the_element_exists("brighttalk-screenshare-preview-text", "240");
        }
    }

    @When ("^I add join.me screenshare$")
    public void i_add_joinme_screenshare(){

        try {

            browserActionSteps.i_click("presenter-screen-add-joinme-button","","");
            browserAssertionSteps.the_element_exists("presenter-screen-joinme-add-popup");
            browserActionSteps.i_enter_value("presenter-screen-joinme-add-url", "testname");

            System.out.println("I add join.me url:" + TestConfig.JOIN_ME_URL);
            System.out.println("I add join.me url:" + TestConfig.JOIN_ME_URL);

            browserActionSteps.i_enter_value("presenter-screen-joinme-add-name", TestConfig.JOIN_ME_URL);
            browserActionSteps.i_click("presenter-screen-joinme-add-popup-button", "", "");
            browserActionSteps.i_click("presenter-screen-joinme-add-popup-confirmation-button", "", "");
            browserAssertionSteps.the_element_exists("presenter-screen-joinme-panel-added");
            browserAssertionSteps.the_element_exists("presenter-screen-joinme-panel-selected");
            browserAssertionSteps.i_should_see_message("brighttalk-screenshare-preview-text", "Preview your screen share");

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @When ("^launch presenter screen$")
    public void launch_presenter_screen(){

        String pageProp = Util.toYamlPropertyString("presenting-page-presenter-screeen-button");
        String locator = PropertiesUtil.getProperty(pageProp);

        String presenterScreenUrl = SeleniumUtils.findElement(driver, By.xpath(locator), "").getAttribute("href");
        driver.get(presenterScreenUrl);

    }

    @When ("^I remove join.me screenshare$")
    public void remove_joinme_screenshare(){

        try {
            browserActionSteps.i_click("presenter-page-header-element", "", "");
            browserActionSteps.i_click("presenter-screen-joinme-remove-button","","");
            browserActionSteps.i_click("presenter-screen-joinme-remove-confirmation-button","","");
            browserAssertionSteps.the_element_exists("presenter-screen-joinme-add-enabled");

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @When ("^I remove BrightTALK screenshare$")
    public void remove_brighttalk_screenshare(){

        try {
            browserActionSteps.i_click("brighttalk-screenshare-remove-link", "","");
            browserActionSteps.i_click("brighttalk-screenshare-remove-confirmation-button", "","");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
