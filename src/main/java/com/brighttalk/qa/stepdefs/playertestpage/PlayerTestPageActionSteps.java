package com.brighttalk.qa.stepdefs.playertestpage;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.List;

@ContextConfiguration(
        classes = TestConfig.class)
public class PlayerTestPageActionSteps {

    @Autowired
    SharedChromeDriver driver;
    @Autowired
    UrlResolver urlResolver;
    @Autowired
    BrightTALKService service;
    @Autowired
    World world;
    @Autowired
    Pages pages;

    @When("^I generate a channel embed that uses the following categories$")
    public void i_generate_a_channel_embed_that_uses_the_following_categories(List<String> values) throws Throwable {

        // Wait for 5 secs as the entered details are lost if they are entered too quickly
        DateTimeFormatter.waitForSomeTime(5);

        // Enter player data
        pages.proPlayerTestPage().enterChannelID(driver, String.valueOf(world.getLocal_channels().get(0).getChannelID()));
        pages.proPlayerTestPage().selectEnvironment(driver);
        pages.proPlayerTestPage().enterCategories(driver, values);
        pages.proPlayerTestPage().clickTheShowPlayerButton(driver);
    }

    @When("^select the category \"([^\"]*)\" value \"([^\"]*)\" from the Categorization Tag Dropdown$")
    public void select_the_category_value_from_the_Categorization_Tag_Dropdown(String category, String tag) throws Throwable {

        pages.proPlayerTestPage().switchToPlayerIframe(driver);

        new Select(driver.findElement(By.name(category))).selectByVisibleText(tag);

    }
}
