package com.brighttalk.qa.stepdefs.playertestpage;


import com.brighttalk.qa.common.*;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.siteElements.WebcastPro;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtil;
import com.brighttalk.qa.utils.Util;
import com.gargoylesoftware.htmlunit.javascript.host.canvas.ext.WEBGL_debug_renderer_info;
import cucumber.api.java.en.Given;
import org.joda.time.DateTime;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(
        classes = TestConfig.class)
public class PlayerTestPagePreconditionSteps {

    @Autowired
    BrightTALKService brightTALKService;
    @Autowired
    EntityAttributeResolver eaResolver;
    @Autowired
    World world;

    @Autowired
    SharedChromeDriver sharedChromeDriver;

    @Autowired
    SharedFirefoxDriver sharedFirefoxDriver;

    @Autowired
    UrlResolver urlResolver;
    @Autowired
    Pages pages;

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Autowired
    BrowserAssertionSteps browserAssertionSteps;

    @Given("^the player test page is open$")
    public void player_test_page_is_open() throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        driver.manage().deleteAllCookies(); // delete all cookies so no BT session left
        String pageProp = Util.toYamlPropertyString("player test page");
        String url  = PropertiesUtil.getProperty(pageProp);
        url = urlResolver.resolve(url);
        SeleniumUtil.loadPage(driver, url);
        System.out.println("Player test page is open!");
    }


    /*
    The method selects the values on the player test page and generates embed code for the given language
    The language samples: english, german, spanish
     */
    @Given("^the webinar embed code is embedded in (.*) language$")
    public void webinar_embed_code_is_embedded(String language) throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        // Enter player data
        System.out.println("Channel id: " + String.valueOf(world.getLocal_channels().get(0).getChannelID()));
        pages.proPlayerTestPage().enterChannelID(driver, String.valueOf(world.getLocal_channels().get(0).getChannelID()));
        pages.proPlayerTestPage().selectEnvironment(driver);
        WebcastPro myProWebinar =  world.getWebcastsPro().get(0);
        pages.proPlayerTestPage().enterCommunicationID(driver, String.valueOf(myProWebinar.getWebcastID()));

        String languageToSelect = "";
        if (language.toLowerCase().contains("english")) languageToSelect = "en-US";
        if (language.toLowerCase().contains("german")) languageToSelect = "de-DE";
        if (language.toLowerCase().contains("spanish")) languageToSelect = "es-ES";
        if (language.toLowerCase().contains("french")) languageToSelect = "fr-FR";
        if (language.toLowerCase().contains("italian")) languageToSelect = "it-IT";
        if (language.toLowerCase().contains("japanese")) languageToSelect = "ja-JP";
        if (language.toLowerCase().contains("korean")) languageToSelect = "ko-KR";
        if (language.toLowerCase().contains("portugese")) languageToSelect = "pt-BR";
        if (language.toLowerCase().contains("russian")) languageToSelect = "ru-RU";
        if (language.toLowerCase().contains("chinese")) languageToSelect = "zh-CN";
        if (language.toLowerCase().contains("none")) languageToSelect = "none";
        if (language.toLowerCase().contains("en-us-elsevier")) languageToSelect = "en-US";
        if (language.toLowerCase().contains("en-us-generic")) languageToSelect = "en-US";
        if (language.toLowerCase().contains("en-us-hsbc")) languageToSelect = "en-US";
        if (language.toLowerCase().contains("en-us-salesforce")) languageToSelect = "en-US";

        pages.proPlayerTestPage().selectLanguage(driver, languageToSelect);
        pages.proPlayerTestPage().clickTheShowPlayerButton(driver);

        DateTimeFormatter.waitForSomeTime(3); // we need to wait as the form does not show RAW iframe code quickly enough

        // Copy the embed code
        String embedCodeValue = pages.proPlayerTestPage().copyEmbedCode(driver);

        // if its not traditonal language it was not on the player test page
        // means we need to replace it manually on the embed code
        if ((language.toLowerCase().contains("en-us-elsevier"))
                || language.toLowerCase().contains("en-us-generic")
                || language.toLowerCase().contains("en-us-hsbc")
                || language.toLowerCase().contains("en-us-salesforce")){

            int languageTagTitlePosition =  embedCodeValue.indexOf("language");
            String stringAfterLanguage = embedCodeValue.substring(languageTagTitlePosition + 13);
            int languageTagValueEnds =  stringAfterLanguage.indexOf("\"");
            String currentLanguageValue = stringAfterLanguage.substring(0, languageTagValueEnds);
            System.out.println("Current language value " + currentLanguageValue);
            embedCodeValue = embedCodeValue.replace(currentLanguageValue, language.toLowerCase());

        }

        // Copy the embed code
        world.setCommunicationEmbedCode(embedCodeValue);

        System.out.println("Communication embed code: " + world.getCommunicationEmbedCode());
    }

    /*
    The method selects the values on the player test page and generates channel embed code for the given language
    The language samples: english, german, spanish
     */
    @Given("^the channel listing page is generated in (.*) language$")
    public void channel_embed_code_is_embedded(String language) throws Throwable {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        // Enter player data
        System.out.println("Channel id: " + String.valueOf(world.getLocal_channels().get(0).getChannelID()));

        pages.proPlayerTestPage().enterChannelID(driver, String.valueOf(world.getLocal_channels().get(0).getChannelID()));

        pages.proPlayerTestPage().selectEnvironment(driver);

        select_language(language);

        pages.proPlayerTestPage().clickTheShowPlayerButton(driver);

    }

    /**
     * This is to select the language on the player test page
     * @param language
     */

    public void select_language(String language){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        String languageToSelect = "";
        if (language.toLowerCase().contains("english")) languageToSelect = "en-US";
        if (language.toLowerCase().contains("german")) languageToSelect = "de-DE";
        if (language.toLowerCase().contains("spanish")) languageToSelect = "es-ES";
        if (language.toLowerCase().contains("french")) languageToSelect = "fr-FR";
        if (language.toLowerCase().contains("italian")) languageToSelect = "it-IT";
        if (language.toLowerCase().contains("japanese")) languageToSelect = "ja-JP";
        if (language.toLowerCase().contains("korean")) languageToSelect = "ko-KR";
        if (language.toLowerCase().contains("portugese")) languageToSelect = "pt-BR";
        if (language.toLowerCase().contains("russian")) languageToSelect = "ru-RU";
        if (language.toLowerCase().contains("chinese")) languageToSelect = "zh-CN";
        if (language.toLowerCase().contains("none")) languageToSelect = "none";
        if (language.toLowerCase().contains("en-us-elsevier")) languageToSelect = "en-US";
        if (language.toLowerCase().contains("en-us-generic")) languageToSelect = "en-US";
        if (language.toLowerCase().contains("en-us-hsbc")) languageToSelect = "en-US";
        if (language.toLowerCase().contains("en-us-salesforce")) languageToSelect = "en-US";

        pages.proPlayerTestPage().selectLanguage(driver, languageToSelect);
    }
}
