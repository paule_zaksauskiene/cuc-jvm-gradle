package com.brighttalk.qa.stepdefs.playertestpage;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.siteElements.WebcastPro;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.junit.Assert;

import java.util.List;


@ContextConfiguration(
        classes = TestConfig.class)
public class PlayerTestPageAssertionSteps {

    @Autowired
    SharedChromeDriver driver;
    @Autowired
    UrlResolver urlResolver;
    @Autowired
    BrightTALKService service;
    @Autowired
    World world;
    @Autowired
    Pages pages;

    By featurecWebinarTitleLocator = By.xpath("//h3[contains(@class, 'content-title')]");
    By featuredWebinarPresenterLocator = By.xpath("//p[contains(@class, 'content-presenter-name')]");

    @Then("^I should not see the Categorization Tag Dropdown$")
    public void i_should_not_see_the_categorization_tag_dropdown() throws Throwable {

        DateTimeFormatter.waitForSomeTime(5); // wait for 5 seconds to make sure the right side of the page is loaded
        SeleniumUtils.verifyElementNotPresent(driver, By.xpath("//div[contains(@class, 'jsChannelCats')]/select"), "The Channel categories selection elements are found when they should not be on the page.");

    }

    @Then("^upcoming webinar details is shown on the player test page$")
    public void upcoming_webinar_details_is_shown_on_the_player_test_page() throws Throwable {

         DateTimeFormatter.waitForSomeTime(5);
         pages.proPlayerTestPage().switchToPlayerIframe(driver);

         WebcastPro myWebcast = world.getWebcastsPro().get(0);
         SeleniumUtils.verifyContainsText(driver, featurecWebinarTitleLocator, myWebcast.getTitle(), "THE WEBINAR TITLE IS NOT FOUND ON THE PLAYER TEST PAGE!");

         SeleniumUtils.verifyContainsText(driver, featuredWebinarPresenterLocator, myWebcast.getPresenter(), "THE WEBINAR PRESENTER IS NOT FOUND ON THE PLAYER TEST PAGE!");

    }

    @Then("^the following webinars appear on the channel listing$")
    public void the_following_webinars_appear_on_the_channel_listing(List<String> values) throws Throwable {

        this.upcoming_webinar_details_is_shown_on_the_player_test_page();

        WebcastPro webinar = world.getWebcastsPro().get(1);
        //SeleniumUtils.findElement(driver, By.xpath("//h3[contains(@class, 'content-title')][contains(.,'" + webinar.getTitle() + "')]"), 120, 1, false, "");

        boolean isElementDisplayed = SeleniumUtils.isDisplayed(driver, By.xpath("//h3[contains(@class, 'content-title')][contains(.,'" + webinar.getTitle() + "')]"), "The webinar is not displayed: " + webinar.getTitle());
        Assert.assertTrue("The webinar is not displayed: " + webinar.getTitle(), isElementDisplayed);

        webinar = world.getWebcastsPro().get(2);
        isElementDisplayed = SeleniumUtils.isDisplayed(driver, By.xpath("//h3[contains(@class, 'content-title')][contains(.,'" + webinar.getTitle() + "')]"), "The webinar is not displayed: " + webinar.getTitle());
        Assert.assertFalse("The webinar is not displayed: " + webinar.getTitle(), isElementDisplayed);

    }

    @Then("^the date and time is shown for the featured webinar (.*)$")
    public void the_date_and_time_is_shown_for_the_featured_webinar (String webinarNumber) throws Throwable {

        int whichWebinar = Integer.parseInt(webinarNumber.trim()) - 1;
        String dayMonthYear = DateTimeFormatter.webinarPageDateOnlyFormat(world.getWebcastsPro().get(whichWebinar));
        String hourMin = DateTimeFormatter.webinarPageOnlyTimeFormat(world.getWebcastsPro().get(whichWebinar));

        String pageProp = Util.toYamlPropertyString("player-test-page-featured-webinar-date");
        String locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.findElement(driver, By.xpath(locator + "[contains(.,'" + dayMonthYear + "')]"), 120, 1, false, "The player-test-page-featured-webinar-date is not found on the page!");

        pageProp = Util.toYamlPropertyString("player-test-page-featuredd-webinar-hourmin");
        locator = PropertiesUtil.getProperty(pageProp);
        SeleniumUtils.findElement(driver, By.xpath(locator + "[contains(.,'" + hourMin + "')]"), 120, 1, false, "The player-test-page-featuredd-webinar-hourmin is not found on the page!");

    }

    @Then("^the date and time is shown for the webinar (.*) in channel listing$")
    public void the_date_and_time_is_shown_for_the_webinar_in_channel_listing (String webinarNumber) throws Throwable {

        int whichWebinar = Integer.parseInt(webinarNumber.trim()) - 1;
        String dayMonthYear = DateTimeFormatter.webinarPageDateOnlyFormat(world.getWebcastsPro().get(whichWebinar));
        String hourMin = DateTimeFormatter.webinarPageOnlyTimeFormat(world.getWebcastsPro().get(whichWebinar));

        String webinarID = world.getWebcastsPro().get(whichWebinar).getWebcastID();

        SeleniumUtils.findElement(driver, By.xpath("//article[@data-id='" + webinarID + "']/div/div/div/time[contains(.,'" + dayMonthYear + "')]"), "");
        SeleniumUtils.findElement(driver, By.xpath("//article[@data-id='" + webinarID + "']/div/div/div/time[2][contains(.,'" + hourMin + "')]"), "");
    }
}
