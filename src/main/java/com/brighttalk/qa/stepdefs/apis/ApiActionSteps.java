package com.brighttalk.qa.stepdefs.apis;

import com.brighttalk.qa.api.RabbitMqApi;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;

@ContextConfiguration(
        classes = TestConfig.class)
public class ApiActionSteps {

    @Autowired
    World world;

    @When("^I send \"([^\"]*)\" message to live platform$")
    public void i_enter_vote_answers(String message) {

        try {
            RabbitMqApi.sendLivePlatformMessage(world.getRegressionTestsManagerUser(), world.getLocal_channels().get(0), world.getWebcastsPro().get(0), message);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Given("^I send the \"([^\"]*)\" message to Rabbit$")
    public void i_send_the_message_to_Rabbit(String message) throws TimeoutException, IOException {

        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("http://exchange.mphd01.test01.brighttalk.net:15672");
        Connection connection = null;

        connection = factory.newConnection();
        Channel channel;
        channel = connection.createChannel();
        channel.close();
        connection.close();

        System.out.println(" [x] Sent '" + message + "'");

    }
}
