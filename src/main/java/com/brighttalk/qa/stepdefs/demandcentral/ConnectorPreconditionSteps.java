package com.brighttalk.qa.stepdefs.demandcentral;

import cucumber.api.java.en.When;
import cucumber.api.java.en.Given;
import org.openqa.selenium.*;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import com.brighttalk.qa.stepdefs.demandcentral.DCLoginActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import java.io.IOException;
import com.brighttalk.qa.scenarios.ConnectorHandler;

@ContextConfiguration(
        classes = TestConfig.class)
public class ConnectorPreconditionSteps {

    @Autowired
    World world;
    @Autowired
    DCLoginActionSteps dcLoginActionSteps;    
    @Autowired
    BrowserActionSteps browserActionSteps;  

    @Given("^I log in as a user with only mandatory fields mapped for Marketo connector$")
    public void i_log_in_with_mandatory_fields_mapped() {

    	com.brighttalk.qa.siteElements.User user;
    	user = world.getConnectorManagerUser();
    	world.getLocal_users().add(user);

    	ConnectorHandler connectorHandler = new ConnectorHandler();
    	connectorHandler.setUpTestConnector(world);

    	try {
    		browserActionSteps.i_visit_page("demand central login");
            dcLoginActionSteps.i_log_in_with_email_password();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}