package com.brighttalk.qa.stepdefs.demandcentral;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import cucumber.api.java.en.When;
import org.openqa.selenium.*;
import com.brighttalk.qa.utils.Util;
import org.openqa.selenium.support.ui.ExpectedConditions;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.PropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import java.util.List;
import cucumber.api.DataTable;
import java.util.Map;

@ContextConfiguration(
        classes = TestConfig.class)
public class ConnectorActionSteps {

    @Autowired
    SharedChromeDriver driver;
    @Autowired
    World world;

    @When("^I update the sync settings as following$")
    public void i_update_sync_settings_as_following(DataTable table) throws Throwable {

    	String leadPartition = "";
    	String[] howOftenArray = {};
    	String emailNotification = "";

    	List<Map<String, String>> data = table.asMaps(String.class, String.class);

        for (Map map : data) {

            leadPartition = map.get("lead partition").toString();
            howOftenArray = map.get("how often").toString().toLowerCase().split("\\s*,\\s*");
            emailNotification = map.get("email notification").toString();
            
        }

        try{

            String pageProp = Util.toYamlPropertyString("sync-settings-accordion");
            String locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.clickElement(driver, By.cssSelector(locator));  

            pageProp = Util.toYamlPropertyString("lead-partition-dropdown");
            locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.clickElement(driver, By.cssSelector(locator)); 

            SeleniumUtils.clickElement(driver, By.cssSelector("div[data-value='" + leadPartition +"']")); 
       
            pageProp = Util.toYamlPropertyString("how-often-dropdown");
            locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.clickElement(driver, By.cssSelector(locator)); 
            SeleniumUtils.clickElement(driver, By.cssSelector("div[data-value='" + howOftenArray[0] +"']")); 

        	if(howOftenArray.length == 3) {

                pageProp = Util.toYamlPropertyString("which-day-dropdown");
                locator = PropertiesUtil.getProperty(pageProp);
                SeleniumUtils.clickElement(driver, By.cssSelector(locator)); 
                SeleniumUtils.clickElement(driver, By.cssSelector("div[data-value='" + howOftenArray[1].substring(0,3).toUpperCase() +"']")); 

                pageProp = Util.toYamlPropertyString("which-hour-dropdown");
            	locator = PropertiesUtil.getProperty(pageProp);
            	SeleniumUtils.clickElement(driver, By.cssSelector(locator)); 
            	SeleniumUtils.clickElement(driver, By.cssSelector("div[data-value='" + howOftenArray[2].substring(0,2).replaceFirst("^0*", "") +"']")); 

        	} 

        	if(howOftenArray.length == 2) { 

                pageProp = Util.toYamlPropertyString("which-hour-dropdown");
            	locator = PropertiesUtil.getProperty(pageProp);
            	SeleniumUtils.clickElement(driver, By.cssSelector(locator)); 
            	SeleniumUtils.clickElement(driver, By.cssSelector("div[data-value='" + howOftenArray[1] +"']"));  

    		}	
   
    		if(emailNotification.equalsIgnoreCase("all")) {

            	pageProp = Util.toYamlPropertyString("error-completion-checkbox");
            	locator = PropertiesUtil.getProperty(pageProp);
            	WebElement element = driver.findElement(By.cssSelector(locator)); 
            	JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element);   			

    		}else if (emailNotification.equalsIgnoreCase("error")) {

            	pageProp = Util.toYamlPropertyString("error-only-checkbox");
            	locator = PropertiesUtil.getProperty(pageProp);
            	WebElement element = driver.findElement(By.cssSelector(locator)); 
            	JavascriptExecutor executor = (JavascriptExecutor)driver;
				executor.executeScript("arguments[0].click();", element);  

    		}
      	  
            pageProp = Util.toYamlPropertyString("sync-settings-save-button");
            locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.clickElement(driver, By.cssSelector(locator)); 
            SeleniumUtils.waitForUntil(driver, ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(locator)), 1, 10, 1); 
            driver.navigate().refresh();             

            pageProp = Util.toYamlPropertyString("sync-settings-accordion");
            locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.clickElement(driver, By.cssSelector(locator));  

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }        

    }
}