package com.brighttalk.qa.stepdefs.demandcentral;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import cucumber.api.java.en.When;
import org.openqa.selenium.*;
import com.brighttalk.qa.utils.Util;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.PropertiesUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import java.util.List;

@ContextConfiguration(
        classes = TestConfig.class)
public class DCLoginActionSteps {

    @Autowired
    SharedChromeDriver driver;
    @Autowired
    World world;

    @When("^I log in with email and password$")
    public void i_log_in_with_email_password() throws Throwable {

        try{

            String pageProp = Util.toYamlPropertyString("dc-login-email-field");
            String locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.sendKeys(driver, By.cssSelector(locator), world.getLocal_users().get(0).getEmail());

            pageProp = Util.toYamlPropertyString("dc-login-password-field");
            locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.sendKeys(driver, By.cssSelector(locator), world.getLocal_users().get(0).getPassword());

            pageProp = Util.toYamlPropertyString("dc-login-login-button");
            locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.clickElement(driver, By.cssSelector(locator));            

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }        

    }

    @When("^I log out from demand central$")
    public void i_log_out_from_demand_central() throws Throwable {

        try{

            String pageProp = Util.toYamlPropertyString("dc-profile-drop-down");
            String locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.clickElement(driver, By.cssSelector(locator));  

            pageProp = Util.toYamlPropertyString("dc-profile-log-out");
            locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.clickElement(driver, By.cssSelector(locator)); 

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }        
    }
}