package com.brighttalk.qa.stepdefs.demandcentral;

import com.brighttalk.qa.common.*;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.siteElements.WebcastPro;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import com.brighttalk.qa.scenarios.UserHandler;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtil;
import com.brighttalk.qa.utils.Util;
import com.gargoylesoftware.htmlunit.javascript.host.canvas.ext.WEBGL_debug_renderer_info;
import cucumber.api.java.en.Given;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(
        classes = TestConfig.class)
public class DCLoginPreconditionSteps {

    @Autowired
    BrightTALKService brightTALKService;
    @Autowired
    EntityAttributeResolver eaResolver;
    @Autowired
    World world;

    @Given("^I am a \"([^\"]*)\"$")
    public void i_am_a(String role) {

    	com.brighttalk.qa.siteElements.User user;
        UserHandler myUserHandler = new UserHandler();

    	switch(role.toLowerCase()) {
            case "brighttalk manager with accounts":
                user = world.getDemandCentralTestsManagerUser();
                break;            
    		case "brighttalk manager without accounts":
    			user = world.getDemandCentralTestsManagerUserNA();
    			break;
    		case "brighttalk user":
    			user = myUserHandler.createUser();
                user.setRegistered(true);
    			break;
    		case "channel owner with accounts":
    			user = world.getDemandCentralTestsOwnerUser();
    			break;
    		case "channel owner without accounts":
    			user = world.getDemandCentralTestsOwnerUserNA();
    			break;
    		default:
    			throw new IllegalArgumentException(role + " is not a valid role");
    	}

      	world.getLocal_users().add(user);
    }   
}