package com.brighttalk.qa.stepdefs.channel;

import com.brighttalk.qa.common.EntityAttributeResolver;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(
        classes = TestConfig.class)
public class ChannelActionSteps {

    @Autowired
    BrightTALKService service;

    @Autowired
    EntityAttributeResolver eaResolver;

    @Autowired
    World world;

    @Autowired
    Pages pages;

    @Autowired
    SharedChromeDriver driver;

    @When("^I visit the Manage Webcast Syndication page$")
    public void i_visit_the_Manage_Webcast_Syndication_page() throws Throwable {

        pages.webcastProSyndicationPage().openPage(driver, world.getLocal_channels().get(0).getChannelID(), world.getWebcastsPro().get(0).getWebcastID());

    }
}
