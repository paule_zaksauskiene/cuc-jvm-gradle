package com.brighttalk.qa.stepdefs.channel;

//import com.brighttalk.qa.api.ChannelApi;
import com.brighttalk.qa.api.ChannelApi;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.helper.WebElementWaiter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.scenarios.UserHandler;
import com.brighttalk.qa.siteElements.CategorizationTag;
import com.brighttalk.qa.siteElements.Channel;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import com.brighttalk.qa.stepdefs.common.dto.KeyValuePair;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Given;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.util.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@ContextConfiguration(
        classes = TestConfig.class)
public class ChannelPreconditionSteps {

    @Autowired
    BrightTALKService service;

    @Autowired
    World world;

    @Autowired
    UrlResolver urlResolver;

    @Autowired
    SharedChromeDriver sharedChromeDriver;

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Given("^I own (?:\\s?(\\d+|a|an|no|several))? (?:channel|channels)")
    public void own_channel(String count) {

        //User channelOwner;
        int channelCount;
        com.brighttalk.qa.siteElements.User managerUser;

        if (world.getLocal_users().isEmpty()) {
            //channelOwner = service.getHandler().registerUser();

            UserHandler myUserHandler = new UserHandler();
            managerUser = myUserHandler.createManageOpsUser();
            world.getLocal_users().add(managerUser);
            world.getLocal_users().get(0).setSessionCookie(myUserHandler.loginUserAPI(managerUser));
            managerUser.setLoggedin(true);

        } else {

            //channelOwner = world.getUsers().get(world.getUsers().size() - 1);
            managerUser = world.getLocal_users().get(world.getLocal_users().size() - 1);
        }

        if (count.equalsIgnoreCase("a") | count.equalsIgnoreCase("an")) {

            channelCount = 1;

        } else if (count.equalsIgnoreCase("several")) {
            channelCount = Util.randomize(3, 9);
        } else if (count.equalsIgnoreCase("no")) {
            channelCount = 0;
        } else {
            channelCount = Integer.valueOf(count);
        }

        if (channelCount > 0) {
            while (channelCount > 0) {
                //service.getHandler().createChannel(channelOwner);
                service.getHandler().createChannel();
                --channelCount;
            }
        }
    }

    @Given("^the following channel features are set$")
    public void set_feature(List<KeyValuePair> table) throws Exception {
        Channel channel;
        String channelId;

        if (world.getLocal_channels().isEmpty()) {

            UserHandler myUserHandler = new UserHandler();
            com.brighttalk.qa.siteElements.User managerUser = myUserHandler.createManageOpsUser();
            world.getLocal_users().add(managerUser);
            world.getLocal_users().get(0).setSessionCookie(myUserHandler.loginUserAPI(managerUser));
            managerUser.setLoggedin(true);

            System.out.println("My new user: " + managerUser.getEmail());
            System.out.println("My new user password: " + managerUser.getPassword());

            service.getHandler().createChannel();
            channel = world.getLocal_channels().get(world.getLocal_channels().size() - 1);

        } else {
            channel = world.getLocal_channels().get(world.getLocal_channels().size() - 1);
        }

        channelId = channel.getChannelID();

        Resource resource = new ClassPathResource("/channel/features.yml");
        Properties props = PropertiesLoaderUtils.loadProperties(resource);

        for (KeyValuePair kvp : table) {

            String key = kvp.getKey();
            String value = kvp.getValue();

            if (StringUtils.hasLength(key)) {

                key = props.getProperty(Util.toYamlPropertyString(key));

                if (StringUtils.hasLength(value)) {

                    if (key.equalsIgnoreCase("customRegistrationTemplate") || key.equalsIgnoreCase("blockedUsers")) {

                        value = Util.toKebabCase(value);
                    }

                    // If the value for the blocked user is new User then we take email from world object
                    if (value.toLowerCase().trim().contains("newuser")){

                        ChannelApi.setChannelFeature(world.getRegressionTestsManagerUser(), channel, key, "true", world.getLocal_users().get(0).getEmail());

                    }

                    if (value.equalsIgnoreCase("enabled")) {

                        ChannelApi.setChannelFeature(world.getRegressionTestsManagerUser(), channel, key, "true", "");
                    }

                    if ((!value.equalsIgnoreCase("enabled")) && (!value.toLowerCase().trim().contains("newuser"))) {

                        if (!value.trim().equals("")){

                            ChannelApi.setChannelFeature(world.getRegressionTestsManagerUser(), channel, key, "true", value);

                        } else

                            ChannelApi.enableDisableChannelFeature(world.getRegressionTestsManagerUser(), channel, key, "false");
                    }

                } else {

                    ChannelApi.enableDisableChannelFeature(world.getRegressionTestsManagerUser(), channel, key, "false");
                }
            }
        }
    }

    @Given("the following channel configuration is set$")
    public void set_config(List<KeyValuePair> table) throws Exception {

        Channel channel;
        String channelId;

        // If there is no channel created yet
        if (world.getLocal_channels().isEmpty()) {

            UserHandler myUserHandler = new UserHandler();
            com.brighttalk.qa.siteElements.User managerUser = myUserHandler.createManageOpsUser();
            world.getLocal_users().add(managerUser);
            world.getLocal_users().get(0).setSessionCookie(myUserHandler.loginUserAPI(managerUser));
            managerUser.setLoggedin(true);

            service.getHandler().createChannel();

        } else {


        }

        channel = world.getLocal_channels().get(world.getLocal_channels().size() - 1);


        for (int i = 0; i < table.size(); i++) {

            String key = table.get(i).getKey();
            String value = table.get(i).getValue();

            Resource resource = new ClassPathResource("/channel/config.yml");
            Properties props = PropertiesLoaderUtils.loadProperties(resource);

            key = props.getProperty(Util.toYamlPropertyString(key));
            ChannelApi.setChannelFeature(world.getLocal_users().get(0), channel, key, value, "");

        }


        /*Resource resource = new ClassPathResource("/channel/config.yml");
        Properties props = PropertiesLoaderUtils.loadProperties(resource);

        String json = "{";

        for (int i = 0; i < table.size(); i++) {
            String key = table.get(i).getKey();
            String value = table.get(i).getValue();

            if (StringUtils.hasLength(key) && StringUtils.hasLength(value)) {
                key = props.getProperty(Util.toYamlPropertyString(key));

                json += "\"" + key + "\":\"" + value + "\"";

                if(i != (table.size() - 1)) {
                    json += ",";
                } else {
                    json += "}";
                }
            }
        }

        //service.getChannelService().configure(channelId, json); */

    }

    @Given("^the portal publishing for channel configuration is set as (.*)$")
    public void the_portal_publishing_for_channel_configuration_is_set_as(String portalPublishingValue) {

        // Set portal publishing to "excluded"
        ChannelApi.updatePortalPublishing(world.getRegressionTestsManagerUser(), world.getLocal_channels().get(0), portalPublishingValue);

    }

    @Given("^a channel with the following categorization tag exists$")
    public void a_channel_with_the_following_categorization_tag_exists(List<String> tags) {

        this.own_channel("1");
        System.out.println("User session: " + world.getLocal_users().get(0).getSessionCookie());
        ChannelApi newChannelApi = new ChannelApi();
        newChannelApi.addCategorizationTags(world, tags);
        System.out.println("Channel categorization tags added");

        List<CategorizationTag> categorizationTags = new ArrayList<CategorizationTag>();
        for (int i = 0; i < tags.size(); i++){

            CategorizationTag category = new CategorizationTag();
            category.setId(Integer.toString(i));
            System.out.println("Category: " + tags.get(i));
            System.out.println("The position of slash" + tags.get(i).indexOf("/"));
            category.setCategory(tags.get(i).substring(0, tags.get(i).indexOf("/")));
            category.setTagValue(tags.get(i).substring(tags.get(i).indexOf("/") + 1));
            categorizationTags.add(category);
        }

        world.setCategorizationTags(categorizationTags);

    }

    @Given ("^a channel has Anonymous realm$")
    public void a_channel_has_Anonymous_realm(){

        try {
            ChannelApi.addAnonymousRealmToChannel(world.getRegressionTestsManagerUser(), world.getLocal_channels().get(0));
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
/*
    @Given("^a pro webinar contains categorization tag (.*)")
    public void a_pro_webinar_contains_categorization_tag(String categorizationTag) {

        try {
            browserActionSteps.i_visit_page("/admin/channel/{channel_id}/summary");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        DateTimeFormatter.waitForSomeTime(3); // We need to wait here, as even if the element is clickable it does not get clicked

        try {
            browserActionSteps.i_click("categorization-tag-section", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        try {

            DateTimeFormatter.waitForSomeTime(2); // wait for 2 seconds untill its expanded (cant use the SeleniumUtils here)
            browserAssertionSteps.the_element_exists("categorization-tag-section-expanded");

        } catch (NoSuchElementException ex) {

            System.out.println("Clicking the second time...");
            try {
                browserActionSteps.i_click("categorization-tag-section", "", "");
            } catch (Throwable throwable) {
                throwable.printStackTrace();
            }

        }

        browserActionSteps.i_enter_value("categorization-tag-section-edit-new-tag", "categorizationTag");
        try {

            browserActionSteps.i_click("categorization-tag-section-add-new-tag-button", "", "");
            browserActionSteps.i_click("categorization-tag-section-add-new-tag-confirmation", "", "");

        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        world.getWebcastsPro().get(0).setCategorizationTag(categorizationTag);

        // Go to the edit booking page and select the categorization tag


    } */

    @Given ("^a channel with id (.*) exists$")
    public void a_channel_with_id_exists(String channelId){

        Channel newChannel = new Channel();
        newChannel.setChannelID(channelId);
        newChannel.setShouldBeDeleted(false);

        world.getLocal_channels().add(newChannel);
        urlResolver.add("{channel_id}", world.getLocal_channels().get(0).getChannelID());

    }

    @Given ("^my channel type is (.*)$")
    public void updateChannelType(String channelType){

        world.getLocal_channels().get(0).setChannelType(channelType.trim());

        try {
            ChannelApi.updateChannelType(world.getRegressionTestsManagerUser(), world.getLocal_channels().get(0));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Given ("^pro webinar is disabled on the channel settings page$")
    public void channelConfigurationDisableProWebinar() {

        // Verify the channel configuration section is not expanded
        SeleniumUtils.verifyElementDisappearsFromPageWithoutRefresh(sharedChromeDriver, By.xpath("//fieldset[@id='form-channel-config-for']/legend[@class='expanded']"), 30, 1, "");

        // Try to click the expand button until it expands
        boolean isExpanded = false;
        while (isExpanded == false){
            try {

                String channelConfXpath = "//fieldset[@id='form-channel-config-for']/legend[contains(.,'Channel configuration')]";
                sharedChromeDriver.findElement(By.xpath(channelConfXpath)).click();
                DateTimeFormatter.waitForSomeTime(1);
                sharedChromeDriver.findElement(By.xpath("//fieldset[@id='form-channel-config-for']/legend[@class='expanded']"));
                System.out.println("The configuration panel expanded");
                isExpanded = true;
                DateTimeFormatter.waitForSomeTime(1);

            } catch (NoSuchElementException ex) {

            }
        }


        if (sharedChromeDriver.findElement(By.xpath("//input[@id='edit-proWebinar']")).isSelected()){

            new WebElementWaiter().waitAndFindByID(sharedChromeDriver, "edit-proWebinar").click();
            try{ Thread.sleep(1000); }catch(InterruptedException ie){System.out.println("WARNING: Thread Sleep 2 Interrupted in ChannelSummaryPage.channelConfigurationDisableProWebinar()");}

        }

        new WebElementWaiter().waitAndFindByXpath(sharedChromeDriver, "//div/input[@name='op']", 60, 1, false).click();

        // We need to wait for some time, because the channel owner pages take some time to update (approx 30 secs)
        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Given ("^social media is disabled on the channel settings page$")
    public void socialMediaDisableShareSocial(){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        try {
            browserActionSteps.i_visit_page("admin-channel-summery");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        try {
            browserActionSteps.i_click("social-media-expand-suppress-icon", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        String pageProp = Util.toYamlPropertyString("social-media-expand-suppress-icon-expanded");
        String locator = PropertiesUtil.getProperty(pageProp);

        new WebElementWaiter().waitAndFindByXpath(driver, locator, 15, 1, false);

        try {
            browserActionSteps.i_click("social-media-share-socialize-checkbox", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        try {
            browserActionSteps.i_click("social-media-submit-button", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Given ("^embed sharing is disabled on the channel settings page$")
    public void embedSharingDisableShareSocial(){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        try {
            browserActionSteps.i_visit_page("admin-channel-summery");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        try {
            browserActionSteps.i_click("social-media-expand-suppress-icon", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        String pageProp = Util.toYamlPropertyString("social-media-expand-suppress-icon-expanded");
        String locator = PropertiesUtil.getProperty(pageProp);

        new WebElementWaiter().waitAndFindByXpath(driver, locator, 15, 1, false);

        try {
            browserActionSteps.i_click("social-media-share-embed-checkbox", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        try {
            browserActionSteps.i_click("social-media-submit-button", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Given ("^email sharing is disabled on the channel settings page$")
    public void emailSharingDisableShareSocial(){

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        try {
            browserActionSteps.i_visit_page("admin-channel-summery");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        try {
            browserActionSteps.i_click("social-media-expand-suppress-icon", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        String pageProp = Util.toYamlPropertyString("social-media-expand-suppress-icon-expanded");
        String locator = PropertiesUtil.getProperty(pageProp);

        new WebElementWaiter().waitAndFindByXpath(driver, locator, 15, 1, false);

        try {
            browserActionSteps.i_click("social-media-share-email-checkbox", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        try {
            browserActionSteps.i_click("social-media-submit-button", "", "");
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }
}
