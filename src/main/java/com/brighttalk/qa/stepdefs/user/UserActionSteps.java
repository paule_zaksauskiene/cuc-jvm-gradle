package com.brighttalk.qa.stepdefs.user;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.scenarios.UserHandler;
import com.brighttalk.qa.siteElements.User;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import static org.junit.Assert.assertTrue;
//import static org.seleniumhq.jetty9.servlets.gzip.GzipHttpOutput.LOG;

@ContextConfiguration(
        classes = TestConfig.class)
public class UserActionSteps {

    @Autowired
    BrightTALKService service;

    @Autowired
    SharedChromeDriver driver;

    @Autowired
    World world;

    @Autowired
    Pages pages;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @When("^I enter new user details$")
    public void i_enter_new_user_details() {

        User user = new User();

        browserActionSteps.i_enter_value("join-form-first-name", user.getFirstName());

        browserActionSteps.i_enter_value("join-form-last-name", user.getSecondName());

        browserActionSteps.i_enter_value("join-form-email-address", user.getEmail());

        browserActionSteps.i_select_value("join-form-timezone", user.getTimezone());

        browserActionSteps.i_enter_value("join-form-password", user.getPassword());

        user.setRegistered(false);

        world.getLocal_users().add(user);
    }
}
