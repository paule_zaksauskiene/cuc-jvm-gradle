package com.brighttalk.qa.stepdefs.user;

import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.siteElements.Timezone;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.util.StringUtils;
import static org.junit.Assert.assertTrue;
//import static org.seleniumhq.jetty9.servlets.gzip.GzipHttpOutput.LOG;

@ContextConfiguration(
        classes = TestConfig.class)
public class UserAssertionSteps {

    @Autowired
    BrightTALKService service;

    @Autowired
    SharedChromeDriver driver;

    @Autowired
    World world;

    @Autowired
    Pages pages;

    @Autowired
    BrowserAssertionSteps browserAssertionSteps;

    @Then("^I should be authenticated$")
    public void should_be_auth() {

        //LOG.info("Executing: I should be authenticated step");

        pages.audiencePage().verifyifYouAreConfirmedOverlayIsShownAndCloseIt(driver);

        String locator = PropertiesUtil.getProperty("user-profile-toggle");

        new WebDriverWait(driver, 30)
                .until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(locator)));

        String session = driver.manage().getCookieNamed("BTSESSION").getValue();

        assertTrue(StringUtils.hasLength(session));

        world.getLocal_users().get(0).setRegistered(true);

    }

    @Then("^I should see the profile data populated by the Lead Gen form$")
    public void should_see_lead_gen_form_data() {

        browserAssertionSteps.i_should_see_message("profile-page-first-name", world.getLocal_users().get(1).getFirstName());

        browserAssertionSteps.i_should_see_message("profile-page-last-name", world.getLocal_users().get(1).getSecondName());

        browserAssertionSteps.i_should_see_message("profile-page-email", world.getLocal_users().get(1).getEmail());

        browserAssertionSteps.i_should_see_message("profile-page-telephone-number", world.getLocal_users().get(1).getTelephone());

        browserAssertionSteps.i_should_see_message("profile-page-job-title", world.getLocal_users().get(1).getJobTitle());

        browserAssertionSteps.i_should_see_message("profile-page-level", world.getLocal_users().get(1).getLevel());

        browserAssertionSteps.i_should_see_message("profile-page-company", world.getLocal_users().get(1).getCompany());

        browserAssertionSteps.i_should_see_message("profile-page-company-size", world.getLocal_users().get(1).getCompanySize());

        browserAssertionSteps.i_should_see_message("profile-page-industry", world.getLocal_users().get(1).getIndustry());

        String timezone = Timezone.locationToTimezoneList.get(world.getLocal_users().get(1).getTimezone().trim());
        browserAssertionSteps.i_should_see_message("profile-page-timezone", timezone);

        browserAssertionSteps.i_should_see_message("profile-page-country", world.getLocal_users().get(1).getCountry());
        browserAssertionSteps.i_should_see_field_empty("profile-page-state");

    }

    @Then("^I should see the profile data populated by the Prospect form$")
    public void should_see_prospect_form_data() {

        browserAssertionSteps.i_should_see_message("profile-page-first-name", world.getLocal_users().get(1).getFirstName());
        browserAssertionSteps.i_should_see_message("profile-page-last-name", world.getLocal_users().get(1).getSecondName());
        browserAssertionSteps.i_should_see_message("profile-page-email", world.getLocal_users().get(1).getEmail());
        browserAssertionSteps.i_should_see_message("profile-page-telephone-number", world.getLocal_users().get(1).getTelephone());
        browserAssertionSteps.i_should_see_field_empty("profile-page-job-title");
        browserAssertionSteps.i_should_see_message("profile-page-level", world.getLocal_users().get(1).getLevel());
        browserAssertionSteps.i_should_see_message("profile-page-company", world.getLocal_users().get(1).getCompany());

        browserAssertionSteps.i_should_see_message("profile-page-company-size", world.getLocal_users().get(1).getCompanySize());

        browserAssertionSteps.i_should_see_field_empty("profile-page-industry");

        String timezone = Timezone.locationToTimezoneList.get(world.getLocal_users().get(1).getTimezone().trim());
        browserAssertionSteps.i_should_see_message("profile-page-timezone", timezone);

        browserAssertionSteps.i_should_see_message("profile-page-country", world.getLocal_users().get(1).getCountry());
        browserAssertionSteps.i_should_see_field_empty("profile-page-state");

    }

    @Then("^I should see the profile data populated by the Moderate form$")
    public void should_see_moderate_form_data() {

        browserAssertionSteps.i_should_see_message("profile-page-first-name", world.getLocal_users().get(1).getFirstName());
        browserAssertionSteps.i_should_see_message("profile-page-last-name", world.getLocal_users().get(1).getSecondName());
        browserAssertionSteps.i_should_see_message("profile-page-email", world.getLocal_users().get(1).getEmail());
        browserAssertionSteps.i_should_see_field_empty("profile-page-telephone-number");
        browserAssertionSteps.i_should_see_message("profile-page-job-title", world.getLocal_users().get(1).getJobTitle());
        browserAssertionSteps.i_should_see_field_empty("profile-page-level");
        browserAssertionSteps.i_should_see_message("profile-page-company", world.getLocal_users().get(1).getCompany());
        browserAssertionSteps.i_should_see_field_empty("profile-page-company-size");
        browserAssertionSteps.i_should_see_field_empty("profile-page-industry");

        String timezone = Timezone.locationToTimezoneList.get(world.getLocal_users().get(1).getTimezone().trim());
        browserAssertionSteps.i_should_see_message("profile-page-timezone", timezone);
        browserAssertionSteps.i_should_see_message("profile-page-country", world.getLocal_users().get(1).getCountry());
        browserAssertionSteps.i_should_see_field_empty("profile-page-state");

    }

    @Then("^I should see the profile data populated by the Minimum form$")
    public void should_see_minimum_form_data() {

        browserAssertionSteps.i_should_see_message("profile-page-first-name", world.getLocal_users().get(1).getFirstName());
        browserAssertionSteps.i_should_see_message("profile-page-last-name", world.getLocal_users().get(1).getSecondName());
        browserAssertionSteps.i_should_see_message("profile-page-email", world.getLocal_users().get(1).getEmail());
        browserAssertionSteps.i_should_see_field_empty("profile-page-telephone-number");
        browserAssertionSteps.i_should_see_field_empty("profile-page-job-title");
        browserAssertionSteps.i_should_see_field_empty("profile-page-level");
        browserAssertionSteps.i_should_see_field_empty("profile-page-company");
        browserAssertionSteps.i_should_see_field_empty("profile-page-company-size");
        browserAssertionSteps.i_should_see_field_empty("profile-page-industry");

        String timezone = Timezone.locationToTimezoneList.get(world.getLocal_users().get(1).getTimezone().trim());
        browserAssertionSteps.i_should_see_message("profile-page-timezone", timezone);
        browserAssertionSteps.i_should_see_field_empty("profile-page-country");
        browserAssertionSteps.i_should_see_field_empty("profile-page-state");

    }
}
