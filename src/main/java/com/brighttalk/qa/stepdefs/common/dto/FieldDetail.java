package com.brighttalk.qa.stepdefs.common.dto;

/**
 * Created by user on 29/08/16.
 */
public class FieldDetail {
    private String field;
    private String value;


    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
