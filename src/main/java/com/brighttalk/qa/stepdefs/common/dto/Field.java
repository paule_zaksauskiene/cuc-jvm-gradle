package com.brighttalk.qa.stepdefs.common.dto;


public class Field {

    private String field;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
