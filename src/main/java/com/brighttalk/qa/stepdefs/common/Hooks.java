package com.brighttalk.qa.stepdefs.common;

import brighttalktest.messagebus.FakeDialIn;
import com.brighttalk.qa.api.AvailableResourcesApi;
import com.brighttalk.qa.api.UserApi;
import com.brighttalk.qa.api.WebcastProApi;
import com.brighttalk.qa.common.*;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.helper.ResourceServiceHandler;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.siteElements.OnDemandVideo;
import com.brighttalk.qa.siteElements.WebcastPro;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import com.brighttalk.qa.stepdefs.presenter.PresenterActionSteps;
import com.brighttalk.qa.stepdefs.webcast.WebcastPreconditionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.Collection;
import java.util.Iterator;
import java.util.Set;

/**
 * Perform actions before and after execution of each test with cucumber hooks
 */
@ContextConfiguration(classes = TestConfig.class)
public class Hooks {

    @Autowired
    BrightTALKService brightTALKService;

    @Autowired
    private SharedChromeDriver sharedChromeDriver;

    @Autowired
    private SharedFirefoxDriver sharedFirefoxDriver;

    @Autowired
    private World world;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Autowired
    BrowserAssertionSteps browserAssertionSteps;

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    @Autowired
    WebcastPreconditionSteps webcastPreconditionSteps;

    @Autowired
    PresenterActionSteps presenterActionSteps;

    /**
     * Delete all cookies at the start of each ui scenario to avoid
     * shared state between tests
     */
    @Before
    public void deleteAllCookiesAndPrepareBrowser(Scenario myScenario) {

        Collection<String> mytags = myScenario.getSourceTagNames();

        world.setBrowserUsed("chrome"); // by default its Chrome
        sharedChromeDriver.get("https://www." + TestConfig.getEnv() + ".brighttalk.net");
        sharedChromeDriver.deleteAllCookies();
        world.getWindowHandles().add(sharedChromeDriver.getWindowHandle());

        String myTag = "chrome";

        Iterator<String> iterator = mytags.iterator();

        while (iterator.hasNext()) {
            if (iterator.next().trim().toLowerCase().contains("firefox")) {

                world.setBrowserUsed("firefox");
                sharedFirefoxDriver.get("https://www." + TestConfig.getEnv() + ".brighttalk.net");
                sharedFirefoxDriver.deleteAllCookies();

            }
        }

        // Add the first window handle
        if (world.getBrowserUsed().contains("firefox")){

            world.getWindowHandles().add(sharedFirefoxDriver.getWindowHandle());
        }

    }


    /**
     * Embed a screenshot in test report if a ui scenario is marked as failed
     */
    //@After
   /* public void a_embedScreenshotIfFailed(Scenario scenario) {

        if (scenario.isFailed()) {
            sharedDriver.embedScreenshot(scenario);
        }
    } */

    /**
     * Delete data created as part of scenario
     */
    @After
    public void teardown(Scenario scenario) {

        WebDriver driver = browserPreconditionSteps.defineWebDriver();

        if (scenario.isFailed()) {

            System.out.println("Taking screenshot");

            if (world.getBrowserUsed().contains("firefox")){

                sharedFirefoxDriver.embedScreenshot(scenario);

            } else {

                sharedChromeDriver.embedScreenshot(scenario);
            }
        }


        for (WebcastPro proWebinar : world.getWebcastsPro()) {

            System.out.println("Test");

            if (proWebinar.isShouldBeDeleted() == true) {

                // Verify the webinar is still live
                StringBuffer webinarDetails = WebcastProApi.getCommunicationDetails(world.getRegressionTestsManagerUser(), world.getLocal_channels().get(0), world.getWebcastsPro().get(0));
                System.out.println("Webinar details: " + webinarDetails);


                if (webinarDetails.toString().contains("<status>upcoming</status>")) {

                    proWebinar.setStatus("cancelled");
                    WebcastProApi.updateWebcastProAsManager(world, proWebinar);

                    if (proWebinar.isResourceNeeded() == true) {

                        if (AvailableResourcesApi.verifyResourceIsTearingDownForCommunication(proWebinar.getWebcastID())) {

                            //Wait for 10 minutes as resource may deallocate automatically - it takes now 10 minutes !
                            DateTimeFormatter.waitForSomeTime(600);

                        }

                        if (AvailableResourcesApi.verifyResourceIsRebootingForCommunication(proWebinar.getWebcastID())) {

                            //Wait for 10 minutes as resource may deallocate automatically - it takes now 10 minutes !
                            DateTimeFormatter.waitForSomeTime(600);

                        }

                        if (AvailableResourcesApi.verifyResourceIsAllocatedForCommunication(proWebinar.getWebcastID())) {

                            AvailableResourcesApi.deallocateResource(proWebinar.getChannelID());
                        }

                        AvailableResourcesApi.verifyResourceIsErroredForCommunicationAndRepair(proWebinar.getWebcastID());

                    }
                }

                if (webinarDetails.toString().contains("<status>live</status>")) {

                    try {

                        if (webinarDetails.toString().contains("<serviced>true")) {

                            browserActionSteps.i_visit_page("presenter-serviced-page");

                        } else {

                            browserActionSteps.i_visit_page("presenter");
                        }

                        // Check if presentation is overruning and close it
                        presenterActionSteps.verifyPresentationOverruningPopupAndCloseIt();

                        browserActionSteps.i_click("end-presenting-button", "", "");
                        browserActionSteps.i_click("end-presentation-confirmation-button", "", "");
                        String pageProp = Util.toYamlPropertyString("message-thank-for-for-presenting-on-brighttalk");
                        String locator = PropertiesUtil.getProperty(pageProp);

                        SeleniumUtils.findElement(driver, By.xpath(locator), "The \"Thank you for presenting on BrightTalk\" message is not shown");
                        FakeDialIn.sendFakeDialIn("exchange.mphd01." + TestConfig.getEnv() + ".brighttalk.net", world.getWebcastsPro().get(0).getWebcastID(), 0);
                        ResourceServiceHandler.releaseResourceAfterTest(proWebinar.getWebcastID());
                        WebcastProApi.sendEncoderResponse(proWebinar.getWebcastID(), world.getRegressionTestsManagerUser());


                    } catch (Throwable throwable) {

                        throwable.printStackTrace();
                    }
                }
            }

            world.getWebcastsPro().remove(proWebinar);
        }

        for (OnDemandVideo onDemandVideo : world.getOnDemandVideos()) {

            world.getOnDemandVideos().remove(onDemandVideo);

        }

        for (com.brighttalk.qa.siteElements.Channel channel : world.getLocal_channels()) {

            if (channel.isShouldBeDeleted()) {

                brightTALKService.getHandler().deleteLocalChannel(channel);
            }

            world.getLocal_channels().remove(channel);

        }

        // Deletes all consumer channels created by the test
        // Currently the consumer channel is deleted as part of the world local_channels object
        // com.brighttalk.qa.siteElements.Channel c = world.getConsumerChannel();

        // if (c != null) {
        //  brightTALKService.getHandler().deleteLocalChannel(c);
        //  world.setConsumerChannel(null);
        // }

        // Deletes all users created by the test
        /*for (User u : world.getUsers()) {
            brightTALKService.getUserService().delete(u.getId());

            world.getUsers().remove(u);
        } */

        for (com.brighttalk.qa.siteElements.User user : world.getLocal_users()) {

            if (user.isRegistered() == true) {

                UserApi.deleteUserNoRestrictions(user);
            }

            System.out.println("User deleted: " + user.getEmail());
            world.getLocal_users().remove(user);
        }

        // Close all browser tabs but the main (required if is more than 1 tab has been opened during the test
        try {
            Set<String> windows = driver.getWindowHandles();
            Iterator<String> iter = windows.iterator();
            String[] winNames = new String[windows.size()];
            int i = 0;

            while (iter.hasNext()) {

                winNames[i] = iter.next();
                i++;

            }

            if (winNames.length > 1) {

                for (i = winNames.length; i > 1; i--) {
                    driver.switchTo().window(winNames[i - 1]);
                    driver.close();
                }
            }

            driver.switchTo().window(winNames[0]);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
