package com.brighttalk.qa.stepdefs.common.dto;


public class Value {
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
