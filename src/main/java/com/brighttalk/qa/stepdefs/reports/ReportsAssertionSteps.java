package com.brighttalk.qa.stepdefs.reports;

import com.brighttalk.qa.api.WebcastProApi;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.helper.DateTimeFormatter;
import com.brighttalk.qa.helper.WebElementWaiter;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.siteElements.User;
import com.brighttalk.qa.siteElements.WebcastPro;
import com.brighttalk.qa.stepdefs.browser.BrowserActionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserAssertionSteps;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;

import java.util.ArrayList;
import java.util.List;

@ContextConfiguration(
        classes = TestConfig.class)
public class ReportsAssertionSteps {

    @Autowired
    SharedChromeDriver driver;

    @Autowired
    World world;

    @Autowired
    BrightTALKService service;

    @Autowired
    BrowserAssertionSteps browserAssertionSteps;

    @Autowired
    BrowserActionSteps browserActionSteps;

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    @Autowired
    UrlResolver urlResolver;

    @Then("^number of pre-registrations is 1$")
    public void verifyNumberOfRegistrations(){

        SeleniumUtils.findElement(driver, By.xpath("//div[contains(@class, 'webcast-summary-reports-wrapper')]/div/div/div/div/div/div[3][contains(.,'" + 1 + "')]"), 600, 60, true, "THE NUMBER OF REGISTRATIONS " + 1 + " IS NOT FOUND ON THE WEBCAST PRE-REGISTRATION REPORT PAGE IS NOT OPENED!");

    }

    @Then("^user appears in pre-registratons table$")
    public void verifyUserAppearsInRegistrationsTable(){

        User myUser = world.getLocal_users().get(1);

        // We need to wait for 10 mins and refresh the page if needed
        SeleniumUtils.findElement(driver, By.xpath("//div[contains(@class, 'report-default')]/table/tbody[contains(.,'" + myUser.getFirstName() + "')]"), 60, 1, true, "THE FIRST NAME IS NOT FOUND ON THE USER REGISTRATIONS TABLE!");

        SeleniumUtils.findElement(driver, By.xpath("//div[contains(@class, 'report-default')]/table/tbody[contains(.,'" + myUser.getSecondName() + "')]"), 60, 1, false, "THE SECOND NAME IS NOT FOUND ON THE USER REGISTRATIONS TABLE!");

        if (!myUser.getCompany().equals("")) {

            SeleniumUtils.findElement(driver, By.xpath("//div[contains(@class, 'report-default')]/table/tbody[contains(.,'" + myUser.getJobTitle() + "')]"), 60, 1, false, "THE JOB TITLE IS NOT FOUND ON THE USER REGISTRATIONS TABLE!");
        }

        if (!myUser.getJobTitle().equals("")) {

            SeleniumUtils.findElement(driver, By.xpath("//div[contains(@class, 'report-default')]/table/tbody[contains(.,'" + myUser.getCompany() + "')]"), 60, 1, false, "THE COMPANY IS NOT FOUND ON THE USER REGISTRATIONS TABLE!");
        }
    }

}
