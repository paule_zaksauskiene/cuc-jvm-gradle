package com.brighttalk.qa.stepdefs.campaign;

import com.brighttalk.qa.api.ChannelApi;
import com.brighttalk.qa.common.*;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.siteElements.DCCampaign;
import com.brighttalk.qa.siteElements.Account;
import com.brighttalk.qa.siteElements.ProgramSummary;
import com.brighttalk.qa.siteElements.Audience;
import com.brighttalk.qa.siteElements.User;
import com.brighttalk.qa.siteElements.WebcastPro;
import com.brighttalk.qa.scenarios.AccountHandler;
import com.brighttalk.qa.scenarios.DCCampaignHandler;
import com.brighttalk.qa.scenarios.UserHandler;
import com.brighttalk.qa.siteElements.Channel;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Calendar;
import java.util.Date;
import java.lang.Integer;
import com.brighttalk.qa.utils.Util;
import com.brighttalk.qa.stepdefs.common.dto.KeyValuePair;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.DataTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import com.brighttalk.qa.integration.BrightTALKService;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

@ContextConfiguration(
        classes = TestConfig.class)
public class DCCampaignPreconditionSteps {

    @Autowired
    World world;

    @Autowired
    UrlResolver urlResolver;

    @Autowired
    BrightTALKService brightTALKService;

	@Given("^I have a dc campaign with following details$")
    public void i_have_a_dc_campaign_with_following_details(List<KeyValuePair> table) throws Throwable{

    	DCCampaign campaign = new DCCampaign();
    	DCCampaignHandler campaignHand = new DCCampaignHandler();

    	AccountHandler accountHandler = new AccountHandler();

    	Account account = accountHandler.createAccount();
    	ProgramSummary programSummary = accountHandler.createProgramSummary(account);

        world.getAccounts().add(account);
        urlResolver.add("{account_id}", world.getAccounts().get(0).getAccountId());
		Channel channel = world.getLocal_channels().get(world.getLocal_channels().size() - 1);

		accountHandler.associateAccountChannel(channel.getChannelID(), account, programSummary);

        for (KeyValuePair kvp : table) {
            String key = kvp.getKey();
            String value = kvp.getValue();

                switch(key.toLowerCase()){
            		case "displayname":
            			campaign.setDisplayName(value);      
            			break;
            		case "status":
            			campaign.setStatus(value);
            			break;
            		case "startdate":
            			campaign.setStartDate(value);
            			break;
            		case "target":
            			campaign.setTotalLeadTarget(Integer.parseInt(value));
            			break;
            		case "leadsfulfilled":
            			campaign.setTotalLeadsFulfilled(Integer.parseInt(value));
            			break;
            		case "budget":
            			campaign.setBudget(Integer.parseInt(value));
            			break;            			
            		case "cpl":
            			campaign.setCpl(value);
            			break;
            		case "earlydelivery":
            			campaign.setEarlyDeliveryFlag(Boolean.parseBoolean(value));
            			break;
            		case "community":
            			campaign.setCommunity(value);
            			break;            			
            		case "companysizefilter":
            			campaign.setCompanySizeFilter(value);
            			break;
            		case "joblevelfilter":
            			campaign.setJobLevelFilter(value);
            			break;
            		case "countries":
            			campaign.setRegions(Util.parseList(value));
            			break;
            		case "partnerex":
            			campaign.setPartnerExclusions(Util.parseList(value));
            			break;
            		case "companyex":
            			campaign.setCompanyExclusions(Util.parseList(value));
            			break;            			
            		default:
            			throw new IllegalArgumentException(key + " is not a valid field");
            	}
        }

        campaign = campaignHand.createCampaign(world, campaign, account, programSummary);
        world.getDCCampaigns().add(campaign);
        urlResolver.add("{dccampaign_id}", world.getDCCampaigns().get(0).getCampaignId());

    }

	@Given("^I have multiple dc campaigns with following details$")
    public void i_have_multiple_dc_campaigns_with_following_details(DataTable table) throws Throwable{

    	DCCampaign campaign;

    	String campaignName;

    	String startDate;

    	String status;

    	Integer target;

    	Integer fulfillment;

    	DCCampaignHandler campaignHand = new DCCampaignHandler();

    	AccountHandler accountHandler = new AccountHandler();

    	Account account = accountHandler.createAccount();
    	ProgramSummary programSummary = accountHandler.createProgramSummary(account);

        world.getAccounts().add(account);
        urlResolver.add("{account_id}", world.getAccounts().get(0).getAccountId());
		Channel channel = world.getLocal_channels().get(world.getLocal_channels().size() - 1);

		accountHandler.associateAccountChannel(channel.getChannelID(), account, programSummary);

    	List<Map<String, String>> data = table.asMaps(String.class, String.class);
    	for (Map map : data) {

    		campaignName = map.get("campaign name").toString();
    		startDate = map.get("start date").toString();
    		status = map.get("status").toString();
    		target = Integer.parseInt(map.get("target").toString());
    		fulfillment = Integer.parseInt(map.get("fulfillment").toString());

			campaign = new DCCampaign(campaignName, startDate, status, target, fulfillment);
		    campaign = campaignHand.createCampaign(world, campaign, account, programSummary);
            world.getDCCampaigns().add(campaign);
            urlResolver.add("{dccampaign_id}", world.getDCCampaigns().get(0).getCampaignId());

    	}

    }


    @Given("^I have a dc campaign with following leads delivered$")
    public void i_have_a_dc_campaign_with_following_leads_delivered(DataTable table) throws Throwable{

        List<Audience> audiences = new ArrayList<Audience>();

        User user;
        Audience aud;
        String deliveryDate;
        String firstName;
        String lastName;
        String jobTitle;
        String company;
        String jobLevel;
        String companySize;
        String industry;
        DCCampaign campaign;

        DCCampaignHandler campaignHand = new DCCampaignHandler();

        AccountHandler accountHandler = new AccountHandler();

        Account account = accountHandler.createAccount();
        ProgramSummary programSummary = accountHandler.createProgramSummary(account);

        world.getAccounts().add(account);
        urlResolver.add("{account_id}", world.getAccounts().get(0).getAccountId());
        Channel channel = world.getLocal_channels().get(world.getLocal_channels().size() - 1);
        WebcastPro communication = world.getWebcastsPro().get(world.getWebcastsPro().size() - 1);

        accountHandler.associateAccountChannel(channel.getChannelID(), account, programSummary);

        campaign = new DCCampaign();
        campaign = campaignHand.createCampaign(world, campaign, account, programSummary);
        world.getDCCampaigns().add(campaign);
        urlResolver.add("{dccampaign_id}", world.getDCCampaigns().get(0).getCampaignId());  

        List<Map<String, String>> data = table.asMaps(String.class, String.class);

        UserHandler userhandler = new UserHandler();

        for (Map map : data) {

            deliveryDate = map.get("delivery date").toString();
            firstName = map.get("first name").toString();
            lastName = map.get("last name").toString();
            jobTitle = map.get("job title").toString();
            company = map.get("company").toString();
            jobLevel = map.get("job level").toString();
            companySize = map.get("company size").toString();
            industry = map.get("industry").toString();

            user = userhandler.createFulfilledUser(firstName, lastName, jobTitle, company, jobLevel, companySize, industry);

            aud = new Audience(deliveryDate, campaign.getCampaignId(), channel.getChannelID(), communication.getWebcastID(), user);

            audiences.add(aud);

        }

        brightTALKService.getHandler().addAudience(audiences);

    }

    @Given("^I have a dc campaign with following (?:webcasts|webinars)$")
    public void i_have_a_dc_campaign_with_following_webinars(DataTable table) throws Throwable{

        String scheduled;
        String description;
        String presenter;
        String status;
        String tags;
        String visibility;
        String published;
        DCCampaign campaign;

        ArrayList<String> tagList; 

        DCCampaignHandler campaignHand = new DCCampaignHandler();

        AccountHandler accountHandler = new AccountHandler();

        Account account = accountHandler.createAccount();

        ProgramSummary programSummary = accountHandler.createProgramSummary(account);

        world.getAccounts().add(account);
        urlResolver.add("{account_id}", world.getAccounts().get(0).getAccountId());
        Channel channel = world.getLocal_channels().get(world.getLocal_channels().size() - 1);        
        ChannelApi.setChannelFeature(world.getRegressionTestsManagerUser(), channel, "customRegistrationTemplate", "true", "minimum");
        urlResolver.add("{channel_id}", channel.getChannelID());

        accountHandler.associateAccountChannel(channel.getChannelID(), account, programSummary);

        List<Map<String, String>> data = table.asMaps(String.class, String.class);

        for (Map map : data) {

            scheduled = map.get("date time").toString();
            description = map.get("description").toString();
            presenter = map.get("presenter").toString();
            status = map.get("status").toString();
            tags = map.get("tags").toString();
            visibility = map.get("visibility").toString();
            published = map.get("published").toString();

            tagList = new ArrayList<String>(Arrays.asList(tags.split(",")));

            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = df.parse(scheduled);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);

            WebcastPro webcast = new WebcastPro();
            webcast.setStartDate(cal);
            webcast.setDescription(description);
            webcast.setPresenter(presenter);
            webcast.setStatus(status);
            webcast.setTags(tagList);
            webcast.setVisibility(visibility);
            webcast.setPublished(published);
            webcast.setShouldBeDeleted(false);

            if(status.equals("recorded")){
               webcast = brightTALKService.getHandler().createDemandPro(webcast); 
            }else{
               webcast = brightTALKService.getHandler().createPro(webcast);
            }
            
        }

        campaign = new DCCampaign();
        System.out.println("Waiting for create campaign data to be ready in report");
        Thread.sleep(9000);
        campaign = campaignHand.createCampaign(world, campaign, account, programSummary);
        world.getDCCampaigns().add(campaign);
        urlResolver.add("{dccampaign_id}", world.getDCCampaigns().get(0).getCampaignId()); 

    }

}
