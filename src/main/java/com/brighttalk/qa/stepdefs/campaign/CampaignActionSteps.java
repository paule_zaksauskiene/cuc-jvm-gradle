package com.brighttalk.qa.stepdefs.campaign;

import org.openqa.selenium.*;
import com.brighttalk.qa.common.SharedChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;

@ContextConfiguration(
        classes = TestConfig.class)
public class CampaignActionSteps {

    @Autowired
	SharedChromeDriver driver;

    @When("^I edit the campaign name as \"([^\"]*)\"$")
    public void i_edit_campaign_name_as(String campaignName) {

    	try{


    		String pageProp = Util.toYamlPropertyString("campaign-listing-page-edit-button");
        	String locator = PropertiesUtil.getProperty(pageProp);
        	SeleniumUtils.clickElement(driver, By.cssSelector(locator));

        	pageProp = Util.toYamlPropertyString("campaign-listing-page-clear-button");
        	locator = PropertiesUtil.getProperty(pageProp);
			SeleniumUtils.clickElement(driver, By.cssSelector(locator));

        	pageProp = Util.toYamlPropertyString("campaign-listing-page-campaign-name-field");
        	locator = PropertiesUtil.getProperty(pageProp);

        	SeleniumUtils.sendKeys(driver, By.cssSelector(locator), campaignName);

        	pageProp = Util.toYamlPropertyString("campaign-listing-page-save-button");
        	locator = PropertiesUtil.getProperty(pageProp);
        	SeleniumUtils.clickElement(driver, By.cssSelector(locator));

    	} catch (Throwable throwable) {
    		throwable.printStackTrace();
    	}

    }

    @When("^I cancel after editing the campaign name as \"([^\"]*)\"$")
    public void i_cancel_edit_campaign_name_as(String campaignName) {

    	try{

        	String pageProp = Util.toYamlPropertyString("campaign-listing-page-edit-button");
        	String locator = PropertiesUtil.getProperty(pageProp);
        	SeleniumUtils.clickElement(driver, By.cssSelector(locator));

        	pageProp = Util.toYamlPropertyString("campaign-listing-page-clear-button");
        	locator = PropertiesUtil.getProperty(pageProp);
			SeleniumUtils.clickElement(driver, By.cssSelector(locator));

        	pageProp = Util.toYamlPropertyString("campaign-listing-page-campaign-name-field");
        	locator = PropertiesUtil.getProperty(pageProp);

        	SeleniumUtils.sendKeys(driver, By.cssSelector(locator), campaignName);

        	pageProp = Util.toYamlPropertyString("campaign-listing-page-cancel-button");
        	locator = PropertiesUtil.getProperty(pageProp);
        	SeleniumUtils.clickElement(driver, By.cssSelector(locator));

    	} catch (Throwable throwable) {
    		throwable.printStackTrace();
    	}

    }

}