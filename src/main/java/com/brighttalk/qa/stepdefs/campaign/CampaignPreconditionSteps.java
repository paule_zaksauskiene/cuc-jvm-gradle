package com.brighttalk.qa.stepdefs.campaign;



import com.brighttalk.qa.siteElements.*;
import com.brighttalk.qa.api.CampaignApi;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.utils.Util;
import java.util.List;
import com.brighttalk.qa.stepdefs.common.dto.KeyValuePair;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;


@ContextConfiguration(
        classes = TestConfig.class)
public class CampaignPreconditionSteps {

    @Autowired
    BrightTALKService brightTALKService;

    @Autowired
    World world;


    @Given("^(?:\\s?(\\d+|a|an|no|several))?(?: (Active|Inactive|Complete))? (?:campaign|package) exists$")
    public void a_campaign_exists(String count, String campaignStatus) {


		int campaignCount;
		Campaign cam = null;

        if (count.equalsIgnoreCase("a") | count.equalsIgnoreCase("an")) {
            campaignCount = 1;
        } else if (count.equalsIgnoreCase("several")) {
            campaignCount = Util.randomize(3, 9);
        } else if (count.equalsIgnoreCase("no")) {
            campaignCount = 0;
        } else {
            campaignCount = Integer.valueOf(count);
        }

        if (campaignCount > 0) {
            while (campaignCount > 0) {

            	cam = new Campaign();
            	cam.setStatus(campaignStatus);
            	if(campaignStatus.equalsIgnoreCase("complete")){
            		cam.setTotalLeadsFulfilled(cam.getTotalLeadTarget());
            	}
                brightTALKService.getHandler().createCampaign(cam);             
                --campaignCount;
            }
        }

    }


    @Given("^I have a campaign with following details$")
    public void i_have_a_campaign_with_following_details(List<KeyValuePair> table) throws Throwable{

		Campaign cam = new Campaign();

        for (KeyValuePair kvp : table) {
            String key = kvp.getKey();
            String value = kvp.getValue();
            switch(key.toLowerCase()){
            	case "displayname":
            		cam.setDisplayName(value);
            		break;
            	case "status":
            		cam.setStatus(value);
            		break;
            	case "startdate":
            		cam.setStartDate(value);
            		break;
            	case "target":
            		cam.setTotalLeadTarget(Integer.parseInt(value));
            		break;
            	case "leadsfulfilled":
            		cam.setTotalLeadsFulfilled(Integer.parseInt(value));
            		break;
            	case "cpl":
            		cam.setCpl(value);
            		break;
            	case "currency":
            		cam.setCurrency(value.toUpperCase());
            		break;
            	case "earlydelivery":
            		cam.setEarlyDeliveryFlag(Boolean.parseBoolean(value));
            		break;
            	case "companysizefilter":
            		cam.setCompanySizeFilter(value);
            		break;
            	case "joblevelfilter":
            		cam.setJobLevelFilter(value);
            		break;
            	case "countries":
            		cam.setRegions(Util.parseList(value));
            		break;
            	case "exclusions":
            		cam.setCompanyExclusions(Util.parseList(value));
            		break;
            	case "community":
            		cam.setCommunity(value);
            		break;
            	default:
            		throw new IllegalArgumentException(key + " is not a valid field");
            }
        }
        brightTALKService.getHandler().createCampaign(cam);

    }

}