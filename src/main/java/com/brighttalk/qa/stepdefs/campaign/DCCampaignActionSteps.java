package com.brighttalk.qa.stepdefs.campaign;

import org.openqa.selenium.*;
import com.brighttalk.qa.common.SharedChromeDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtils;
import com.brighttalk.qa.utils.Util;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import com.brighttalk.qa.stepdefs.browser.BrowserPreconditionSteps;
import java.util.concurrent.TimeUnit;

@ContextConfiguration(
        classes = TestConfig.class)
public class DCCampaignActionSteps {

    @Autowired
    BrowserPreconditionSteps browserPreconditionSteps;

    @Autowired
	SharedChromeDriver sharedChromeDriver;

    @When("^I edit the dccampaign name as \"([^\"]*)\"$")
    public void i_edit_dccampaign_name_as(String campaignName) {

    	try{

    		WebDriver driver = browserPreconditionSteps.defineWebDriver();
            
    		String pageProp = Util.toYamlPropertyString("dc-campaign-listing-penicon");
        	String locator = PropertiesUtil.getProperty(pageProp);
        	SeleniumUtils.clickElement(driver, By.cssSelector(locator));

        	pageProp = Util.toYamlPropertyString("dc-campaign-listing-nameedit-field");
        	locator = PropertiesUtil.getProperty(pageProp);
			SeleniumUtils.clearText(driver, By.cssSelector(locator));
            SeleniumUtils.sendKeys(driver, By.cssSelector(locator), campaignName);

        	pageProp = Util.toYamlPropertyString("dc-campaign-listing-save-icon");
        	locator = PropertiesUtil.getProperty(pageProp);
        	SeleniumUtils.clickElement(driver, By.cssSelector(locator));


    	} catch (Throwable throwable) {
    		throwable.printStackTrace();
    	}

    }

    @When("^I cancel after editing the dccampaign name as \"([^\"]*)\"$")
    public void i_cancel_edit_dccampaign_name_as(String campaignName) {

    	try{

    		WebDriver driver = browserPreconditionSteps.defineWebDriver();

        	String pageProp = Util.toYamlPropertyString("dc-campaign-listing-penicon");
        	String locator = PropertiesUtil.getProperty(pageProp);
        	SeleniumUtils.clickElement(driver, By.cssSelector(locator));

            pageProp = Util.toYamlPropertyString("dc-campaign-listing-nameedit-field");
            locator = PropertiesUtil.getProperty(pageProp);
            SeleniumUtils.clearText(driver, By.cssSelector(locator));
            SeleniumUtils.sendKeys(driver, By.cssSelector(locator), campaignName);

        	pageProp = Util.toYamlPropertyString("dc-campaign-listing-cancel-icon");
        	locator = PropertiesUtil.getProperty(pageProp);
        	SeleniumUtils.clickElement(driver, By.cssSelector(locator));

    	} catch (Throwable throwable) {
    		throwable.printStackTrace();
    	}

    }
}