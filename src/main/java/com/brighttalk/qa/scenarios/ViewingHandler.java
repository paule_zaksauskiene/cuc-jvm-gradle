package com.brighttalk.qa.scenarios;

import com.brighttalk.qa.api.UserApi;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.siteElements.User;

import java.sql.*;

public class ViewingHandler {

	public ViewingHandler() {


    }

	public String getViewingId(String communicationId) {
		
		User managerUser = new User("Manager");
		UserApi.registerUser(managerUser);
		
		Connection conn = null;
  	    Statement stmt = null;
        ResultSet rs = null;
        String viewingID = "";

  		try {
  			
  			if (TestConfig.getEnv().contains("int01")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int01.brighttalk.net/audience?user=root&password=fred");
  			if (TestConfig.getEnv().contains("int02")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int02.brighttalk.net/audience?user=root&password=fred");
  			if (TestConfig.getEnv().contains("int04")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int04.brighttalk.net/audience?user=root&password=fred");
  			if (TestConfig.getEnv().contains("int05")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int05.brighttalk.net/audience?user=root&password=fred");
  			if (TestConfig.getEnv().contains("test01")) conn = DriverManager.getConnection("jdbc:mysql://db.core.test01.brighttalk.net/audience?user=" + TestConfig.DB_TEST01_USER + "&password=" + TestConfig.DB_TEST01_PASSWORD);
  			
  			stmt = conn.createStatement();
            String myQuery =  "SELECT external_id FROM viewing WHERE communication_id = '" + communicationId + "'";
            rs = stmt.executeQuery(myQuery);

            while (rs.next()) {

                viewingID = rs.getString("external_id");
            }

  	 	}
  		
  		catch (SQLException ex) {
  	 			 	   
  	 		// handle any errors
  	 	    System.out.println("SQLException: " + ex.getMessage());
  	 	    System.out.println("SQLState: " + ex.getSQLState());
  	 	    System.out.println("VendorError: " + ex.getErrorCode());

  	 	}

        return viewingID;
    }
}
