package com.brighttalk.qa.scenarios;

import com.brighttalk.qa.init.EnvSetup;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.siteElements.WebcastPro;

import java.sql.*;
import java.util.Calendar;

public class WebcastHandler {

    public static String getAssetID(String commId, String source_file_extension){

        // First of all we need to find the asset ID for our communication
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String assetID = "0";

        try {

            if (TestConfig.getEnv().contains("int01")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int01.brighttalk.net/asset_management?user=root&password=fred");
            if (TestConfig.getEnv().contains("int02")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int02.brighttalk.net/asset_management?user=root&password=fred");
            if (TestConfig.getEnv().contains("int04")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int04.brighttalk.net/asset_management?user=root&password=fred");
            if (TestConfig.getEnv().contains("int05")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int05.brighttalk.net/asset_management?user=root&password=fred");
            if (TestConfig.getEnv().contains("test01")) conn = DriverManager.getConnection("jdbc:mysql://db.test01.brighttalk.net/asset_management?user=" + EnvSetup.DB_TEST01_USER + "&password=" + EnvSetup.DB_TEST01_PASSWORD);

            stmt = conn.createStatement();
            String myQuery = "SELECT id FROM asset WHERE target_id = '" + commId + "' AND source_file_extension = '" + source_file_extension + "'";
            rs = stmt.executeQuery(myQuery);

            while (rs.next()) {

                assetID = rs.getString("id");
            }

            System.out.println(" [+] Asset Id for communication: "  + assetID);

        }

        catch (SQLException ex) {

            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

        }
        return assetID;
    }

    // Verify if the webinar starts before live support working hours
    public boolean verifyWebinarStartsWithinLiveSupportWorkingHours(WebcastPro myWebcastPro){

        boolean fallsIntoSupportWorkingHours;
        Calendar startSupportWorkingHours = Calendar.getInstance();
        startSupportWorkingHours.setTime(myWebcastPro.getStartDate().getTime());

        startSupportWorkingHours.set(Calendar.HOUR_OF_DAY, 9);
        startSupportWorkingHours.set(Calendar.MINUTE, 0);
        startSupportWorkingHours.set(Calendar.SECOND, 0);

        Calendar stopSupportWorkingHours = Calendar.getInstance();
        stopSupportWorkingHours.setTime(myWebcastPro.getStartDate().getTime());
        stopSupportWorkingHours.set(Calendar.HOUR_OF_DAY, 1);
        stopSupportWorkingHours.set(Calendar.MINUTE, 0);
        stopSupportWorkingHours.set(Calendar.SECOND, 0);

        fallsIntoSupportWorkingHours = !((myWebcastPro.getStartDate().compareTo(startSupportWorkingHours) < 0) && (myWebcastPro.getStartDate().compareTo(stopSupportWorkingHours) > 0));

        return fallsIntoSupportWorkingHours;
    }
}
