package com.brighttalk.qa.scenarios;

import com.brighttalk.qa.conf.TestConfig;
import java.sql.*;
import java.util.ArrayList;

public class AttachmentHandler {
	
	public ArrayList<String> getAttachmentIds(String communicationId) {
		

		Connection conn = null;
  	    Statement stmt = null; 
  		ResultSet rs = null;
  	    ArrayList<String> attachmentIds = new ArrayList<>();
  	   
  		try {
  			
  			if (TestConfig.getEnv().contains("int01")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int01.brighttalk.net/channel?user=root&password=fred");
  			if (TestConfig.getEnv().contains("int02")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int02.brighttalk.net/channel?user=root&password=fred");
  			if (TestConfig.getEnv().contains("int04")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int04.brighttalk.net/channel?user=root&password=fred");
  			if (TestConfig.getEnv().contains("int05")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int05.brighttalk.net/channel?user=root&password=fred");
  			if (TestConfig.getEnv().contains("test01")) conn = DriverManager.getConnection("jdbc:mysql://db.core.test01.brighttalk.net/channel?user="+ TestConfig.DB_TEST01_USER +"&password=" + TestConfig.DB_TEST01_PASSWORD);
  			
  			stmt = conn.createStatement();
  			String myQuery = "SELECT target_id FROM communication_asset WHERE type = 'resource' AND communication_id = '" + communicationId + "'";
  			rs = stmt.executeQuery(myQuery);
  			
  			while (rs.next()) {

                attachmentIds.add(rs.getString("target_id"));
  			}
  	 	}  
  		
  		catch (SQLException ex) {
  	 			 	   
  	 		// handle any errors
  	 	    System.out.println("SQLException: " + ex.getMessage());
  	 	    System.out.println("SQLState: " + ex.getSQLState());
  	 	    System.out.println("VendorError: " + ex.getErrorCode());
  	 	    
  	 	}
		
		return attachmentIds;
	}
}
