package com.brighttalk.qa.scenarios;

import com.brighttalk.qa.siteElements.Account;
import com.brighttalk.qa.siteElements.ProgramSummary;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.common.World;
import org.springframework.stereotype.Component;
import com.brighttalk.qa.common.UrlResolver;
import org.springframework.beans.factory.annotation.Autowired;
import java.sql.*;


public class AccountHandler {

	public Account createAccount() {

		Account account = new Account();
		String accountSfId = account.getAccountSfId();
		String clientName = account.getClientName();
		String type = account.getType();
		String parentClientSfId = account.getParentClientSfId();
		String currency = account.getCurrency();
		String ownerSfId = account.getOwnerSfId();
		String description = account.getDescription();
		String country = account.getCountry();
		String community = account.getCommunity();
		String subCommunity = account.getSubCommunity();
		String industry = account.getIndustry();
		String isActive = account.getIsActive() == true ? "1" : "0";

		Connection conn = null;
  	    Statement stmt = null; 
		ResultSet rs = null;
        String accountId = "";
        try {

            if (TestConfig.getEnv().contains("int02")) conn = DriverManager.getConnection("jdbc:mysql://campaign-db-int02.cx29zyoeyf9j.eu-west-1.rds.amazonaws.com/campaign?user=root&password=Pitestinta6ye");
            if (TestConfig.getEnv().contains("test01")) conn = DriverManager.getConnection("jdbc:mysql://campaign-db-test01.cx29zyoeyf9j.eu-west-1.rds.amazonaws.com/campaign?user=root&password=Pitestinta6ye");

            stmt = conn.createStatement();
            String myQuery = "INSERT INTO account (account_sf_id, client_name, type, parent_client_sf_id, currency, owner_sf_id, description, country, community, sub_community, industry, is_active) VALUES ('"
            				+ accountSfId + "', '" + clientName + "', '" + type + "', '" + parentClientSfId + "', '" + currency + "', '" + ownerSfId + "', '" + description + "', '" + country 
            				+ "', '" + community + "', '" + subCommunity + "', '" + industry + "', '" + isActive + "')";
            stmt.executeUpdate(myQuery);				

            System.out.println(" [+] Salesforce Account " + accountSfId + " inserted.");

            myQuery = "SELECT id FROM account WHERE account_sf_id = '" + accountSfId + "'";
            rs = stmt.executeQuery(myQuery);
            while (rs.next()) {

                accountId = rs.getString("id");
            }

        }

        catch (SQLException ex) {

            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

        }

        account.setAccountId(accountId);

		return account;

	}


	public ProgramSummary createProgramSummary(Account account) {

		ProgramSummary proSum = new ProgramSummary();
		proSum.setAccountId(account.getAccountId());

		String programSummarySfId = proSum.getProgramSummarySfId();
		String name = proSum.getName();
		String title = proSum.getTitle();
		String salesEmail = proSum.getSalesEmail();
		String csmSfId = proSum.getCsmSfId();
		String contractStartDate = proSum.getContractStartDate();
		String contractEndDate = proSum.getContractEndDate();
		String status = proSum.getStatus();
		String totalValue = proSum.getTotalValue();
		String percentCom = proSum.getPercentCom();
		String deleted = proSum.getDeleted() == true ? "1" : "0";
		String isActive = proSum.getIsActive() == true ? "1" : "0";

		Connection conn = null;
  	    Statement stmt = null; 
  	    ResultSet rs = null;

        String programSummaryId = "";
        try {  

            if (TestConfig.getEnv().contains("int02")) conn = DriverManager.getConnection("jdbc:mysql://campaign-db-int02.cx29zyoeyf9j.eu-west-1.rds.amazonaws.com/campaign?user=root&password=Pitestinta6ye");
            if (TestConfig.getEnv().contains("test01")) conn = DriverManager.getConnection("jdbc:mysql://campaign-db-test01.cx29zyoeyf9j.eu-west-1.rds.amazonaws.com/campaign?user=root&password=Pitestinta6ye");

            stmt = conn.createStatement();            
            String myQuery = "INSERT INTO program_summary (program_summary_sf_id, account_id, name, title, sales_executive_email, csm_sf_Id, contract_start_date, contract_end_date, status, total_value, percentage_complete, deleted, is_active) VALUES ('"
            				+ programSummarySfId + "', '" + proSum.getAccountId() + "', '" + name + "', '" + title + "', '" + salesEmail + "', '" + csmSfId + "', '" + contractStartDate + "', '" + contractEndDate 
            				+ "', '" + status + "', '" + totalValue + "', '" + percentCom + "', '" + deleted + "', '" + isActive + "')";
            stmt.executeUpdate(myQuery);				

            System.out.println(" [+] Program Summary " + name + " inserted.");

            myQuery = "SELECT id FROM program_summary WHERE program_summary_sf_id = '" + programSummarySfId + "'";
            rs = stmt.executeQuery(myQuery);
            while (rs.next()) {

                programSummaryId = rs.getString("id");
            }

        }

        catch (SQLException ex) {

            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

        }

        proSum.setProgramSummaryId(programSummaryId);

		return proSum;

	}


	public void associateAccountChannel(String channelId, Account account, ProgramSummary programSummary) {

		Connection conn = null;
  	    Statement stmt = null; 
  	    
  	    String accountId = account.getAccountId();
  	    String programSummaryId = programSummary.getProgramSummaryId();

        try {

            if (TestConfig.getEnv().contains("int02")) conn = DriverManager.getConnection("jdbc:mysql://campaign-db-int02.cx29zyoeyf9j.eu-west-1.rds.amazonaws.com/campaign?user=root&password=Pitestinta6ye");
            if (TestConfig.getEnv().contains("test01")) conn = DriverManager.getConnection("jdbc:mysql://campaign-db-test01.cx29zyoeyf9j.eu-west-1.rds.amazonaws.com/campaign?user=root&password=Pitestinta6ye");

            stmt = conn.createStatement();

            String myQuery = "INSERT INTO account_channel_configuration (channel_id, account_id, program_summary_id, is_active) VALUES ('"
            				+ channelId + "', '" + accountId + "', '" + programSummaryId + "', 1)";
            stmt.executeUpdate(myQuery);				

            System.out.println(" [+] Channel " + channelId + " Account " + accountId + " ProgramSummary " + programSummaryId + " inserted.");

        }

        catch (SQLException ex) {

            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

        }

	}
}