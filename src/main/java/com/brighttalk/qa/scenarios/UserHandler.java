package com.brighttalk.qa.scenarios;

import com.brighttalk.qa.api.UserApi;
import com.brighttalk.qa.common.SharedChromeDriver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.siteElements.User;
import com.brighttalk.qa.utils.PropertiesUtil;
import com.brighttalk.qa.utils.SeleniumUtil;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import java.sql.*;

public class UserHandler {

	public User createUser() {
		
		User anyUser = new User();
		UserApi.registerUser(anyUser);
		
		return anyUser;
		
	}

    public User createUser(String firstName, String secondName, String jobTitle, String company) {

        User anyUser = new User(firstName, secondName, jobTitle, company);
        UserApi.registerUser(anyUser);

        return anyUser;

    }

    public User createFulfilledUser(String firstName, String secondName, String jobTitle, String company, String jobLevel, String companySize, String industry) {

        User anyUser = new User(firstName, secondName, jobTitle, company, jobLevel, companySize, industry);
        UserApi.registerUser(anyUser);

        Connection conn = null;
        Statement stmt = null; 
        ResultSet rs = null;
        String userId = "";

        try {
        
          if (TestConfig.getEnv().contains("int01")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int01.brighttalk.net/user?user=root&password=fred");
          if (TestConfig.getEnv().contains("int02")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int02.brighttalk.net/user?user=root&password=fred");
          if (TestConfig.getEnv().contains("int04")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int04.brighttalk.net/user?user=root&password=fred");
          if (TestConfig.getEnv().contains("int05")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int05.brighttalk.net/user?user=root&password=fred");
          if (TestConfig.getEnv().contains("test01")) conn = DriverManager.getConnection("jdbc:mysql://db.test01.brighttalk.net/user?user=" + TestConfig.DB_TEST01_USER + "&password=" + TestConfig.DB_TEST01_PASSWORD);
        
          stmt = conn.createStatement();
          String myQuery = "SELECT id FROM user WHERE email = '" + anyUser.getEmail() + "'";
          rs = stmt.executeQuery(myQuery);
        
          while (rs.next()) {
          
            userId = rs.getString("id");
          }

          //String userId = getUserId(anyUser);
          anyUser.setUserID(userId);

          
          myQuery = "UPDATE profile SET telephone = '" + anyUser.getTelephone() + "', company_name = '" + company + "', job_title = '" + jobTitle + "', level = '" 
          + jobLevel + "', company_size = '" + companySize + "', industry = '" + industry + "', country = '" + anyUser.getCountry() + "', state_region = '" + anyUser.getState()
          + "' WHERE user_id = '" + userId + "'";
          stmt.executeUpdate(myQuery);        

         }catch (SQLException ex) {
               
          System.out.println("SQLException: " + ex.getMessage());
          System.out.println("SQLState: " + ex.getSQLState());
          System.out.println("VendorError: " + ex.getErrorCode());
          
         }
        return anyUser;

    }


    public User createManageOpsUser() {
		
		User managerUser = new User("Manager");
		UserApi.registerUser(managerUser);
        managerUser.setUserStatus("created");
		
		Connection conn = null;
  	    Statement stmt = null; 
  		ResultSet rs = null;
  	    String managerID = "0";
  	   
  		try {
  			
  			if (TestConfig.getEnv().contains("int01")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int01.brighttalk.net/user?user=root&password=fred");
  			if (TestConfig.getEnv().contains("int02")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int02.brighttalk.net/user?user=root&password=fred");
  			if (TestConfig.getEnv().contains("int04")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int04.brighttalk.net/user?user=root&password=fred");
  			if (TestConfig.getEnv().contains("int05")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int05.brighttalk.net/user?user=root&password=fred");
  			if (TestConfig.getEnv().contains("test01")) conn = DriverManager.getConnection("jdbc:mysql://db.test01.brighttalk.net/user?user=" + TestConfig.DB_TEST01_USER + "&password=" + TestConfig.DB_TEST01_PASSWORD);
  			
  			stmt = conn.createStatement();
  			String myQuery = "SELECT id FROM user WHERE email = '" + managerUser.getEmail() + "'";
  			rs = stmt.executeQuery(myQuery);
  			
  			while (rs.next()) {
  				
  				managerID = rs.getString("id");
  			}
  			
  			String managerRole = "'manager'";
  			myQuery = "INSERT INTO user_role (user_id, role, is_active, created, last_updated) VALUES (" + managerID + ", " + managerRole + ", 1, 0, 0 )";
 			stmt.executeUpdate(myQuery);
 			
 			String operationsRole = "'operations'";
 			myQuery = "INSERT INTO user_role (user_id, role, is_active, created, last_updated) VALUES (" + managerID + ", " + operationsRole + ", 1, 0, 0 )";
 			stmt.executeUpdate(myQuery);
 			
 			System.out.println(" [+] MANAGER/OPS account created: "  + managerUser.getEmail());
  	
  	 	}  
  		
  		catch (SQLException ex) {
  	 			 	   
  	 		// handle any errors
  	 	    System.out.println("SQLException: " + ex.getMessage());
  	 	    System.out.println("SQLState: " + ex.getSQLState());
  	 	    System.out.println("VendorError: " + ex.getErrorCode());
  	 	    
  	 	}

        managerUser.setRegistered(true);
        managerUser.setLoggedin(false);
        return managerUser;
	}

    public User addManagerOpsPermissionsForUser(User myUser){

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String managerID = "0";

        try {

            if (TestConfig.getEnv().contains("int01")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int01.brighttalk.net/user?user=root&password=fred");
            if (TestConfig.getEnv().contains("int02")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int02.brighttalk.net/user?user=root&password=fred");
            if (TestConfig.getEnv().contains("int04")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int04.brighttalk.net/user?user=root&password=fred");
            if (TestConfig.getEnv().contains("int05")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int05.brighttalk.net/user?user=root&password=fred");
            if (TestConfig.getEnv().contains("test01")) conn = DriverManager.getConnection("jdbc:mysql://db.test01.brighttalk.net/user?user=" + TestConfig.DB_TEST01_USER + "&password=" + TestConfig.DB_TEST01_PASSWORD);

            stmt = conn.createStatement();
            String myQuery = "SELECT id FROM user WHERE email = '" + myUser.getEmail() + "'";
            rs = stmt.executeQuery(myQuery);

            while (rs.next()) {

                managerID = rs.getString("id");
            }

            String managerRole = "'manager'";
            myQuery = "INSERT INTO user_role (user_id, role, is_active, created, last_updated) VALUES (" + managerID + ", " + managerRole + ", 1, 0, 0 )";
            stmt.executeUpdate(myQuery);

            String operationsRole = "'operations'";
            myQuery = "INSERT INTO user_role (user_id, role, is_active, created, last_updated) VALUES (" + managerID + ", " + operationsRole + ", 1, 0, 0 )";
            stmt.executeUpdate(myQuery);

            System.out.println(" [+] MANAGER/OPS account created: "  + myUser.getEmail());

        }

        catch (SQLException ex) {

            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

        }

        return myUser;
    }

    public User createManagerUser() {

        User managerUser = new User("Manager");
        UserApi.registerUser(managerUser);

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String managerID = "0";

        try {

            if (TestConfig.getEnv().contains("int01"))
                conn = DriverManager.getConnection("jdbc:mysql://db.core.int01.brighttalk.net/user?user=root&password=fred");
            if (TestConfig.getEnv().contains("int02"))
                conn = DriverManager.getConnection("jdbc:mysql://db.core.int02.brighttalk.net/user?user=root&password=fred");
            if (TestConfig.getEnv().contains("int04"))
                conn = DriverManager.getConnection("jdbc:mysql://db.core.int04.brighttalk.net/user?user=root&password=fred");
            if (TestConfig.getEnv().contains("int05"))
                conn = DriverManager.getConnection("jdbc:mysql://db.core.int05.brighttalk.net/user?user=root&password=fred");
            if (TestConfig.getEnv().contains("test01"))
                conn = DriverManager.getConnection("jdbc:mysql://db.core.test01.brighttalk.net/user?user=" + TestConfig.DB_TEST01_USER + "&password=" + TestConfig.DB_TEST01_PASSWORD );

            stmt = conn.createStatement();
            String myQuery = "SELECT id FROM user WHERE email = '" + managerUser.getEmail() + "'";
            rs = stmt.executeQuery(myQuery);

            while (rs.next()) {

                managerID = rs.getString("id");
            }

            String managerRole = "'manager'";
            myQuery = "INSERT INTO user_role (user_id, role, is_active, created, last_updated) VALUES (" + managerID + ", " + managerRole + ", 1, 0, 0 )";
            stmt.executeUpdate(myQuery);

            System.out.println(" [+] MANAGER account created: " + managerUser.getEmail());

        } catch (SQLException ex) {

            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

        }

        return managerUser;
    }
	
	 public void loginOnPortal(SharedChromeDriver driver, World world, UrlResolver urlResolver) {

         String cookie =  world.getLocal_users().get(0).getSessionCookie();
         String replaceCookie = cookie.replaceAll(";", "");
         cookie = replaceCookie.replace("BTSESSION=", "");

         SeleniumUtil.loadPage(driver, urlResolver.resolve(PropertiesUtil.getProperty("brighttalk-home")));
         driver.manage().addCookie(new Cookie("BTSESSION", cookie));

 	  }

    public String getUserId(User myUser){

        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        String managerID = "0";

        try {

            if (TestConfig.getEnv().contains("int01"))
                conn = DriverManager.getConnection("jdbc:mysql://db.core.int01.brighttalk.net/user?user=root&password=fred");
            if (TestConfig.getEnv().contains("int02"))
                conn = DriverManager.getConnection("jdbc:mysql://db.core.int02.brighttalk.net/user?user=root&password=fred");
            if (TestConfig.getEnv().contains("int04"))
                conn = DriverManager.getConnection("jdbc:mysql://db.core.int04.brighttalk.net/user?user=root&password=fred");
            if (TestConfig.getEnv().contains("int05"))
                conn = DriverManager.getConnection("jdbc:mysql://db.core.int05.brighttalk.net/user?user=root&password=fred");
            if (TestConfig.getEnv().contains("test01"))
                conn = DriverManager.getConnection("jdbc:mysql://db.core.test01.brighttalk.net/user?user=" + TestConfig.DB_TEST01_USER + "&password=" + TestConfig.DB_TEST01_PASSWORD);

            stmt = conn.createStatement();
            String myQuery = "SELECT id FROM user WHERE email = '" + myUser.getEmail() + "'";
            rs = stmt.executeQuery(myQuery);

            while (rs.next()) {

                managerID = rs.getString("id");
            }

        } catch (SQLException ex) {

            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

        }
        return managerID;
    }

    public void injectUserSessionCookie(User user, WebDriver driver){

        String userSessionId = "";
        userSessionId = UserApi.loginUser(user);
        System.out.println("userSessionId: " + userSessionId);
        Cookie ck = new Cookie("BTSESSION", userSessionId);
        driver.manage().addCookie(ck);

    }

    public String loginUserAPI(User user){

        String userSessionId = "";
        userSessionId = UserApi.loginUser(user);
        System.out.println("userSessionId: " + userSessionId);
        return "BTSESSION=" + userSessionId;
    }
}
