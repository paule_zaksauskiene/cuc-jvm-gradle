package com.brighttalk.qa.scenarios;

import com.brighttalk.qa.conf.TestConfig;
import java.sql.*;
import com.brighttalk.qa.api.UserApi;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import com.brighttalk.qa.common.World;
import org.springframework.stereotype.Component;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import java.io.IOException;
import org.junit.Assert;


public class ConnectorHandler {

	@Autowired
    World world;

	public void setUpTestConnector(World world) {


		Connection conn = null;
  	    Statement stmt = null; 
		ResultSet rs = null;

        try {

            if (TestConfig.getEnv().contains("test01")) conn = DriverManager.getConnection("jdbc:mysql://connector-db-test01.cx29zyoeyf9j.eu-west-1.rds.amazonaws.com/connector?user=root&password=Grd63Tes61xfs");

            stmt = conn.createStatement();
            String myQuery = "UPDATE connector SET status = 'Connected', notification = 'none', schedule_type = 'hourly', schedule_value = '0 10 * * * ? *', fields_mapped_count = 4 WHERE channel_id = 2000257657";
            stmt.executeUpdate(myQuery);				
            System.out.println(" [+] Test Connector for channel 2000257657 is reset.");

        }

        catch (SQLException ex) {

            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

        }

    	try
    	{	
			String btSession = UserApi.loginUser (world.getLocal_users().get(0));
			btSession = "BTSESSION=" + btSession;
			HttpClient httpclient = HttpClientBuilder.create().build();
    		HttpPut putUpdateMappings = new HttpPut("https://www.test01.brighttalk.net/service/connector/channel/2000257657/connector/5/mappings?clone=no");

    		JSONObject json = new JSONObject();

    		JSONArray mappedFields = new JSONArray();

        	JSONObject mapping = new JSONObject();
        	mapping.put("brighttalkObject","subscribers");
        	mapping.put("brighttalkField","user.id");
        	mapping.put("targetObject","leads");
        	mapping.put("targetField","person.bTLKUserID");   
        	mappedFields.add(mapping);

        	mapping = new JSONObject();
        	mapping.put("brighttalkObject","subscribersWebcastActivity");
        	mapping.put("brighttalkField","webcast.clientBookingRef");
        	mapping.put("targetObject","leads");
        	mapping.put("targetField","person.bTLKWebcastClientBookingRef");   
        	mappedFields.add(mapping);

        	mapping = new JSONObject();
        	mapping.put("brighttalkObject","subscribers");
        	mapping.put("brighttalkField","user.email");
        	mapping.put("targetObject","leads");
        	mapping.put("targetField","person.email");   
        	mappedFields.add(mapping);

        	mapping = new JSONObject();
        	mapping.put("brighttalkObject","subscribersWebcastActivity");
        	mapping.put("brighttalkField","activityType");
        	mapping.put("targetObject","leads");
        	mapping.put("targetField","person.bTLKActivityType");   
        	mappedFields.add(mapping);

    		json.put("mappedFields",mappedFields);

    		StringEntity input = new StringEntity(json.toString());
    		System.out.println("The update mappings json object is : =====================");
    		System.out.println(json.toString());

    		input.setContentType("application/json");
    		putUpdateMappings.addHeader("Cookie", btSession);
    		putUpdateMappings.setEntity(input);

      		HttpResponse responseFromUpdateMappings = httpclient.execute( putUpdateMappings );

      		Assert.assertTrue("THE RESPONSE WAS NOT 200 AS EXPECTED...", responseFromUpdateMappings.getStatusLine().getStatusCode() == 200);

    	} catch (IOException myIOException) {

      		Assert.fail("IOException in updateConnectorMappings: " + myIOException.getMessage());

    	}

	}
}