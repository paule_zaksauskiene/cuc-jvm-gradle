package com.brighttalk.qa.scenarios;

import com.brighttalk.qa.siteElements.Account;
import com.brighttalk.qa.siteElements.ProgramSummary;
import com.brighttalk.qa.siteElements.DCCampaign;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.api.DCCampaignApi;
import com.brighttalk.qa.conf.TestConfig;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import java.sql.*;
import java.io.IOException;

@Component
public class DCCampaignHandler {

	@Autowired
    World world;
    @Autowired
    UrlResolver urlResolver;

	public DCCampaign createCampaign(World world, DCCampaign myCampaign, Account myAccount, ProgramSummary myProgramSummary) {

		try{
			DCCampaignApi.createCampaign(world, myCampaign, myAccount, myProgramSummary);
		        
		} catch (IOException e) {
            e.printStackTrace();
        }

		Connection conn = null;
  	    Statement stmt = null; 
  	    ResultSet rs = null;
  	    String campaignId = "";


        try {

            if (TestConfig.getEnv().contains("int02")) conn = DriverManager.getConnection("jdbc:mysql://campaign-db-int02.cx29zyoeyf9j.eu-west-1.rds.amazonaws.com/campaign?user=root&password=Pitestinta6ye");
            if (TestConfig.getEnv().contains("test01")) conn = DriverManager.getConnection("jdbc:mysql://campaign-db-test01.cx29zyoeyf9j.eu-west-1.rds.amazonaws.com/campaign?user=root&password=Pitestinta6ye");

            stmt = conn.createStatement();
            String myQuery = "UPDATE campaign SET campaign_sf_id = 'a4MD0000001v1Sj' WHERE name = '" + myCampaign.getCampaignName() + "'";
            stmt.executeUpdate(myQuery);				

            System.out.println(" [+] Campaign Salesforce ID is updated for campaign " + myCampaign.getCampaignName());

            myQuery = "SELECT campaign_key FROM campaign WHERE name = '" + myCampaign.getCampaignName() + "'";

            rs = stmt.executeQuery(myQuery);

            while (rs.next()) {

                campaignId = rs.getString("campaign_key");
            }

            myCampaign.setCampaignId(campaignId);

        }

        catch (SQLException ex) {

            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

        }

        world.getDCCampaigns().add(myCampaign);  
        return myCampaign;

	}
}