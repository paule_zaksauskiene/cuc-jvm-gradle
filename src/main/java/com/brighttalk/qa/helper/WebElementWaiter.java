package com.brighttalk.qa.helper;

import com.google.common.base.Function;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.*;

import java.util.concurrent.TimeUnit;

public class WebElementWaiter {


	public WebElement waitAndFindByID (WebDriver driver, final String id){
		
		 ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
		        public Boolean apply(WebDriver d) {
		          d.findElement(By.id(id));
		          return true;
		        }
		      };

		      FluentWait<WebDriver> w = new WebDriverWait(driver, 50);
		      w.withMessage("The element id: " + id + " is not found on the page: " + driver.getCurrentUrl()).until(e);
		      return driver.findElement(By.id(id));
		      
	}
	
	public WebElement waitAndFindByCss (WebDriver driver, final String css){
		
		 ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
		        public Boolean apply(WebDriver d) {
		          d.findElement(By.cssSelector(css));
		          return true;
		        }
		      };

		      FluentWait<WebDriver> w = new WebDriverWait(driver, 50);
		      w.withMessage("The element css: " + css + " is not found on the page: " + driver.getCurrentUrl()).until(e);
		      return driver.findElement(By.cssSelector(css));
		      
	}
	
	public WebElement waitAndFindByClass (WebDriver driver, final String className){
		
		 ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
		        public Boolean apply(WebDriver d) {
		          d.findElement(By.className(className));
		          return true;
		        }
		      };

		      FluentWait<WebDriver> w = new WebDriverWait(driver, 50);
		      w.withMessage("The element class name: " + className + " is not found on the page: " + driver.getCurrentUrl()).until(e);
		      return driver.findElement(By.className(className));
		      
	}


	public WebElement waitAndFindByXpath (WebDriver driver, final String xPath, long timeoutInSeconds, long pollingInSeconds, final boolean refresh){

        Wait<WebDriver> wait = new FluentWait<>(driver)
            .withTimeout(timeoutInSeconds, TimeUnit.SECONDS)
            .pollingEvery(pollingInSeconds, TimeUnit.SECONDS)
            .ignoring(Exception.class)
            .withMessage("The test has failed on page: " + driver.getCurrentUrl() + "\nThe element not found: " + xPath + "\nWaited " + timeoutInSeconds + " seconds.");

        return wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {

                if (refresh) {
                    //LOG.info(String.format(System.out.println(" [-] Element <%s> not found" + xPath););
                    System.out.println(" [-] Element not Found: " + xPath);
                    driver.navigate().refresh();
                }
                ExpectedConditions.presenceOfElementLocated(By.xpath(xPath));
                WebElement element = driver.findElement(By.xpath(xPath));
                if (element != null) {
                    if (element.isDisplayed()) {
                        //LOG.info(String.format(" [+] Element <%s> found", xPath));
                        System.out.println(" [+] Element Found: " + xPath);
                    }
                } else {
                    //LOG.info(String.format(" [-] Element <%s> not found", xPath));
                    System.out.println(" [-] Element not Found: " + xPath);
                }
                return element;
            }
        });
	}
	
	public WebElement waitAndFindByLinktext (WebDriver driver, final String linkText){
		
		 ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
		        public Boolean apply(WebDriver d) {
		          d.findElement(By.linkText(linkText));
		          return true;
		        }
		      };

		      FluentWait<WebDriver> w = new WebDriverWait(driver, 50);
		      w.withMessage("The element linkText name: " + linkText + " is not found on the page: " + driver.getCurrentUrl()).until(e);
		      return driver.findElement(By.linkText(linkText));
	}    
	
	public WebElement waitAndFindByHeader (WebDriver driver, final String text){
			
		 ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
		        public Boolean apply(WebDriver d) {
		          d.findElement(By.tagName("h1")).getText();
		          return true;
		        }
		      };

		 FluentWait<WebDriver> w = new WebDriverWait(driver, 50);
		 w.withMessage("The element h1: " + text + " is not found on the page: " + driver.getCurrentUrl()).until(e);
		 Assert.assertEquals(text, driver.findElement(By.tagName("h1")).getText());
		 return driver.findElement(By.tagName("h1"));
		   
	} 
	 
	 public WebElement waitAndFindByTag (WebDriver driver, final String tagName, final String text){
			
		 ExpectedCondition<Boolean> e = new ExpectedCondition<Boolean>() {
		        public Boolean apply(WebDriver d) {
		          d.findElement(By.tagName(tagName)).getText();
		          return true;
		        }
		      };

		 FluentWait<WebDriver> w = new WebDriverWait(driver, 50);
		 w.withMessage("The element h1: " + text + " is not found on the page: " + driver.getCurrentUrl()).until(e);
		 System.out.println("I get on the page: " + driver.findElement(By.tagName(tagName)).getText());
		 Assert.assertEquals(text, driver.findElement(By.tagName(tagName)).getText());
		 return driver.findElement(By.tagName(tagName));
		   
	}       
}
