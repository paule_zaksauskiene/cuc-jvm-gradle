package com.brighttalk.qa.helper;

import com.brighttalk.qa.siteElements.WebcastPro;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by pzaksauskiene
 */
public class DateTimeFormatter {

    /**
     * To create string in format: 4:00 pm Mar 08 2014
     */
    public static String presenterInstructionsDateTimeFormat(WebcastPro myWebcastPro) {

        String myHour = Integer.toString(myWebcastPro.getStartDate().get(Calendar.HOUR));

        // If its midnight, then it should be changed to 12 am, not 0 am
        if (myWebcastPro.getStartDate().get(Calendar.HOUR) == 0) {

            myHour = "12";
        }

        String myMin = "";
        if (myWebcastPro.getStartDate().get(Calendar.MINUTE) < 10){

            myMin = "0" + Integer.toString(myWebcastPro.getStartDate().get(Calendar.MINUTE));
        } else {

            myMin = Integer.toString(myWebcastPro.getStartDate().get(Calendar.MINUTE));
        }

        if (myMin.trim().equals("0")){

            myMin = myMin + "0";
        }

        String myAmPm;

        if (myWebcastPro.getStartDate().get(Calendar.AM_PM) == 0) {

            myAmPm = "am";
        } else
            myAmPm = "pm";

        List<String> monthName = new ArrayList<>();
        monthName.add("Jan");
        monthName.add("Feb");
        monthName.add("Mar");
        monthName.add("Apr");
        monthName.add("May");
        monthName.add("Jun");
        monthName.add("Jul");
        monthName.add("Aug");
        monthName.add("Sep");
        monthName.add("Oct");
        monthName.add("Nov");
        monthName.add("Dec");

        String myMonth = monthName.get(myWebcastPro.getStartDate().get(Calendar.MONTH));

        String myDay;

        if (myWebcastPro.getStartDate().get(Calendar.DAY_OF_MONTH) < 10) {

            myDay = "0" + Integer.toString(myWebcastPro.getStartDate().get(Calendar.DAY_OF_MONTH));
        } else myDay = Integer.toString(myWebcastPro.getStartDate().get(Calendar.DAY_OF_MONTH));

        String myYear = Integer.toString(myWebcastPro.getStartDate().get(Calendar.YEAR));

        return myHour + ":" + myMin + " " + myAmPm + " " + myMonth + " " + myDay + " " + myYear;
    }

    /**
     * To create string in format: 4:00 pm Mar 08 2014
     * Jun 09 2016 10:36 am
     */
    public static String webinarPageDateTimeFormat(WebcastPro myWebcastPro) {

        String myHour = Integer.toString(myWebcastPro.getStartDate().get(Calendar.HOUR));


        String myMin = Integer.toString(myWebcastPro.getStartDate().get(Calendar.MINUTE));
        int minutes = Integer.parseInt(myMin);
        if (minutes < 10) myMin = "0" + myMin;

        String myAmPm;

        if (myWebcastPro.getStartDate().get(Calendar.AM_PM) == 0) {

            myAmPm = "am";
        } else
            myAmPm = "pm";

        if ((myHour.contentEquals("0")) && (myAmPm.contentEquals("pm"))) myHour = "12";
        if ((myHour.contentEquals("0")) && (myAmPm.contentEquals("am"))) myHour = "12";

        List<String> monthName = new ArrayList<>();
        monthName.add("Jan");
        monthName.add("Feb");
        monthName.add("Mar");
        monthName.add("Apr");
        monthName.add("May");
        monthName.add("Jun");
        monthName.add("Jul");
        monthName.add("Aug");
        monthName.add("Sep");
        monthName.add("Oct");
        monthName.add("Nov");
        monthName.add("Dec");

        String myMonth = monthName.get(myWebcastPro.getStartDate().get(Calendar.MONTH));

        String myDay;

        if (myWebcastPro.getStartDate().get(Calendar.DAY_OF_MONTH) < 10) {

            myDay = "0" + Integer.toString(myWebcastPro.getStartDate().get(Calendar.DAY_OF_MONTH));
        } else myDay = Integer.toString(myWebcastPro.getStartDate().get(Calendar.DAY_OF_MONTH));

        String myYear = Integer.toString(myWebcastPro.getStartDate().get(Calendar.YEAR));

        return myMonth + " " + myDay + " " + myYear + " " + myHour + ":" + myMin + " " + myAmPm;
    }

    /**
     * To create string in format: 4:00 pm
     */
    public static String webinarPageOnlyTimeFormat(WebcastPro myWebcastPro) {

        String myHour = Integer.toString(myWebcastPro.getStartDate().get(Calendar.HOUR));
        if (myHour.equals("0")) myHour = "12";
        String myMin = Integer.toString(myWebcastPro.getStartDate().get(Calendar.MINUTE));
        int minutes = Integer.parseInt(myMin);
        if (minutes < 10) myMin = "0" + myMin;

        String myAmPm;

        if (myWebcastPro.getStartDate().get(Calendar.AM_PM) == 0) {

            myAmPm = "am";
        } else
            myAmPm = "pm";

        if ((myHour.contentEquals("0")) && (myAmPm.contentEquals("pm"))) myHour = "12";

        return myHour + ":" + myMin + " " + myAmPm;
    }

    /**
     * To create string in format: Mar 08 2014 by given Calendar object
     */
    public static String webinarPageDateOnlyFormat(WebcastPro myWebcastPro) {

        List<String> monthName = new ArrayList<>();
        monthName.add("Jan");
        monthName.add("Feb");
        monthName.add("Mar");
        monthName.add("Apr");
        monthName.add("May");
        monthName.add("Jun");
        monthName.add("Jul");
        monthName.add("Aug");
        monthName.add("Sep");
        monthName.add("Oct");
        monthName.add("Nov");
        monthName.add("Dec");

        String myMonth = monthName.get(myWebcastPro.getStartDate().get(Calendar.MONTH));

        String myDay;

        myDay = Integer.toString(myWebcastPro.getStartDate().get(Calendar.DAY_OF_MONTH));
        String myYear = Integer.toString(myWebcastPro.getStartDate().get(Calendar.YEAR));

        return myMonth + " " + myDay + " " + myYear;
    }


    /*
         To wait number of seconds  specified
     */
    public static void waitForSomeTime(int howManySecondsToWait) {

        try {
                while (howManySecondsToWait > 0){

                    Thread.sleep(1000);
                    System.out.println("Sleeping for " + howManySecondsToWait + " secs ..");
                    howManySecondsToWait = howManySecondsToWait - 1;

                }

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
