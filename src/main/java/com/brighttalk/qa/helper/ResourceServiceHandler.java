package com.brighttalk.qa.helper;

import com.brighttalk.qa.api.AvailableResourcesApi;
import com.brighttalk.qa.init.EnvSetup;
import com.brighttalk.qa.page_objects.common.Pages;
import org.junit.Assert;

public class ResourceServiceHandler {

    public ResourceServiceHandler() {

    }

    public void verifyAvailableResources() {
        int resourceCount = AvailableResourcesApi.getAvailableResources();
        if(resourceCount > 0) {
            System.out.println(" [+] Resources available");
        } else {
            Assert.fail("\n========================================================================" +
                    "\nTHERE IS NO AVAILABLE RESOURCES VIA RESOURCES API! Test will stop.." +
                    "\n=========================================================================");
        }

    }

    public static void releaseResourceAfterTest(String communicationID){

        // Wait for 10 minutes as resource may deallocate automatically - it takes now 10 minutes !
        DateTimeFormatter.waitForSomeTime(600);

        verifyResourceIsAllocatedAndDeallocate(communicationID);
        AvailableResourcesApi.verifyResourceIsErroredForCommunicationAndRepair(communicationID);

    }

    public static void verifyResourceIsAllocatedAndDeallocate(String communicationId){

        AvailableResourcesApi.verifyResourseIsAllocatedAndDeallocate(communicationId);

    }

    public static void cleanResourceService(){

        AvailableResourcesApi.verifyResourcesAreAllocatedAndDeallocate();
        AvailableResourcesApi.verifyResourcesAreErroredAndRepair();
    }
}
