package com.brighttalk.qa;

import com.brighttalk.qa.conf.CustomSpringCucumberRunner;
import cucumber.api.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(CustomSpringCucumberRunner.class)
@CucumberOptions(
        features = "src/main/features/web",
        glue = "com.brighttalk.qa",
        tags = {"@nightly"},
        junit = {"--filename-compatible-names"},
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber-report.json", "junit:target/cucumber-junit-report.xml"}
)
public class RunTests {

}