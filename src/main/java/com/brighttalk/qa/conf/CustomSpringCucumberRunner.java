package com.brighttalk.qa.conf;

import cucumber.api.junit.Cucumber;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import org.junit.runners.model.InitializationError;

import java.io.IOException;

public class CustomSpringCucumberRunner extends Cucumber {
    /**
     * Constructor called by JUnit.
     *
     * @param clazz the class with the @RunWith annotation.
     * @throws IOException                         if there is a problem
     * @throws InitializationError if there is another problem
     */
    public CustomSpringCucumberRunner(Class clazz) throws InitializationError, IOException {

        super(clazz);

        System.setProperty("spring.profiles.active", "selenium");
    }
}
