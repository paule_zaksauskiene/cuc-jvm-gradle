package com.brighttalk.qa.conf;

import com.brighttalk.qa.common.*;
import com.brighttalk.qa.page_objects.common.Pages;
import com.brighttalk.qa.utils.PropertiesUtil;
import org.springframework.context.annotation.*;

@Configuration
@ComponentScan({"com.brighttalk"})
@PropertySources({
        @PropertySource("classpath:config.yml"),
        @PropertySource("classpath:locators.yml"),
        @PropertySource("classpath:urls.yml")
})
//@Import({ RabbitMQConfig.class })
public class TestConfig {

    public static String env = "test01";
    public static String regressionTestsManagerUserEmail = "regressionTestsManagerUser@brighttalk.com";
    public static String regressionTestsManagerUserPassword  = "password";
    public static String demandCentralTestsManagerUserEmail = "demandcentral@brighttalk.com";
    public static String demandCentralTestsManagerUserPassword  = "wf3326493";
    public static String demandCentralTestsManagerUserNAEmail = "fanbrighttest+6@brighttalk.com";
    public static String demandCentralTestsManagerUserNAPassword  = "wf3326493";
    public static String demandCentralTestsOwnerUserEmail = "fanbrighttest+4@brighttalk.com";
    public static String demandCentralTestsOwnerUserPassword  = "wf3326493";
    public static String demandCentralTestsOwnerUserNAEmail = "fanbrighttest+2@brighttalk.com";
    public static String demandCentralTestsOwnerUserNAPassword  = "wf3326493";
    public static String connectorManagerUserEmail = "demandcentral@brighttalk.com";
    public static String connectorManagerUserPassword  = "wf3326493";
    /*
	* TEST01 DATABASE CREDENTIALS
	*/
    public static final String DB_TEST01_USER  = "pzaksauskiene";
    public static final String DB_TEST01_PASSWORD  = "moh8SahS";

    public static boolean user_login_flag = false;

    /*
	* JOIN.ME details
	*/
    public static final String JOIN_ME_URL  = "https://join.me/678-991-863";

    //Forgot Password URL
    public static final String FORGOT_PASSWORD = "https://www." +env+ ".brighttalk.net/mybrighttalk/forgotpassword" ;
    public static final String CHANGE_PASSWORD = "https://www." +env+ ".brighttalk.net/mybrighttalk/changepassword";

    @Bean
    @Profile("selenium")
    SharedChromeDriver sharedChromeDriver() {

        return new SharedChromeDriver();
    }

    @Bean
    @Profile("selenium")
    SharedFirefoxDriver sharedFirefoxDriver() {

        return new SharedFirefoxDriver();
    }

    @Bean
    PropertiesUtil properties() {
        return new PropertiesUtil();
    }

    @Bean
    UrlResolver urlResolver() {

        UrlResolver urlResolver = new UrlResolver();
        urlResolver.add("{env}", TestConfig.getEnv());

        return urlResolver;
    }

    @Bean
    EntityAttributeResolver eaResolver() {
        return new EntityAttributeResolver();
    }

    @Bean
    World world() {
        return new World();
    }

    @Bean
    Pages pages() {

        return new Pages();
    }



    public static String getEnv() {
        return env;
    }

    public static void setEnv(String env) {
        TestConfig.env = env;
    }

    public static String getRegressionTestsManagerUserPassword() {
        return regressionTestsManagerUserPassword;
    }

    public static void setRegressionTestsManagerUserPassword(String regressionTestsManagerUserPassword) {
        TestConfig.regressionTestsManagerUserPassword = regressionTestsManagerUserPassword;
    }

    public static boolean isUser_login_flag() {
        return user_login_flag;
    }

    public static void setUser_login_flag(boolean user_login_flag) {
        TestConfig.user_login_flag = user_login_flag;
    }
}
