package com.brighttalk.qa.siteElements;

import java.util.ArrayList;

public class Vote {

    private String voteQuestion;
    private ArrayList<String> voteOptions;
    private WebcastPro webinar;
    private String voteId;
    private String status;

    private boolean isAdded; // indicates if vote is sucessfully added to the webinar, initially its false

    public Vote(){

        this.voteQuestion = "VoteQuestion_"+System.currentTimeMillis();
        this.voteOptions = new ArrayList<>();
        voteOptions.add("Option1");
        voteOptions.add("Option2");
        voteOptions.add("Option3");
        voteOptions.add("Option4");
        voteOptions.add("Option5");

        isAdded = false;

    }
    public Vote(String voteQuestion, String voteAnswer1, String voteAnswer2, String voteAnswer3, String voteAnswer4, String voteAnswer5){

        this.voteQuestion = voteQuestion;
        this.voteOptions = new ArrayList<>();
        voteOptions.add(voteAnswer1);
        voteOptions.add(voteAnswer2);
        voteOptions.add(voteAnswer3);
        voteOptions.add(voteAnswer4);
        voteOptions.add(voteAnswer5);

        isAdded = false;
    }

    public String getVoteQuestion() {
        return voteQuestion;
    }

    public void setVoteQuestion(String voteQuestion) {
        this.voteQuestion = voteQuestion;
    }

    public ArrayList<String> getVoteOptions() {
        return voteOptions;
    }

    public void setVoteOptions(ArrayList<String> voteOptions) {
        this.voteOptions = voteOptions;
    }

    public WebcastPro getWebinar() {
        return webinar;
    }

    public void setWebinar(WebcastPro webinar) {
        this.webinar = webinar;
    }

    public String getVoteId() {
        return voteId;
    }

    public void setVoteId(String voteId) {
        this.voteId = voteId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public boolean isAdded() {
        return isAdded;
    }

    public void setAdded(boolean added) {
        isAdded = added;
    }
}
