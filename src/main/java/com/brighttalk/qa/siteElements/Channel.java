package com.brighttalk.qa.siteElements;

public class Channel {

	private String  channelID;
	private String  channelTitle;
	private final String  channelTagline;
	private final String  channelDescription;
	private final String  channelOrganization;
	private final String  channelTags;
    private Survey  channelSurvey;
    /**
     * Can be premium, custom, basic, enterprise, professional, standard
     */
	private String  channelType;
	private User 	channelOwner;
	private ContactDetails contactDetails;

	// The channel can be in different status : "notCreated" (when its still not created), "created" (created on environment)
	private String channelStatus;
	private boolean shouldBeDeleted;


	public Channel(){
		
		this.channelID			= "";
		this.channelTitle 		= "Selenium Test Channel "+ System.currentTimeMillis();
		this.channelDescription = "Selenium Test Channel Description";
		this.channelTagline 	= "Selenium Test Channel Tagline";
		this.channelOrganization= "Selenium Test Channel Organization";
		this.channelTags 		= "Selenium Test Channel tag";
		this.channelType 		= "premium";
		this.channelStatus		= "notCreated";
		this.shouldBeDeleted	= true;
	}

	public Channel(String channelID, String channelTitle, String type,
			String channelDescription, String channelTags, String channelOrganization,
			String channelTagline, ContactDetails contactDetails) {
		
		super();
		this.channelID = channelID;
		this.channelTitle = channelTitle;
		this.channelType = type;
		this.channelDescription = channelDescription;
		this.channelTags = channelTags;
		this.channelOrganization = channelOrganization;
		this.channelTagline = channelTagline;
		this.contactDetails = contactDetails;
		this.shouldBeDeleted	= true;
	}

	public void setChannelOwner(User ch_owner){
		this.channelOwner = ch_owner;
		this.channelOwner.is_channel_owner = true;
	}
	
	public User getChannelOwner(){
		return channelOwner;
	}

	public void setChannelID(String channelID) {
		this.channelID = channelID;
	}
	public String getChannelID() {
		return channelID;
	}
	
	public String getChannelTitle() {
		return channelTitle;
	}
	
	public void setChannelTitle(String channelTitle) {
		
		this.channelTitle = channelTitle;
	}
	public String getChannelTagline(){
		return channelTagline;
	}
	public String getChannelDescription(){
		return channelDescription;
	}
	
	public ContactDetails getContactDetails() {
		return contactDetails;
	}
	public void setContactDetails(ContactDetails contactDetails) {
		this.contactDetails = contactDetails;
	}
	
	public String getChannelOrganization(){
		return channelOrganization;
	}
	public String getChannelTags(){
		return channelTags;
	}
	public String getChannelType() {
		return channelType;
	}
	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

    public Survey getChannelSurvey() {
        return channelSurvey;
    }

    public void setChannelSurvey(Survey channelSurvey) {

        this.channelSurvey = channelSurvey;
    }

	public String getChannelStatus() {
		return channelStatus;
	}

	public void setChannelStatus(String channelStatus) {
		this.channelStatus = channelStatus;
	}

	public boolean isShouldBeDeleted() {
		return shouldBeDeleted;
	}

	public void setShouldBeDeleted(boolean shouldBeDeleted) {
		this.shouldBeDeleted = shouldBeDeleted;
	}
}
