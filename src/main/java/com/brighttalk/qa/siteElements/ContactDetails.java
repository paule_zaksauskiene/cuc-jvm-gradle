package com.brighttalk.qa.siteElements;

public class ContactDetails {
	
	private User contactUser;
	private Address contactAddress;
	
	public ContactDetails(User contactUser, Address contactAddress) {
		super();
		this.contactUser = contactUser;
		this.contactAddress = contactAddress;
	}
	
	public User getContactUser() {
		return contactUser;
	}
	public void setContactUser(User contactUser) {
		this.contactUser = contactUser;
	}
	public Address getContactAddress() {
		return contactAddress;
	}
	public void setContactAddress(Address contactAddress) {
		this.contactAddress = contactAddress;
	}
	
}
