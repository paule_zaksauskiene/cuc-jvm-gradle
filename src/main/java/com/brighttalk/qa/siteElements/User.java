package com.brighttalk.qa.siteElements;

public class User {
	
	 private String firstName;
	 private String secondName;
	 private String email;
	 private String telephone;
	 private String jobTitle;
	 private String level;
	 private String company;
	 private String companySize;
	 private String industry;
	 private String timezone;
	 private String country;
	 private String state;
	 private String password;
	 private boolean isLoggedin;
     private String sessionCookie;
     private String userID;


	private boolean isRegistered = false;

	// The status defines if user is created or not on the environment "created", "notCreated" (default)
	 private String userStatus;
	 
	 public boolean is_channel_owner;
	 public boolean is_manager; 
	 
	 public User() {
		
		this.firstName 	= "seleniumFName";
		this.secondName = "seleniumLName";
		this.email 		= "TestUser"+System.currentTimeMillis()+"@brighttalk.com"; 
		this.telephone 	= "123456";
		this.jobTitle 	= "seleniumJobTitle";
		this.level 		= "Director";
		this.company 	= "seleniumCompany";
		this.companySize = "101-250";
		this.industry 	= "Agriculture & Agribusiness";
		this.timezone 	= "Europe/London";
		this.country 	= "United Kingdom";
		this.state 		= "";
		this.password 	= "password";
		this.userStatus	= "notCreated";
		is_channel_owner = false;
		is_manager		 = false;
	
	}

	public User(String firstName, String secondName, String jobTitle, String company) {

		this.firstName 	= firstName;
		this.secondName = secondName;
		this.email 		= "TestUser"+System.currentTimeMillis()+"@brighttalk.com";
		this.telephone 	= "123456";
		this.jobTitle 	= jobTitle;
		this.level 		= "Director";
		this.company 	= company;
		this.companySize = "101-250";
		this.industry 	= "Agriculture & Agribusiness";
		this.timezone 	= "Europe/London";
		this.country 	= "United Kingdom";
		this.state 		= "";
		this.password 	= "password";
		this.userStatus	= "notCreated";
		is_channel_owner = false;
		is_manager		 = false;

	}

	public User(String firstName, String secondName, String jobTitle, String company, String level, String companySize, String industry) {

		this.firstName 	= firstName;
		this.secondName = secondName;
		this.email 		= "TestUser"+System.currentTimeMillis()+"@brighttalk.com";
		this.telephone 	= "123456";
		this.jobTitle 	= jobTitle;
		this.level 		= level;
		this.company 	= company;
		this.companySize = companySize;
		this.industry 	= industry;
		this.timezone 	= "America/Los_Angeles";
		this.country 	= "United States";
		this.state 		= "California";
		this.password 	= "password";
		this.userStatus	= "notCreated";
		is_channel_owner = false;
		is_manager		 = false;
				

	}
	 
	 public User(String manager) {
		 
		 this();
		 this.email = manager+"User"+System.currentTimeMillis()+"@brighttalk.com";
		 is_manager = true;
	 }


	public boolean isLoggedin() {
		return isLoggedin;
	}

	public void setLoggedin(boolean loggedin) {
		isLoggedin = loggedin;
	}
	 
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSecondName() {
		return secondName;
	}

	public void setSecondName(String secondName) {
		this.secondName = secondName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getCompanySize() {
		return companySize;
	}

	public void setCompanySize(String companySize) {
		this.companySize = companySize;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getTimezone() {
		return timezone;
	}

	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(String userStatus) {
		this.userStatus = userStatus;
	}

	public void createAnonymousUserData(User anonymousUser){

		anonymousUser.setFirstName("Not Subscribed");
		anonymousUser.setSecondName("");
		anonymousUser.setJobTitle("Withheld");
		anonymousUser.setCompany("Withheld");

	}

	public String getSessionCookie() {
		return sessionCookie;
	}

	public void setSessionCookie(String sessionCookie) {
		this.sessionCookie = sessionCookie;
	}

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public boolean isRegistered() {
		return isRegistered;
	}

	public void setRegistered(boolean registered) {
		isRegistered = registered;
	}

}


