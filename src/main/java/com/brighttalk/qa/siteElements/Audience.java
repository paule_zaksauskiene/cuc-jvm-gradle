package com.brighttalk.qa.siteElements;

import com.brighttalk.qa.utils.Util;
import com.brighttalk.qa.siteElements.User;

public class Audience {

	private String userId;
	private String campaignId;
	private String channelId;
	private String communicationId;
	private String jobTitle;
	private String company;
	private String jobLevel;
	private String companySize;
	private String industry;
	private String country;
	private String state;
	private String originalReferal;
	private String activityType;
	private String leadType;
	private String embedUrl;
	private String source;
	private String activityDate;
	private boolean autoDelivered;
	private boolean firstActivity;

	public Audience() {

		this.userId = "12345";
		this.campaignId = "12345";
		this.channelId = "12345";
		this.communicationId = "12345";
		this.jobTitle = "seleniumJobTitle";
		this.company = "seleniumCompany";
		this.jobLevel = "Director";
		this.companySize = "101-250";
		this.industry = "Agriculture & Agribusiness";
		this.country = "United States";
		this.state = "California";
		this.originalReferal = "paid";
		this.activityType = "Recorded";
		this.leadType = "content";
		this.embedUrl = "";
		this.source = "";
		this.activityDate = "2017-08-21 00:00:00";
		this.autoDelivered = true;
		this.firstActivity = true;


	}

	public Audience(String activityDate, String campaignId, String channelId, String communicationId, User user) {

		this();
		this.activityDate = activityDate;		
		this.campaignId = campaignId;
		this.channelId = channelId;
		this.communicationId = communicationId;
		this.userId = user.getUserID();
		this.jobTitle = user.getJobTitle();
		this.company = user.getCompany();
		this.jobLevel = user.getLevel();
		this.companySize = user.getCompanySize();
		this.industry = user.getIndustry();


	}

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCampaignId() {
		return campaignId;
	}
	public void setCampaignId(String campaignId) {
		this.campaignId = campaignId;
	}

	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}

	public String getCommunicationId() {
		return communicationId;
	}
	public void setCommunicationId(String communicationId) {
		this.communicationId = communicationId;
	}

	public String getJobTitle() {
		return jobTitle;
	}
	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}

	public String getJobLevel() {
		return jobLevel;
	}
	public void setJobLevel(String jobLevel) {
		this.jobLevel = jobLevel;
	}

	public String getCompanySize() {
		return companySize;
	}
	public void setCompanySize(String companySize) {
		this.companySize = companySize;
	}

	public String getIndustry() {
		return industry;
	}
	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}

	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

	public String getOriginalReferal() {
		return originalReferal;
	}
	public void setOriginalReferal(String originalReferal) {
		this.originalReferal = originalReferal;
	}

	public String getActivityType() {
		return activityType;
	}
	public void setActivityType(String activityType) {
		this.activityType = activityType;
	}

	public String getLeadType() {
		return leadType;
	}
	public void setLeadType(String leadType) {
		this.leadType = leadType;
	}

	public String getEmbedUrl() {
		return embedUrl;
	}
	public void setEmbedUrl(String embedUrl) {
		this.embedUrl = embedUrl;
	}

	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}

	public String getActivityDate() {
		return activityDate;
	}
	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}

	public boolean getAutoDelivered() {
		return autoDelivered;
	}
	public void setAutoDelivered(boolean autoDelivered) {
		this.autoDelivered = autoDelivered;
	}

	public boolean getFirstActivity() {
		return firstActivity;
	}
	public void setFirstActivity(boolean firstActivity) {
		this.firstActivity = firstActivity;
	}

}