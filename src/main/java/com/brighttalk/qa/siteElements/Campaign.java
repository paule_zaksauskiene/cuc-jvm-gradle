package com.brighttalk.qa.siteElements;

import com.brighttalk.qa.utils.Util;
import java.util.*;

public class Campaign {

	private String  campaignID;
	private String  displayName;
	private String  campaignName;
	private String  campaignSfId;

    private String  status;
    private String  leadType;
    private String 	programSummaryId;
    private String  client;
    private String  recipients;
    private String  csmEmail;
    private String  leadEmail;
    private String  campaignValue;
    private String  leadDeliveredValue;
    private String  leadFulfillmentPrcnt;

	private String startDate;
	private String endDate;
	private String firstDeliveryDate;
	private String lastDeliveryDate;	
	private int totalLeadTarget;
	private int totalLeadsFulfilled;

	private boolean noFilter;
	private boolean earlyDeliveryFlag;
	private boolean reEngage;
	private boolean isActive;
	private boolean promote;

	private int leadsByMonth;
	private String cpl;
	private String currency;
	private String jobLevelFilter;
	private String companySizeFilter;
	private ArrayList<String> regions;
	private ArrayList<String> companyExclusions;
	private String community;


	public Campaign(){
		
		String rand = Util.getRand();
		this.campaignID				= "AUD-" + rand;
		this.displayName 			= "";
		this.campaignName       	= "Selenium Test Content Leads";
		this.campaignSfId 	    	= "a4MD0000001v1Sj";
		this.status             	= "Active";
		this.leadType    			= "Webinar";
		this.programSummaryId		= "PROG-" + rand;
		this.client     			= rand;
		this.recipients  			= "fwang@brighttalk.com";
		this.csmEmail       	    = "fwang@brighttalk.com";
		this.leadEmail              = "fwang@brighttalk.com";
		this.campaignValue          = "";
		this.leadDeliveredValue     = "";
		this.leadFulfillmentPrcnt   = "";
		this.startDate			    = "2017-06-16";
		this.endDate				= "2017-08-16";
		this.firstDeliveryDate		= "2017-06-16";
		this.lastDeliveryDate		= "2017-08-16";
		this.totalLeadTarget 		= Util.randomize(200, 1500);
		this.totalLeadsFulfilled	= Util.randomize(1, 199);
		this.noFilter				= false;
		this.earlyDeliveryFlag		= true; 
		this.promote                = true;    
		this.reEngage				= false;
		this.isActive 				= true;
		this.leadsByMonth			= 50;
		this.cpl 					= "57";
		this.currency 				= "USD";
		this.jobLevelFilter 		= "Manager+";
		this.companySizeFilter 		= "100+";
		this.community 				= "Information Technology";
		regions = new ArrayList<String>();
		this.regions.add("United States");
		this.regions.add("Canada");
		this.regions.add("United Kingdom");
		companyExclusions = new ArrayList<String>();
		this.companyExclusions.add("BrightTALK");
		this.companyExclusions.add("Google, Inc");
		this.companyExclusions.add("Moody's");
	}

	public Campaign(String displayName, String campaignName,
		    String status, int totalLeadTarget,
			int totalLeadsFulfilled, boolean earlyDeliveryFlag,
			int leadsByMonth, String cpl, String currency,
			String jobLevelFilter, String companySizeFilter, String community,
			ArrayList<String> regions, ArrayList<String> companyExclusions) {
		
		String rand = Util.getRand();
		this.campaignID				= "AUD-" + rand;
		this.displayName 			= displayName;
		this.campaignName       	= campaignName;
		this.campaignSfId 	    	= "a4MD0000001v1Sj";
		this.status             	= status;
		this.leadType    			= "Webinar";
		this.programSummaryId		= "PROG-" + rand;
		this.client     			= rand;
		this.recipients  			= "fwang@brighttalk.com";
		this.csmEmail       	    = "fwang@brighttalk.com";
		this.leadEmail              = "fwang@brighttalk.com";
		this.campaignValue          = "";
		this.leadDeliveredValue     = "";
		this.leadFulfillmentPrcnt   = "";
		this.startDate			    = "2017-06-16";
		this.endDate				= "2017-08-16";
		this.firstDeliveryDate		= "2017-06-16";
		this.lastDeliveryDate		= "2017-08-16";
		this.totalLeadTarget 		= totalLeadTarget;
		this.totalLeadsFulfilled	= totalLeadsFulfilled;
		this.noFilter				= false;
		this.earlyDeliveryFlag		= earlyDeliveryFlag;  
		this.promote                = true;   
		this.reEngage				= false;
		this.isActive 				= true;
		this.leadsByMonth			= leadsByMonth;
		this.cpl 					= cpl;
		this.currency 				= currency;
		this.jobLevelFilter 		= jobLevelFilter;
		this.companySizeFilter 		= companySizeFilter;
		this.community 				= community;
		this.regions				= regions;
		this.companyExclusions		= companyExclusions;
	}

	public String getCampaignId(){
		return campaignID;
	}

	public void setDisplayName(String displayName){
		this.displayName = displayName;
	}

	public String getDisplayName(){
		return displayName;
	}

	public void setCampaignName(String campaignName){
		this.campaignName = campaignName;
	}

	public String getCampaignName(){
		return campaignName;
	}	

	public void setCampaignSfId(String campaignSfId){
		this.campaignSfId = campaignSfId;
	}

	public String getCampaignSfId(){
		return campaignSfId;
	}	

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}	

	public void setLeadType(String leadType){
		this.leadType = leadType;
	}

	public String getLeadType(){
		return leadType;
	}	

	public void setProgramSummaryId(String programSummaryId){
		this.programSummaryId = programSummaryId;
	}

	public String getProgramSummaryId(){
		return programSummaryId;
	}	

	public void setClient(String client){
		this.client = client;
	}

	public String getClient(){
		return client;
	}	

	public void setRecipients(String recipients){
		this.recipients = recipients;
	}

	public String getRecipients(){
		return recipients;
	}	

	public void setCsmEmail(String csmEmail){
		this.csmEmail = csmEmail;
	}

	public String getCsmEmail(){
		return csmEmail;
	}	

	public void setLeadEmail(String leadEmail){
		this.leadEmail = leadEmail;
	}

	public String getLeadEmail(){
		return leadEmail;
	}	

	public void setCampaignValue(String campaignValue){
		this.campaignValue = campaignValue;
	}

	public String getCampaignValue(){
		return campaignValue;
	}	

	public void setLeadDeliveredValue(String leadDeliveredValue){
		this.leadDeliveredValue = leadDeliveredValue;
	}

	public String getLeadDeliveredValue(){
		return leadDeliveredValue;
	}

	public void setLeadFulfillmentPrcnt(String leadFulfillmentPrcnt){
		this.leadFulfillmentPrcnt = leadFulfillmentPrcnt;
	}

	public String getLeadFulfillmentPrcnt(){
		return leadFulfillmentPrcnt;
	}

	public void setStartDate(String startDate){
		this.startDate = startDate;
	}

	public String getStartDate(){
		return startDate;
	}	

	public void setEndDate(String endDate){
		this.endDate = endDate;
	}

	public String getEndDate(){
		return endDate;
	}	

	public void setFirstDeliveryDate(String firstDeliveryDate){
		this.firstDeliveryDate = firstDeliveryDate;
	}

	public String getFirstDeliveryDate(){
		return firstDeliveryDate;
	}

	public void setLastDeliveryDate(String lastDeliveryDate){
		this.lastDeliveryDate = lastDeliveryDate;
	}

	public String getLastDeliveryDate(){
		return lastDeliveryDate;
	}	

	public void setTotalLeadTarget(int totalLeadTarget){
		this.totalLeadTarget = totalLeadTarget;
	}

	public int getTotalLeadTarget(){
		return totalLeadTarget;
	}	

	public void setTotalLeadsFulfilled(int totalLeadsFulfilled){
		this.totalLeadsFulfilled = totalLeadsFulfilled;
	}

	public int getTotalLeadsFulfilled(){
		return totalLeadsFulfilled;
	}	

	public void setEarlyDeliveryFlag(boolean earlyDeliveryFlag){
		this.earlyDeliveryFlag = earlyDeliveryFlag;
	}

	public boolean getEarlyDeliveryFlag(){
		return earlyDeliveryFlag;
	}	

	public void setPromote(boolean promote){
		this.promote = promote;
	}

	public boolean getPromote(){
		return promote;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean getIsActive(){
		return isActive;
	}	

	public void setLeadsByMonth(int leadsByMonth){
		this.leadsByMonth = leadsByMonth;
	}

	public int getLeadsByMonth(){
		return leadsByMonth;
	}	

	public void setCpl(String cpl){
		this.cpl = cpl;
	}

	public String getCpl(){
		return cpl;
	}	

	public void setCurrency(String currency){
		this.currency = currency;
	}

	public String getCurrency(){
		return currency;
	}	

	public void setJobLevelFilter(String jobLevelFilter){
		this.jobLevelFilter = jobLevelFilter;
	}

	public String getJobLevelFilter(){
		return jobLevelFilter;
	}	

	public void setCompanySizeFilter(String companySizeFilter){
		this.companySizeFilter = companySizeFilter;
	}

	public String getCompanySizeFilter(){
		return companySizeFilter;
	}	

	public void setCommunity(String community){
		this.community = community;
	}

	public String getCommunity(){
		return community;
	}	

	public void setRegions(ArrayList<String> regions){
		this.regions = regions;
	}

	public ArrayList<String> getRegions(){
		return regions;
	}	

	public void setCompanyExclusions(ArrayList<String> companyExclusions){
		this.companyExclusions = companyExclusions;
	}

	public ArrayList<String> getCompanyExclusions(){
		return companyExclusions;
	}	

	public void setReEngageFlag(boolean reEngage){
		this.reEngage = reEngage;
	}

	public boolean getReEngageFlag(){
		return reEngage;
	}	

	public void setNoFilterFlag(boolean noFilter){
		this.noFilter = noFilter;
	}

	public boolean getNoFilterFlag(){
		return noFilter;
	}


}
