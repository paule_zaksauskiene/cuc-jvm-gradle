package com.brighttalk.qa.siteElements;

import com.brighttalk.qa.utils.Util;

public class ProgramSummary {

	private String programSummaryId;
	private String programSummarySfId;
	private String accountId;
	private String name;
	private String title;
	private String salesEmail;
	private String csmSfId;
	private String contractStartDate;
	private String contractEndDate;
	private String status;
	private String totalValue;
	private String percentCom;
	private boolean deleted;
	private boolean isActive;

	public ProgramSummary() {

		String rand = Util.getRand();
		this.programSummaryId = "";
		this.programSummarySfId = rand;
		this.accountId = "1";
		this.name = "PROG-" + rand.substring(0,6);
		this.title = "Selenium Test Program Summary " + rand.substring(0,5);
		this.salesEmail = "fwang@brighttalk.com";
		this.csmSfId = "C" + rand;
		this.contractStartDate = "2017-06-15";
		this.contractEndDate = "2017-09-15";
		this.status = "Active";
		this.totalValue = "200.45";
		this.percentCom = "55.56";
		this.deleted = false;
		this.isActive = true;

	}

	public String getProgramSummaryId() {
		return programSummaryId;
	}

	public void setProgramSummaryId(String programSummaryId) {
		this.programSummaryId = programSummaryId;
	}

	public String getProgramSummarySfId() {
		return programSummarySfId;
	}

	public void setProgramSummarySfId(String programSummarySfId) {
		this.programSummarySfId = programSummarySfId;
	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSalesEmail() {
		return salesEmail;
	}

	public void setSalesEmail(String salesEmail) {
		this.salesEmail = salesEmail;
	}

	public String getCsmSfId() {
		return csmSfId;
	}

	public void setCsmSfId(String csmSfId) {
		this.csmSfId = csmSfId;
	}

	public String getContractStartDate() {
		return contractStartDate;
	}

	public void setContractStartDate(String contractStartDate) {
		this.contractStartDate = contractStartDate;
	}

	public String getContractEndDate() {
		return contractEndDate;
	}

	public void setContractEndDate(String contractEndDate) {
		this.contractEndDate = contractEndDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(String totalValue) {
		this.totalValue = totalValue;
	}

	public String getPercentCom() {
		return percentCom;
	}

	public void setPercentCom(String percentCom) {
		this.percentCom = percentCom;
	}

	public boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(boolean deleted) {
		this.deleted = deleted;
	}

	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

}