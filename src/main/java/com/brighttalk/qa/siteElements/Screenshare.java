package com.brighttalk.qa.siteElements;

public class Screenshare {
	
	private String joinMeLink;
	private String name;
	
	public Screenshare(String joinMeLink, String name) {
		
		super();
		this.joinMeLink = joinMeLink;
		this.name = name;
	}
	
	
	public String getJoinMeLink() {
		return joinMeLink;
	}
	public void setJoinMeLink(String joinMeLink) {
		this.joinMeLink = joinMeLink;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
