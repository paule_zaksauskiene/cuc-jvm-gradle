package com.brighttalk.qa.siteElements;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Timezone {
	
	public static final Map<String, String> timezoneList;
    public static final Map<String, String> locationToTimezoneList;
	
	static {

        // Timezone to location
		Map<String, String> aMap = new HashMap<>();
		
        aMap.put("United Kingdom - London", "GMT");
        aMap.put("United States - Chicago", "America/Chicago");
        aMap.put("Europe/London", "GMT");
        
        timezoneList = Collections.unmodifiableMap(aMap);

        // Location to timezone
        Map<String, String> locationToTimezoneMap = new HashMap<>();

        locationToTimezoneMap.put("America/Chicago", "United States - Chicago");
        locationToTimezoneMap.put("Europe/London", "United Kingdom - London");

        locationToTimezoneList = Collections.unmodifiableMap(locationToTimezoneMap);
    }

}
