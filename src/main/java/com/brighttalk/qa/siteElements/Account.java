package com.brighttalk.qa.siteElements;

import com.brighttalk.qa.utils.Util;

public class Account {

	private String accountId;
	private String accountSfId;
	private String clientName;
	private String type;
	private String parentClientSfId;
	private String currency;
	private String ownerSfId;
	private String description;
	private String country;
	private String community;
	private String subCommunity;
	private String industry;
	private boolean isActive;

	public Account() {

		String rand = Util.getRand();
		this.accountId = "";
		this.accountSfId = rand;
		this.clientName = "Selenium Test Client " + rand.substring(0,5);
		this.type = "client";
		this.parentClientSfId = "";
		this.currency = "USD";
		this.ownerSfId = "W" + rand;
		this.description = "Selenium Test Account";
		this.country = "United States";
		this.community = "Information Technology";
		this.subCommunity = "Cloud Computing";
		this.industry = "Cloud Services";
		this.isActive = true;

	}

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountSfId() {
		return accountSfId;
	}

	public void setAccountSfId(String accountSfId) {
		this.accountSfId = accountSfId;
	}

	public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getParentClientSfId() {
		return parentClientSfId;
	}

	public void setParentClientSfId(String parentClientSfId) {
		this.parentClientSfId = parentClientSfId;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getOwnerSfId() {
		return ownerSfId;
	}

	public void setOwnerSfId(String ownerSfId) {
		this.ownerSfId = ownerSfId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCommunity() {
		return community;
	}

	public void setCommunity(String community) {
		this.community = community;
	}

	public String getSubCommunity() {
		return subCommunity;
	}

	public void setSubCommunity(String subCommunity) {
		this.subCommunity = subCommunity;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(boolean isActive) {
		this.isActive = isActive;
	}

}