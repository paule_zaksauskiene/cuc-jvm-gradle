package com.brighttalk.qa.siteElements;

import java.util.Calendar;

public class OnDemandVideo {

	private String channelID;
	private String onDemandVideoID;
	private String title;
	private String description;
	private String presenter;
	private Calendar startDate;
	private String tags;
	private String bookingType;
	private String visibility;
	private String status; // can be "processing, "recorded"

	private String publishStatus;

	public OnDemandVideo() {

		title = "Test_video_"+System.currentTimeMillis();
		description = "Test_video_Description";
		presenter = "author";
		tags = "Selenium test tags";
		bookingType = "within"; //This can "outside" as well
		visibility = "public"; //can be private
		status = "processing";
	}
	   
	public String getChannelID() {
		return channelID;
	}


	public void setChannelID(String channelID) {
		this.channelID = channelID;
	}


	public String getOnDemandVideoID() {
		return onDemandVideoID;
	}


	public void setOnDemandVideoID(String onDemandVideoID) {
		this.onDemandVideoID = onDemandVideoID;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getPresenter() {
		return presenter;
	}


	public void setPresenter(String presenter) {
		this.presenter = presenter;
	}


	public Calendar getStartDate() {
		return startDate;
	}


	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}


	public String getTags() {
		return tags;
	}


	public void setTags(String tags) {
		this.tags = tags;
	}


	public String getBookingType() {
		return bookingType;
	}


	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}


	public String getVisibility() {
		return visibility;
	}


	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPublishStatus() {
		return publishStatus;
	}

	public void setPublishStatus(String publishStatus) {
		this.publishStatus = publishStatus;
	}

}
