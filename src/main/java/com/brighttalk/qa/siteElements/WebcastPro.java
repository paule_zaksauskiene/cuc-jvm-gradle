package com.brighttalk.qa.siteElements;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class WebcastPro {

	private String channelID;
	private String webcastID;
	private String title;
	private String description;
	private String presenter;
	private String duration;
	private Calendar startDate;
	private String timezone;
	private ArrayList<String> tags;
	private String bookingType;
	private String visibility;
	private String status;
	private String PIN;
	private String campaignReference;
	private String published;
    private Calendar publishUnpublishDate;
	private String webcastUrl;
	private boolean isServiced;
	List<Attachment> webcastAttachments;
	private Survey WebcastSurvey;
    List<Community> webcastCommunities;
    private String categorizationTag;
    private String viewerAccess;
	List<Vote> webcastVotes;
	private boolean isResourceNeeded;
	private boolean shouldBeDeleted;
	private String featureImageName;

	
	public WebcastPro(){
		   
		this("");
    }
	   
	public WebcastPro(String when) {

			title 		= "Selenium_Test_webcast_"+System.currentTimeMillis();
			description = "Description for Test_webcast";
			presenter 	= "Brighttalk webcast presenter";
			duration  	= "15";
			startDate 	= Calendar.getInstance();
			if (when.contains("tomorrow")){
				
				startDate.add(Calendar.DAY_OF_MONTH, 1);
			} else 
			{
				startDate = Calendar.getInstance();
				startDate.add(Calendar.MONTH, 1);
			}
			
			startDate.set(Calendar.MINUTE, 15);
			timezone 	= "United Kingdom - London";
			tags = new ArrayList<>();
			tags.add("TestTags");
			bookingType = "Within channel's content plan"; //This can "outside" as well
			visibility 	= "public"; //can be private
			published = "Publish";
			status 		= "upcoming";
			campaignReference = "";
			webcastUrl = "";
            webcastCommunities = null;
            categorizationTag = "";
            viewerAccess = "Registration";
		    webcastVotes = new ArrayList<Vote>();
			webcastAttachments = new ArrayList<Attachment>();
		    isServiced = false;
		    isResourceNeeded = true;
		    shouldBeDeleted = true;
	}
	
	public String getPublished() {
		return published;
	}

	public void setPublished(String published) {
		this.published = published;
	}
	
	public String getWebcastUrl() {
		return webcastUrl;
	}

	public void setWebcastUrl(String webcastUrl) {
		this.webcastUrl = webcastUrl;
	}

	public List<Attachment> getWebcastAttachments() {
		return webcastAttachments;
	}
	public void setWebcastAttachments(List<Attachment> webcastAttachments) {
		this.webcastAttachments = webcastAttachments;
	}
	public void setPIN(String pIN) {
		PIN = pIN;
	}
	public String getPIN() {
		return PIN;
	}
	public String getBookingType() {
		return bookingType;
	}
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public String getChannelID() {
		return channelID;
	}
	public void setChannelID(String channelID) {
		this.channelID = channelID;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPresenter() {
		return presenter;
	}
	public void setPresenter(String presenter) {
		this.presenter = presenter;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public Calendar getStartDate() {
		return startDate;
	}
	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	
	public ArrayList<String> getTags() {
		return tags;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	public String getWebcastID() {
		return webcastID;
	}
	public void setWebcastID(String webcastID) {
		this.webcastID = webcastID;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getCampaignReference() {

		return campaignReference;
	}

	public void setCampaignReference(String campaignReference) {

		this.campaignReference = campaignReference;
	}

	   
    public Survey getWebcastSurvey() {
			return WebcastSurvey;
	}

	public void setWebcastSurvey(Survey webcastSurvey) {
			WebcastSurvey = webcastSurvey;
	}

    public List<Community> getWebcastCommunities() {
        return webcastCommunities;
    }

    public void setWebcastCommunities(List<Community> webcastCommunities) {
        this.webcastCommunities = webcastCommunities;
    }

    public String getCategorizationTag() {
        return categorizationTag;
    }

    public void setCategorizationTag(String categorizationTag) {
        this.categorizationTag = categorizationTag;
    }
    public String getViewerAccess() {
        return viewerAccess;
    }

    public void setViewerAccess(String viewerAccess) {
        this.viewerAccess = viewerAccess;
    }

    public Calendar getPublishUnpublishDate() {
        return publishUnpublishDate;
    }

    public void setPublishUnpublishDate(Calendar publishUnpublishDate) {
        this.publishUnpublishDate = publishUnpublishDate;
    }

	public List<Vote> getWebcastVotes() {
		return webcastVotes;
	}

	public void setWebcastVotes(List<Vote> webcastVotes) {
		this.webcastVotes = webcastVotes;
	}

	public boolean isServiced() {
		return isServiced;
	}

	public void setServiced(boolean serviced) {
		isServiced = serviced;
	}
	public boolean isResourceNeeded() {
		return isResourceNeeded;
	}

	public void setResourceNeeded(boolean resourceNeeded) {
		isResourceNeeded = resourceNeeded;
	}

	public boolean isShouldBeDeleted() {
		return shouldBeDeleted;
	}

	public void setShouldBeDeleted(boolean shouldBeDeleted) {
		this.shouldBeDeleted = shouldBeDeleted;
	}

	public String getFeatureImageName() {
		return featureImageName;
	}

	public void setFeatureImageName(String featureImageName) {
		this.featureImageName = featureImageName;
	}

}
