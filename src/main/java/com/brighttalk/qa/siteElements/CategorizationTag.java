package com.brighttalk.qa.siteElements;

/**
 * Created by Paule on 10/01/2017.
 */
public class CategorizationTag {

    private String id;
    private String category;
    private String tagValue;
    //private com.brighttalk.testing.service.channel.dto.Channel ownerChannel;

    //private com.brighttalk.testing.service.communication.dto.Webcast ownerWebcast;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTagValue() {
        return tagValue;
    }

    public void setTagValue(String tagValue) {
        this.tagValue = tagValue;
    }

   // public Channel getOwnerChannel() {
    //    return ownerChannel;
    //}

   // public void setOwnerChannel(Channel ownerChannel) {
      //  this.ownerChannel = ownerChannel;
    //}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    //public Webcast getOwnerWebcast() {
     //   return ownerWebcast;
   // }

    //public void setOwnerWebcast(Webcast ownerWebcast) {
        //this.ownerWebcast = ownerWebcast;
    //}
}
