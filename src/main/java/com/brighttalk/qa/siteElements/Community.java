package com.brighttalk.qa.siteElements;

import java.util.ArrayList;

public class Community {
	
	
	public String title = "SeleniumCommunity"+System.currentTimeMillis();
	public String alias = "alias"+System.currentTimeMillis();
	private String description;


	//public ArrayList<String> innerCommunities = new ArrayList<String>();
	public ArrayList<Community> innerCommunities;

	public Community(String title, ArrayList<Community> subcommunities){
		
		this.title = title;
		this.innerCommunities = subcommunities;
	}

	

	public Community() {
		
		description = "Test description";
		this.innerCommunities = new ArrayList<>();
		
	}

    // Creates single random community with subcommunity
    public Community (String single){

        this.description = "description";
        this.innerCommunities = new ArrayList<>();

        Community subCommunity = new Community();
        subCommunity.setTitle(subCommunity.getTitle() + "1");
        subCommunity.setAlias(subCommunity.getAlias() + "1");
        innerCommunities.add(subCommunity);

    }

	
	public String getTitle() {
			return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
		
}
	
	public String getAlias() {
		return alias;
	}
	
	public void setAlias(String alias) {
		this.alias = alias;
		}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public ArrayList<Community> getInnerCommunities() {
		return innerCommunities;
	}

	public void setInnerCommunities(ArrayList<Community> innerCommunities) {
		this.innerCommunities = innerCommunities;
	}
}
