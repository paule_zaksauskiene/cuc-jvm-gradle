package com.brighttalk.qa.siteElements;

import java.util.ArrayList;

public class Survey {
	
	boolean isActive;
	ArrayList<SurveyQuestion> surveyQuestions;
	
   public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public ArrayList<SurveyQuestion> getSurveyQuestions() {
		return surveyQuestions;
	}

	public void setSurveyQuestions(ArrayList<SurveyQuestion> surveyQuestions) {
		this.surveyQuestions = surveyQuestions;
	}

	public Survey(boolean isActive, ArrayList<SurveyQuestion> surveyQuestions) {
		super();
		this.isActive = isActive;
		this.surveyQuestions = surveyQuestions;
	}
	
	public Survey(){
		
   }
}
