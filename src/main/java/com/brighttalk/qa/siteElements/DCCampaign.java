package com.brighttalk.qa.siteElements;

import com.brighttalk.qa.utils.Util;
import java.util.*;
import java.lang.Integer;

public class DCCampaign {

	private String  campaignID;
	private String  displayName;
	private String  campaignName;
	private String  campaignSfId;
	private String  accountSfId;

    private String  status;
    private String  leadType;
    private String 	programSummaryId;
    private String  client;
    private String  recipients;
    private String  csmEmail;
    private String  leadEmail;

	private String startDate;
	private String endDate;
	private String firstDeliveryDate;
	private String lastDeliveryDate;	
	private int totalLeadTarget;
	private int totalLeadsFulfilled;
	private int totalSummitLeadsFulfilled;
	private int budget;
	private int newBudget;
    private int  leadDeliveredValue;
    private String  leadFulfillmentPrcnt;
    private int  campaignValue;

	private boolean noFilter;
	private boolean earlyDeliveryFlag;
	private boolean reEngage;
	private boolean isActive;
	private boolean promote;
	private boolean createBookingFlag;

	private int leadsByMonth;
	private String leadFulfillmentPrcntMonthly;
	private String cpl;
	private String currency;
	private String jobLevelFilter;
	private String companySizeFilter;
	private ArrayList<String> regions;
	private ArrayList<String> companyExclusions;
	private ArrayList<String> partnerExclusions;
	private String community;
	private String comments;

	public DCCampaign(){

		String rand = Util.getRand();
		String[] jobLevel = {"All", "Manager+"};
		String[] companySize = {"All", "10+", "100+", "250+", "500+", "1000+", "2500+", "5000+"};
		String[] status = {"Active", "Inactive", "Complete", "Pending"};
		
		this.campaignID				= "AUD-" + rand;
		this.displayName 			= "";
		this.campaignName       	= "Selenium-" + rand;
		this.campaignSfId 	    	= "a4MD0000001v1Sj";
		this.status             	= Util.getRandString(status);;
		this.leadType    			= "Webinar";
		this.programSummaryId		= "PROG-" + rand;
		this.client     			= rand;
		this.recipients  			= "fwang@brighttalk.com";
		this.csmEmail       	    = "fwang@brighttalk.com";
		this.leadEmail              = "fwang@brighttalk.com";
		this.startDate			    = "2017-06-16";
		this.endDate				= "2017-10-16";
		this.firstDeliveryDate		= "2017-06-16";
		this.lastDeliveryDate		= "2017-08-16";
		this.totalLeadTarget 		= Util.randomize(200, 1500);
		this.totalLeadsFulfilled	= Util.randomize(1, 199);
		this.noFilter				= false;
		this.earlyDeliveryFlag		= true; 
		this.promote                = true;    
		this.reEngage				= false;
		this.isActive 				= true;
		this.createBookingFlag		= false;
		this.leadsByMonth			= 50;
		this.cpl 					= "57";
		this.currency 				= "USD";
		this.campaignValue          = Integer.parseInt(this.cpl)*this.totalLeadTarget;
		this.budget                 = Integer.parseInt(this.cpl)*this.totalLeadTarget;
		this.leadDeliveredValue     = Integer.parseInt(this.cpl)*this.totalLeadsFulfilled;
		this.leadFulfillmentPrcnt   = "";
		this.jobLevelFilter 		= Util.getRandString(jobLevel);
		this.companySizeFilter 		= Util.getRandString(companySize);
		this.comments 				= "";
		this.community 				= "Information Technology";
		regions = new ArrayList<String>();
		this.regions.add("United States");
		this.regions.add("Canada");
		this.regions.add("United Kingdom");
		companyExclusions = new ArrayList<String>();
		this.companyExclusions.add("BrightTALK");
		this.companyExclusions.add("Google, Inc");
		this.companyExclusions.add("Moody's");
		partnerExclusions = new ArrayList<String>();
		this.partnerExclusions.add("BrightTALK");

	}


	public DCCampaign(String displayName, String startdate, String status, int target, int fulfillment) {

		this();
		this.displayName 		 = displayName;
		this.startDate   		 = startdate;
		this.status      		 = status;
		this.totalLeadTarget     = target;
		this.totalLeadsFulfilled = fulfillment;

	}

	public void setCampaignId(String campaignID){
		this.campaignID = campaignID;
	}

	public String getCampaignId(){
		return campaignID;
	}

	public void setDisplayName(String displayName){
		this.displayName = displayName;
	}

	public String getDisplayName(){
		return displayName;
	}

	public void setCampaignName(String campaignName){
		this.campaignName = campaignName;
	}

	public String getCampaignName(){
		return campaignName;
	}	

	public void setCampaignSfId(String campaignSfId){
		this.campaignSfId = campaignSfId;
	}

	public String getCampaignSfId(){
		return campaignSfId;
	}	

	public void setAccountSfId(String accountSfId){
		this.accountSfId = accountSfId;
	}

	public String getAccountSfId(){
		return accountSfId;
	}	

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}	

	public void setLeadType(String leadType){
		this.leadType = leadType;
	}

	public String getLeadType(){
		return leadType;
	}	

	public void setProgramSummaryId(String programSummaryId){
		this.programSummaryId = programSummaryId;
	}

	public String getProgramSummaryId(){
		return programSummaryId;
	}	

	public void setClient(String client){
		this.client = client;
	}

	public String getClient(){
		return client;
	}	

	public void setRecipients(String recipients){
		this.recipients = recipients;
	}

	public String getRecipients(){
		return recipients;
	}	

	public void setCsmEmail(String csmEmail){
		this.csmEmail = csmEmail;
	}

	public String getCsmEmail(){
		return csmEmail;
	}	

	public void setLeadEmail(String leadEmail){
		this.leadEmail = leadEmail;
	}

	public String getLeadEmail(){
		return leadEmail;
	}	

	public void setCampaignValue(int campaignValue){
		this.campaignValue = campaignValue;
	}

	public int getCampaignValue(){
		return campaignValue;
	}	

	public void setLeadDeliveredValue(int leadDeliveredValue){
		this.leadDeliveredValue = leadDeliveredValue;
	}

	public int getLeadDeliveredValue(){
		return leadDeliveredValue;
	}

	public void setLeadFulfillmentPrcnt(String leadFulfillmentPrcnt){
		this.leadFulfillmentPrcnt = leadFulfillmentPrcnt;
	}

	public String getLeadFulfillmentPrcnt(){
		return leadFulfillmentPrcnt;
	}

	public void setLeadFulfillmentPrcntMonthly(String leadFulfillmentPrcntMonthly){
		this.leadFulfillmentPrcntMonthly = leadFulfillmentPrcntMonthly;
	}

	public String getLeadFulfillmentPrcntMonthly(){
		return leadFulfillmentPrcntMonthly;
	}

	public void setStartDate(String startDate){
		this.startDate = startDate;
	}

	public String getStartDate(){
		return startDate;
	}	

	public void setEndDate(String endDate){
		this.endDate = endDate;
	}

	public String getEndDate(){
		return endDate;
	}	

	public void setFirstDeliveryDate(String firstDeliveryDate){
		this.firstDeliveryDate = firstDeliveryDate;
	}

	public String getFirstDeliveryDate(){
		return firstDeliveryDate;
	}

	public void setLastDeliveryDate(String lastDeliveryDate){
		this.lastDeliveryDate = lastDeliveryDate;
	}

	public String getLastDeliveryDate(){
		return lastDeliveryDate;
	}	

	public void setTotalLeadTarget(int totalLeadTarget){
		this.totalLeadTarget = totalLeadTarget;
	}

	public int getTotalLeadTarget(){
		return totalLeadTarget;
	}	

	public void setTotalLeadsFulfilled(int totalLeadsFulfilled){
		this.totalLeadsFulfilled = totalLeadsFulfilled;
	}

	public int getTotalLeadsFulfilled(){
		return totalLeadsFulfilled;
	}	

	public void setTotalSummitLeadsFulfilled(int totalSummitLeadsFulfilled){
		this.totalSummitLeadsFulfilled = totalSummitLeadsFulfilled;
	}

	public int getTotalSummitLeadsFulfilled(){
		return totalSummitLeadsFulfilled;
	}	

	public void setEarlyDeliveryFlag(boolean earlyDeliveryFlag){
		this.earlyDeliveryFlag = earlyDeliveryFlag;
	}

	public void setBudget(int budget){
		this.budget = budget;
	}

	public int getBudget(){
		return budget;
	}	

	public void setNewBudget(int newBudget){
		this.newBudget = newBudget;
	}

	public int getNewBudget(){
		return newBudget;
	}	

	public boolean getEarlyDeliveryFlag(){
		return earlyDeliveryFlag;
	}	

	public void setPromote(boolean promote){
		this.promote = promote;
	}

	public boolean getPromote(){
		return promote;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean getIsActive(){
		return isActive;
	}	

	public void setCreateBookingFlag(boolean createBookingFlag){
		this.createBookingFlag = createBookingFlag;
	}

	public boolean getCreateBookingFlag(){
		return createBookingFlag;
	}	

	public void setLeadsByMonth(int leadsByMonth){
		this.leadsByMonth = leadsByMonth;
	}

	public int getLeadsByMonth(){
		return leadsByMonth;
	}	

	public void setCpl(String cpl){
		this.cpl = cpl;
	}

	public String getCpl(){
		return cpl;
	}	

	public void setCurrency(String currency){
		this.currency = currency;
	}

	public String getCurrency(){
		return currency;
	}	

	public void setJobLevelFilter(String jobLevelFilter){
		this.jobLevelFilter = jobLevelFilter;
	}

	public String getJobLevelFilter(){
		return jobLevelFilter;
	}	

	public void setCompanySizeFilter(String companySizeFilter){
		this.companySizeFilter = companySizeFilter;
	}

	public String getCompanySizeFilter(){
		return companySizeFilter;
	}	

	public void setCommunity(String community){
		this.community = community;
	}

	public String getCommunity(){
		return community;
	}	

	public void setRegions(ArrayList<String> regions){
		this.regions = regions;
	}

	public ArrayList<String> getRegions(){
		return regions;
	}	

	public void setCompanyExclusions(ArrayList<String> companyExclusions){
		this.companyExclusions = companyExclusions;
	}

	public ArrayList<String> getCompanyExclusions(){
		return companyExclusions;
	}	

	public void setPartnerExclusions(ArrayList<String> partnerExclusions){
		this.partnerExclusions = partnerExclusions;
	}

	public ArrayList<String> getPartnerExclusions(){
		return partnerExclusions;
	}	

	public void setReEngageFlag(boolean reEngage){
		this.reEngage = reEngage;
	}

	public boolean getReEngageFlag(){
		return reEngage;
	}	

	public void setNoFilterFlag(boolean noFilter){
		this.noFilter = noFilter;
	}

	public boolean getNoFilterFlag(){
		return noFilter;
	}

	public void setComments(String comments){
		this.comments = comments;
	}

	public String getComments(){
		return comments;
	}
}