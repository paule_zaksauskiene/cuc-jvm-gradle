package com.brighttalk.qa.siteElements;

import java.util.Calendar;

public class Summit {
	
	private String title;
	private String description;
	Calendar targetDate;
	private String featureImageLink;
	private String tags;
	private String urlAlias;
	private String summitID;
	
	
	public String getSummitID() {
		return summitID;
	}
	public void setSummitID(String summitID) {
		this.summitID = summitID;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Calendar getTargetDate() {
		return targetDate;
	}
	public void setTargetDate(Calendar targetDate) {
		this.targetDate = targetDate;
	}
	public String getFeatureImageLink() {
		return featureImageLink;
	}
	public void setFeatureImageLink(String featureImageLink) {
		this.featureImageLink = featureImageLink;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public String getUrlAlias() {
		return urlAlias;
	}
	public void setUrlAlias(String urlAlias) {
		this.urlAlias = urlAlias;
	}
	public Summit(Calendar targetDate) {
		
		this.title = "Test_Summit_"+System.currentTimeMillis();
		this.description = "description";
		this.targetDate = targetDate;
		this.featureImageLink = "";
		this.tags = " test tag";
		this.urlAlias = "testurl"+System.currentTimeMillis();
	}
	
}
