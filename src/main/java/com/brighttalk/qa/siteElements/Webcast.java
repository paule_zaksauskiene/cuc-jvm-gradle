package com.brighttalk.qa.siteElements;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;


public class Webcast {

	private String channelID;
	private String webcastID;
	private String title;
	private String description;
	private String presenter;
	private String duration;
	private Calendar startDate;
	private String timezone;
	private String bookingType;
	private String visibility;
	private String status;
	private String PIN;
	List<Attachment> webcastAttachments;
	private String campaignReference;
	private ArrayList<String> tags;
	private String published;
	private Calendar publishUnpublishDate;
	List<Community> webcastCommunities;
	private String webcastUrl;


	public Webcast(){
		   
		this("");
    }
	   
	public Webcast(String when) {

			title 		= "Test_webcast_"+System.currentTimeMillis();
			description = "Description for Test_webcast";
			presenter 	= "Brighttalk webcast presenter";
			duration  	= "15";
			startDate 	= Calendar.getInstance(TimeZone.getTimeZone("Europe/London"));
			if (when.contains("tomorrow")){
				
				startDate.add(Calendar.DAY_OF_MONTH, 1);
			} else 
			{
				startDate = Calendar.getInstance();
				startDate.add(Calendar.YEAR, 1);
			}
			
			timezone 	= "United Kingdom - London";
			tags = new ArrayList<>();
			tags.add("TestTags");
			bookingType = "within"; //This can "outside" as well
			visibility 	= "public"; //can be private
			status 		= "upcoming";
			campaignReference = "";
			published = "Publish";
		    webcastCommunities = null;
			webcastUrl = "";
	}
	
	public List<Attachment> getWebcastAttachments() {
		return webcastAttachments;
	}
	public void setWebcastAttachments(List<Attachment> webcastAttachments) {
		this.webcastAttachments = webcastAttachments;
	}
	public void setPIN(String pIN) {
		PIN = pIN;
	}
	public String getPIN() {
		return PIN;
	}
	public String getBookingType() {
		return bookingType;
	}
	public void setBookingType(String bookingType) {
		this.bookingType = bookingType;
	}
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}

	public String getChannelID() {
		return channelID;
	}
	public void setChannelID(String channelID) {
		this.channelID = channelID;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPresenter() {
		return presenter;
	}
	public void setPresenter(String presenter) {
		this.presenter = presenter;
	}
	public String getDuration() {
		return duration;
	}
	public void setDuration(String duration) {
		this.duration = duration;
	}
	public Calendar getStartDate() {
		return startDate;
	}
	public void setStartDate(Calendar startDate) {
		this.startDate = startDate;
	}
	public String getTimezone() {
		return timezone;
	}
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
	public String getWebcastID() {
		return webcastID;
	}
	public void setWebcastID(String webcastID) {
		this.webcastID = webcastID;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getCampaignReference() {

		return campaignReference;
	}

	public void setCampaignReference(String campaignReference) {

		this.campaignReference = campaignReference;
	}

	public void setTags(ArrayList<String> tags) {
		this.tags = tags;
	}

	public ArrayList<String> getTags() {
		return tags;
	}

	public String getPublished() {
		return published;
	}

	public void setPublished(String published) {
		this.published = published;
	}

	public List<Community> getWebcastCommunities() {
		return webcastCommunities;
	}

	public void setWebcastCommunities(List<Community> webcastCommunities) {
		this.webcastCommunities = webcastCommunities;
	}

	public String getWebcastUrl() {
		return webcastUrl;
	}

	public void setWebcastUrl(String webcastUrl) {
		this.webcastUrl = webcastUrl;
	}

	public Calendar getPublishUnpublishDate() {
		return publishUnpublishDate;
	}

	public void setPublishUnpublishDate(Calendar publishUnpublishDate) {
		this.publishUnpublishDate = publishUnpublishDate;
	}

}
