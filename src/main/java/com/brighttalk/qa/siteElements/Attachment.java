package com.brighttalk.qa.siteElements;

import com.brighttalk.qa.init.EnvSetup;

public class Attachment {
	
	private String attachmentType; //can be "file", "link to a file", "link to a web page"
	private String fileDocumentType;
	private String attachmentTitle;
	private String attachmentDescription;
	private String attachmentUrl;
	private String fileFormat;

	private String attachmentID;  // this is returned from the system as the attachment id element on DOM useful for new player tests
	
	public String getFileFormat() {
		return fileFormat;
	}
	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}
	public String getAttachmentType() {
		return attachmentType;
	}
	public void setAttachmentType(String attachmentType) {
		this.attachmentType = attachmentType;
	}
	public String getAttachmentTitle() {
		return attachmentTitle;
	}
	public void setAttachmentTitle(String attachmentTitle) {
		this.attachmentTitle = attachmentTitle;
	}
	public String getAttachmentDescription() {
		return attachmentDescription;
	}
	public void setAttachmentDescription(String attachmentDescription) {
		this.attachmentDescription = attachmentDescription;
	}
	public String getAttachmentUrl() {
		return attachmentUrl;
	}
	public void setAttachmentUrl(String attachmentUrl) {
		this.attachmentUrl = attachmentUrl;
	}
	
	public String getFileDocumentType() {
		return fileDocumentType;
	}
	public void setFileDocumentType(String fileDocumentType) {
		this.fileDocumentType = fileDocumentType;
	}

	public String getAttachmentID() {
		return attachmentID;
	}

	public void setAttachmentID(String attachmentID) {
		this.attachmentID = attachmentID;
	}
	
	public Attachment(String type, String location) {
		super();
		
		attachmentTitle = "My Link Attachment title_" + System.currentTimeMillis();
		System.out.println("My Attachment: " + attachmentTitle);
		attachmentDescription = "My Link Attachment description";
		attachmentUrl = "https://www."+ EnvSetup.HOME + ".brighttalk.net";
			
		attachmentType = type;
		fileDocumentType = location;
		fileFormat = "Other / Unknown";
	
	}
	
	public Attachment(String empty) {
		super();
		
		attachmentTitle = "";
		attachmentDescription = "My Link Attachment descripion";
		attachmentUrl = "";
		
	}
}
