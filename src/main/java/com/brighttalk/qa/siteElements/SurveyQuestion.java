package com.brighttalk.qa.siteElements;

import java.util.Map;

public class SurveyQuestion {
	   
	   String questionType;
	   String audiencePrompt;
	   String isMandatory;
	   String reportingId;
       boolean isScrollable;
       boolean readOnly;


	boolean isMultipleSelectionPossible;

	String defaultText;
	   Map<User, String> userAnswers; 
	   

	public String getAudiencePrompt() {
		return audiencePrompt;
	}

	public void setAudiencePrompt(String audiencePrompt) {
		this.audiencePrompt = audiencePrompt;
	}


	public String getIsMandatory() {
		return isMandatory;
	}

	public void setIsMandatory(String isMandatory) {
		this.isMandatory = isMandatory;
	}

	public String getReportingId() {
		return reportingId;
	}

	public void setReportingId(String reportingId) {
		this.reportingId = reportingId;
	}

	public Map<User, String> getUserAnswers() {
		return userAnswers;
	}

	public void setUserAnswers(Map<User, String> userAnswers) {
		this.userAnswers = userAnswers;
	}
	
	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public boolean getIsScrollable() {
		return isScrollable;
	}

	public void setIsScrollable(boolean isScrollable) {

		this.isScrollable = isScrollable;
	}

	public boolean isReadOnly() {
		return readOnly;
	}

	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}

	public String getDefaultText() {
		return defaultText;
	}

	public void setDefaultText(String defaultText) {
		this.defaultText = defaultText;
	}

	public boolean isMultipleSelectionPossible() {
		return isMultipleSelectionPossible;
	}

	public void setMultipleSelectionPossible(boolean multipleSelectionPossible) {
		isMultipleSelectionPossible = multipleSelectionPossible;
	}


	public SurveyQuestion(String questionType, String audiencePrompt, String isMandatory, String reportingId, boolean isScrollable, boolean readOnly, String defaultText, boolean isMultipleSelectionPOssible) {

			super();
			this.questionType = questionType;
			this.audiencePrompt = audiencePrompt;
			this.isMandatory = isMandatory;
			this.reportingId = reportingId;
		    this.isScrollable = isScrollable;
		    this.readOnly = readOnly;
			this.defaultText = defaultText;
			this.isMultipleSelectionPossible = isMultipleSelectionPOssible;
		}
   }
