package com.brighttalk.qa.integration;


import com.brighttalk.qa.conf.TestConfig;
import com.brighttalk.qa.integration.handler.BrightTALKServiceDataHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;

@Repository
public class BrightTALKService {

    @Autowired
    BrightTALKServiceDataHandler handler;

    @PostConstruct
    public void init() {

        //Services.setEnv(TestConfig.getEnv());
        handler.setService(this);
    }

   // public ChannelService getChannelService() {

   //      return Services.getChannel();
    //}


    //public CommunicationService getCommunicationService() {

    //    return Services.getCommunication();
    //}

   // public CommunityService getCommunityService() {
     //   return Services.getCommunity();
   // }

    //public SummitService getSummitService() {
    //    return Services.getSummit();
    //}

   // public UserService getUserService() {

    //    return Services.getUser();
   // }

    public BrightTALKServiceDataHandler getHandler() {

        return handler;
    }
}
