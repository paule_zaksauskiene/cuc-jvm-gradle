package com.brighttalk.qa.integration.handler;


import com.brighttalk.qa.api.ChannelApi;
import com.brighttalk.qa.api.WebcastProApi;
import com.brighttalk.qa.api.CampaignApi;
import com.brighttalk.qa.api.AudienceApi;
import com.brighttalk.qa.common.EntityAttributeResolver;
import com.brighttalk.qa.common.UrlResolver;
import com.brighttalk.qa.common.World;
import com.brighttalk.qa.integration.BrightTALKService;
import com.brighttalk.qa.siteElements.WebcastPro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import com.brighttalk.qa.siteElements.ContactDetails;
import com.brighttalk.qa.siteElements.Address;
import com.brighttalk.qa.siteElements.Campaign;
import com.brighttalk.qa.siteElements.Audience;
import com.brighttalk.qa.conf.TestConfig;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.sql.*;
import java.io.IOException;
import java.util.Calendar;
import java.util.List;

@Component
public class BrightTALKServiceDataHandler {

    private BrightTALKService service;

    @Autowired
    EntityAttributeResolver eaResolver;
    @Autowired
    UrlResolver urlResolver;
    @Autowired
    World world;

    public void setService(BrightTALKService service) {

        this.service = service;
    }

    public com.brighttalk.qa.siteElements.Channel createChannel() {
        // Channel channel = service.getChannelService().create();

        ContactDetails myContactDetails = new ContactDetails(
                world.getLocal_users().get(0),
                new Address("test", "test", "test", "test", "test")
        );

        com.brighttalk.qa.siteElements.Channel myChannel = new com.brighttalk.qa.siteElements.Channel();
        myChannel.setContactDetails(myContactDetails);
        ChannelApi.createChannel(world.getLocal_users().get(0), myChannel);
        ChannelApi.setChannelFeature(world.getLocal_users().get(0), myChannel, "proWebinar", "true", "");

        //Assert.notNull(channel);
        //eaResolver.add("channel", channel);
        //urlResolver.add("{channel_id}", String.valueOf(channel.getId()));
        world.getLocal_channels().add(myChannel);
        urlResolver.add("{channel_id}", world.getLocal_channels().get(0).getChannelID());

        return myChannel;
    }


    public void deleteLocalChannel(com.brighttalk.qa.siteElements.Channel channel) {

        ChannelApi.cancelAllBookingsOnChannel(world.getLocal_users().get(0), channel);

        // Should be disabled untill the problem https://brighttalktech.jira.com/browse/SCREEN-3737 is solved
        ChannelApi.deleteChannel(world.getRegressionTestsManagerUser(), channel);

        urlResolver.getMap().remove("{channel_id}");
        world.getLocal_channels().remove(channel);

        System.out.println("Channel deleted: " + channel.getChannelID());
    }


    public WebcastPro createPro(WebcastPro myWebcastPro) {

        try {
            myWebcastPro.setWebcastID(WebcastProApi.createUpcomingWebcastHd(world, myWebcastPro));
        } catch (IOException e) {
            e.printStackTrace();
        }

        world.getWebcastsPro().add(myWebcastPro);

        return myWebcastPro;
    }

    public WebcastPro createServicedPro() {

        WebcastPro myWebcastPro = new WebcastPro();

        myWebcastPro.setServiced(true);
        myWebcastPro.setDuration("15");
        myWebcastPro.setStatus("upcoming");
        myWebcastPro.setStartDate(Calendar.getInstance());
        myWebcastPro.getStartDate().add(Calendar.YEAR, 1);
        myWebcastPro.getStartDate().set(Calendar.MINUTE, 25);

        myWebcastPro.setWebcastID(WebcastProApi.createWebcastProAsManager(world, myWebcastPro));

        urlResolver.add("{comm_id}", String.valueOf(myWebcastPro.getWebcastID()));
        world.getWebcastsPro().add(myWebcastPro);

        return myWebcastPro;
    }

    public void createCampaign(Campaign myCampaign) {

        try {
            CampaignApi.createCampaign(world, myCampaign);
            CampaignApi.updateDisplayName(world,myCampaign);
        } catch (IOException e) {
            e.printStackTrace();
        }

        world.getCampaigns().add(myCampaign);
        urlResolver.add("{campaign_id}", world.getCampaigns().get(0).getCampaignId());

    }

    public void addAudience(List<Audience> audiences) {

        try {
            AudienceApi.addAudiences(world, audiences);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public WebcastPro createDemandPro(WebcastPro myWebcastPro) {

        try {
            Calendar startDate =  myWebcastPro.getStartDate();
            Calendar futureDate = Calendar.getInstance();
            futureDate.add(Calendar.MONTH, 1);
            myWebcastPro.setStartDate(futureDate);
            myWebcastPro.setWebcastID(WebcastProApi.createUpcomingWebcastHd(world, myWebcastPro));
            myWebcastPro.setStartDate(startDate);

        } catch (IOException e) {
            e.printStackTrace();
        }

        Connection conn = null;
        Statement stmt = null; 
        ResultSet rs = null;

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String scheduled = df.format(myWebcastPro.getStartDate().getTime());

        try {

            if (TestConfig.getEnv().contains("int02")) conn = DriverManager.getConnection("jdbc:mysql://db.core.int02.brighttalk.net/channel?user=root&password=fred");
            if (TestConfig.getEnv().contains("test01")) conn = DriverManager.getConnection("jdbc:mysql://db.test01.brighttalk.net/channel?user=" + TestConfig.DB_TEST01_USER + "&password=" + TestConfig.DB_TEST01_PASSWORD);

            stmt = conn.createStatement();
            String myQuery = "UPDATE communication SET scheduled = '" + scheduled + "', status = '" + myWebcastPro.getStatus() + "', publish_status = '" + myWebcastPro.getPublished()
            + "' WHERE id = '" + myWebcastPro.getWebcastID() + "'";
            stmt.executeUpdate(myQuery);                

            System.out.println(" [+] Demand Pro Webcast " + myWebcastPro.getWebcastID() + " is created.");

        }

        catch (SQLException ex) {

            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());

        }


        world.getWebcastsPro().add(myWebcastPro);

        return myWebcastPro;
    }

   // public User registerUser() {
  //      User user;
    //    user = service.getUserService().register();

    //    urlResolver.add("{user_id}", String.valueOf(user.getId()));
   //     world.getUsers().add(user);

    //    return user;
    //}

   // public void deleteUser(User user) {

   //     service.getUserService().delete(user.getId());
  //      urlResolver.getMap().remove("{user_id}");
  //      world.getUsers().remove(user);
  //  }

    public void deleteLocalUser(com.brighttalk.qa.siteElements.User user) {


        urlResolver.getMap().remove("{user_id}");
        //world.getUsers().remove(user);
    }
}
