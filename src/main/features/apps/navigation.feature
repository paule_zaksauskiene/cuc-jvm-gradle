@app
Feature: App navigation
  As an app dev/product owner
  I want easy to use app navigation
  So that app users can utilize all the app has to offer from any screen

  Scenario Outline: Tab navigation
    Given I am authenticated
    When I visit the "<screen>" screen
    Then I should see the following tabs
      | tabs           |
      | Knowledge Feed |
      | Downloads      |
      | Channels       |
      | Settings       |
    And the "<active tab>" should be selected
    Examples:
      | screen         | active tab         |
      | Knowledge Feed | Knowledge Feed tab |
      | Downloads      | Downloads tab      |
      | Channels       | Channels tab       |
      | Settings       | Settings tab       |
