@app
Feature: Authentication
  As an app user
  I want to be able log in
  So that I can access content from channel I am subscribed to

  Scenario: Join for free
    When I click on the "Join for free button"
    Then I should be on the "BrightTALK registration" page

  Scenario: Basic log in
    Given I have a BrightTALK user account
    When I log in
    Then I should be on the "Knowledge Feed" view

  Scenario: Persisted log in
    Given I have a BrightTALK user account
    And I have logged in
    And terminated the app
    When I start the app again
    Then I should be on the "Knowledge Feed" view

  Scenario: Forgotten Password
    When I click on the "Forgotten password button"
    Then I should be on the "BrightTALK forgotten password" page