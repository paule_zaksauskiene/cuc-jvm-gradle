@app
Feature: Knowledge Feed
  As an app user
  I want to be able see content from all my subscribed channels in a single view
  So that I can stay up-to-date with my subscribed channels


  Scenario Outline: Webcast details from Knowledge Feed view
    Given I am authenticated
    And I am subscribed to a channel
    And that channel has 1 <status> webinar
    When I visit the "Knowledge Feed" screen
    And I click on the 1st "webcast list item"
    Then I should be on the "Webcast Details" screen
    And I should see a "thumbnail"
    And I should see a "title" with text matching "{webcast_title}"
    And I should see a "<label>"
    And I should see a "ratings"
    And I should see a "duration"
    And I should see a "button" with text matching "<button text>"
    And I should see a "description"
    Examples:
      | status   | label                         | button text |
      | live     | live label                    | Watch Live  |
      | recorded | recorded label with datestamp | Play        |

  Scenario Outline: Webcast details from Channels view
    Given I am authenticated
    And I am subscribed to a channel
    And that channel has 1 <status> webinar
    When I visit the "Channels" screen
    And I click on the 1st "channel list iten"
    And I click on the 1st "webcast list item"
    Then I should be on the "Webcast Details" screen
    And I should see a "thumbnail"
    And I should see a "title" with text matching "{webcast_title}"
    And I should see a "<label>"
    And I should see a "ratings"
    And I should see a "duration"
    And I should see a "view button" with text matching "<button text>"
    And I should <download button visibility> a "download button"
    And I should see a "description"
    Examples:
      | status   | label                         | button text | download button visibility |
      | live     | live label                    | Watch Live  | not see                    |
      | recorded | recorded label with datestamp | Play        | see                        |

#    //add upcoming webinar tests, the app currently allows you to pre-register and download the
#    //calendar. This is only accessible from channels > webcast > webcast_details. This is not
#    // accessible from knowledge_feed.