@app
Feature: Downloads
  As an app user
  I want to be able download content from any of my subscribed channels
  So that I can view offline


  Scenario: Basic download
    Given I am authenticated
    When I visit the "Download" screen
    Then I should see a "no downloads message" with text matching "You have no downloads"
    And I should see a "Refresh button" in the "View Header"

#    //more tests to add are::
#    //downloading when internet connection is unavailable mid-download - to should fail providing -
#    // options to retry. This is the same for any failing scenario
#
#    //downloaded content playback tests are written in offline_viewing.feature.

