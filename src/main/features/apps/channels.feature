@app
Feature: Channel
  As an app user
  I want to be able see all my subscribed channels in a single view
  So that I am able to find content from specific channels


  Scenario: No content message is displayed
    Given I am authenticated
    And I am not subscribed to any channels
    When I visit the "Channels" screem
    Then I should see a "knowledge feed no content message" with text matching "You are not subscribed to any channels"

  Scenario: Basic channel list
    Given I am authenticated
    And I am subscribed to a channel
    When I visit the "Channels" screen
    Then I should see 1 "channel"
    And I should see a "title" with text matching "{channel_title}"
    And I should see a "tagline" with text matching "{channel_tagline}"
    And I should see an "arrow icon"

  Scenario Outline: Load more content
    Given I am authenticated
    And I am subscribed to <total number of channels> channel
    When I visit the "Channels" screen
    And I scroll to the end of the list
    Then I expect to see <number of content to be loaded> to be loaded and added to the bottom of the list

    Examples:
      | total number of channels | number of content to be loaded |
      | 19                      | 0                              |
      | 20                      | 0                              |
      | 21                      | 1                              |
      | 40                      | 20                             |
      | 41                      | 20                             |

  Scenario: Channel webcast listings
    Given I am authenticated
    And I am subscribed to a channel
    And that channel has 1 <status> webinar
    When I visit the "Knowledge Feed" screen
    Then I should see 1 "webcast"
    And I should see a "thumbnail"
    And I should see a "title" with text matching "{webcast_title}"
    And I should see a "<label>"

  Scenario Outline: Channel webcast listings
    Given I am authenticated
    And I am subscribed to a channel
    And that channel has 1 <status> webinar
    When I visit the "Channels" screen
    Then I should see 1 "webcast"
    And I should see a "thumbnail"
    And I should see a "title" with text matching "{webcast_title}"
    And I should see a "<label>"
    Examples:
      | status   | label                         |
      | live     | live label                    |
      | recorded | recorded label with datestamp |