@app
Feature: Player
  As an app user
  I want to have the best viewing experience
  So that I can actually enjoy these webcasts


  @internal
  Scenario Outline: players
    Given I am authenticated
    And I find an "<webcast>" to play
    When I play it
    Then I should see a <player>
    Examples:
      | webcast                | player        |
      | audio & slides webinar | custom player |
      | onDemand video         | jw player     |
      | pro webinar            | jw player     |

  Scenario Outline: chromecast
    Given I am authenticated
    And I have a chromecast device set up
    And I find an "<webcast>" to play
    When I play it
    And stream via chromecast
    Then I should be able to view the content without any problems
    Examples:
      | webcast                |
      | audio & slides webinar |
      | onDemand video         |
      | pro webinar            |
