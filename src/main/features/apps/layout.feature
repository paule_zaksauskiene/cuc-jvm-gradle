@app
Feature: App layout
  As an app dev/product owner
  I want a beautifully simple UI
  So app users can have a good user experience

  Scenario Outline: Basic app layout
    Given I am authenticated
    When I visit the "<screen>" screen
    Then I should see a "View Header" with text matching "<screen>"
    Then I should <refresh button visibility> a "Refresh button" in the "View Header"
      | tabs           |
      | Knowledge Feed |
      | Downloads      |
      | Channels       |
      | Settings       |
    Examples:
      | screen         | refresh button visibility |
      | Knowledge Feed | see                       |
      | Downloads      | see                       |
      | Channels       | see                       |
      | Settings       | not see                   |

  Scenario: Webcast details from Knowledge Feed view
    Given I am authenticated
    And I am subscribed to a channel
    And that channel has 1 <status> webinar
    When I visit the "Knowledge Feed" screen
    And I click on the 1st "webcast list item"
    Then I should be on the "Webcast Details" screen
    And I should see a "View Header" with text matching "Webcast Details"
    And I should not see a "Refresh button" in the "View Header"

  Scenario: Webcast details from Channels view
    Given I am authenticated
    And I am subscribed to a channel
    And that channel has 1 <status> webinar
    When I visit the "Channels" screen
    And I click on the 1st "channel list iten"
    And I click on the 1st "webcast list item"
    Then I should be on the "Webcast Details" screen
    And I should see a "View Header" with text matching "{channel_title}"
    And I should not see a "Refresh button" in the "View Header"
