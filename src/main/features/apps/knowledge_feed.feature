@app
Feature: Knowledge Feed
  As an app user
  I want to be able see content from all my subscribed channels in a single view
  So that I can stay up-to-date with my subscribed channels


  Scenario Outline: No content message is displayed
    Given I am authenticated
    And I am <subscription type>
    When I visit the "Knowledge feed" screem
    Then I should see a "knowledge feed no content message" with text matching "You are not subscribed to any channels"
    Examples:
      | subscription type                        |
      | subscribed to a channel with no webcasts |
      | not subscribed to any channel            |

  Scenario Outline: Basic knowledge feed content
    Given I am authenticated
    And I am subscribed to a channel
    And that channel has 1 <status> webinar
    When I visit the "Knowledge Feed" screen
    Then I should see 1 "webcast"
    And I should see a "thumbnail"
    And I should see a "title" with text matching "{webcast_title}"
    And I should see a "<label>"
    Examples:
      | status   | label                         |
      | live     | live label                    |
      | recorded | recorded label with datestamp |

  Scenario: Knowledge Feed content list order
    Given I am authenticated
    And I am subscribed to a channel with the following webcasts
      | Status   | Title     | Scheduled    |
      | recorded | Webcast 1 | 1 hour ago   |
      | recorded | Webcast 2 | 1 day ago    |
      | live     | Webcast 3 | 1 minute ago |
    When I visit the "Knowledge feed" screen
    Then I should see webcasts in the following order
      | Title     |
      | Webcast 3 |
      | Webcast 1 |
      | Webcast 2 |

  Scenario Outline: Load more content
    Given I am authenticated
    And I am subscribed to a channel
    And that channel has <total number of content> webcasts
    When I visit the "Knowledge feed" screen
    And I scroll to the end of the list
    Then I expect to see <number of content to be loaded> to be loaded and added to the bottom of the list

    Examples:
      | total number of content | number of content to be loaded |
      | 19                      | 0                              |
      | 20                      | 0                              |
      | 21                      | 1                              |
      | 40                      | 20                             |
      | 41                      | 20                             |

#//note this view shoulds live and recorded webinars