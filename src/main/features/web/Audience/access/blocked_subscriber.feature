Feature: Blocked subscriber
  As an owner of the BrightTALK channel
  I want to be able to block subscribers
  So that they can not access the channel content

  @nightly
  Scenario: As an channel owner I want block subscriber so the upcoming pro webinar is not accessible
   And I am logged in as new user
   Given the following channel features are set
      | key                           | value    |
      | blocked-users                 | newUser  |
      | registration level            | Minimum  |
    And a pro webinar exists
    When I visit the "Player Test Page" page
    And the webinar embed code is embedded in english language
    And I click on the "player-test-page-attend-button" in the "player-iframe" iframe
    And I switch to "player-iframe" frame
    And I should see the "player-test-page-restricted-access-message" with text matching "This channel owner has restricted the access to its content. We apologize for any inconvenience this has created. This does not affect your BrightTALK registration which will continue to operate with other channels that you have subscribed to"