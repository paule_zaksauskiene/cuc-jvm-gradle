Feature: Anonymous content
  As a channel owner of the BrightTALK channels
  I want to be able schedule anonymous webinars
  So that unregistered audience can access them

  Scenario: As an channel owner I want to set up the anonymous pro webinar for the unregistered viewers to view
    Given the channel is created
    And channel has no Anonymous realm assigned
    When the create pro webinar form  or edit booking form is opened
    Then the viewer access setting is not shown

  Scenario: The unregistered audience can fill the survey and register to pro webinar
    When BrightTALK manager adds the Anonymous realm to the channel
    And pro webinar has "No registration" viewer access set
    And unregistered audience opens the audience page for this webinar
    Then the audience can fill in the survey
    And confirmation message is shown on the pro webinar audience page
