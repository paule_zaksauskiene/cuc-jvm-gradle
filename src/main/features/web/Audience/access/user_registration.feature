Feature: User registration
  As a BrightTALK website visitor
  I want to be able to register to BrightTALK
  So that I can access the site content

  @nightly
  Scenario: user registers on join now page
    Given I visit the "brighttalk-home" page
    And I click on the "join-now-link"
    And the page is loaded
    When I enter new user details
    And I click on the "join-button"
    Then the element "search-page-meta-tag" is present