Feature: Recorded
  As a presenter of a pro webinar
  I want to be able to run webinar
  So that the audience can access the recorded contents

  @nightly
  Scenario: Recorded pro webinar can be accessed by audience
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar duration is 2 mins
    And pro webinar is live
    When I wait for 120 secs
    And I see the 2 minutes overrun pop up with 1 mins left
    And I see the 3 minutes overrun pop up with 0 mins left
    Then I see the Presentation ended pop up
    And pro webinar is recorded
    And I visit the "Audience" page
    And I should see the flash player

  @CON-242
  Scenario: Audience can start viewing the serviced pro webinar
    Given I am logged in as channel owner
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Pro Webinar Slides            | enabled |
    And a serviced webcast exists
    And serviced pro webinar is live
    And I select PPT file "SeleniumTest.ppt" to begin upload
    And temporal step
    And PPT slides are uploaded
    And I click on the "presenter-page-studio-tab"
    And for element "presenter-page-studio-tab-add-url" I enter value "rtmp://cp70386.edgefcs.net/ondemand/mp4:odflash/landingpages/ops/kuwait.mp4"
    And for element "presenter-page-studio-tab-add-url-title" I enter value "test1"
    And I click on the "presenter-page-studio-tab-add-url-button"
    And I visit the "audience" page
    Then I should see the flash player