Feature: Transcoding
  As a presenter of a pro webinar
  I want to be able to add votes
  So that the audience can see them

  @transcoding
  Scenario: Vote details are shown for the audience for on demand webinar 1
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    And pro webinar is live
    And the vote is open
    And audience has voted
    And pro webinar is recorded
    When I visit the "Audience" page
    Then vote is shown on audience page

  @transcoding
  Scenario: Vote details are shown for the audience for on demand webinar 2
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    And pro webinar is live
    And the vote is open
    And audience has voted
    And pro webinar is recorded
    When I visit the "Audience" page
    Then vote is shown on audience page

  @transcoding
  Scenario: Vote details are shown for the audience for on demand webinar 3
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    And pro webinar is live
    And the vote is open
    And audience has voted
    And pro webinar is recorded
    When I visit the "Audience" page
    Then vote is shown on audience page

  @transcoding
  Scenario: Vote details are shown for the audience for on demand webinar 4
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    And pro webinar is live
    And the vote is open
    And audience has voted
    And pro webinar is recorded
    When I visit the "Audience" page
    Then vote is shown on audience page

  @transcoding
  Scenario: Vote details are shown for the audience for on demand webinar 5
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    And pro webinar is live
    And the vote is open
    And audience has voted
    And pro webinar is recorded
    When I visit the "Audience" page
    Then vote is shown on audience page

  @transcoding
  Scenario: Vote details are shown for the audience for on demand webinar 6
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    And pro webinar is live
    And the vote is open
    And audience has voted
    And pro webinar is recorded
    When I visit the "Audience" page
    Then vote is shown on audience page

  @transcoding
  Scenario: Vote details are shown for the audience for on demand webinar 7
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    And pro webinar is live
    And the vote is open
    And audience has voted
    And pro webinar is recorded
    When I visit the "Audience" page
    Then vote is shown on audience page

  @transcoding
  Scenario: Vote details are shown for the audience for on demand webinar 8
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    And pro webinar is live
    And the vote is open
    And audience has voted
    And pro webinar is recorded
    When I visit the "Audience" page
    Then vote is shown on audience page

  @transcoding
  Scenario: Vote details are shown for the audience for on demand webinar 9
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    And pro webinar is live
    And the vote is open
    And audience has voted
    And pro webinar is recorded
    When I visit the "Audience" page
    Then vote is shown on audience page

  @transcoding
  Scenario: Vote details are shown for the audience for on demand webinar 10
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    And pro webinar is live
    And the vote is open
    And audience has voted
    And pro webinar is recorded
    When I visit the "Audience" page
    Then vote is shown on audience page

  @transcoding
  Scenario: Vote details are shown for the audience for on demand webinar 11
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    And pro webinar is live
    And the vote is open
    And audience has voted
    And pro webinar is recorded
    When I visit the "Audience" page
    Then vote is shown on audience page