Feature: Sharing
  As a audience of the pro webinar
  I want to be able see the sharing icons
  So that I can share webinar details with other people

  @nightly
  Scenario: As an audience I want to share webinar details in Linkedin
    Given I am logged in as manager
    And a pro webinar exists
    When I visit the "Audience" page
    And I click on the "audience-page-sharing-linkedin"
    And I wait for 4 secs
    And I switch to the new browser window
    Then I should be on the "https://www.linkedin.com" page

  @nightly
  Scenario: As an audience I want to share webinar details in Twitter
    Given I am logged in as manager
    And a pro webinar exists
    When I visit the "Audience" page
    And I click on the "audience-page-sharing-twitter"
    And I wait for 4 secs
    And I switch to the new browser window
    Then I should be on the "https://twitter.com/intent/tweet" page

  @nightly
  Scenario: As an audience I want to share webinar details in Google plus
    Given I am logged in as manager
    And a pro webinar exists
    When I visit the "Audience" page
    And I click on the "audience-page-sharing-googleplus"
    And I wait for 4 secs
    And I switch to the new browser window
    Then I should be on the "google.com" page

  @nightly
   Scenario: As a channel manage I can disable social links sharing options on the audience page
     Given I am logged in as BrightTALK manager
     And I own a channel
     And social media is disabled on the channel settings page
     And a pro webinar exists
     And I wait for 10 secs
     When I visit the "Audience" page
     And the element "audience-page-sharing-social-links" is present
     And the "audience-page-sharing-linkedin" is not present
     And the "audience-page-sharing-facebook" is not present
     And the "audience-page-sharing-twitter" is not present
     And the "audience-page-sharing-googleplus" is not present

  @nightly
  Scenario: As a channel manage I can disable embed sharing options on the audience page
    Given I am logged in as BrightTALK manager
    And I own a channel
    And embed sharing is disabled on the channel settings page
    And a pro webinar exists
    And I wait for 10 secs
    When I visit the "Audience" page
    And the element "audience-page-sharing-social-links" is present
    And the "audience-page-sharing-embed" is not present

  @nightly
  Scenario: As a channel manage I can disable email sharing options on the audience page
    Given I am logged in as BrightTALK manager
    And I own a channel
    And email sharing is disabled on the channel settings page
    And a pro webinar exists
    And I wait for 10 secs
    When I visit the "Audience" page
    And the element "audience-page-sharing-social-links" is present
    And temporal step
    And the "audience-page-sharing-email" is not present

  Scenario: As an audience I want to share webinar details in Twitter
    Given the upcoming pro webinar page is opened
    When audience clicks the Twitter icon
    Then the Twitter sharing window is opened

  Scenario: As an audience I want to share webinar details in Linkedin
    Given the upcoming pro webinar page is opened
    When audience clicks the LinkedIn icon
    Then the LinkedIn sharing window is opened

  Scenario: As an audience I want to share webinar details in Google+
    Given the upcoming pro webinar page is opened
    When audience clicks the Google+ icon
    Then the Google+ sharing window is opened

  Scenario: As an audience I want to share webinar details by email
    Given the upcoming pro webinar page is opened
    When audience clicks the Email icon
    And viewer enters the recipient email
    And clicks the Send button
    Then the sucessful email sent message is shown

  Scenario: As an audience I want to share webinar with embed
    Given the upcoming pro webinar page is opened
    When audience clicks the Embed icon
    Then the Embed sharing pop up is opened
    And the embed code is displayed on the pop up

  Scenario: As an channel owner I want to disable the sharing options on my channel
    Given the upcoming pro webinar is created
    When channel owner disables the social networks, embed sharing and email sharing on the channel settings
    Then the social network sharing, embed sharing and email sharing icons is not shown on the audience page

