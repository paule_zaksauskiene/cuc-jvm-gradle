Feature: As a audience of the embedded webinar
  I want to be able to see BrightTALK screen share
  So I have access to all embedded pro webinar contents

  Scenario: BrightTALK screen share is shown on the embedded player before live
    Given a pro webinar exists
    And the pro webinar is less than 5 mins before live
    And the BrightTALK screenshare is added
    When I visit the "Player Test Page" page
    Then I should see a BrightTALK screen share

  Scenario: BrightTALK screen share is shown on the embedded player during live
    Given a pro webinar exists
    And the pro webinar is live
    And the BrightTALK screenshare is added
    When I visit the "Player Test Page" page
    Then I should see a BrightTALK screen share