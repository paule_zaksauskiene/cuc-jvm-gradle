
Feature:
  As a user
  I want to be able to access the embedded page
  So that I can gain more access to that piece of content easily

 @indev
  Scenario: Embedded player show the Details tab of the pro webinar
    Given a webcast exists
    And the following channel features are set
    | key                           | value   |
    | registration level            | Minimum |
    And the player test page is open
    And the webinar embed code is embedded in <language> language
    When user opens the embed page for the webinar
    And clicks the Attend button
    And I switch to "player-iframe" frame
    And I click on the "player-test-page-details-tab"
