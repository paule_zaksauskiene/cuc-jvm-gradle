Feature:
  As a user
  I want to be able to log in through the embedded page
  So that I can see the channel listing

 @nightly
  Scenario: Channel listing shows featured and upcoming webinar when language is "none"
    Given a webcast exists
    And the following channel features are set
     | key                           | value   |
     | registration level            | Minimum |
    And "1" more upcoming webinars are added to a channel
    And the player test page is open
    And the channel listing page is generated in none language
    When I switch to "player-iframe" frame
    Then the date and time is shown for the featured webinar 1
    And the date and time is shown for the webinar 2 in channel listing