
Feature:
  As a user
  I want to be able to log in through the embedded page
  So that I can gain more access to that piece of content easily

 @nightly
  Scenario Outline: Embedded player can be accessed for different languages
    Given a webcast exists
    And the following channel features are set
    | key                           | value   |
    | registration level            | Minimum |
    And the player test page is open
    And the webinar embed code is embedded in <language> language
    When user opens the embed page for the webinar
    And clicks the Attend button
    And temporal step
    And enters login details
    And temporal step
    Then the user is confirmed to attend webinar
     Examples:
      | language           |
      | en-us-elsevier     |
      | en-us-generic      |
      | en-us-hsbc         |
      | en-us-salesforce   |

  @nightly
  Scenario Outline: Embedded player can be accessed for german language
    Given a webcast exists
    And the following channel features are set
      | key                           | value   |
      | registration level            | Minimum |
    And the player test page is open
    And the webinar embed code is embedded in <language> language
    When user opens the embed page for the webinar
    And clicks the Attend button
    And enters login details
    Then the user is confirmed to attend webinar in german language
    Examples:
    | language          |
    | german            |
