Feature: Categorization Tags

  @nightly
  Scenario: categorization tags configured for channel without any webcasts
    Given a channel with the following categorization tag exists
      | cat/tag |
      | cat1/tag1 |
    When I visit the "Player Test Page" page
    And I generate a channel embed that uses the following categories
      | cat        |
      | cat1       |
    Then I should not see the Categorization Tag Dropdown

  @nightly
  Scenario: categorization tag configured for channel with 1 webcast displayed in channel embed
    Given a channel with the following categorization tag exists
      | cat/tag     |
      | cat1/tag1   |
    And a webcast with categorization tags exists
      | cat/tag     |
      | cat1/tag1   |
    When I visit the "Player Test Page" page
    And I generate a channel embed that uses the following categories
      | cat        |
      | cat1       |
    Then I should not see the Categorization Tag Dropdown
    And upcoming webinar details is shown on the player test page

  @nightly
  Scenario: categorization tag configured for channel with 2 webcast displayed in channel embed
    Given a channel with the following categorization tag exists
      | cat/tag |
    And the following webcasts exist
      | 1hr from now   |
      | 3hrs from now  |
      | 5hrs from now  |
    And categorization tag is set for the first two webcasts
      | cat/tag     |
    When I visit the "Player Test Page" page
    And I generate a channel embed that uses the following categories
      | cat       |
    And select the category "cat" value "tag" from the Categorization Tag Dropdown
    Then the following webinars appear on the channel listing
      | 1hr from now   |
      | 3hrs from now  |
    And temporal step

  Scenario: categorization tag configured for channel with 1 webcast displayed in player embed
    Given a channel with the following categorization tag exists
      | tags    |
      | cat/tag |
    And a webcast with categorization tags exists
    When I visit the "Embed Tool" page
    And I generate a player embed that uses the following field values
      | categories |
      | cat        |
    Then I should not see the "Categorization Tag Dropdown"

  Scenario Outline: categorization tag not configured for channel with 2 webcast displayed in channel embed
    Given a channel with the following categorization tag exists
      | tags    |
      | cat/tag |
    And it has the following webcasts
      | schedule date | categorization tags     |
      | 1hr from now  | <set_tags_for_webcast1> |
      | 2hrs from now | <set_tags_for_webcast2> |
    When I visit the "Embed Tool" page
    And I generate a channel embed that uses the following field values
      | categories      |
      | nonexistent_cat |
    Then I <visibility> see the "Categorization Tag Dropdown"
    Examples:
      | set_tags_for_webcast1 | set_tags_for_webcast2 | visibility |
      | yes                   | yes                   | should not |
      | yes                   | no                    | should not |
      | no                    | yes                   | should not |
      | no                    | no                    | should not |

  Scenario Outline: categorization tag not configured for channel with 2 webcast displayed in player embed
    Given a channel with the following categorization tag exists
      | tags    |
      | cat/tag |
    And it has the following webcasts
      | schedule date | categorization tags     |
      | 1hr from now  | <set_tags_for_webcast1> |
      | 2hrs from now | <set_tags_for_webcast2> |
    When I visit the "Embed Tool" page
    And I generate a player embed that uses the following field values
      | communication  | categories      |
      | closest to now | nonexistent_cat |
    Then I <visibility> see the "Categorization Tag Dropdown"
    Examples:
      | set_tags_for_webcast1 | set_tags_for_webcast2 | visibility |
      | yes                   | yes                   | should not |
      | yes                   | no                    | should not |
      | no                    | yes                   | should not |
      | no                    | no                    | should not |

  Scenario Outline: categorization tag configured for channel with 2 webcast displayed in player embed
    Given a channel with the following categorization tag exists
      | tags    |
      | cat/tag |
    And the following webcasts exist
      | schedule date | categorization tags      |
      | 1hr from now  | <set tags for webcast1?> |
      | 2hrs from now | <set tags for webcast2?> |
    When I visit the "Embed Tool" page
    And I generate a player embed that uses the following field values
      | communication  | categories |
      | closest to now | cat        |
    Then I <visibility> see the "Categorization Tag Dropdown"
    Examples:
      | set tags for webcast1? | set tags for webcast2? | visibility |
      | yes                    | no                     | should     |
      | no                     | yes                    | should not |

  Scenario Outline: max limit of multiple categorization tag dropdown(s) on channel embed
    Given a channel with the following categorization tags exists
      | tags    |
      | <tag_1> |
      | <tag_2> |
      | <tag_3> |
      | <tag_4> |
    And it has the following webcasts
      | schedule date | categorization tags     |
      | 1hr from now  | <set_tags_for_webcast1> |
      | 2hrs from now | <set_tags_for_webcast2> |
    When I visit the "Embed Tool" page
    And I generate a channel embed that uses the following field values
      | categories              |
      | <embed_tag_field_value> |
    Then I should see <quantity> "Categorization Tag Dropdown"

    Examples:
      | tag_1       | tag_2       | tag_3       | tag_4       | embed_tag_field_value   | quantity |
      | cat_1/tag_1 | cat_1/tag_2 | cat_1/tag_3 | cat_1/tag_4 | cat_1                   | 1        |
      | cat_1/tag_1 | cat_2/tag_1 | cat_1/tag_2 | cat_2/tag_2 | cat_1,cat_2             | 2        |
      | cat_1/tag_1 | cat_2/tag_1 | cat_3/tag_1 | cat_3/tag_2 | cat_1,cat_2,cat_3       | 3        |
      | cat_1/tag_1 | cat_2/tag_1 | cat_3/tag_1 | cat_4/tag_1 | cat_1,cat_2,cat_3,cat_4 | 3        |

#    Add more examples i.e. special chars, long text, international chars

  Scenario Outline: max limit of multiple categorization tag dropdown(s) on player embed
    Given a channel with the following categorization tags exists
      | tags    |
      | <tag_1> |
      | <tag_2> |
      | <tag_3> |
      | <tag_4> |
    And it has the following webcasts
      | schedule date | categorization tags     |
      | 1hr from now  | <set_tags_for_webcast1> |
      | 2hrs from now | <set_tags_for_webcast2> |
    When I visit the "Embed Tool" page
    And I generate a player embed that uses the following field values
      | communication  | categories              |
      | closest to now | <embed_tag_field_value> |
    Then I should see <quantity> "Categorization Tag Dropdown"

    Examples:
      | tag_1       | tag_2       | tag_3       | tag_4       | embed_tag_field_value   | quantity |
      | cat_1/tag_1 | cat_1/tag_2 | cat_1/tag_3 | cat_1/tag_4 | cat_1                   | 1        |
      | cat_1/tag_1 | cat_2/tag_1 | cat_1/tag_2 | cat_2/tag_2 | cat_1,cat_2             | 2        |
      | cat_1/tag_1 | cat_2/tag_1 | cat_3/tag_1 | cat_3/tag_2 | cat_1,cat_2,cat_3       | 3        |
      | cat_1/tag_1 | cat_2/tag_1 | cat_3/tag_1 | cat_4/tag_1 | cat_1,cat_2,cat_3,cat_4 | 3        |

#    Add more examples i.e. special chars, long text, international chars

  Scenario: categorization tag dropdown(s) visibility after changing webcast

  Scenario: categorization tag dropdown(s) visibility after changing audience tab

  Scenario: categorization tag dropdown(s) visibility after answering a survey

  Scenario: categorization tag dropdown(s) visibility after logging in

  Scenario: categorization tag dropdown(s) visibility after registering a new user

  Scenario: categorization tag dropdown(s) visibility after logging in and answering a survey

  Scenario: categorization tag dropdown(s) visibility after registering a new user and answering a survey

  Scenario: categorization tag filtering

  Scenario: categorization tag filtering with multiple tags
