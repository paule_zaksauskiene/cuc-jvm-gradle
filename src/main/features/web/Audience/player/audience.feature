Feature: Audience
  As a audience of the pro webinar
  I want to be able to attend webinar
  So that I can access the contents of it

  @nightly
  Scenario: cancel pro webinar less than 5 mins before live
    Given I am logged in as manager
    And a pro webinar exists
    And a pro webinar starts in 5 minutes
    And no resource is needed
    When I visit the "player" page
    And I open new browser window 2
    And I visit the "view-booking-page"
    And I cancel booking
    And I wait for 60 secs
    And I activate browser window 1
    And I refresh the page
    Then I should see the "audience-page-unavailable-for-viewing"

  @nightly
  Scenario: Audience received presenter announcement
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
      | registration level            | Minimum |
    And a pro webinar exists
    And a pro webinar starts in 4 minutes
    And I visit the "presenter" page
    And I close presenter screen overlay
    And I click on the "presenter-page-announcement-section"
    And for element "presenter-page-announcement-textarea" I enter value "Announcement test"
    And I wait for 20 secs
    And I click on the "presenter-page-display-announcement-button"
    And I click on the "presenter-page-display-announcement-confirmation-button"
    And I visit the "audience" page
    And I switch to audience page tab frame
    Then I should see the "audience-page-announcement-text" with text matching "Announcement test"
    And I should see the "audience-page-annoucement-close-button"

  @nightly
  Scenario: Audience closes presenter announcement
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
      | registration level            | Minimum |
    And a pro webinar exists
    And a pro webinar starts in 4 minutes
    And I visit the "presenter" page
    And I close presenter screen overlay
    And I click on the "presenter-page-announcement-section"
    And for element "presenter-page-announcement-textarea" I enter value "Announcement test"
    And I click on the "presenter-page-display-announcement-button"
    And I click on the "presenter-page-display-announcement-confirmation-button"
    When I visit the "audience" page
    And I switch to audience page tab frame
    And I click on the "audience-page-annoucement-close-button"
    Then the "audience-page-annoucement-close-button" is not present
    And the "audience-page-announcement-text" is not present

  @nightly
  Scenario: Presenter stops presenting audience announcement
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
      | registration level            | Minimum |
    And a pro webinar exists
    And a pro webinar starts in 4 minutes
    And I visit the "presenter" page
    And I close presenter screen overlay
    And I click on the "presenter-page-announcement-section"
    And for element "presenter-page-announcement-textarea" I enter value "Announcement test"
    And I click on the "presenter-page-display-announcement-button"
    And I click on the "presenter-page-display-announcement-confirmation-button"
    And the element "presenter-page-post-announcement-message" is present
    And I click the stop presenter announcement displaying button
    When I visit the "audience" page
    And I switch to audience page tab frame
    Then the "audience-page-annoucement-close-button" is not present
    And the "audience-page-announcement-text" is not present

  @nightly
  Scenario: Presenter cancels presenter announcement before display
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
      | registration level            | Minimum |
    And a pro webinar exists
    And a pro webinar starts in 4 minutes
    And I visit the "presenter" page
    And I close presenter screen overlay
    And I click on the "presenter-page-announcement-section"
    And for element "presenter-page-announcement-textarea" I enter value "Announcement test"
    And I click on the "presenter-page-display-announcement-button"
    And I click on the "presenter-page-cancel-display-message-button"
    And the "presenter-page-post-announcement-message" is not present
    And I visit the "audience" page
    And I switch to audience page tab frame
    Then the "audience-page-annoucement-close-button" is not present
    And the "audience-page-announcement-text" is not present

  @CON-242
  Scenario: Serviced pro webinar is recorded
    Given I am logged in as channel owner
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a serviced webcast exists
    And serviced pro webinar is live
    When I select PPT file "SeleniumTest.ppt" to begin upload
    And PPT slides are uploaded
    And I click on the "presenter-page-studio-tab"
    And for element "presenter-page-studio-tab-add-url" I enter value "rtmp://cp70386.edgefcs.net/ondemand/mp4:odflash/landingpages/ops/kuwait.mp4"
    And for element "presenter-page-studio-tab-add-url-title" I enter value "test1"
    And I click on the "presenter-page-studio-tab-add-url-button"
    And pro webinar is recorded
    And I visit the "Audience" page
    Then I should see the flash player

  @nightly
  Scenario: The new jpeg feature image appears on the audience page
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    And a pro webinar exists
    When I visit the "edit-booking-page" page
    And for element "audio-and-slides-webinar-booking-form-timeminutes" I select value "15"
    And I select jpeg feature image
    And I click Proceed button on the Edit booking form
    And I delete all cookies
    And I visit the "player" page
    Then the feature image appears on the audience page