Feature: Votes
  As a presenter of a pro webinar
  I want to be able to add votes
  So that the audience can see them

  @nightly
  Scenario: French vote appears on the audience page
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote with the following details
      |èàùéâêîôûëïç|èàùéâêîôûëïç|èàùéâêîôûëïç_|èàùéâêîôûëïç__|èàùéâêîôûëïç___|èàùéâêîôûëïç____|
    And pro webinar is live
    And the vote is open
    When I visit the "Audience" page
    Then vote is shown on audience page

  @nightly
  Scenario: German vote appears on the audience page
    Given I am logged in as manager
    And temporal step
    And a pro webinar exists
    And temporal step
    And pro webinar contains vote with the following details
      |äöüß€|äöüß€|äöüß€_|äöüß€__|äöüß€___|äöüß€____|
    And temporal step
    And pro webinar is live
    And the vote is open
    When I visit the "Audience" page
    Then vote is shown on audience page

  @nightly
  Scenario: Spanish vote appears on the audience page
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote with the following details
      |áéíñóúü¿¡|áéíñóúü¿¡_|áéíñóúü¿¡__|áéíñóúü¿¡___|áéíñóúü¿¡____|áéíñóúü¿¡_____|
    And pro webinar is live
    And the vote is open
    When I visit the "Audience" page
    Then vote is shown on audience page

  @nightly
  Scenario: Japanese vote appears on the audience page
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote with the following details
      |思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています|思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています_|思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています__|思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています___|思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています____|思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています_____|
    And pro webinar is live
    And the vote is open
    When I visit the "Audience" page
    Then vote is shown on audience page

  @nightly
  Scenario: Korean vote appears on the audience page
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote with the following details
      |생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는|생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는_|생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는__|생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는___|생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는____|생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는_____|
    And pro webinar is live
    And the vote is open
    When I visit the "Audience" page
    Then vote is shown on audience page

  @nightly
  Scenario: Russian vote appears on the audience page
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote with the following details
      |ёъяшертыуиопющэасдфгчйкльжзхцвбнм|ёъяшертыуиопющэасдфгчйкльжзхцвбнм_|ёъяшертыуиопющэасдфгчйкльжзхцвбнм__|ёъяшертыуиопющэасдфгчйкльжзхцвбнм___|ёъяшертыуиопющэасдфгчйкльжзхцвбнм____|ёъяшертыуиопющэасдфгчйкльжзхцвбнм_____|
    And pro webinar is live
    And the vote is open
    When I visit the "Audience" page
    Then vote is shown on audience page

  @nightly
  Scenario: Chinese vote appears on the audience page
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote with the following details
      |每天都有思想領袖正在積極分享他們的見解|每天都有思想領袖正在積極分享他們的見解|每天都有思想領袖正在積極分享他們的見解_|每天都有思想領袖正在積極分享他們的見解__|每天都有思想領袖正在積極分享他們的見解___|每天都有思想領袖正在積極分享他們的見解____|
    And pro webinar is live
    And the vote is open
    When I visit the "Audience" page
    Then vote is shown on audience page

  @nightly
  Scenario: Vote, containing special characters appears on the audience page
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote with the following details
      |!@£$%^&*()_+±{}:?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆|!@£$%^&*()_+±{}:?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆_|!@£$%^&*()_+±{}:?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆__|!@£$%^&*()_+±{}:?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆___|!@£$%^&*()_+±{}:?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆____|!@£$%^&*()_+±{}:?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆_____|
    And pro webinar is live
    And the vote is open
    When I visit the "Audience" page
    Then vote is shown on audience page

  @nightly
  Scenario: Closed vote details are shown for the audience
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    And pro webinar is live
    And vote has been run
    When I visit the "Audience" page
    Then vote question is shown on audience page

  @nightly
  Scenario: Vote details are shown for the audience for on demand webinar
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    And pro webinar is live
    And the vote is open
    And audience has voted
    And pro webinar is recorded
    When I visit the "Audience" page
    Then vote is shown on audience page

  @nightly
  Scenario: Run votes are shown and unrun votes are not shown for on demand pro webinar
    Given I am logged in as manager
    When I visit the "Audience" page for webinar 1510155665
    Then vote with question VoteQuestion_1478860500082 is shown on audience page
    And vote with question VoteQuestion_1478860500636 is shown on audience page
    And vote with question VoteQuestion_1478860501196 is not shown on audience page

  Scenario: The active vote details is displayed on the player page
    When I am viewing a Live webcast
    And the vote is active
    Then I see the Vote tab displayed on the player
    And vote details are shown

  Scenario: Viewing the vote panel when no active votes are displayed
    When I am viewing a Live webcast that hasn't run votes yet
    And the Presenter has added at least one Poll on their Presenter screen
    And I click the Vote tab.
    Then in the Vote panel a message is displayed - "There are currently no votes available"

  Scenario: the button to see other votes is shown on the audience page
    Given I am an audience member
    When I am viewing a Live webcast
    And where 1 or more votes have already been run
    Then display a "Back to vote list" <Localized> button is shown to see other votes

  Scenario: votes details are shown when audience page is opened for active votes
    Given the pro webinar is live
    When I arrive on a Live webcast player with a vote in progress
    And some viewers has already voted
    Then I see the votes panel display automatically showing.
    And the vote result information is shown for the audience

  Scenario: the past votes are shown for the audience
    Given the pro webinar is live
    When audience opens the page when vote is finished
    Then the list of those votes that have run is shown

  Scenario: the votes results are shown for the on demand webinar
    Given I am an audience member
    When I am viewing an on demand webcast, and I click on the vote tab for a webinar that ran only 1 vote
    Then I see the votes panel display the vote results panel displaying.

  Scenario: only run votes are shown for audiences of the on-demand webinar
    Given The pro webinar with some votes has been run
    When I am viewing an on demand webcast
    And I click on the vote tab for a webinar that ran more than 1 vote.
    Then only those votes that have run are displayed.
    When I click on a vote
    Then I see votes panel display the vote results panel.

  Scenario Outline: Opening a vote with special characters
    When The pro webinar has votes added in different languages
    And presentation is started
    And presenter open vote following values
      | field              | value      |
      | Vote Form question | <question> |
      | Vote Form answerA  | <answer_a> |
      | Vote Form answerB  | <answer_b> |
      | Vote Form answerB  | <answer_c> |
      | Vote Form answerB  | <answer_d> |
      | Vote Form answerB  | <answer_e> |
    Then audience should see the "Vote question title" with text matching <question> for that "Vote"
    And audience should see the "Vote question answers" with text matching <answer_a>, <answer_b>, <answer_c>, <answer_d>, <answer_e> for that "Vote"
    Examples:
      |question|answer_a|answer_b|answer_c|answer_d|answer_e|
      |èàùéâêîôûëïç|èàùéâêîôûëïç|èàùéâêîôûëïç|èàùéâêîôûëïç|èàùéâêîôûëïç|èàùéâêîôûëïç|
      |äöüß€|äöüß€|äöüß€|äöüß€|äöüß€|äöüß€|
      |áéíñóúü|áéíñóúü|áéíñóúü|áéíñóúü|áéíñóúü|áéíñóúü|
      |思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています|思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています|思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています|思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています|思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています|思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています|
      |생각지도자의매일수천적극적으로통찰력을공유하는|생각지도자의매일수천적극적으로통찰력을공유하는|생각지도자의매일수천적극적으로통찰력을공유하는생각지도자의매일수천적극적으로통찰력을공유하는|생각지도자의매일수천적극적으로통찰력을공유하는|생각지도자의매일수천적극적으로통찰력을공유하는|생각지도자의매일수천적극적으로통찰력을공유하는                   |
      |ёъяшертыуиопющэасдфгчйкльжзхцвбнм|ёъяшертыуиопющэасдфгчйкльжзхцвбнм|ёъяшертыуиопющэасдфгчйкльжзхцвбнм|ёъяшертыуиопющэасдфгчйкльжзхцвбнм|ёъяшертыуиопющэасдфгчйкльжзхцвбнм|ёъяшертыуиопющэасдфгчйкльжзхцвбнм|
      |每天都有思想領袖正在積極分享他們的見解|每天都有思想領袖正在積極分享他們的見解|每天都有思想領袖正在積極分享他們的見解|每天都有思想領袖正在積極分享他們的見解|每天都有思想領袖正在積極分享他們的見解|每天都有思想領袖正在積極分享他們的見解|
      |!@£$%^&*()_+±{}:?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆|!@£$%^&*()_+±{}:?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆|!@£$%^&*()_+±{}:?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆|!@£$%^&*()_+±{}:?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆|!@£$%^&*()_+±{}:?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆|!@£$%^&*()_+±{}:?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆|

  Scenario: the audience can see the updated vote result
    Given the pro webinar has vote
    And the pro webinar is live
    When the viewer submits the vote
    And another viewer submits the vote
    Then the total vote result is seen for both viewers
