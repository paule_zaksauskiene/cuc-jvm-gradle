Feature: Ratings
  As a audience of the pro webinar
  I want to be able send ratings
  So that presenters can see them

  Scenario: As an audience I can not send ratings if stars are not selected
    Given the channel is created
    When the pro webinar is scheduled to start in less than 5 minutes
    And audience has joined the webinar on the player test page
    And audience enters only rating text
    Then the Send Rating button remains disabled

  Scenario: the rating text allows only 500 of characters to send
    When the audience enters more than 500 characters to the rating text
    Then only 500 characters are shown on the text area
    And Send Rating button is enabled
