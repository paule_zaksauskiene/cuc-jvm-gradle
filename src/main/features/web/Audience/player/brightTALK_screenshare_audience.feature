Feature: As a audience of the webinar
  I want to be able to see BrightTALK screen share
  So I have access to all pro webinar contents

  Scenario: BrightTALK screen share is shown for the audience before live
    Given a pro webinar exists
    And the pro webinar is less than 5 mins before live
    And the BrightTALK screenshare is added
    When I visit the "audience" page
    Then I should see a BrightTALK screen share

  Scenario: BrightTALK screen share is shown for the audience during live
    Given a pro webinar exists
    And the pro webinar is live
    And the BrightTALK screenshare is added
    When I visit the "audience" page
    Then I should see a BrightTALK screen share

  Scenario: BrightTALK screen share is shown before and after powerpoint slides
    Given a pro webinar exists
    And the pro webinar is live
    And the BrightTALK screenshare is added
    And the Powerpoint slides are updated
    And the BrightTALK screenshare is active
    When I visit the "audience" page
    Then I should see a BrightTALK screen share

  Scenario: BrightTALK screen share is shown over powerpoint slides
    Given a pro webinar exists
    And the pro webinar is live
    And the BrightTALK screenshare is added
    And the Powerpoint slides are updated
    And the BrightTALK screenshare is active
    When I visit the "audience" page
    Then I should see a BrightTALK screen share

  Scenario: Powerpoint slides is shown over BrightTALK screen share
    Given a pro webinar exists
    And the pro webinar is live
    And the BrightTALK screenshare is added
    And the Powerpoint slides are updated
    And the Powerpoint slides is active
    When I visit the "audience" page
    Then I should see a Powerpoint slides

   Scenario: The join.me link is shown for the audience after BrightTALK screenshare is removed
     Given a pro webinar exists
     And the pro webinar is live
     And the BrightTALK screenshare is added
     When remove BrightTALK screen share
     And add the join.me link
     And I visit the "audience" page
     Then the join.me screenshare is shown

  Scenario: BrightTALK screen share is added when vote is active
    Given a pro webinar exists
    And the pro webinar is live
    And I visit the "presenter" page
    And the BrightTALK screenshare is added
    And vote is active
    When I visit the "audience" page
    Then the BrightTALK screenshare is shown
    And the vote is open

  Scenario: BrightTALK screen share is updated for the audience
    Given a pro webinar exists
    And the pro webinar is live
    And I visit the "presenter" page
    And the BrightTALK screenshare is added
    And the BrightTALK screenshare is updated
    When I visit the "audience" page
    Then the BrightTALK screenshare is shown

  Scenario: The BrightTALK screen share resumes on audience page after internet connection is lost
    And a pro webinar exists
    And the pro webinar is less than 30 mins before live
    And the BrightTALK screenshare is added
    When I visit the "audience" page
    And internet connection is lost
    And the blank screen is shown on the audience page
    And internet connection comes back up
    Then the BrightTALK screen share is show



