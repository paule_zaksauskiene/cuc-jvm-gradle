Feature: Attachments on the audience page
  As a channel owner of the BrightTALK channels
  I want to be able add attachments for the pro webinars
  So that audience can download them

  @nightly
  Scenario: As an audience I want to download attachment for the recorded webinar
    Given I am logged in as manager
    And a channel with id 2000128227 exists
    And a webcast with id 1510217417 exists
    And webcast already contains "link to a file" attachment with url "http://www.telegraph.co.uk"
    When I visit the "audience" page
    And I switch to audience page tab frame
    And I click on the "audience-page-attachments-tab"
    And I switch to audience page tab frame
    And I click on attachment "link to a file"
    And I switch to a new tab
    Then the "telegraph-page" opens

  @nightly
  Scenario: As an audience I want to download attachment for the recorded webinar
    Given I am logged in as manager
    And a channel with id 2000128227 exists
    And a webcast with id 1510217417 exists
    And webcast already contains "link to a web page" attachment with url "http://www.telegraph.co.uk"
    When I visit the "audience" page
    And I switch to audience page tab frame
    And I click on the "audience-page-attachments-tab"
    And I switch to audience page tab frame
    And I click on attachment "link to a web page"
    And I switch to a new tab
    Then the "telegraph-page" opens

  Scenario: Attachments access on live webinar
    Given I own a channel
    And a webcast exists
    And the following attachments for that webcast exists
      | title        | description    | link              |
      | Attachment 1 | Blahblahblah 1 | http://link.com/1 |
    And the webcast goes live
    When I visit the "Player page"
    Then I should see the "Attachments Tab" in the "Audience Tabs"

  Scenario: Attachments access on live webinar
    Given I own a channel
    And a webcast exists
    And the following attachments for that webcast exists
      | title        | description    | link              |
      | Attachment 1 | Blahblahblah 1 | http://link.com/1 |
    And the webcast goes live
    When I visit the "Player page"
    And I click on the "Attachments Tab" in the "Audience Tabs"
    Then I should see an "Attachment" with the following details
      | field                        | value             |
      | Attachment title field       | Attachment 1      |
      | Attachment description field | Blahblahblah 1    |
      | Attachment link field        | http://link.com/1 |