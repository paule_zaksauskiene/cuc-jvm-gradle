Feature:
  As a user
  I want to be able to register on the Minimum registration form
  So that I can gain more access to that piece of content easily

  @nightly
  Scenario: User is registered and logged in after submitting the Minimum level reg-form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Minimum  |
    When I visit webinar audience play page
    And I fill in the following form details
      | field                           | value                            |
      | Reg-form first name field       | seleniumFName                    |
      | Reg-form last name field        | seleniumLName                    |
      | Reg-form email field            | random                           |
      | Reg-form password field         | password                         |
    When I click on the "Reg-form proceed button"
    And I should see the "audience-page-your-place-confirmed" with text matching "Your place is confirmed"
    And I visit the "profile-page" page
    Then I should see the profile data populated by the Minimum form

  @nightly
  Scenario: Existing user is trying to register and log in after submitting the Minimum level reg-form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Minimum  |
    When I visit webinar audience play page
    And I fill in the following form details
      | field                           | value                                       |
      | Reg-form first name field       | seleniumFName                               |
      | Reg-form last name field        | seleniumLName                               |
      | Reg-form email field            | regressionTestsManagerUser@brighttalk.com   |
      | Reg-form password field         | password                                    |
    When I click on the "Reg-form proceed button"
    Then I should see the "registration-form-minimum-email-not-valid" with text matching " Email already in use"

  @nightly
  Scenario: User is required to enter all mandatory fields to the Minimum level reg-form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Minimum |
    When I visit webinar audience play page
    And I wait for 5 secs
    And I click on the "Reg-form proceed button"
    Then I should see the "registration-form-minimum-email-not-valid" with text matching "Email is not valid."
    And I should see the "registration-form-minimum-firstname-please-complete" with text matching "Please complete."
    And I should see the "registration-form-minimum-lastname-please-complete" with text matching "Please complete."
    And I should see the "registration-form-minimum-password-please-complete" with text matching "Must have at least 6 characters"

  @nightly
  Scenario: The user leaves the email field blank on the Minimum form and is prompted to enter a valid email when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Minimum  |
    When I visit webinar audience play page
    And for element "reg-form-email-field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-minimum-email-not-valid" with text matching "Email is not valid."

  @nightly
  Scenario: The user leaves the password field blank on the Minimum form and is prompted to enter a valid password when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Minimum  |
    When I visit webinar audience play page
    And for element "Reg-form password field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    And I should see the "registration-form-minimum-password-please-complete" with text matching "Must have at least 6 characters"

  @nightly
  Scenario: The user inputs a free email on the Minimum form and the Company field should not appear
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Minimum  |
    When I visit webinar audience play page
    And I wait for 5 secs
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | notexistinguser@gmail.com         |
    And for element "reg-form-first-name-field" I enter value ""
    And I wait for 3 secs
    Then the "reg-form company field" is not present

  @nightly
  Scenario: The user enters invalid email on the Minimum form and is prompted to enter a valid email when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Minimum |
    When I visit webinar audience play page
    And I wait for 5 secs
    And for element "reg-form-email-field" I enter value "THISISINVALIDEMAIL"
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-email-not-valid" with text matching "Email is not valid."

  @nightly
  Scenario: The user inputs a company email address that is present in the BrightTALKs report database on the Minimum form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Minimum |
    When I visit webinar audience play page
    And I wait for 5 secs
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | newusertest@brighttalk.com        |
    And I wait for 3 secs
    And the "reg-form company field" is not present

  @nightly
  Scenario: The user leaves last name field empty on the Minimum form and is prompted to enter a last name field when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Minimum |
    When I visit webinar audience play page
    And I wait for 5 secs
    And for element "Reg-form last name field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-minimum-lastname-please-complete" with text matching "Please complete."

  @nightly
  Scenario: The user enters 5 characters to the password field on the Minimum form and is prompted to enter a valid password when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Minimum |
    When I visit webinar audience play page
    And I wait for 5 secs
    And for element "Reg-form password field" I enter value "iustr"
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-minimum-password-please-complete" with text matching "Must have at least 6 characters"

  @nightly
  Scenario: The user inputs a company email address that is not present in the BrightTALKs report database on the Minimum form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Minimum |
    When I visit webinar audience play page
    And I wait for 5 secs
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | notexisting@notexistingcompany.com|
    And for element "reg-form-first-name-field" I enter value ""
    And I wait for 3 secs
    And temporal step
    Then the "reg-form company field" is not present
