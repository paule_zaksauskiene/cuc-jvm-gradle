Feature:
  As a user
  I want to be able to register on the White Label registration form
  So that I can gain more access to that piece of content easily

  @obsolete
  Scenario: User is registered and logged in after submitting the White label level reg-form
    Given a webinar exists
    And the following channel features are set
      | key                | value       |
      | registration level | white-label |
    When I visit webinar audience page
    And temporal step
    And I fill in the following form details in the "Registration form" iframe
      | field                           | value                            |
      | Reg-form first name field       | seleniumFName                    |
      | Reg-form last name field        | seleniumLName                    |
      | Reg-form email field            | random                           |
      | Reg-form job title field        | seleniumJobTitle                 |
      | Reg-form company field          | seleniumCompany                  |
      | Reg-form timezone field         | Europe/London                    |
      | Reg-form country field          | United Kingdom                   |
      | Reg-form password field         | password                         |
      | Reg-form confirm password field | password                         |
    When I click on the "Reg-form proceed button" in the "Registration form" iframe
    Then I should be authenticated
