Feature:
  As a user
  I want to be able to register on the Moderate registration form
  So that I can gain more access to that piece of content easily

  @nightly
  Scenario: User is registered and logged in after submitting the Moderate level reg-form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Moderate |
    When I visit webinar audience play page
    And I wait for 5 secs
    And I fill in the following form details
      | field                           | value                            |
      | Reg-form first name field       | seleniumFName                    |
      | Reg-form last name field        | seleniumLName                    |
      | Reg-form email field            | random                           |
      | Reg-form job title field        | seleniumJobTitle                 |
      | reg-form company field          | seleniumCompany                  |
      | Reg-form password field         | password                         |
    When I click on the "Reg-form proceed button"
    And I should see the "audience-page-your-place-confirmed" with text matching "Your place is confirmed"
    And I visit the "profile-page" page
    Then I should see the profile data populated by the Moderate form

  @nightly
  Scenario: User is required to enter all mandatory fields to the Moderate level reg-form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Moderate |
    When I visit webinar audience play page
    And I wait for 5 secs
    And I click on the "Reg-form proceed button"
    Then I should see the "registration-form-moderate-email-not-valid" with text matching "Email is not valid."
    And I should see the "registration-form-moderate-firstname-please-complete" with text matching "Please complete."
    And I should see the "registration-form-moderate-lastname-please-complete" with text matching "Please complete."
    And I should see the "registration-form-moderate-password-please-complete" with text matching "Must have at least 6 characters"

  @nightly
  Scenario: The user leaves the email field blank on the Moderate form and is prompted to enter a valid email when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Moderate |
    When I visit webinar audience play page
    And I wait for 5 secs
    And for element "reg-form-email-field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-moderate-email-not-valid" with text matching "Email is not valid."

  @nightly
  Scenario: The user leaves the first name field blank on the Moderate form and is prompted to enter first name when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Moderate |
    When I visit webinar audience play page
    And I wait for 5 secs
    And for element "reg-form-first-name-field" I enter value ""
    And for element "reg-form-email-field" I enter value ""
    Then I should see the "registration-form-moderate-firstname-please-complete" with text matching "Please complete."

  @nightly
  Scenario: The user inputs a free email on the Moderate form and the Company field should not appear
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Moderate |
    When I visit webinar audience play page
    And the "reg-form company field" is not displayed on the page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | notexistinguser@gmail.com         |
    And for element "reg-form-first-name-field" I enter value ""
    And I wait for 3 secs
    Then I fill in the following form details
      | field                    | value                             |
      | reg-form company field   | notexistingcompany                |

  @nightly
  Scenario: The user enters invalid email on the Moderate form and is prompted to enter a valid email when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Moderate |
    When I visit webinar audience play page
    And I wait for 5 secs
    And for element "reg-form-email-field" I enter value "THISISINVALIDEMAIL"
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-moderate-email-not-valid" with text matching "Email is not valid."

  @nightly
  Scenario: The user job title empty on the Moderate form and is prompted to enter a job title when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Moderate |
    When I visit webinar audience play page
    And for element "Reg-form job title field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    And I wait for 3 secs
    And temporal step
    Then the "registration-form-moderate-job-title-please-complete_please_complete" is not present

  @nightly
  Scenario: The user inputs a company email address that is present in the BrightTALKs report database on the Moderate form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Moderate |
    When I visit webinar audience play page
    And I wait for 5 secs
    And the "reg-form company field" is not displayed on the page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | newusertest@brighttalk.com        |
    And I wait for 3 secs
    And the "reg-form company field" is not displayed on the page

  @nightly
  Scenario: The user leaves last name field empty on the Moderate form and is prompted to enter a last name field when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Moderate |
    When I visit webinar audience play page
    And I wait for 5 secs
    And for element "Reg-form last name field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-moderate-lastname-please-complete" with text matching "Please complete."

  @nightly
  Scenario: The user inputs a company email address that is not present in the BrightTALKs report database on the Moderate form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Moderate |
    When I visit webinar audience play page
    And the "reg-form company field" is not displayed on the page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | notexisting@notexistingcompany.com|
    And for element "reg-form-first-name-field" I enter value ""
    Then I fill in the following form details
      | field                    | value                             |
      | reg-form company field   | notexistingcompany                |