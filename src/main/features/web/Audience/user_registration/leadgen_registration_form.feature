Feature:
  As a user
  I want to be able to register on the Lead Gen registration form
  So that I can gain more access to that piece of content easily

  @nightly
  Scenario: User is registered and logged in after submitting the Lead Gen level reg-form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And temporal step
    And I fill in the following form details
      | field                           | value                            |
      | Reg-form first name field       | seleniumFName                    |
      | Reg-form last name field        | seleniumLName                    |
      | Reg-form email field            | random                           |
      | Reg-form telephone field        | 123456                           |
      | Reg-form job title field        | seleniumJobTitle                 |
      | reg-form-company-field          | seleniumCompany                  |
      | Reg-form level field            | Director                         |
      | reg-form-industry-field         | Agriculture & Agribusiness       |
      | reg-form-company-size-field     | 101-250                          |
      | Reg-form level field            | Director                         |
      | Reg-form password field         | password                         |
    And I click on the "Reg-form proceed button"
    And I should see the "audience-page-your-place-confirmed" with text matching "Your place is confirmed"
    Then I visit the "profile-page" page
    And I should see the profile data populated by the Lead Gen form

  @nightly
  Scenario: User is required to enter all mandatory fields to the Lead Gen level reg-form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And I click on the "Reg-form proceed button"
    Then I should see the "registration-form-leadgen-email-not-valid" with text matching "Email is not valid."
    And I should see the "registration-form-leadgen-firstname-please-complete" with text matching "Please complete."
    And I should see the "registration-form-leadgen-lastname-please-complete" with text matching "Please complete."
    And I should see the "registration-form-leadgen-telephone-please-complete" with text matching "Must have at least 5 digits"
    And I should see the "registration-form-leadgen-job-title-please-complete" with text matching "Please complete."
    And the element "registration-form-leadgen-level-please-complete" is present
    And I should see the "registration-form-leadgen-password-please-complete" with text matching "Must have at least 6 characters"

  @nightly
  Scenario: The user leaves the email field blank on the Lead Gen form and is prompted to enter a valid email when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And for element "reg-form-email-field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-email-not-valid" with text matching "Email is not valid."

  @nightly
  Scenario: The user inputs a company email address that is not present in the BrightTALKs report database on the Lead Gen form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And the "reg-form company field" is not displayed on the page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | notexisting@notexistingcompany.com|
    And for element "reg-form-first-name-field" I enter value ""
    Then I fill in the following form details
      | field                    | value                             |
      | reg-form company field   | notexistingcompany                |

  @nightly
  Scenario: The user leaves the password field blank on the Lead Gen form and is prompted to enter a valid password when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And for element "Reg-form password field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    And I should see the "registration-form-leadgen-password-please-complete" with text matching "Must have at least 6 characters"

  @nightly
  Scenario: The user leaves the first name field blank on the Lead Gen form and is prompted to enter first name when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And for element "reg-form-first-name-field" I enter value ""
    And for element "reg-form-email-field" I enter value ""
    Then I should see the "registration-form-leadgen-firstname-please-complete" with text matching "Please complete."

  @nightly
  Scenario: The user inputs a free email on the Lead Gen form and the Company field appears
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And the "reg-form company field" is not displayed on the page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | notexistinguser@gmail.com         |
    And for element "reg-form-first-name-field" I enter value ""
    Then I fill in the following form details
      | field                    | value                             |
      | reg-form company field   | notexistingcompany                |

  @nightly
  Scenario: The user enters invalid email on the Lead Gen form and is prompted to enter a valid email when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And for element "reg-form-email-field" I enter value "THISISINVALIDEMAIL"
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-email-not-valid" with text matching "Email is not valid."

  @nightly
  Scenario: The user job title empty on the Lead Gen form and is prompted to enter a job title when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And for element "Reg-form job title field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    And temporal step
    Then I should see the "registration-form-leadgen-job-title-please-complete" with text matching "Please complete."

  @nightly
  Scenario: The user inputs a company email address that is present in the BrightTALKs report database on the Lead Gen form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And the "reg-form company field" is not displayed on the page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | newusertest@brighttalk.com        |
    And I wait for 3 secs
    And temporal step
    And the "reg-form company field" is not displayed on the page

  @nightly
  Scenario: The user leaves last name field empty on the Lead Gen form and is prompted to enter a last name field when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And for element "Reg-form last name field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-lastname-please-complete" with text matching "Please complete."

  @nightly
  Scenario: The user leaves telephone field empty on the Lead Gen form and is prompted to enter a Telephone field when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And for element "Reg-form telephone field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-telephone-please-complete" with text matching "Must have at least 5 digits"

  @nightly
  Scenario: The user enters 4 digits to the telephone field on the Lead Gen form and is prompted to enter a Telephone field when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And for element "Reg-form telephone field" I enter value "4567"
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-telephone-please-complete" with text matching "Must have at least 5 digits"

  @nightly
  Scenario: User does not select the job level from the drop down after clicking on it
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And I click on the "Reg-form level field"
    And for element "Reg-form level field" I select value "Director"
    And for element "Reg-form level field" I select value ""
    Then the element "registration-form-leadgen-level-please-complete" is present

  @nightly
  Scenario: The user enters 5 characters to the password field on the Lead Gen form and is prompted to enter a valid password when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And for element "Reg-form password field" I enter value "iustr"
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-password-please-complete" with text matching "Must have at least 6 characters"

  @nightly
  Scenario: The user leaves the company field empty on the Lead Gen form and is prompted to enter a company field when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And the "reg-form company field" is not displayed on the page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | notexisting@notexistingcompany.com|
    And for element "reg-form-last-name-field" I enter value ""
    And for element "reg-form company field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-company-please-complete" with text matching "Please complete."

  @nightly
  Scenario: The user leaves the no of emplyees field empty on the Lead Gen form and is prompted to enter a company field when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | notexisting@notexistingcompany.com|
    And for element "reg-form-last-name-field" I enter value ""
    And for element "reg-form company field" I enter value "notexistingcompany.com"
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "reg-form-company-size-field"
    And I should see the "reg-form-industry-field"

  @nightly
  Scenario: The user leaves the industry field empty on the Lead Gen form and is prompted to enter a company field when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | notexisting@notexistingcompany.com|
    And for element "reg-form-last-name-field" I enter value ""
    And for element "reg-form company field" I enter value "notexistingcompany.com"
    And for element "reg-form-first-name-field" I enter value ""
    And for element "reg-form-industry-field" I select value "Education"
    And for element "reg-form-first-name-field" I enter value ""
    And for element "reg-form-industry-field" I select value ""
    And for element "reg-form-first-name-field" I enter value ""
    And the element "registration-form-leadgen-industry-please-complete" is present

  @nightly
  Scenario: The user leaves the company size drop down unselected on the Lead Gen form and is prompted to enter an industry when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    When I visit webinar audience play page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | notexisting@notexistingcompany.com|
    And for element "reg-form-last-name-field" I enter value ""
    And for element "reg-form company field" I enter value "notexistingcompany.com"
    And for element "reg-form-first-name-field" I enter value ""
    And for element "reg-form-company-size-field" I select value "101-250"
    And for element "reg-form-first-name-field" I enter value ""
    And for element "reg-form-company-size-field" I select value ""
    And for element "reg-form-first-name-field" I enter value ""
    And the element "registration-form-leadgen-companysize-please-complete" is present