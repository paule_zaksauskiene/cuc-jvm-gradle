Feature:
  As a user
  I want to be able to register on the Prospect registration form
  So that I can gain more access to that piece of content easily

  @nightly
  Scenario: User is registered and logged in after submitting the Prospect level reg-form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Prospect |
    When I visit webinar audience play page
    And I wait for 5 secs
    And I fill in the following form details
      | field                           | value                            |
      | Reg-form first name field       | seleniumFName                    |
      | Reg-form last name field        | seleniumLName                    |
      | Reg-form email field            | random                           |
      | Reg-form telephone field        | 123456                           |
      | reg-form company field          | seleniumCompany                  |
      | Reg-form level field            | Director                         |
      | reg-form-company-size-field     | 101-250                          |
      | Reg-form password field         | password                         |
    And I click on the "Reg-form proceed button"
    And I should see the "audience-page-your-place-confirmed" with text matching "Your place is confirmed"
    Then I visit the "profile-page" page
    And I should see the profile data populated by the Prospect form

  @nightly
  Scenario: The user leaves the email field blank on the Prospect form and is prompted to enter a valid email when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Prospect |
    When I visit webinar audience play page
    And I wait for 5 secs
    And for element "reg-form-email-field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    And I wait for 1 secs
    Then I should see the "registration-form-prospect-email-not-valid" with text matching "Email is not valid."

  @nightly
  Scenario: The user leaves the first name field blank on the Prospect form and is prompted to enter first name when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Prospect |
    When I visit webinar audience play page
    And I wait for 5 secs
    And for element "reg-form-first-name-field" I enter value ""
    And for element "reg-form-email-field" I enter value ""
    Then I should see the "registration-form-prospect-firstname-please-complete" with text matching "Please complete."

  @nightly
  Scenario: The user inputs a free email on the Prospect form and the Company field appears
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Prospect |
    When I visit webinar audience play page
    And the "reg-form company field" is not displayed on the page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | notexistinguser@gmail.com         |
    And for element "reg-form-first-name-field" I enter value ""
    Then I fill in the following form details
      | field                    | value                             |
      | reg-form company field   | notexistingcompany                |

  @nightly
  Scenario: The user inputs a company email address that is present in the BrightTALKs report database on the Prospect form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Prospect |
    When I visit webinar audience play page
    And I wait for 5 secs
    And the "reg-form company field" is not displayed on the page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | newusertest@brighttalk.com        |
    And I wait for 3 secs
    And the "reg-form company field" is not displayed on the page

  @nightly
  Scenario: The user leaves last name field empty on the Prospect form and is prompted to enter a last name field when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Prospect |
    When I visit webinar audience play page
    And I wait for 5 secs
    And for element "Reg-form last name field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-prospect-lastname-please-complete" with text matching "Please complete."

  @nightly
  Scenario: The user leaves the password field blank on the Prospect form and is prompted to enter a valid password when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Prospect |
    When I visit webinar audience play page
    And I wait for 5 secs
    And for element "Reg-form password field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    And I should see the "registration-form-prospect-password-please-complete" with text matching "Must have at least 6 characters"

  @nightly
  Scenario: The user enters 5 characters to the password field on the Prospect form and is prompted to enter a valid password when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Prospect |
    When I visit webinar audience play page
    And I wait for 5 secs
    And for element "Reg-form password field" I enter value "iustr"
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-prospect-password-please-complete" with text matching "Must have at least 6 characters"

  @nightly
  Scenario: The user enters 4 digits to the telephone field on the Prospect form and is prompted to enter a Telephone field when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Prospect |
    When I visit webinar audience play page
    And I wait for 5 secs
    And for element "Reg-form telephone field" I enter value "4567"
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-prospect-telephone-please-complete" with text matching "Must have at least 5 digits"

  @nightly
  Scenario: The user leaves telephone field empty on the Prospect form and is prompted to enter a Telephone field when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Prospect |
    When I visit webinar audience play page
    And I wait for 5 secs
    And for element "Reg-form telephone field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-prospect-telephone-please-complete" with text matching "Must have at least 5 digits"

  @nightly
  Scenario: The user inputs a company email address that is not present in the BrightTALKs report database on the Prospect form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Prospect |
    When I visit webinar audience play page
    And the "reg-form company field" is not displayed on the page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | notexisting@notexistingcompany.com|
    And for element "reg-form-first-name-field" I enter value ""
    Then I fill in the following form details
      | field                    | value                             |
      | reg-form company field   | notexistingcompany                |

