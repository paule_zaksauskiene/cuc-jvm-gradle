
Feature:
  As a user
  I want to be able to register on Lead Gen form through the embedded page
  So that I can gain more access to that piece of content easily

 @nightly
  Scenario: Register new user on the embed Lead Gen form when company is not known
    Given a webcast exists
    And the following channel features are set
    | key                           | value    |
    | registration level            | Lead Gen |
    And the player test page is open
    And the webinar embed code is embedded in none language
    When clicks the Attend button
    And I switch to "player-iframe" frame
    And I fill in the following form details
     | field                           | value                            |
     | Reg-form first name field       | seleniumFName                    |
     | Reg-form last name field        | seleniumLName                    |
     | Reg-form email field            | random                           |
     | Reg-form telephone field        | 123456                           |
     | Reg-form job title field        | seleniumJobTitle                 |
     | reg-form-company-field          | seleniumCompany                  |
     | Reg-form level field            | Director                         |
     | reg-form-industry-field         | Agriculture & Agribusiness       |
     | reg-form-company-size-field     | 101-250                          |
     | Reg-form password field         | password                         |
   And I click on the "Reg-form proceed button" in the "player-iframe" iframe
   And temporal step
   And I switch to "player-iframe" frame
   And I should see the "audience-page-your-place-confirmed" with text matching "Your place is confirmed"
   Then I visit the "profile-page" page
   And I should see the profile data populated by the Lead Gen form

  @nightly
  Scenario: The user leaves the company field empty on the embed Lead Gen form and is prompted to enter a company field when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    And the player test page is open
    And the webinar embed code is embedded in none language
    When clicks the Attend button
    And I switch to "player-iframe" frame
    And temporal step
    And the "reg-form company field" is not displayed on the page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | notexisting@notexistingcompany.com|
    And temporal step
    And I switch to "player-iframe" frame
    And for element "reg-form-last-name-field" I enter value ""
    And for element "reg-form company field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-company-please-complete" with text matching "Please complete."

  @nightly
  Scenario: The user leaves the first name field blank on the embed Lead Gen form and is prompted to enter first name when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    And the player test page is open
    And the webinar embed code is embedded in none language
    And clicks the Attend button
    When I switch to "player-iframe" frame
    And for element "reg-form-first-name-field" I enter value ""
    And for element "reg-form-email-field" I enter value ""
    Then I should see the "registration-form-leadgen-firstname-please-complete" with text matching "Please complete."

  @nightly
  Scenario: User does not select the job level from the drop down after clicking on iton the embed Lead Gen form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    And the player test page is open
    And the webinar embed code is embedded in none language
    And clicks the Attend button
    When I switch to "player-iframe" frame
    And I click on the "Reg-form level field"
    And I switch to "player-iframe" frame
    And for element "Reg-form level field" I select value "Director"
    And for element "Reg-form level field" I select value ""
    Then the element "registration-form-leadgen-level-please-complete" is present

  @nightly
  Scenario: The user job title empty on the embed Lead Gen form and is prompted to enter a job title when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    And the player test page is open
    And the webinar embed code is embedded in none language
    And clicks the Attend button
    When I switch to "player-iframe" frame
    And for element "Reg-form job title field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-job-title-please-complete" with text matching "Please complete."

  @nightly
  Scenario: The user leaves the password field blank on the Lead Gen form and is prompted to enter a valid password when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    And the player test page is open
    And the webinar embed code is embedded in none language
    And clicks the Attend button
    When I switch to "player-iframe" frame
    And for element "Reg-form password field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    And I should see the "registration-form-leadgen-password-please-complete" with text matching "Must have at least 6 characters"

  @nightly
  Scenario: The user enters 5 characters to the password field on the embed Lead Gen form and is prompted to enter a valid password when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    And the player test page is open
    And the webinar embed code is embedded in none language
    And clicks the Attend button
    When I switch to "player-iframe" frame
    And for element "Reg-form password field" I enter value "iustr"
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-password-please-complete" with text matching "Must have at least 6 characters"

  @nightly
  Scenario: The user enters 4 digits to the telephone field on the embed Lead Gen form and is prompted to enter a Telephone field when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    And the player test page is open
    And the webinar embed code is embedded in none language
    And clicks the Attend button
    When I switch to "player-iframe" frame
    And for element "Reg-form telephone field" I enter value "4567"
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-telephone-please-complete" with text matching "Must have at least 5 digits"

  @nightly
  Scenario: The user leaves telephone field empty on the embed Lead Gen form and is prompted to enter a Telephone field when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    And the player test page is open
    And the webinar embed code is embedded in none language
    And clicks the Attend button
    When I switch to "player-iframe" frame
    And for element "Reg-form telephone field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-telephone-please-complete" with text matching "Must have at least 5 digits"

  @nightly
  Scenario: The user leaves the email field blank on the embed Lead Gen form and is prompted to enter a valid email when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    And the player test page is open
    And the webinar embed code is embedded in none language
    And clicks the Attend button
    When I switch to "player-iframe" frame
    And for element "reg-form-email-field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-email-not-valid" with text matching "Email is not valid."

  @nightly
  Scenario: Existing user is trying to register and log in after submitting the embed Lead Gen level reg-form on embed page
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen  |
    And the player test page is open
    And the webinar embed code is embedded in none language
    And clicks the Attend button
    When I switch to "player-iframe" frame
    And I fill in the following form details
      | field                           | value                                       |
      | Reg-form first name field       | seleniumFName                               |
      | Reg-form last name field        | seleniumLName                               |
      | Reg-form email field            | regressionTestsManagerUser@brighttalk.com   |
      | Reg-form telephone field        | 123456                                      |
      | Reg-form job title field        | seleniumJobTitle                            |
      | Reg-form level field            | Director                                    |
      | Reg-form password field         | password                                    |
    When I click on the "Reg-form proceed button" in the "player-iframe" iframe
    And I switch to "player-iframe" frame
    Then I should see the "registration-form-leadgen-email-not-valid" with text matching " Email already in use"

  @nightly
  Scenario: The user leaves last name field empty on the embed Lead Gen form and is prompted to enter a last name field when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    And the player test page is open
    And the webinar embed code is embedded in none language
    And clicks the Attend button
    When I switch to "player-iframe" frame
    And for element "Reg-form last name field" I enter value ""
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-lastname-please-complete" with text matching "Please complete."

  @nightly
  Scenario: The user enters invalid email on the embed Lead Gen form and is prompted to enter a valid email when the user moves to the next box field
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    And the player test page is open
    And the webinar embed code is embedded in none language
    And clicks the Attend button
    When I switch to "player-iframe" frame
    And for element "reg-form-email-field" I enter value "THISISINVALIDEMAIL"
    And for element "reg-form-first-name-field" I enter value ""
    Then I should see the "registration-form-leadgen-email-not-valid" with text matching "Email is not valid."

  @nightly
  Scenario: The user inputs a company email address that is present in the BrightTALKs report database on the embed Lead Gen form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    And the player test page is open
    And the webinar embed code is embedded in none language
    And clicks the Attend button
    When I switch to "player-iframe" frame
    And the "reg-form company field" is not displayed on the page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | newusertest@brighttalk.com        |
    And I wait for 3 secs
    And I switch to "player-iframe" frame
    And the "reg-form company field" is not displayed on the page

  @nightly
  Scenario: The user inputs a company email address that is not present in the BrightTALKs report database on the embed Lead Gen form
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    And the player test page is open
    And the webinar embed code is embedded in none language
    And clicks the Attend button
    When I switch to "player-iframe" frame
    And the "reg-form company field" is not displayed on the page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | notexisting@notexistingcompany.com|
    And I switch to "player-iframe" frame
    And for element "reg-form-first-name-field" I enter value ""
    Then I fill in the following form details
      | field                    | value                             |
      | reg-form company field   | notexistingcompany                |

  @nightly
  Scenario: The user inputs a free email on the embed Lead Gen form and the Company field appears
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Lead Gen |
    And the player test page is open
    And the webinar embed code is embedded in none language
    And clicks the Attend button
    When I switch to "player-iframe" frame
    And the "reg-form company field" is not displayed on the page
    And I fill in the following form details
      | field                    | value                             |
      | Reg-form email field     | notexistinguser@gmail.com         |
    And I switch to "player-iframe" frame
    And for element "reg-form-first-name-field" I enter value ""
    Then I fill in the following form details
      | field                    | value                             |
      | reg-form company field   | notexistingcompany                |