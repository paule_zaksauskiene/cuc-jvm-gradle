
Feature:
  As a user
  I want to be able to register on Minimum form through the embedded page
  So that I can gain more access to that piece of content easily

 @nightly
  Scenario: Register new user on the embed Minimum form
    Given a webcast exists
    And no resource is needed
    And the following channel features are set
    | key                           | value    |
    | registration level            | Minimum  |
    And the player test page is open
    And the webinar embed code is embedded in none language
    When clicks the Attend button
    And I switch to "player-iframe" frame
    And I fill in the following form details
     | field                           | value                            |
     | Reg-form first name field       | seleniumFName                    |
     | Reg-form last name field        | seleniumLName                    |
     | Reg-form email field            | random                           |
     | Reg-form password field         | password                         |
   And I click on the "Reg-form proceed button" in the "player-iframe" iframe
   And I switch to "player-iframe" frame
   And I should see the "audience-page-your-place-confirmed" with text matching "Your place is confirmed"
   Then I visit the "profile-page" page
   And I should see the profile data populated by the Minimum form
