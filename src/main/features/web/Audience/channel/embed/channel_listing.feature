Feature: Channel listing on player test page
  As a channel owner of the BrightTALK channels
  I want to be able schedule pro webinars
  So that audience can access them

  Scenario: As an channel owner I want to have channel listing for unspecified language
    Given the two pro webinars are in channel
    When audience opens the channel listing embed page
    And no language is specified
    Then the pro webinar scheduled time and date is shown as set on the server
