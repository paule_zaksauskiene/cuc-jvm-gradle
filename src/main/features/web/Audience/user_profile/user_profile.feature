Feature: User profile
  As an audience of BrightTALK
  I want to be able to access my profile page
  So that I can see or modify my data

  @nightly
  Scenario: As a new BrightTALK user I want to use international and specific characters in my profile data
    And I am logged in as new user
    When I visit the "edit-profile-page" page
    And for element "edit-profile-page-first-name" I enter value "èàùéâêîôûëïçäöüß€áéíñóúü¿¡思想的指導者の毎日何千人も積極생각"
    And for element "edit-profile-page-second-name" I enter value "매일 수천àÀáÁãÃçÇéÉêÊÍíóÓúÚüÜёъяшертыуиопющэасдфгчйкл"
    And for element "edit-profile-page-job-title" I enter value "ьжзхцвбнм每天都有思想領袖正在積極分享他們的見解!@£$%^&*()"
    And for element "edit-profile-page-company" I enter value "_+±{}|?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆"
    And for element "edit-profile-page-telephone" I enter value "123456"
    And for element "edit-profile-page-level" I select value "Other"
    And for element "edit-profile-page-company-size" I select value "1-10"
    And for element "edit-profile-page-industry" I select value "Agriculture & Agribusiness"
    And for element "edit-profile-page-country" I select value "United Kingdom"
    And I click on the "edit-profile-page-submit-button"
    Then I should see the "profile-page-first-name" with text matching "èàùéâêîôûëïçäöüß€áéíñóúü¿¡思想的指導者の毎日何千人も積極생각"
    And I should see the "profile-page-last-name" with text matching "매일 수천àÀáÁãÃçÇéÉêÊÍíóÓúÚüÜёъяшертыуиопющэасдфгчйкл"
    And I should see the "profile-page-job-title" with text matching "ьжзхцвбнм每天都有思想領袖正在積極分享他們的見解!@£$%^&*()"
    And I should see the "profile-page-company" with text matching "_+±{}|?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆"
    And I should see the "profile-page-telephone-number" with text matching "123456"
    And I should see the "profile-page-level" with text matching "Other"
    And I should see the "profile-page-company-size" with text matching "1-10"
    And I should see the "profile-page-industry" with text matching "Agriculture & Agribusiness"
    And I should see the "edit-profile-page-country" with text matching "United Kingdom"