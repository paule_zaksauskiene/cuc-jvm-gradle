Feature: Attachments
  As a owner of the pro webinar
  I want to be able add attachments
  So that presenters can download them

  @nightly
  Scenario: Attachments are shown for the presenter
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    And pro webinar contains "link to a file" attachment
    And pro webinar contains "link to a web page" attachment
    When I visit the "presenter" page
    And I click on the "presenter-page-attachments"
    Then attachment 1 is shown on the page
    And attachment 2 is shown on the page

  @nightly
  Scenario: File type attachment is shown for the presenter
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    And pro webinar contains file type attachment
    When I visit the "presenter" page
    And I click on the "presenter-page-attachments"
    Then attachment 1 is shown on the page
    And I should see the "presenter-page-attachments-list-first-attachment-type" with text matching "Plain text"
    And I should see the "presenter-page-attachments-list-first-attachment-size" with text matching "0.1 KB"

  @nightly @firefox
  Scenario: File type attachment is shown for the presenter
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    And pro webinar contains file type attachment
    When I visit the "presenter" page
    And I click on the "presenter-page-attachments"
    Then attachment 1 is shown on the page
    And I should see the "presenter-page-attachments-list-first-attachment-type" with text matching "Plain text"
    And I should see the "presenter-page-attachments-list-first-attachment-size" with text matching "0.1 KB"