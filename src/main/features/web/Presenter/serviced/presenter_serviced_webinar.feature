Feature: Presenter screen of the serviced pro webinar
  As a presenter
  I want to be able to open the presenter screen of the serviced pro webinar
  So that I can see the serviced webinar tab features

  @nightly
  Scenario: The presenter screen shows the Studio tab title for serviced webinar
    Given I am logged in as channel owner
    And I own a channel
    And a serviced webcast exists
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
    When I visit the "presenter-serviced-page"
    Then the element "presenter-page-studio-tab-title" is present

  @CON-242
  Scenario: Add video url to the serviced pro webinar presenter screen Studio tab
    Given I am logged in as channel owner
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Pro Webinar Slides            | enabled |
    And a serviced webcast exists
    And a pro webinar starts in 28 minutes
    And no resource is needed
    When I visit the "presenter-serviced-page"
    And I select PPT file "SeleniumTest.ppt" to begin upload
    And PPT slides are uploaded
    And I click on the "presenter-page-studio-tab"
    And for element "presenter-page-studio-tab-add-url" I enter value "rtmp://cp70386.edgefcs.net/ondemand/mp4:odflash/landingpages/ops/kuwait.mp4"
    And for element "presenter-page-studio-tab-add-url-title" I enter value "test1"
    And I click on the "presenter-page-studio-tab-add-url-button"
    And I should see the "presenter-page-studio-tab-url-one" with text matching "mp4:odflash/landingpages/ops/kuwait"
    And I should see the "presenter-page-studio-tab-url-one-title" with text matching "test1"
    And the "presenter-page-studio-tab-url-stopfirst-button" is not displayed on the page
    And I click on the "presenter-page-studio-tab-url-playfirst-button"
    Then I should see the "presenter-page-studio-tab-url-stopfirst-button"

  @nightly
  Scenario: Add max number of video urls to the Studio tab
    Given I am logged in as channel owner
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Pro Webinar Slides            | enabled |
    And a serviced webcast exists
    When I visit the "presenter-serviced-page"
    And I click on the "presenter-page-studio-tab"
    And for element "presenter-page-studio-tab-add-url" I enter value "rtmp://cp70386.edgefcs.net/ondemand/mp4:odflash/landingpages/ops/kuwait.mp4"
    And for element "presenter-page-studio-tab-add-url-title" I enter value "test1"
    And I click on the "presenter-page-studio-tab-add-url-button"
    And for element "presenter-page-studio-tab-add-url" I enter value "rtmp://cp70386.edgefcs.net/ondemand/mp4:odflash/landingpages2/ops/kuwait.mp4"
    And for element "presenter-page-studio-tab-add-url-title" I enter value "test2"
    And I click on the "presenter-page-studio-tab-add-url-button"
    And for element "presenter-page-studio-tab-add-url" I enter value "rtmp://cp70386.edgefcs.net/ondemand/mp4:odflash/landingpages3/ops/kuwait.mp4"
    And for element "presenter-page-studio-tab-add-url-title" I enter value "test3"
    And I click on the "presenter-page-studio-tab-add-url-button"
    Then I should see the "presenter-page-studio-tab-url-one" with text matching "mp4:odflash/landingpages/ops/kuwait"
    And I should see the "presenter-page-studio-tab-url-one-title" with text matching "test1"
    And I should see the "presenter-page-studio-tab-url-two" with text matching "mp4:odflash/landingpages2/ops/kuwait"
    And I should see the "presenter-page-studio-tab-url-two-title" with text matching "test2"
    And I should see the "presenter-page-studio-tab-url-three" with text matching "mp4:odflash/landingpages3/ops/kuwait"
    And I should see the "presenter-page-studio-tab-url-three-title" with text matching "test3"

  @nightly
  Scenario: Remove video url from Studio tab
    Given I am logged in as channel owner
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Pro Webinar Slides            | enabled |
    And a serviced webcast exists
    And I visit the "presenter-serviced-page"
    And I click on the "presenter-page-studio-tab"
    And for element "presenter-page-studio-tab-add-url" I enter value "rtmp://cp70386.edgefcs.net/ondemand/mp4:odflash/landingpages/ops/kuwait.mp4"
    And for element "presenter-page-studio-tab-add-url-title" I enter value "test1"
    And I click on the "presenter-page-studio-tab-add-url-button"
    And I should see the "presenter-page-studio-tab-url-one" with text matching "mp4:odflash/landingpages/ops/kuwait"
    When I click on the "presenter-page-studio-tab-url-remove-first-button"
    Then the "presenter-page-studio-tab-url-one" is not displayed on the page