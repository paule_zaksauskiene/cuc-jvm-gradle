Feature: Powerpoint
  As a presenter of the pro webinar
  I want to be able upload powerpoint file
  So that presenters and audience can see the slides

  @nightly
  Scenario: The Upload powerpoint details are shown on the presenter screen
    Given I am logged in as manager
    And a pro webinar exists
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    When I visit the "presenter" page
    Then I should see a "powerpoint logo"
    And I should see a "Upload powerpoint text"
    And I should see the "Upload powerpoint button"
    And I should see a "powerpoint file size limit"

  @CON-242
  Scenario: Upload powerpoint for upcoming webinar as channel owner
    Given I am logged in as channel owner
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    When I visit the "presenter" page
    And I select PPT file "SeleniumTest.ppt" to begin upload
    Then the element "presenter-page-powerpoint-no-slides-preview-hidden" appears in "3000" secs

  @CON-242
  Scenario: Upload powerpoint that is too large
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    When I visit the "presenter" page
    And I select PPT file "MoreThan100Mb.pptx" to begin upload
    Then the element "presenter-page-file-exceed-warning" is present

  @CON-242
  Scenario: The uploaded PPT remains after reopening the presenter page
    Given I am logged in as manager
    And a pro webinar exists
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar starts in 27 minutes
    And I visit the "presenter" page
    And I select PPT file "SeleniumTest.ppt" to begin upload
    And PPT slides are uploaded
    And I visit the "presenter" page
    Then PPT slides are uploaded
    And the number of slides is 5

  @CON-242
  Scenario: Upload powerpoint for live webinar
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    And pro webinar is live
    And I select PPT file "SeleniumTest.ppt" to begin upload
    Then PPT slides are uploaded
    And the number of slides is 5
    And the element "presenter-screen-powerpoint-forward-button" is present
    And the element "presenter-screen-powerpoint-back-button" is present

  @CON-242
  Scenario: Cancel removing of PPT slides
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    And pro webinar is live
    When I select PPT file "SeleniumTest.ppt" to begin upload
    And PPT slides are uploaded
    And I click a button to remove slides
    And I click on the "presenter-screen-powerpoint-cancel-on-replace-slides"
    And the number of slides is 5

  @CON-242
  Scenario: Remove PPT slides
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    And a pro webinar starts in 27 minutes
    And I visit the "presenter" page
    When I select PPT file "SeleniumTest.ppt" to begin upload
    And PPT slides are uploaded
    And I click a button to remove slides
    And I click on the "presenter-screen-remove-slides-confirmation-button"
    Then the "presenter-screen-add-powerpoint-hidden-element" is not present
    And inactive slides icon becomes grey

  @CON-242
  Scenario: Replace slides
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    And a pro webinar starts in 27 minutes
    And I visit the "presenter" page
    When I select PPT file "SeleniumTest.ppt" to begin upload
    And PPT slides are uploaded
    And I click a button to remove slides
    And I click on the "presenter-screen-remove-slides-confirmation-button"
    And I select PPT file "SeleniumTest2.ppt" to begin upload
    Then PPT slides are uploaded
    And the number of slides is 3

  @CON-242
  Scenario: Slide upload process on two presenter screens and processing fails
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    And I visit the "presenter" page
    And I open new browser window 2
    And I visit the "presenter" page
    And I activate browser window 1
    And I select PPT file "Selenium_corrupted.ppt" to begin upload
    And the element "presenter-page-powerpoint-step2-progress" is present
    And the element "presenter-page-powerpoint-step3-progress" is present
    And the element "presenter-screen-add-screenshare-button-disabled" is present
    And I activate browser window 1
    And the element "presenter-page-powerpoint-step3-progress" is present
    And the element "presenter-page-powerpoint-processing-failed" is present
    And the element "presenter-screen-joinme-add-enabled" is present
    And the element "presenter-page-powerpoint-upload-enabled" is present
    And I activate browser window 1
    And the element "presenter-page-powerpoint-processing-failed" is present
    And the element "presenter-screen-joinme-add-enabled" is present
    And the element "presenter-page-powerpoint-upload-enabled" is present

  @CON-242
  Scenario: Upload PPT and verify two presenter screens
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    And a pro webinar starts in 27 minutes
    And I visit the "presenter" page
    And I open new browser window 2
    And I visit the "presenter" page
    And I activate browser window 1
    When I select PPT file "SeleniumTest40mb.pptx" to begin upload
    And the element "presenter-page-powerpoint-upload-disabled" is present
    And the element "presenter-screen-add-screenshare-button-disabled" is present
    And PPT slides are uploaded
    And I activate browser window 1
    And PPT slides are uploaded
    And I click a button to remove slides
    And I click on the "presenter-screen-remove-slides-confirmation-button"
    Then the element "presenter-screen-add-screenshare-button-disabled" is present
    And the element "presenter-page-powerpoint-upload-enabled" is present
    And the element "presenter-screen-joinme-add-enabled" is present

  @nightly
  Scenario: Upload virus powerpoint file for upcoming webinar
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    When I visit the "presenter" page
    And I select PPT file "eicar-test-virus.ppt" to begin upload
    Then the element "presenter-page-powerpoint-virus-detected-message" is present
   
  Scenario: multiple presenters are updated when PPT slide deck is uploaded and removed on the presenter screen.
Given five presenters have presenter screens opened
When one of the presenters uploads the PPT slide deck
Then all presenters are updated

Scenario: All presenters windows are updated when PPT slide deck is removed
Given five presenters have presenter screens opened
When one of the presenters removes the PPT slides
Then all presenter screens are updated