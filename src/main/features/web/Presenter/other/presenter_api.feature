Feature: Presenter page
  As a presenter of the pro webinar
  I want to be able to open the presenter screen
  So thatI can see the information of the webinar

  @CON-242
  Scenario: The presenter is not in "Content changing" status when "presentation-slidedeck-remove" is sent twice
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    And a pro webinar starts in 29 minutes
    When I visit the "presenter" page
    And resource is allocated
    And I select PPT file "SeleniumTest.ppt" to begin upload
    And PPT slides are uploaded
    And I remove slides from presenter screen
    And PPT slides are removed
    And I wait for 60 secs
    And I send "presentation-slidedeck-remove" message to live platform
    And I wait for 60 secs
    Then the "presenter screen bridge preparing screen" is not present
    And the "presenter screen upload powerpoint button disabled" is not present
    And I should see a "Upload powerpoint text"
    And I should see the "Upload powerpoint button"
    And I should see a "powerpoint file size limit"