Feature: Presenter page
  As a presenter of the pro webinar
  I want to be able to open the presenter screen
  So thatI can see the information of the webinar

  @joinme
  Scenario: Presenter screen shows required data for upcoming pro webinar
    Given I am logged in as manager
    And a pro webinar exists
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    When I visit the "presenter" page
    Then I should see a "dial-in-numbers-and-pin"
    And I should see a "presenters-label"
    And I should see the "views-label"
    And I should see a "viewer-count-zero"
    And I should see a "welcome-screen"
    And I should see a "screenshare-tab"
    And I should see a "questions-from-audience-panel"
    And I should see a "questions-number-zero"
    And I should see a "feedback-from-audience-panel"
    And the element "feedback-from-audience-zero" is present
    And I should see a "attachments-panel"
    And the element "attachments-number" is present
    And I should see a "presentation-details"
    And I should see a webinar title
    And I should see a webinar presenter
    And I should see a webinar duration
    And I should see a "presenter-screen-promote-to-your-contacts"
    And I should see a "presenter-screen-start-presenting-button"
    And I should see the "presenter-screen-presenter-count" with text matching "0"
    And I should see the "presenter-screen-screenshare-poster"
    And I should see the time left before live
    And I click on the "presenter-screen-dial-in-numbers-title"
    And I should see the "presenter-screen-phone-title"
    And I should see the "presenter-screen-pin-title"
    And I should see the "presenter-screen-international-dial-in-options"
    And the element "presenter-screen-join-me-title" is present
    And the element "presenter-screen-join-me-info" is present
    And the element "presenter-screen-join-me-not-displayed" is present
    And the element "presenter-screen-live-support-disabled" is present

  @nightly
  Scenario: Presenter screen overlay can be closed
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    And a pro webinar starts in 14 minutes
    And no resource is needed
    When I visit the "presenter" page
    And I click on the "presenter-screen-close-overlay-button"
    Then the element "presenter-page-pop-up-closed" is present

  @joinme
  Scenario: Live support is disabled when pro webinar is less than 30 mins to live and screenshare enabled
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    And a pro webinar starts in 30 minutes
    When I visit the "presenter" page
    And I click on the "presenter-screen-joinme-tab"
    Then I should see the "add-joinme-information"
    And I should see the "presenter-screen-add-joinme-button"
    And the element "presenter-screen-joinme-your-audience-wont-see-anything" is present

  @nightly
  Scenario: Live support is enabled when pro webinar is less than 30 mins to live and screenshare enabled
    Given I am logged in as manager
    And a pro webinar exists
    And a pro webinar starts in 30 minutes
    And no resource is needed
    When I visit the "presenter" page
    And the live support is enabled on support hours

   @nightly
   Scenario: Zero presenters are dialled in pop up is shown when time is T-0
     Given I am logged in as manager
     And a pro webinar exists
     And a pro webinar starts in 2 minutes
     When I visit the "presenter" page
     And time to start is after T-0 on the presenter screen
     And I close the presenters please dial in now pop up
     Then the element "presenter-page-pop-up-closed" is present
     Then the element "presenter-screen-zero-presenters-pop-up" is present

  @nightly
  Scenario: The "Presenters please dial in now" pop up appears 15 mins before live
    Given I am logged in as manager
    And a pro webinar exists
    And a pro webinar starts in 15 minutes
    When I visit the "presenter" page
    And temporal step
    Then the element "presenter-page-presenters-please-dial-in" is present

  @nightly
  Scenario: The "Presenters please dial in now" pop up appears 5 mins before live
    Given I am logged in as manager
    And a pro webinar exists
    And a pro webinar starts in 8 minutes
    And no resource is needed
    When I visit the "presenter" page
    And I close the presenters please dial in now pop up
    And temporal step
    Then the element "presenter-page-pop-up-closed" is present
    And temporal step
    And the element "presenter-page-presenters-please-dial-in" is present
    
  @nightly
  Scenario: The webinar is automatically cancelled after some time if not run
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar duration is 1 mins
    And a pro webinar starts in 2 minutes
    And no resource is needed
    When I visit the "presenter" page
    And I wait for 600 secs
    Then the element "presenter-page-cancelled-webinar" is present

   @nightly
   Scenario: cancel pro webinar less than 5 mins before live
     Given I am logged in as manager
     And a pro webinar exists
     And a pro webinar starts in 5 minutes
     And no resource is needed
     When I visit the "presenter" page
     And I open new browser window 2
     And I visit the "view-booking-page"
     And I cancel booking
     And I activate browser window 1
     Then I should see the "presenter-page-cancelled-popup"

  @nightly
  Scenario: Cancel upcoming pro webinar
    Given I am logged in as manager
    And a pro webinar exists
    When I visit the "presenter" page
    And I open new browser window 2
    And I visit the "view-booking-page"
    And I cancel booking
    And I activate browser window 1
    Then I should see the "presenter-page-cancelled-popup"

  @nightly
  Scenario: reschedule pro webinar less than 5 mins before live and verify presenter screen is updated
    Given I am logged in as manager
    And a pro webinar exists
    And a pro webinar starts in 5 minutes
    And no resource is needed
    When I visit the "presenter" page
    And I open new browser window 2
    And I visit the "edit-booking-page"
    And for element "pro-webinar-booking-form-startdate" I enter value "Aug 03 2027"
    And for element "audio-and-slides-webinar-booking-form-timeminutes" I select value "00"
    And I click Proceed button on the Edit booking form
    And I accept alert message
    And I activate browser window 1
    And I wait for 60 secs
    And temporal step
    And I click on the "presenter-page-reload-button"
    Then I should see the "presenter-page-date-time" with text matching "Aug 3 2027"

  @nightly
  Scenario: As an presenter I want to send the message on presenter chat
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
      | registration level            | Minimum |
    And a pro webinar exists
    And a pro webinar starts in 25 minutes
    And no resource is needed
    And I visit the "presenter" page
    When I open new browser window 2
    And I visit the "presenter" page
    And I wait for 30 secs
    And I send the message "This is Selenium test message" on presenter chat
    And I activate browser window 1
    And I click on the "presenter-page-presenter-chat-section"
    Then I should see a "presenter-page-presenter-chat-message" with text matching "This is Selenium test message"

  @nightly
  Scenario: change date of the upcoming pro webinar and verify the presenter screen is updated
    Given I am logged in as manager
    And a pro webinar exists
    When I visit the "presenter" page
    And I open new browser window 2
    And I visit the "edit-booking-page"
    And for element "pro-webinar-booking-form-startdate" I enter value "Aug 03 2027"
    And for element "audio-and-slides-webinar-booking-form-timeminutes" I select value "00"
    And I click Proceed button on the Edit booking form
    And I accept alert message
    And I activate browser window 1
    And I wait for 60 secs
    And I click on the "presenter-page-reload-button"
    Then I should see the "presenter-page-date-time" with text matching "Aug 3 2027"

  @nightly
  Scenario: change start time, duration of the upcoming pro webinar and verify the presenter screen is updated
    Given I am logged in as manager
    And a pro webinar exists
    When I visit the "presenter" page
    And I open new browser window 2
    And I visit the "edit-booking-page"
    And for element "pro-webinar-booking-form-startdate" I enter value "Aug 03 2027"
    And for element "audio-and-slides-webinar-booking-form-timeminutes" I select value "15"
    And for element "pro-webinar-booking-form-timehours" I select value "13"
    And for element "pro-webinar-booking-form-duration" I select value "45"
    And I click Proceed button on the Edit booking form
    And I accept alert message
    And I activate browser window 1
    And I wait for 120 secs
    And I click on the "presenter-page-reload-button"
    Then I should see the "presenter-page-date-time" with text matching "2:15pm"
    And I should see the "presenter-screen-webinar-duration" with text matching "45"

  @nightly
  Scenario: change timezone of the upcoming pro webinar and verify the presenter screen is updated
    Given I am logged in as manager
    And a pro webinar exists
    When I visit the "presenter" page
    And I open new browser window 2
    And I visit the "edit-booking-page"
    And for element "pro-webinar-booking-form-startdate" I enter value "Aug 03 2027"
    And for element "pro-webinar-booking-form-timehours" I select value "13"
    And for element "audio-and-slides-webinar-booking-form-timeminutes" I select value "15"
    And for element "pro-webinar-booking-form-timezone" I select value "America/Chicago"
    And I click Proceed button on the Edit booking form
    And I accept alert message
    And I activate browser window 1
    And I wait for 60 secs
    And I click on the "presenter-page-reload-button"
    And temporal step
    Then the element "presenter-page-timezone-london" is present
    Then I should see the "presenter-page-date-time" with text matching "8:15pm"
