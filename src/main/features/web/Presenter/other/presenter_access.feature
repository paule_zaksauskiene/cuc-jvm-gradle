Feature: Presenter page
  As a presenter of the pro webinar
  I want to be able to open the presenter screen
  So thatI can see the information of the webinar

  @nightly
  Scenario: Presenting screen shows required data for upcoming pro webinar
    Given I am logged in as manager
    And a pro webinar exists
    When I visit the "presenter-access-page" page
    And I enter webinar pin
    And I click on the "presenter-access-page-enter-button"
    Then webinar details are shown on the presenting page

  @nightly
  Scenario: Presenter screen shows login page for non logged in user
    Given a pro webinar exists
    When I visit the "presenter" page
    And I click on the "presenter-page-continue-button"
    Then the element "login-page-metatag" is present

  @nightly
  Scenario: Presenting page opens for the new user after entering the PIN
    Given a pro webinar exists
    And I am logged in as new user
    When I visit the "presenter" page
    And I click on the "presenter-page-continue-button"
    And I enter webinar pin
    And I click on the "presenter-access-page-enter-button"
    Then the "presenting-page-metatag" opens

  @nightly
  Scenario: Presenting page opens for the manager not ops after entering the PIN
    Given a pro webinar exists
    And I am logged in as manager not ops
    When I visit the "presenter-access-page" page
    And I enter webinar pin
    And I click on the "presenter-access-page-enter-button"
    Then webinar details are shown on the presenting page
