Feature: As a presenter of the webinar
  I want to be able to add BrightTALK screenshare details
  So the audience and other presenters can see my screen

  @IN-8313
  Scenario: BrightTALK screen share can be added for one screen
    Given I am logged in as manager
    And a pro webinar exists
    And a pro webinar starts in 29 minutes
    When I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
    And I click on the "presenter-screen-add-brighttalk-screenshare-button"
    And I wait for 10 secs
    And I click on the "brighttalk-screenshare-one-computer-one-screen-button"
    And I wait for 10 secs
    And temporal step
    And I click on the "brighttalk-screenshare-start-sharing-button"
    And I wait for 60 secs
    Then the element "brighttalk-screenshare-preview-text" is present
    And the element "brighttalk-screenshare-your-audience-wont-see-text" is present
    And the element "brighttalk-screenshare-added-text" is present
    And the element "brighttalk-screenshare-added-presenter" is present
    And I should see the "brighttalk-screenshare-remove-link" with text matching "Remove screen share"
    And the element "brighttalk-screenshare-remove-icon" is present

  @IN-8313
  Scenario: BrightTALK screen share can be added for the second screen
    Given I am logged in as manager
    And a pro webinar exists
    And a pro webinar starts in 29 minutes
    When I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
    And I click on the "presenter-screen-add-brighttalk-screenshare-button"
    And I click on the "brighttalk-screenshare-one-computer-multiple-screens-button"
    And I click on the "brighttalk-screenshare-start-sharing-button"
    And I wait for 30 secs
    And temporal step
    Then the element "brighttalk-screenshare-preview-text" is present
    And the element "brighttalk-screenshare-your-audience-wont-see-text" is present
    And the element "brighttalk-screenshare-added-text" is present
    And the element "brighttalk-screenshare-added-presenter" is present
    And I should see the "brighttalk-screenshare-remove-link" with text matching "Remove screen share"
    And the element "brighttalk-screenshare-remove-icon" is present

  @nightly
  Scenario: BrightTALK screenshare can not be added before T-30
    Given I am logged in as manager
    And a pro webinar exists
    When I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
    Then the element "presenter-screen-add-brighttalk-screenshare-button" is present
    And the "brighttalk-screenshare-start-sharing-button" is not present

  @nightly
  Scenario: BrightTALK screenshare can not be added post live
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar is live
    And I end pro webinar
    When I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
    Then the element "presenter-screen-add-brighttalk-screenshare-button" is present
    And the "brighttalk-screenshare-start-sharing-button" is not present

  @IN-8313
  Scenario: BrightTALK screen share button appears again after presenter screen is reopened
    Given I am logged in as manager
    And a pro webinar exists
    And a pro webinar starts in 29 minutes
    When I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
    And I click on the "presenter-screen-joinme-tab"
    And I click on the "presenter-screen-add-brighttalk-screenshare-button"
    And I click on the "brighttalk-screenshare-one-computer-one-screen-button"
    And I click on the "brighttalk-screenshare-start-sharing-button"
    And I wait for 15 secs
    And the element "brighttalk-screenshare-preview-text" is present
    And I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
    Then I should see the "presenter-screen-add-brighttalk-screenshare-button"

  @nightly @firefox
  Scenario: The join.me sharing button is shown on presenter page
    And I am logged in as manager
    And a pro webinar exists
    And a pro webinar starts in 29 minutes
    When I visit the "presenter" page
    And I click on the "presenter-screen-joinme-tab"
    Then the element "presenter-screen-add-joinme-button" is present

  @CON-242
  Scenario: Add Powerpoint slides when "BrightTALK screen share" is added
    Given I am logged in as manager
    And a pro webinar exists
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar starts in 29 minutes
    When I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
    And I add BrightTALK screenshare
    And I click on the "presenter-page-slides-tab"
    And I select PPT file "SeleniumTest.ppt" to begin upload
    Then PPT slides are uploaded
    And the number of slides is 5
    And I click on the "presenter-screen-joinme-tab"
    Then the element "brighttalk-screenshare-your-audience-wont-see-text" is present

    @failing
    Scenario: both presenter windows are updated when BrightTALK screenshare is added
      Given I am logged in as manager
      And a pro webinar exists
      And a pro webinar starts in 29 minutes
      When I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
      And I open new browser window 2
      And I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
      And I add BrightTALK screenshare
      And I activate browser window 1
      Then the element "brighttalk-screenshare-preview-text" is present
      And the element "brighttalk-screenshare-your-audience-wont-see-text" is present
      And the element "brighttalk-screenshare-added-text" is present
      And the element "brighttalk-screenshare-added-presenter" is present
      And I should see the "brighttalk-screenshare-remove-link" with text matching "Remove screen share"
      And the element "brighttalk-screenshare-remove-icon" is present

  @IN-8313
  Scenario: both presenter windows are updated when BrightTALK screenshare is removed
    Given I am logged in as manager
    And a pro webinar exists
    And a pro webinar starts in 29 minutes
    When I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
    And I open new browser window 2
    And I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
    And I add BrightTALK screenshare
    And I remove BrightTALK screenshare
    And I activate browser window 1
    Then the element "presenter-screen-add-brighttalk-screenshare-button" is present
    And I should see the "presenter-screen-add-brighttalk-screenshare-button"

   @IN-8313
   Scenario: The second BrightTALK screen share can be added by other presenters after the first is removed
    Given I am logged in as manager
    And a pro webinar exists
    And a pro webinar starts in 29 minutes
    When I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
    And I open new browser window 2
    And I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
    And I add BrightTALK screenshare
    And I remove BrightTALK screenshare
    And I activate browser window 1
    And I wait for 120 secs
    And I add BrightTALK screenshare
    Then the element "brighttalk-screenshare-preview-text" is present
    And the element "brighttalk-screenshare-your-audience-wont-see-text" is present

  @nightly
  Scenario: Close the "Share your screen" overlay
    Given I am logged in as manager
    And a pro webinar exists
    And a pro webinar starts in 29 minutes
    When I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
    And I click on the "presenter-screen-add-brighttalk-screenshare-button"
    And the element "brighttalk-screenshare-share-your-screen-overlay-open" is present
    And I click on the "presenter-page-popup-close-button"
    Then the element "brighttalk-screenshare-share-your-screen-overlay-closed" is present

  @IN-8313
  Scenario: Reschedule gig for the next day when BrightTALK screenshare is added
    Given I am logged in as manager
    And a pro webinar exists
    And a pro webinar starts in 29 minutes
    When I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
    And I add BrightTALK screenshare
    And a pro webinar starts in 1440 minutes
    And I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
    Then the "brighttalk-screenshare-preview-text" is not present
    And the "brighttalk-screenshare-added-text" is not present
    And the "brighttalk-screenshare-your-audience-wont-see-text" is not present
    And the "brighttalk-screenshare-added-presenter" is not present
    And the element "presenter-screen-join-me-title" is present

  Scenario: the user authentication on the BrightTALK screenshare panel
    Given a pro webinar exists
    And the pro webinar is less than 30 mins before live
    When I visit the "presenter" page
    And I click on the "BrightTALK screen share button"
    And I logout from BrightTALK
    And I open the BrightTALK screen share link
    And I enter the BrightTALK user credentials
    Then the BrightTALK screen share is shown

  Scenario: other than Chrome browsers show warning message
    Given the <browser> is opened
    And a pro webinar exists
    And the pro webinar is less than 30 mins before live
    When I visit the "presenter" page
    And I click on the "BrightTALK screen share button"
    And I open the BrightTALK screen share link on another browser tab
    Then I should see a "BrightTALK sharing warning message 1" with text matching "Google Chrome browser required to share your screen"
    And I should see a "BrightTALK sharing warning message 2" with text matching "Please open this page in a Chrome browser"

  Scenario: Install BrightTALK screen share tool when it is not installed
    Given the BrighTALK screen share tool is not installed
    And a pro webinar exists
    And the pro webinar is less than 30 mins before live
    When I visit the "presenter" page
    And I click on the "BrightTALK screen share button"
    And I open the BrightTALK screen share link on another browser tab
    And I click on the "BrightTALK screen share install button"
    And I click on the "Start sharing your screen button"
    Then the BrightTALK screen share is shown

  Scenario: The BrightTALK screen share resumes after lost internet connection
    And a pro webinar exists
    And the pro webinar is less than 30 mins before live
    And the BrightTALK screenshare is added
    When internet connection is lost
    And the blank screen is shown on the presenter screen
    And internat connection comes back up
    Then the BrightTALK screen share is shown

  Scenario: close the BrightTALK screen share remove confirmation dialog
    Given a pro webinar exists
    And the pro webinar is less than 30 mins before live
    And the BrightTALK screenshare is added
    When I click on the "Remove BrightTALK screen share button"
    And I see the "Remove screenshare panel"
    And I click on the "Cancel button"
    Then the BrightTALK screen share is shown

  Scenario: BrightTALK screen share is disabled when pro webinar is rescheduled
    Given a pro webinar exists
    And the pro webinar is less than 30 mins before live
    When I visit the "presenter" page
    And I can see the "BrightTALK screen share button"
    And pro webinar is rescheduled to start in 1 day
    Then the BrightTALK screen share button is not visible

  Scenario: BrightTALK screen share is disabled when pro webinar is rescheduled
    Given a pro webinar exists
    And the pro webinar is less than 30 mins before live
    And the BrightTALK screenshare is added
    When pro webinar is rescheduled to start in 1 day
    And I visit the "presenter" page
    Then (TBD - ask Pete)

  Scenario: Add BrightTALK screen share form is secure
    Given a pro webinar exists
    And the pro webinar is less than 30 mins before live
    When I visit the "presenter" page
    Then the CSRF token is on the page

  Scenario: another presenter opens presenter screen when BrightTALK screen share is being added
    Given a pro webinar exists
    And the pro webinar is less than 30 mins before live
    And I click on the "BrightTALK screen share button"
    And I open the BrightTALK screen share link on another browser tab
    When another presenter opens the presenter screen
    Then the BrightTALK screen share
