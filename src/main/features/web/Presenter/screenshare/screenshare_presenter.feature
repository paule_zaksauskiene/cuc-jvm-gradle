Feature: please add As, I, So/In order feature description
  As/So/In order
  As/I
  So/In order/I

  Scenario: multiple presenters are updated when join.me link is added, edited and removed on the presenter screen.
    Given five presenters have presenter screens opened
    When one of the presenters adds the join me link
    Then all presenters are updated

  Scenario: All presenters windows are updated when join.me link is removed
    Given five presenters have presenter screens opened
    When one of the presenters removes the join.me link
    Then all presenter screens are updated