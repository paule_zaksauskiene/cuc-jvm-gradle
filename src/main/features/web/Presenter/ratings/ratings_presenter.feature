Feature: Ratings
  As a audience of the pro webinar
  I want to be able send ratings
  So that presenters can see them

  @nightly
  Scenario: As an audience I want to send ratings without a text
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
      | registration level            | Minimum |
    And a pro webinar exists
    And a pro webinar starts in 5 minutes
    And no resource is needed
    And I visit the "presenter" page
    When I open new browser window 2
    And I visit the "audience" page
    And I click on the "audience-page-ratings-tab" in the "audience-tabs-frame" iframe
    And I select 3 rating stars
    And click "Send Rating" button
    And I activate browser window 1
    Then I should see the rating count 1
    And I should see the 1 rating with 3 stars

  @nightly
  Scenario Outline: As an audience I want to send ratings
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
      | registration level            | Minimum |
    And a pro webinar exists
    And a pro webinar starts in 5 minutes
    And no resource is needed
    And I visit the "presenter" page
    When I open new browser window 2
    And I visit the "audience" page
    And I click on the "audience-page-ratings-tab" in the "audience-tabs-frame" iframe
    And I select 3 rating stars
    And I enter rating text "<feedback_text>"
    And click "Send Rating" button
    And I activate browser window 1
    Then I should see the rating count 1
    And I should see the 1 rating with 3 stars
    And I activate browser window 1
    And I close presenter screen overlay
    And I click on the "feedback-from-audience-panel"
    And I should see the "presenter-page-rating-text" with text matching "<feedback_text>"
    Examples:
      |feedback_text         |
      |Selenium Feedback Text|
      |!@£$%^&*()_+±{}:|?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœùûüÿ’“”àâæçéèêëïîôœこれは何？これは何ですか？これはカエルだよ。これはカエ何ですか？あれは太陽です、どこでしたでしょうかàâæçéèêëïîôœこれは何？これは何ですか§`å∑´®†¥¨^øπ¬˚∆これは何？これは何ですか？これはカエルだよ|
      |èàùéâêîôûëïçäöüß€áéíñóúü¿¡思想的指導者の毎日何千人も積極생각지도자의 매일 수천àÀáÁãÃçÇéÉêÊÍíóÓúÚüÜёъяшертыуиопющэасдфгчйкльжзхцвбнм每天都有思想領袖正在積極分享他們的見解!@£$%^&*()_+±{}:|?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆|

  @nightly
  Scenario: As an audience I want to send ratings
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
      | registration level            | Minimum |
    And a pro webinar exists
    And a pro webinar starts in 5 minutes
    And no resource is needed
    And I visit the "presenter" page
    When I open new browser window 2
    And I visit the "audience" page
    And I click on the "audience-page-ratings-tab" in the "audience-tabs-frame" iframe
    And I select 3 rating stars
    And I enter rating text "Everyday thousands of thought leaders are actively sharing their insights, their ideas and their most up-to-date knowledge with professionals all over the globe through the technologies that BrightTALK has created. Everyday thousands of thought leaders are actively sharing their insights, their ideas and their most up-to-date knowledge with professionals all over the globe through the technologies that BrightTALK has created.At BrightTALK, we believe that people learn the most when they hear dir"
    And click "Send Rating" button
    And I activate browser window 1
    Then I should see the rating count 1
    And I should see the 1 rating with 3 stars
    And I activate browser window 1
    And I close presenter screen overlay
    And I click on the "feedback-from-audience-panel"
    And I should see the "presenter-page-rating-text" with text matching "Everyday thousands of thought leaders are actively sharing their insights, their ideas and their most up-to-date knowledge with professionals all over the globe through the technologies that BrightTALK has created. Everyday thousands of thought leaders are actively sharing their insights, their ideas and their most up-to-date knowledge with professionals all over the globe through the technologies that BrightTALK has created.At BrightTALK, we believe that people learn the most when they hear dir"

  Scenario: As an audience I want to send ratings with maximum characters
  Given the pro webinar is live
  When the audience selects rating stars, enters more than 500 characters to the feedback text area
  And clicks the Send rating button
  Then the rating appears on the presenting screen showing only 500 of characters

  Scenario: As an audience I want to send rating including international and specific characters
  Given the pro webinar is less than 5 mins to live
  When audience opens the player page
  And sends the rating including special and international characters
  Then the presenter screen shows the rating text correctly

  Scenario: multiple presenters are updated when feedback is sent from audience
    Given five presenters have presenter screens opened
    When audience sends the feedback with text
    Then all presenters screens are updated

