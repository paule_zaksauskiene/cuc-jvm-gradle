Feature: Deleting a vote
  As a presenter of a pro webinar
  I want to delete my votes/polls
  So that I my audience do not see it

  @nightly
  Scenario: Delete vote
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    When I visit the "Presenter" page
    And I click the "Votes from audience section"
    And I click the Vote view button
    And I click the "Delete Vote button"
    And I click the "Delete Vote Confirmation button"
    Then I should see a "message no votes added" with text matching "There are no polls added for this webinar."

  @nightly
  Scenario: Delete vote and verify the second presenter screen is updated
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    When I visit the "Presenter" page
    And I open new browser window 2
    When I visit the "Presenter" page
    And I click the "Votes from audience section"
    And I click the Vote view button
    And I click the "Delete Vote button"
    And I click the "Delete Vote Confirmation button"
    And I activate browser window 1
    And I click the "Votes from audience section"
    Then I should see a "message no votes added" with text matching "There are no polls added for this webinar."
