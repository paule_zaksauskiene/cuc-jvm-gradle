Feature: Adding a vote
  As a presenter of a pro webinar
  I want to be able to create votes/polls
  So that I can learn more about my viewers

  @nightly
  Scenario: Add vote without details
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "Presenter" page
    When I click the "Votes from audience section"
    And I click the "Add vote button"
    And I click the "Add Vote Confirmation button"
    Then I should see a "warning message to enter vote details" with text matching "Please enter a question and at least 2 answers to save a poll"

  @nightly
  Scenario: Add vote without question
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "Presenter" page
    When I click the "Votes from audience section"
    And I click the "Add vote button"
    And I enter all vote answers
    And I click the "Add Vote Confirmation button"
    Then I should see a "warning message to enter vote details" with text matching "Please enter a question and at least 2 answers to save a poll"

  @nightly
  Scenario: Add vote without answers
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "Presenter" page
    When I click the "Votes from audience section"
    And I click the "Add vote button"
    And I enter vote question
    And I click the "Add Vote Confirmation button"
    Then I should see a "warning message to enter vote details" with text matching "Please enter a question and at least 2 answers to save a poll"

  @nightly
  Scenario: Add vote with duplicate answers
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "Presenter" page
    When I click the "Votes from audience section"
    And I click the "Add vote button"
    And I enter vote question
    And I enter two duplicate vote answers
    And I click the "Add Vote Confirmation button"
    Then I should see a "warning message cannot have identical answers" with text matching "You cannot have identical answers"
    And I should see a "warning message please enter identical answers" with text matching "Please enter different answers"

  @nightly
  Scenario: Add vote with two answers
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "Presenter" page
    When I click the "Votes from audience section"
    When I click the "Add vote button"
    And I enter vote question
    And I enter "the first" vote answer
    And I enter "the second" vote answer
    And I click the "Add Vote Confirmation button"
    Then I should see a "text polls added" with text matching "Polls Added"
    And the vote appears on the presenter screen

  @nightly
  Scenario: Add vote with French characters
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "Presenter" page
    When I click the "Votes from audience section"
    When I click the "Add vote button"
    And I enter vote question as "è, à, ù, é, â, ê, î, ô, û, ë, ï, ç"
    And I edit vote answer "1" as "è, à, ù, é, â, ê, î, ô, û, ë, ï"
    And I edit vote answer "2" as "è, à, ù, é, â, ê, î, ô, û, ë"
    And I edit vote answer "3" as "è, à, ù, é, â, ê, î, ô, û"
    And I edit vote answer "4" as "è, à, ù, é, â, ê, î, ô"
    And I edit vote answer "5" as "è, à, ù, é, â, ê, î"
    And I click the "Add Vote Confirmation button"
    And I click the Vote view button
    And the vote details appear on the presenter screen

  @nightly
  Scenario: Add vote with German characters
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "Presenter" page
    When I click the "Votes from audience section"
    When I click the "Add vote button"
    And I enter vote question as "äöüß€"
    And I edit vote answer "1" as "äöüß€"
    And I edit vote answer "2" as "äöüß€_"
    And I edit vote answer "3" as "äöüß€__"
    And I edit vote answer "4" as "äöüß€___"
    And I edit vote answer "5" as "äöüß€____"
    And I click the "Add Vote Confirmation button"
    And I click the Vote view button
    And the vote details appear on the presenter screen

  @nightly
  Scenario: Add vote with Spanish characters
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "Presenter" page
    When I click the "Votes from audience section"
    When I click the "Add vote button"
    And I enter vote question as "áéíñóúü¿¡"
    And I edit vote answer "1" as "áéíñóúü¿¡"
    And I edit vote answer "2" as "áéíñóúü¿¡_"
    And I edit vote answer "3" as "áéíñóúü¿¡__"
    And I edit vote answer "4" as "áéíñóúü¿¡___"
    And I edit vote answer "5" as "áéíñóúü¿¡____"
    And I click the "Add Vote Confirmation button"
    And I click the Vote view button
    And the vote details appear on the presenter screen

  @nightly
  Scenario: Add vote with Japanese characters
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "Presenter" page
    When I click the "Votes from audience section"
    When I click the "Add vote button"
    And I enter vote question as "思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています"
    And I edit vote answer "1" as "思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています"
    And I edit vote answer "2" as "思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています_"
    And I edit vote answer "3" as "思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています__"
    And I edit vote answer "4" as "思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています___"
    And I edit vote answer "5" as "思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています____"
    And I click the "Add Vote Confirmation button"
    And I click the Vote view button
    And the vote details appear on the presenter screen

  @nightly
  Scenario: Add vote with Korean characters
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "Presenter" page
    When I click the "Votes from audience section"
    When I click the "Add vote button"
    And I enter vote question as "생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는"
    And I edit vote answer "1" as "생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는"
    And I edit vote answer "2" as "생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는_"
    And I edit vote answer "3" as "생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는__"
    And I edit vote answer "4" as "생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는___"
    And I edit vote answer "5" as "생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는____"
    And I click the "Add Vote Confirmation button"
    And I click the Vote view button
    And the vote details appear on the presenter screen

  @nightly
  Scenario: Add vote with Portugese characters
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "Presenter" page
    When I click the "Votes from audience section"
    When I click the "Add vote button"
    And I enter vote question as "à, À, á, Á, ã, Ã, ç, Ç, é, É, ê, Ê, Í, í, ó, Ó, ú, Ú, ü, Ü"
    And I edit vote answer "1" as "à, À, á, Á, ã, Ã, ç, Ç, é, É, ê, Ê, Í, í, ó, Ó, ú, Ú, ü, Ü"
    And I edit vote answer "2" as "à, À, á, Á, ã, Ã, ç, Ç, é, É, ê, Ê, Í, í, ó, Ó, ú, Ú, ü, Ü_"
    And I edit vote answer "3" as "à, À, á, Á, ã, Ã, ç, Ç, é, É, ê, Ê, Í, í, ó, Ó, ú, Ú, ü, Ü__"
    And I edit vote answer "4" as "à, À, á, Á, ã, Ã, ç, Ç, é, É, ê, Ê, Í, í, ó, Ó, ú, Ú, ü, Ü___"
    And I edit vote answer "5" as "à, À, á, Á, ã, Ã, ç, Ç, é, É, ê, Ê, Í, í, ó, Ó, ú, Ú, ü, Ü____"
    And I click the "Add Vote Confirmation button"
    And I click the Vote view button
    And the vote details appear on the presenter screen

  @nightly
  Scenario: Add vote with Russian characters
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "Presenter" page
    When I click the "Votes from audience section"
    When I click the "Add vote button"
    And I enter vote question as "ёъяшертыуиопющэасдфгчйкльжзхцвбнм"
    And I edit vote answer "1" as "ёъяшертыуиопющэасдфгчйкльжзхцвбнм"
    And I edit vote answer "2" as "ёъяшертыуиопющэасдфгчйкльжзхцвбнм_"
    And I edit vote answer "3" as "ёъяшертыуиопющэасдфгчйкльжзхцвбнм__"
    And I edit vote answer "4" as "ёъяшертыуиопющэасдфгчйкльжзхцвбнм___"
    And I edit vote answer "5" as "ёъяшертыуиопющэасдфгчйкльжзхцвбнм____"
    And I click the "Add Vote Confirmation button"
    And I click the Vote view button
    And the vote details appear on the presenter screen

  @nightly
  Scenario: Add vote with Chinese characters
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "Presenter" page
    When I click the "Votes from audience section"
    When I click the "Add vote button"
    And I enter vote question as "每天都有思想領袖正在積極分享他們的見解"
    And I edit vote answer "1" as "每天都有思想領袖正在積極分享他們的見解"
    And I edit vote answer "2" as "每天都有思想領袖正在積極分享他們的見解_"
    And I edit vote answer "3" as "每天都有思想領袖正在積極分享他們的見解__"
    And I edit vote answer "4" as "每天都有思想領袖正在積極分享他們的見解___"
    And I edit vote answer "5" as "每天都有思想領袖正在積極分享他們的見解____"
    And I click the "Add Vote Confirmation button"
    And I click the Vote view button
    And the vote details appear on the presenter screen

  @nightly
  Scenario: Add vote with special characters
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "Presenter" page
    When I click the "Votes from audience section"
    When I click the "Add vote button"
    And I enter vote question as "!@£$%^&*()_+±{}:|?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆"
    And I edit vote answer "1" as "!@£$%^&*()_+±{}:|?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆_"
    And I edit vote answer "2" as "!@£$%^&*()_+±{}:|?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆__"
    And I edit vote answer "3" as "!@£$%^&*()_+±{}:|?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆___"
    And I edit vote answer "4" as "!@£$%^&*()_+±{}:|?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆____"
    And I edit vote answer "5" as "!@£$%^&*()_+±{}:|?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆_____"
    And I click the "Add Vote Confirmation button"
    And I click the Vote view button
    And the vote details appear on the presenter screen

  @nightly
  Scenario: Add max number of votes
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains 10 votes
    When I visit the "Presenter" page
    And I click the "Votes from audience section"
    And the "Add Vote button" is not displayed on the page
    And I click the Vote view button
    And I click the "Delete Vote button"
    And I click the "Delete Vote Confirmation button"
    And temporal step
    Then I should see the "Add vote button"

  @nightly
  Scenario: Add vote with two answers and verify on the second presenter screen
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "Presenter" page
    And I open new browser window 2
    And I visit the "Presenter" page
    When I click the "Votes from audience section"
    When I click the "Add vote button"
    And I enter vote question
    And I enter "the first" vote answer
    And I enter "the second" vote answer
    And I click the "Add Vote Confirmation button"
    And I activate browser window 1
    When I click the "Votes from audience section"
    Then I should see a "text polls added" with text matching "Polls Added"
    And the vote appears on the presenter screen

   Scenario: Add Vote button
    When I click on the "Vote section button"
    Then I should see an "Add votes button"

  Scenario: Add Vote form appearance
    When I click on the "Vote section button"
    And I click on the "Add votes button"
    Then I should see a "Vote form"
    And I should see a "Vote question input box" in the "Vote form"
    And I should see a "Vote answer message" with text matching "Answers (up to 5):" in the "Vote form"
    And I should see 5 "Vote answer input box" in the "Vote form"
    And I should see a "Add vote button" in the "Vote form"
    And I should see a "Cancel vote button" in the "Vote form"

  Scenario Outline: Vote form validation
    When I click on the "Votes section button"
    And I click on the "Add votes button"
    And I fill the "Votes form" with the following values
      | field              | value      |
      | Vote Form question | <question> |
      | Vote Form answerA  | <answer_a> |
      | Vote Form answerB  | <answer_b> |
      | Vote Form answerC  | <answer_c> |
      | Vote Form answerD  | <answer_d> |
      | Vote Form answerE  | <answer_e> |
    Then I should see the following message(s)
      | field                | message                                                       |
      | Vote confirm message | Please enter a question and at least 2 answers to save a poll |
    And I should see the "Vote ok button"
    Examples:
      | question      | answer_a    | answer_b    | answer_c    | answer_d    | answer_e    |
      |               |             |             |             |             |             |
      | test_question |             |             |             |             |             |
      | test_question | test_answer |             |             |             |             |
      |               | test_answer |             |             |             |             |
      |               | test_answer | test_answer |             |             |             |
      |               | test_answer | test_answer | test_answer |             |             |
      |               | test_answer | test_answer | test_answer | test_answer |             |
      |               | test_answer | test_answer | test_answer | test_answer | test_answer |

  Scenario: Vote form submission with duplicate answers
    When I click on the "Votes section button"
    And I click on the "Add votes button"
    And I fill the "Votes form" with the following values
      | field              | value         |
      | Vote Form question | test_question |
      | Vote Form answerA  | test_answer   |
      | Vote Form answerB  | test_answer   |
    Then I should see the following message(s)
      | field                     | message                           |
      | Vote Form confirm message | You cannot have identical answers |
      | Vote Form prompt message  | Please enter different answers    |

  Scenario Outline: Vote ok after form validation fails
    And I click on the "Votes section button"
    And I click on the "Add votes button"
    And I fill the "Votes form" with the following values
      | field              | value      |
      | Vote Form question | <question> |
      | Vote Form answerA  | <answer_a> |
      | Vote Form answerB  | <answer_b> |
      | Vote Form answerC  | <answer_c> |
      | Vote Form answerD  | <answer_d> |
      | Vote Form answerE  | <answer_e> |
    And I see the following message(s)
      | field                | message                                                       |
      | Vote confirm message | Please enter a question and at least 2 answers to save a poll |
    And I see the "Vote ok button"
    When I click on the "Vote ok button"
    Then I should see a "Vote form"
    And I should see the "Vote question input box" with text matching "<question>" for that "Vote"
    Examples:
      | question      | answer_a | answer_b | answer_c | answer_d | answer_e |
      | test_question |          |          |          |          |          |

  Scenario: Successfully adding a vote
    When I click on the "Votes section button"
    And I click on the "Add votes button"
    And I fill the "Votes form" with the following values
      | field              | value         |
      | Vote Form question | test_question |
      | Vote Form answerA  | test_answer_a |
      | Vote Form answerB  | test_answer_b |
    Then I should see the "Polls Added label" in the "Votes listings"
    And I should see 1 "Vote" in the "Vote listings"
    And I should see the "Vote question title" with text matching "Q: test_question" for that "Vote"
    And I should see the "Vote view icon" for that "Vote"

  Scenario: Vote summary from listings
    When I click on the "Votes section button"
    And I click on the "Add votes button"
    And I fill the "Votes form" with the following values
      | field              | value         |
      | Vote Form question | test_question |
      | Vote Form answerA  | test_answer_a |
      | Vote Form answerB  | test_answer_b |
    And I click on the "View vote icon" for the 1st "Vote" in the "Vote listings"
    Then I should see the "Vote question title" with text matching "Q: test_question" in the "Vote details"
    And I should see the following "Vote answers" in the "Vote details"
      | Vote answerA  | Vote answerB  |
      | test_answer_a | test_answer_b |
    And I should see the "Back to votes list button"
    And I should see the "Delete vote button"
    And I should see the "Edit vote button"


  Scenario Outline: Adding a vote with special characters
    When I click on the "Votes section button"
    And I click on the "Add votes button"
    And I fill the "Votes form" with the following values
      | field              | value      |
      | Vote Form question | <question> |
      | Vote Form answerA  | <answer_a> |
      | Vote Form answerB  | <answer_b> |
      | Vote Form answerB  | <answer_c> |
      | Vote Form answerB  | <answer_d> |
      | Vote Form answerB  | <answer_e> |
    Then I should see 1 "Vote" in the "Vote listings"
    And I should see the "Vote question title" with text matching <question> for that "Vote"
    When I click the button to open vote details
    Then I should see the "Vote question answers" with text matching <answer_a>, <answer_b>, <answer_c>, <answer_d>, <answer_e> for that "Vote"
    Examples:
      | question                                                   | answer_a                                                                  | answer_b                                                     | answer_c                                                                  | answer_d                                                      | answer_e                                                                   |
      | è, à, ù, é, â, ê, î, ô, û, ë, ï, ç                         | è, à, ù, é, â, ê, î, ô, û, ë, ï                                           | è, à, ù, é, â, ê, î, ô, û, ë                                 | è, à, ù, é, â, ê, î, ô, û                                                 | è, à, ù, é, â, ê, î, ô                                        | è, à, ù, é, â, ê                                                           |
      | äöüß€                                                      | äöüß€                                                                     | äöüß€_                                                       | äöüß€__                                                                   | äöüß€___                                                      | äöüß€____                                                                  |
      | áéíñóúü                                                    | áéíñóúü                                                                   | áéíñóúü_                                                     | áéíñóúü__                                                                 | áéíñóúü___                                                    | áéíñóúü____                                                                |
      | 思想的指導者の毎日何千人も積極的                                           | 思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています                                            | 思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています_                              | 思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています__                                          | 思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています___                             | 思想的指導者の毎日何千人も積極的に彼らの洞察を共有しています_____                                        |
      | 생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는                              | 생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는                                             | 생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는_생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는_ | 생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는___                                          | 생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는___                              | 생각 지도자의 매일 수천 적극적으로 통찰력을 공유하는____                                          |
      | à, À, á, Á, ã, Ã, ç, Ç, é, É, ê, Ê, Í, í, ó, Ó, ú, Ú, ü, Ü | à, À, á, Á, ã, Ã, ç, Ç, é, É, ê, Ê, Í, í, ó, Ó, ú, Ú, ü, Ü                | à, À, á, Á, ã, Ã, ç, Ç, é, É, ê, Ê, Í, í, ó, Ó, ú, Ú, ü, Ü_  | à, À, á, Á, ã, Ã, ç, Ç, é, É, ê, Ê, Í, í, ó, Ó, ú, Ú, ü, Ü___             | à, À, á, Á, ã, Ã, ç, Ç, é, É, ê, Ê, Í, í, ó, Ó, ú, Ú, ü, Ü___ | à, À, á, Á, ã, Ã, ç, Ç, é, É, ê, Ê, Í, í, ó, Ó, ú, Ú, ü, Ü____             |
      | ёъяшертыуиопющэасдфгчйкльжзхцвбнм                          | ёъяшертыуиопющэасдфгчйкльжзхцвбнм_                                        | ёъяшертыуиопющэасдфгчйкльжзхцвбнм__                          | ёъяшертыуиопющэасдфгчйкльжзхцвбнм__                                       | ёъяшертыуиопющэасдфгчйкльжзхцвбнм___                          | ёъяшертыуиопющэасдфгчйкльжзхцвбнм_____                                     |
      | 每天都有思想領袖正在積極分享他們的見解                                        | 每天都有思想領袖正在積極分享他們的見解                                                       | 每天都有思想領袖正在積極分享他們的見解_                                         | 每天都有思想領袖正在積極分享他們的見解__                                                     | 每天都有思想領袖正在積極分享他們的見解___                                        | 每天都有思想領袖正在積極分享他們的見解____                                                    |
      | !@£$%^&*()_+±{}:                                           | ?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆ | !@£$%^&*()_+±{}:                                             | ?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆ | !@£$%^&*()_+±{}:                                              | ?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆_ |


  Scenario: multiple presenter windows are updated when votes are added
    Given five presenter screens are opened for pro webinar
    When one of the presenters adds vote
    Then the vote appears on all presenter screens