Feature: Running votes
  As a presenter of a pro webinar
  I want to run my votes/polls
  So that I can get the vote results from audience

  @nightly
  Scenario: Run vote
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    And pro webinar is live
    And the vote is open
    And audience has voted
    When I visit the "Presenter" page
    And temporal step
    Then the following vote results are shown on the presenter page
      |1|1|100%|