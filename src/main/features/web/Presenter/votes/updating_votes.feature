Feature: Updating a vote
  As a presenter of a pro webinar
  I want to manage my votes/polls
  So that I can get exactly what I want to know from my viewers

  @nightly
  Scenario: Edit vote
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    When I visit the "Presenter" page
    And I click the "Votes from audience section"
    And I click the Vote view button
    And I click the "Edit Vote button"
    And I edit vote question as "vote_question_edited"
    And I edit vote answer "1" as "vote_question_edited1"
    And I edit vote answer "2" as "vote_question_edited2"
    And I edit vote answer "3" as "vote_question_edited3"
    And I edit vote answer "4" as "vote_question_edited4"
    And I edit vote answer "5" as "vote_question_edited5"
    And I click "Edit Vote Save button"
    And I click the "Back to vote list button"
    Then the vote appears on the presenter screen

  @nightly
  Scenario: Edit vote and verify it has been updated on the second presenter screen
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    When I visit the "Presenter" page
    And I open new browser window 2
    And I visit the "Presenter" page
    And I click the "Votes from audience section"
    And I click the Vote view button
    And I click the "Edit Vote button"
    And I edit vote question as "vote_question_edited"
    And I edit vote answer "1" as "vote_question_edited1"
    And I edit vote answer "2" as "vote_question_edited2"
    And I edit vote answer "3" as "vote_question_edited3"
    And I edit vote answer "4" as "vote_question_edited4"
    And I edit vote answer "5" as "vote_question_edited5"
    And I click "Edit Vote Save button"
    And I click the "Back to vote list button"
    And I activate browser window 1
    And I click the "Votes from audience section"
    And temporal step
    Then the vote appears on the presenter screen