Feature: Navigate to votes list
  As a presenter of a pro webinar
  I want to be able to click the back to list button
  So that I can navigate back to list of votes

  @nightly
   Scenario: Navigate to votes list
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote
    When I visit the "Presenter" page
    And I click the "Votes from audience section"
    And I click the Vote view button
    And I click the "Back to vote list button"
    Then the vote appears on the presenter screen