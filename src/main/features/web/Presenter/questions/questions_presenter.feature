Feature: Questions
  As a audience of the pro webinar
  I want to be able send questions
  So that presenters can see them

  @nightly
  Scenario: As an audience I want to send question
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
      | registration level            | Minimum |
    And a pro webinar exists
    And a pro webinar starts in 5 minutes
    And no resource is needed
    And I visit the "presenter" page
    When I open new browser window 2
    And I visit the "audience" page
    And I click on the "audience-page-questions-tab" in the "audience-tabs-frame" iframe
    And I switch to audience page tab frame
    And for element "audience-page-question-textarea" I enter value "èàùéâêîôûëïçäöüß€áéíñóúü¿¡思想的指導者の毎日何千人も積極생각지도자의 매일 수천àÀáÁãÃçÇéÉêÊÍíóÓúÚüÜёъяшертыуиопющэасдфгчйкльжзхцвбнм每天都有思想領袖正在積極分享他們的見解!@£$%^&*()_+±{}:|?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆"
    And I click on the "audience-page-send-question-button"
    And I activate browser window 1
    And I close presenter screen overlay
    And I click on the "presenter-page-questions-section"
    Then I should see the "presenter-page-number-of-questions" with text matching "1"
    And I should see the "presenter-page-question-text" with text matching "èàùéâêîôûëïçäöüß€áéíñóúü¿¡思想的指導者の毎日何千人も積極생각지도자의 매일 수천àÀáÁãÃçÇéÉêÊÍíóÓúÚüÜёъяшертыуиопющэасдфгчйкльжзхцвбнм每天都有思想領袖正在積極分享他們的見解!@£$%^&*()_+±{}:|?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆"
    And the element "presenter-page-mark-as-answered-button" is present
    And the element "presenter-page-question-prioritize-button" is present