Feature: Feature image for Video on demand communication
  As a owner of the pro webinar
  I want to be able add on demand video containing feature image
  So that audience can see it

  @nightly @firefox
  Scenario: As a channel owner using a no flash browser when I visit a video upload and select to manually upload an image I will see a button to select the image to upload
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    When I click on the "on-demand-video-feature-image-manual"
    Then the element "on-demand-video-feature-image-browse-visible" appears in "5" secs

  @testlocal @firefox
  Scenario: As a channel owner using a no flash browser when I visit a video upload and select to manually upload an image I will see a button to select the image to upload
    Given I visit the "local-prepare-upload-page" page
    When I click on the "on-demand-video-feature-image-manual"
    Then the element "on-demand-video-feature-image-browse-visible" appears in "5" secs

  @nightly @firefox
  Scenario: As a channel owner using a no flash browser when I select a featured image to upload the image filename will show
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    When I click on the "on-demand-video-feature-image-manual"
    And I have selected a "feature_image.JPEG" feature image file for video upload
    Then the element "on-demand-video-selected-feature-image-displayed" is present
    And the element "on-demand-video-selected-feature-image-file-name" is present
    And I should see the "on-demand-video-selected-feature-image-file-name" with text matching "feature_image.JPEG"
    And I should see the "on-demand-video-selected-feature-image-file-name" with text matching "(23.93KB)"

  @testlocal @firefox
  Scenario: As a channel owner using a no flash browser when I select a featured image to upload the image filename will show
    Given I visit the "local-prepare-upload-page" page
    When I click on the "on-demand-video-feature-image-manual"
    When I have selected a "feature_image.JPEG" feature image file for video upload
    And the element "on-demand-video-selected-feature-image-displayed" is present
    And the element "on-demand-video-selected-feature-image-file-name" is present
    Then I should see the "on-demand-video-selected-feature-image-file-name" with text matching "feature_image.JPEG"
    And I should see the "on-demand-video-selected-feature-image-file-name" with text matching "(23.93KB)"

  @nightly @firefox
  Scenario: As a channel owner using a no flash browser who has selected an image to upload to a video upload when I click to remove the selected image it will be removed
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    When I click on the "on-demand-video-feature-image-manual"
    And I have selected a "feature_image.JPEG" feature image file for video upload
    And the element "on-demand-video-feature-image-remove" is present
    And I click button to remove feature image
    Then the element "on-demand-video-feature-image-not-selected" is present

  @testlocal @firefox
  Scenario: As a channel owner using a no flash browser who has selected an image to upload to a video upload when I click to remove the selected image it will be removed
    Given I visit the "local-prepare-upload-page" page
    When I click on the "on-demand-video-feature-image-manual"
    When I have selected a "feature_image.JPEG" feature image file for video upload
    And the element "on-demand-video-feature-image-remove" is present
    When I click button to remove feature image
    Then the element "on-demand-video-feature-image-not-selected" is present

  @nightly @firefox
  Scenario: As a channel owner using a no flash browser who has selected an image to upload to a video upload when I click browse and re select another image the new image filename will be shown
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    And I click on the "on-demand-video-feature-image-manual"
    And I have selected a "feature_image.JPEG" feature image file for video upload
    When I have selected a "test1.JPG" feature image file for video upload
    And the element "on-demand-video-selected-feature-image-displayed" is present
    And the element "on-demand-video-selected-feature-image-file-name" is present
    Then I should see the "on-demand-video-selected-feature-image-file-name" with text matching "test1.JPG"
    And I should see the "on-demand-video-selected-feature-image-file-name" with text matching "(13.84KB)"

  @nightly @firefox
  Scenario: As a channel owner who has created a video upload when I try to upload a feature image that is above 1mb I will see a warning informing me I cannot upload a file this big
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    And I click on the "on-demand-video-feature-image-manual"
    When I have selected a "Pizigani_1367_Chart_1MB.jpg" feature image file for video upload
    Then I should see the "on-demand-video-upload-failed-message" with text matching "Image file too large"