Feature: Video on demand file reselection
  As a owner of the pro webinar
  I want to be able reselect another on demand video file
  So that the new video file is selected

  @nightly @firefox
  Scenario: As a channel owner using a no flash browser who has selected a video to upload when I click to remove the selected video it will be removed
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    And I have selected a "SampleVideo_1280x720_20mb.mp4" video file
    When I click on the "on-demand-video-remove-video-file-button"
    Then the element "on-demand-video-selected-feature-image-displayed" disappears from page

  @nightly @firefox
  Scenario: As a channel owner using a no flash browser who has selected a video to upload when I click browse and re select another video the new image filename will be shown
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    And I have selected a "SampleVideo_1280x720_20mb.mp4" video file
    And I should see the "on-demand-video-selected-file" with text matching "SampleVideo_1280x720_20mb.mp4"
    When I have selected a "SampleVideo_1280x720_30mb.mp4" video file
    Then I should see the "on-demand-video-selected-file" with text matching "SampleVideo_1280x720_30mb.mp4"
    And I should see the "on-demand-video-selected-file" with text matching "(30.03MB)"