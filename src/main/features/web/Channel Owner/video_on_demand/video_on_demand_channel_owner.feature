Feature: Video on demand
  As a owner of the pro webinar
  I want to be able add on demand video
  So that audience can watch them

  @nightly @firefox
  Scenario: As a channel owner using a no flash browser when I visit a video upload with no uploaded video I will see a section allowing me to upload a video
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    And I visit the "video-on-demand-upload-prepare-and-preview" page
    Then I should see the "on-demand-video-upload-video-section" with text matching "Upload video"

  @testlocal @firefox
  Scenario: As a channel owner using a no flash browser when I visit a video upload with no uploaded video I will see a section allowing me to upload a video
    Given I visit the "local-prepare-upload-page" page
    And the element "on-demand-video-upload-video-section" appears in "60" secs
    Then I should see the "on-demand-video-upload-video-section" with text matching "Upload video"

  @testlocal @firefox
  Scenario: As a channel owner when using a no flash browser when I start to upload a video I will see progress bars showing my upload progress
    Given I visit the "local-prepare-upload-page" page
    And I have selected a "SampleVideo_1280x720_20mb.mp4" video file
    And the element "on-demand-video-auto-capture-image-selected" is present
    And for element "on-demand-video-upload-location" I select text "Europe"
    When I click on the "on-demand-video-upload-button"
    Then the element "on-demand-video-draggable-bar" is present
    And the element "on-demand-video-upload-progress-bar-visible" is present

  @nightly @firefox
  Scenario: As a channel owner who has successfully started a video upload when the uploading stage of the video is complete then I should see the progress for the transcoding stage
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    And I have started to upload a video
    Then the element "on-demand-video-transcoding-in-progress" is present
    And I should see the "on-demand-video-transcoding-status" with text matching "Processing"

  @testlocal @firefox
  Scenario: As a channel owner who has successfully started a video upload when the uploading stage of the video is complete then I should see the progress for the transcoding stage
    Given I visit the "local-prepare-upload-page" page
    And I have started to upload a video
    Then the element "on-demand-video-transcoding-in-progress" is present
    And I should see the "on-demand-video-transcoding-status" with text matching "Processing"

  @nightly @firefox
  Scenario: As a channel owner who is uploading a video when the video transcoding phase of the upload is complete then I should be told the video has been uploaded
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    And I have started to upload a video
    And the element "on-demand-video-transcoding-in-progress" is present
    When the element "on-demand-video-upload-finished" appears in "3000" secs
    Then I should see the "on-demand-video-upload-finished-message" with text matching "Uploaded! This video is now ready for review"
    And I should see the "on-demand-video-flash-required" with text matching "Flash is required to view this webcast/channel"

  @testlocal @firefox
  Scenario: As a channel owner who is uploading a video when the video transcoding phase of the upload is complete then I should be told the video has been uploaded
    Given I visit the "local-prepare-upload-page" page
    And I have started to upload a video
    And the element "on-demand-video-transcoding-in-progress" is present
    When the element "on-demand-video-upload-finished" appears in "600" secs
    Then I should see the "on-demand-video-upload-finished-message" with text matching "Uploaded! This video is now ready for review"
    And I should see the "on-demand-video-flash-required" with text matching "Flash is required to view this webcast/channel"

  @nightly @firefox
  Scenario: As a channel owner who has created a video upload when I try to upload a file that is not the accepted file type I will see a warning
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    And I have selected a "video_unsupported_extension.png" video file
    Then I should see the "on-demand-video-upload-failed-message" with text matching "Upload of this video file failed"

  @testlocal @firefox
  Scenario: As a channel owner who has created a video upload when I try to upload a file that is not the accepted file type I will see a warning
    Given I visit the "local-prepare-upload-page" page
    And I have selected a "video_unsupported_extension.png" video file
    Then I should see the "on-demand-video-upload-failed-message" with text matching "Upload of this video file failed"

  @nightly @firefox
  Scenario: As a channel owner who has created a video upload when I try to upload a file that is less than 12.8 kilobytes I will see a warning informing me I cannot upload a file this small
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    And I have selected a "image_test1.mp4" video file
    Then I should see the "on-demand-video-upload-failed-message" with text matching "Video file too small"

  @testlocal @firefox
  Scenario: As a channel owner who has created a video upload when I try to upload a file that is less than 12.8 kilobytes I will see a warning informing me I cannot upload a file this small
    Given I visit the "local-prepare-upload-page" page
    And I have selected a "image_test1.mp4" video file
    Then I should see the "on-demand-video-upload-failed-message" with text matching "Video file too small"

  @nightly @firefox
  Scenario: As a channel owner who has created a video upload when I try to upload invalid video file I will see a warning informing me the transcoding can not be done    Given I am logged in as manager
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    And I have selected a "invalid_video_file.mp4" video file
    And for element "on-demand-video-upload-location" I select text "Europe"
    When I click on the "on-demand-video-upload-button"
    Then the element "on-demand-video-invalid-file-transcode-failed-message" appears in "600" secs

  @testlocal @firefox
  Scenario: As a channel owner who has created a video upload when I try to upload invalid video file I will see a warning informing me the transcoding can not be done
    Given I visit the "local-prepare-upload-page" page
    And I have selected a "invalid_video_file.mp4" video file
    And for element "on-demand-video-upload-location" I select text "Europe"
    When I click on the "on-demand-video-upload-button"
    Then the element "on-demand-video-invalid-file-transcode-failed-message" appears in "600" secs