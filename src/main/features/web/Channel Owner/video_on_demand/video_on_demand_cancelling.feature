Feature: Video on demand upload cancelling
  As a owner of the pro webinar
  I want to be able cancel the upload of on demand video
  So that the video is not uploaded

  @nightly @firefox
  Scenario: As a channel owner when I a see a progress bar for the video transcoding I will also see a cancel button
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    When I have started to upload a video
    And the element "on-demand-video-transcoding-in-progress" is present
    Then the element "on-demand-video-transcoding-cancel-button" is present
    Then I should see the "on-demand-video-transcoding-cancel-button" with text matching "Cancel"

  @nightly @firefox
  Scenario: As a channel owner when I click to cancel the video upload when the video transcoding progress bar is showing I am asked whether I would like to continue
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    When I have started to upload a video
    And the element "on-demand-video-transcoding-in-progress" is present
    And I click on the "on-demand-video-transcoding-cancel-button"
    Then I should see the "on-demand-video-transcoding-cancel-message" with text matching "Cancel video transcoding"

  @nightly @firefox
  Scenario: As a channel owner when I click to cancel the video upload after being asked If I'm sure I want to cancel I am taken back to the video upload form
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    When I have started to upload a video
    And the element "on-demand-video-transcoding-in-progress" is present
    And I click on the "on-demand-video-transcoding-cancel-button"
    And I click on the "on-demand-video-transcoding-cancel-confirmation-button"
    Then the element "on-demand-video-selected-file-displayed" disappears from page
    And I should see the "on-demand-video-prepare-and-preview-page-title" with text matching "Prepare & preview"

  @failing
  Scenario: As a channel owner when I navigate away from the page when uploading is in progress and return back, the upload is automatically cancelled
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    And I have started to upload a video
    And the element "on-demand-video-draggable-bar" is present
    And I wait for 5 secs
    And I visit the "manage-channel-page" page
    And I wait for 5 secs
    And I visit the "video-on-demand-upload-prepare-and-preview" page
    Then the element "on-demand-video-prepare-and-preview-page" is present
    And the "on-demand-video-draggable-bar" is not present

  @nightly @firefox
  Scenario: As a channel owner when I navigate away from the page when transcoding is in progress and return back, the transcoding continues
    Given I am logged in as manager
    And I own a channel
    And I visit the "manage-channel-page" page
    And new on demand video exists
    When I have started to upload a video
    And the element "on-demand-video-transcoding-in-progress" is present
    And I wait for 10 secs
    And I visit the "manage-channel-page" page
    And I wait for 5 secs
    And I visit the "video-on-demand-upload-prepare-and-preview" page
    Then the element "on-demand-video-transcoding-in-progress" is present
