Feature: Booking pro webinar
  As a channel owner
  I want to be able fill the pro webinar booking form
  So that I can create upcoming pro webinar

  @nightly
  Scenario: Channel owner books upcoming pro webinar
     # The start date of the webinar "pro-webinar-booking-form-startdate" should be set in minutes from current time, e.g. "1440" is 1 day from now
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    When I visit the pro webinar booking page
    And I fill in the following form details in the "pro-webinar-booking-form" element
      | field                                       | value                            |
      | pro-webinar-booking-form-title-field        | Selenium pro webinar             |
      | pro-webinar-booking-form-description-field  | Selenium pro webinar description |
      | pro-webinar-booking-form-presenter          | Selenium pro webinar presenter   |
      | pro-webinar-booking-form-duration           | 30                               |
      | pro-webinar-booking-form-startdate          | 1440                             |
      | pro-webinar-booking-form-timehours          | 10                               |
      | pro-webinar-booking-form-timeminutes        | 30                               |
      | pro-webinar-booking-form-timezone           | Europe/Brussels                  |
      | pro-webinar-booking-form-tags               | Selenium test tags               |
      | pro-webinar-booking-form-campaign-reference | Selenium test campaign reference |
    When I click on the "Pro Webinar Booking Form Proceed Button" in the "Pro Webinar Booking Form" element
    Then I should see the "prepare-and-present-page-title" with text matching "Prepare and present"
    And new pro webinar is booked

  @nightly
  Scenario: reschedule pro webinar less than 5 mins before live
    Given I am logged in as manager
    And a pro webinar exists
    And a pro webinar starts in 4 minutes
    And no resource is needed
    And I visit the "edit-booking-page"
    And for element "pro-webinar-booking-form-startdate" I enter value "Aug 03 2027"
    And for element "audio-and-slides-webinar-booking-form-timeminutes" I select value "00"
    And temporal step
    And I wait for 10 secs
    And I click Proceed button on the Edit booking form
    And I accept alert message
    Then I should see the "view-booking-page-start-date" with text matching "Aug 03 2027"

  @nightly
  Scenario: the warning message is shown when webinar is being rescheduled for past date
    Given I am logged in as manager
    And a pro webinar exists
    And I visit the "edit-booking-page"
    And for element "pro-webinar-booking-form-startdate" I enter value "Aug 03 2017"
    And for element "audio-and-slides-webinar-booking-form-timeminutes" I select value "00"
    And I click Proceed button on the Edit booking form
    And I accept alert message
    Then the element "edit-booking-page-past-date-warning" is present

  @nightly
  Scenario: reschedule upcoming pro webinar
    Given I am logged in as manager
    And a pro webinar exists
    And no resource is needed
    And I visit the "edit-booking-page"
    And for element "pro-webinar-booking-form-startdate" I enter value "Aug 03 2027"
    And for element "audio-and-slides-webinar-booking-form-timeminutes" I select value "15"
    And for element "pro-webinar-booking-form-timehours" I select value "13"
    And for element "pro-webinar-booking-form-duration" I select value "30"
    And I click Proceed button on the Edit booking form
    And I accept alert message
    Then I should see the "view-booking-page-start-date" with text matching "Aug 03 2027"
    And I should see the "view-booking-page-start-date" with text matching "1:15 pm"
    And I should see the "view-booking-page-duration" with text matching "30"

  @nightly
  Scenario: Change timezone of the upcoming pro webinar
    Given I am logged in as manager
    And a pro webinar exists
    And no resource is needed
    And I visit the "edit-booking-page"
    And for element "pro-webinar-booking-form-timezone" I select value "America/Chicago"
    And for element "audio-and-slides-webinar-booking-form-timeminutes" I select value "15"
    And for element "pro-webinar-booking-form-startdate" I enter value "Aug 03 2027"
    And I click Proceed button on the Edit booking form
    And I accept alert message
    Then I should see the "view-booking-page-timezone" with text matching "United States - Chicago"

  @nightly
  Scenario: Non chanel owner can not access the edit booking page
    Given I am logged in as manager
    And a pro webinar exists
    When I delete all cookies
    And I visit the "edit-booking-page" page
    And I login new user
    Then the element "login-page-you-are-not-autorized-page" is present

  @nightly
  Scenario: The warning message is shown if the feature file is too large
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    When I visit the pro webinar booking page
    And I fill in the following form details in the "pro-webinar-booking-form" element
      | field                                       | value                            |
      | pro-webinar-booking-form-title-field        | Selenium pro webinar             |
      | pro-webinar-booking-form-description-field  | Selenium pro webinar description |
      | pro-webinar-booking-form-presenter          | Selenium pro webinar presenter   |
      | pro-webinar-booking-form-duration           | 30                               |
      | pro-webinar-booking-form-startdate          | 1440                             |
      | pro-webinar-booking-form-timehours          | 10                               |
      | pro-webinar-booking-form-timeminutes        | 30                               |
      | pro-webinar-booking-form-timezone           | Europe/Brussels                  |
      | pro-webinar-booking-form-tags               | Selenium test tags               |
      | pro-webinar-booking-form-campaign-reference | Selenium test campaign reference |
    And I select feature image that is too large
    And I click on the "Pro Webinar Booking Form Proceed Button" in the "Pro Webinar Booking Form" element
    Then I should see the "pro-webinar-booking-form-image-too-large-error" with text matching "Image size should be 640 x 360 pixels [Max size 1Mb]"

  @nightly
  Scenario: The pro webinar create option is not available if pro webinar is disabled on channel settings
    Given I am logged in
    And I own a channel
    And my channel type is premium
    And I visit the "admin-channel-summery" page
    And pro webinar is disabled on the channel settings page
    When I visit the "manage-channel-page" page
    And I click on the "manage-channel-page-add-content-button"
    And I click on the "manage-channel-page-schedule-webinar-button"
    Then the element "audio-and-slides-webinar-booking-form" is present

  @nightly
  Scenario: The pro webinar can be booked including the png feature file
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    When I visit the pro webinar booking page
    And I fill in the following form details in the "pro-webinar-booking-form" element
      | field                                       | value                            |
      | pro-webinar-booking-form-title-field        | Selenium pro webinar             |
      | pro-webinar-booking-form-description-field  | Selenium pro webinar description |
      | pro-webinar-booking-form-presenter          | Selenium pro webinar presenter   |
      | pro-webinar-booking-form-duration           | 30                               |
      | pro-webinar-booking-form-startdate          | 1440                             |
      | pro-webinar-booking-form-timehours          | 10                               |
      | pro-webinar-booking-form-timeminutes        | 30                               |
      | pro-webinar-booking-form-timezone           | Europe/Brussels                  |
      | pro-webinar-booking-form-tags               | Selenium test tags               |
      | pro-webinar-booking-form-campaign-reference | Selenium test campaign reference |
    And I select png feature image
    And I click on the "Pro Webinar Booking Form Proceed Button" in the "Pro Webinar Booking Form" element
    Then I should see the "prepare-and-present-page-title" with text matching "Prepare and present"
    And new pro webinar is booked
    And I visit the "view-booking-page" page
    Then the feature image appears on the view booking page

  @nightly
  Scenario: The categorization tag can be assigned to new pro webinar
    Given I am logged in as manager
    And a channel with the following categorization tag exists
      | selenium_test_group/seleniumtag1 |
    When I visit the pro webinar booking page
    And I fill in the following form details in the "pro-webinar-booking-form" element
      | field                                       | value                            |
      | pro-webinar-booking-form-title-field        | Selenium pro webinar             |
      | pro-webinar-booking-form-description-field  | Selenium pro webinar description |
      | pro-webinar-booking-form-presenter          | Selenium pro webinar presenter   |
      | pro-webinar-booking-form-duration           | 30                               |
      | pro-webinar-booking-form-startdate          | 1440                             |
      | pro-webinar-booking-form-timehours          | 10                               |
      | pro-webinar-booking-form-timeminutes        | 30                               |
      | pro-webinar-booking-form-timezone           | Europe/Brussels                  |
      | pro-webinar-booking-form-tags               | Selenium test tags               |
      | pro-webinar-booking-form-campaign-reference | Selenium test campaign reference |
    And I click on the "pro-webinar-booking-categorization-tag"
    And I click on the "Pro Webinar Booking Form Proceed Button" in the "Pro Webinar Booking Form" element
    Then I should see the "prepare-and-present-page-title" with text matching "Prepare and present"
    And new pro webinar is booked
    And I visit the "view-booking-page" page
    And I should see the "view-booking-page-categorization-tag" with text matching "selenium_test_group/seleniumtag1"