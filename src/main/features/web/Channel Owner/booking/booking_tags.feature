Feature: Adding tags on pro webinar booking form
  As a channel owner
  I want to be able add tags on the pro webinar booking form
  So that they are included in the booking

  @nightly
  Scenario: Remove tag on the booking form
     # The start date of the webinar "pro-webinar-booking-form-startdate" should be set in minutes from current time, e.g. "1440" is 1 day from now
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    And I visit the pro webinar booking page
    And I fill in the following form details in the "pro-webinar-booking-form" element
      | field                                       | value                            |
      | pro-webinar-booking-form-tags               | Selenium test tags               |
    And I should see the "pro-webinar-booking-form-list-of-added-tags-first-tag" with text matching "test tags"
    When I click on the "pro-webinar-booking-form-list-of-added-tags-first-tag-remove-button"
    Then the element "pro-webinar-booking-form-list-of-added-tags-first-tag" disappears from page

  @nightly
  Scenario: Verify the auto suggestion for the tag
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    When I visit the pro webinar booking page
    And for element "pro-webinar-booking-form-tags" I enter value "BrightTA"
    And I should see the "pro-webinar-booking-form-list-auto-suggestion" with text matching "BrightTALK"
    When I click on the "pro-webinar-booking-form-list-auto-suggestion"
    And I should see the "pro-webinar-booking-form-list-of-added-tags-first-tag" with text matching "BrightTALK"

  @nightly
  Scenario: Enter tag containing max length of characters into the pro webinar booking form
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    When I visit the pro webinar booking page
    And for element "pro-webinar-booking-form-tags" I enter value "asdfasdfasddfgdsfgsdfgsdSeleiumtest"
    Then I should see the "pro-webinar-booking-form-tags-max-length" with text matching "Tag max length reached - Click Add tag button."

  @nightly
  Scenario: Enter duplicate tag into the pro webinar booking form
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    When I visit the pro webinar booking page
    And I fill in the following form details in the "pro-webinar-booking-form" element
      | field                                       | value                            |
      | pro-webinar-booking-form-tags               | 22abc123456                      |
    And I fill in the following form details in the "pro-webinar-booking-form" element
      | field                                       | value                            |
      | pro-webinar-booking-form-tags               | 22abc123456                      |
    And temporal step
    Then I should see the "pro-webinar-booking-form-tags-max-length" with text matching "Tag already added."

  @nightly
  Scenario: Remove tag on the edit booking form
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    And a pro webinar exists
    And no resource is needed
    When I visit the "edit-booking-page" page
    And I click on the "pro-webinar-booking-form-list-of-added-tags-first-tag-remove-button"
    Then the element "pro-webinar-booking-form-list-of-added-tags-first-tag" disappears from page

  @nightly
  Scenario: Verify the auto suggestion for the tag on the pro webinar edit booking form
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    And a pro webinar exists
    And no resource is needed
    When I visit the "edit-booking-page" page
    And for element "pro-webinar-booking-form-tags" I enter value "Bright"
    And I should see the "pro-webinar-booking-form-list-auto-suggestion" with text matching "BrightTALK"
    When I click on the "pro-webinar-booking-form-list-auto-suggestion"
    And temporal step
    And I should see the "pro-webinar-booking-form-list-of-added-tags-first-tag" with text matching "BrightTALK"

  @nightly
  Scenario: Enter tag containing max length of characters into the pro webinar edit booking form
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    And a pro webinar exists
    And no resource is needed
    When I visit the "edit-booking-page" page
    And for element "pro-webinar-booking-form-tags" I enter value "asdfasdfasddfgdsfgsdfgsdSeleiumtest"
    Then I should see the "pro-webinar-booking-form-tags-max-length" with text matching "Tag max length reached - Click Add tag button."

  @nightly
  Scenario: Enter duplicate tag into the pro webinar edit booking form
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    And a pro webinar exists
    And no resource is needed
    When I visit the "edit-booking-page" page
    And I fill in the following form details in the "pro-webinar-booking-form" element
      | field                                       | value                            |
      | pro-webinar-booking-form-tags               | 22abc123456                      |
    And I fill in the following form details in the "pro-webinar-booking-form" element
      | field                                       | value                            |
      | pro-webinar-booking-form-tags               | 22abc123456                      |
    Then I should see the "pro-webinar-booking-form-tags-max-length" with text matching "Tag already added."
