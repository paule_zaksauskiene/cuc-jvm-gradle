Feature: Booking a serviced pro webinar
  As a channel owner
  I want to be able fill the pro webinar booking form
  So that I can see the booking details on my pages

  @nightly
  Scenario: Manager books serviced pro webinar
    Given I am logged in
    And I am logged in as manager
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    When I visit the pro webinar booking page
    And I fill in the following form details in the "pro-webinar-booking-form" element
      | field                                       | value                            |
      | pro-webinar-booking-form-title-field        | Selenium pro webinar             |
      | pro-webinar-booking-form-description-field  | Selenium pro webinar description |
      | pro-webinar-booking-form-presenter          | Selenium pro webinar presenter   |
      | pro-webinar-booking-form-duration           | 30                               |
      | pro-webinar-booking-form-startdate          | 1440                             |
      | pro-webinar-booking-form-timehours          | 10                               |
      | pro-webinar-booking-form-timeminutes        | 30                               |
      | pro-webinar-booking-form-timezone           | Europe/Brussels                  |
      | pro-webinar-booking-form-tags               | Selenium test tags               |
      | pro-webinar-booking-form-campaign-reference | Selenium test campaign reference |
    And I click on the "pro-webinar-booking-from-serviced"
    And I click Proceed button on the Edit booking form
    And I should see the "prepare-and-present-page-title" with text matching "Prepare and present"
    And new pro webinar is booked
    And I visit the "view-booking-page" page
    Then I should see the "view-booking-page-serviced" with text matching "True"

  @nightly
  Scenario: Channel owner can not book serviced pro webinar
    Given I am logged in as channel owner
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    When I visit the pro webinar booking page
    And the "pro-webinar-booking-from-serviced" is not present

  @nightly
  Scenario: Channel owner can not edit serviced webinar
    Given I am logged in as channel owner
    And I own a channel
    And a serviced webcast exists
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    When I visit the "view-booking-page" page
    And the "view-booking-page-serviced" is not present

  @nightly
  Scenario: Manager can edit serviced webinar
    Given I am logged in as manager
    And I own a channel
    And a serviced webcast exists
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    When I visit the "edit-booking-page" page
    And the element "edit-booking-page-edit-serviced-selected" is present
    And I click on the "edit-booking-page-edit-serviced"
    And I fill in the following form details in the "pro-webinar-booking-form" element
      | field                                       | value                            |
      | pro-webinar-booking-form-startdate          | 1440                             |
      | pro-webinar-booking-form-timehours          | 10                               |
      | pro-webinar-booking-form-timeminutes        | 30                               |
    And I click on the "edit-booking-page-edit-next"
    Then the element "view-booking-page-edit-serviced-false" is present