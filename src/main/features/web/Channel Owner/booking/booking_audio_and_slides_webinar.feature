Feature: Booking pro webinar
  As a channel owner
  I want to be able fill the pro webinar booking form
  So that I can create upcoming pro webinar

  @nightly
  Scenario: Channel owner books upcoming audio and slides webinar
     # The start date of the webinar "pro-webinar-booking-form-startdate" should be set in minutes from current time, e.g. "1440" is 1 day from now
    Given I am logged in
    And I own a channel
    When I visit the "audio-and-slides-booking" page
    And I fill in the following form details in the "audio-and-slides-webinar-booking-form" element
      | field                                                    | value                            |
      | audio-and-slides-webinar-booking-form-title-field        | Selenium pro webinar             |
      | audio-and-slides-webinar-booking-form-description-field  | Selenium pro webinar description |
      | audio-and-slides-webinar-booking-form-presenter          | Selenium pro webinar presenter   |
      | audio-and-slides-webinar-booking-form-duration           | 30                               |
      | audio-and-slides-webinar-booking-form-startdate          | 1440                             |
      | audio-and-slides-webinar-booking-form-timehours          | 10                               |
      | audio-and-slides-webinar-booking-form-timeminutes        | 30                               |
      | audio-and-slides-webinar-booking-form-timezone           | Europe/Brussels                  |
      | audio-and-slides-webinar-booking-form-tags               | Selenium test tags               |
      | audio-and-slides-webinar-booking-form-campaign-reference | Selenium test campaign reference |
    When I click Proceed button on the Edit booking form
    Then the "audio-and-slides-webinar-prepare-and-present-page" opens