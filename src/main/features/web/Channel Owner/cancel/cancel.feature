Feature: Booking pro webinar
  As a channel owner
  I want to be able fill the pro webinar booking form
  So that I can create upcoming pro webinar

  @nightly
  Scenario: Cancelled pro webinar can not be edited by channel owner
    Given I am logged in as manager
    And a pro webinar exists
    And no resource is needed
    And I visit the "view-booking-page"
    And I cancel booking schedule
    And I visit the "view-booking-page"
    And the element "edit-booking-page-edit-settings-button" disappears from page