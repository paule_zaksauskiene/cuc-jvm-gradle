Feature: Editing campaign displayed name
  As a channel owner
  I want to be able to edit my campaign display name  

@test
  Scenario: Update the campaign name as channel owner
    Given I am logged in as channel owner
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Campaigns         		      | enabled |
	And a pro webinar exists
	And a Complete campaign exists
	When I visit the "campaign listing" page
	And I edit the campaign name as "Fan campaign name change"
	Then I should see the "campaign-listing-page-campaign-name" with text matching "Fan campaign name change"

@test
  Scenario: Try to edit campaign name as empty
    Given I am logged in as channel owner
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Campaigns         		      | enabled |
	And a pro webinar exists
	And I have a campaign with following details
      | key                                       	| value                            |
      | displayName        							| Fan original campaign            |
      | status                               		| Active  						   |
      | startDate                           	    | 2017-06-16					   | 
      | target									    | 1000 							   |
      | leadsFulfilled         					    | 200   						   |
      | CPL           								| 59                               |
      | currency           							| GBP                              |	  
      | earlyDelivery         					    | true                             |
      | companySizeFilter       				    | 2500-5000                        |
      | jobLevelFilter        						| Director+                        |
      | countries           						| United States,Canada,China,Japan |
      | exclusions              				    | Google,Amazon               	   |
      | community 									| Information Technology 		   |
	When I visit the "campaign listing" page
	And I edit the campaign name as ""
	Then I should see the "campaign-listing-page-campaign-name" with text matching "Fan original campaign"  

@test
  Scenario: Try to cancel in the middle of editing
    Given I am logged in as channel owner
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Campaigns         		      | enabled |
	And a pro webinar exists
	And I have a campaign with following details
      | key                                       	| value                            |
      | displayName        							| Fan original campaign            |
      | status                               		| Active  						   |
      | startDate                           	    | 2017-06-16					   | 
      | target									    | 1000 							   |
      | leadsFulfilled         					    | 200   						   |
      | CPL           								| 59                               |
      | currency           							| GBP                              |	  
      | earlyDelivery         					    | true                             |
      | companySizeFilter       				    | 2500-5000                        |
      | jobLevelFilter        						| Director+                        |
      | countries           						| United States,Canada,China,Japan |
      | exclusions              				    | Google,Amazon               	   |
      | community 									| Information Technology 		   |
	When I visit the "campaign listing" page
	And I cancel after editing the campaign name as "Fan campaign name change"
	Then I should see the "campaign-listing-page-campaign-name" with text matching "Fan original campaign"