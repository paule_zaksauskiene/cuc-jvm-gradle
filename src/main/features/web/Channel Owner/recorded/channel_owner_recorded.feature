Feature: Recorded
  As a presenter of a pro webinar
  I want to be able to run webinar
  So that the channel owner can access the recorded pro webinar settings

  @nightly
  Scenario: The webinar remains private after transcoding
    Given I am logged in as manager
    And a pro webinar exists
    And a pro webinar is private
    When pro webinar is live
    And pro webinar is recorded
    And I visit the "View booking page"
    Then I should see the "private visibility"

  @nightly
  Scenario: The categorization tag appears after transcoding
    Given I am logged in as manager
    And a channel with the following categorization tag exists
      | Promotion/Featured |
    And a webcast with categorization tags exists
      | Promotion/Featured |
    And pro webinar is live
    When pro webinar is recorded
    And I visit the "View booking page"
    Then I should see the "categorization tag" with text matching "Promotion/Featured"

  @nightly
  Scenario: The campaign reference remains after transcoding
    Given I am logged in as manager
    And a pro webinar exists
    And a pro webinar contains campaign reference "Selenium test campaign reference"
    And pro webinar is live
    When pro webinar is recorded
    And I visit the "View booking page"
    Then I should see the "campaign reference" with text matching "Selenium test campaign reference"

  @nightly
  Scenario: The feature image is set for recorded webinar
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar is live
    And pro webinar is recorded
    When I visit the "View booking page"
    Then I see the feature image

  @nightly
  Scenario: The viewer access remains after transcoding
    Given I am logged in as manager
    And a pro webinar exists
    And a channel has Anonymous realm
    And a pro webinar viewer access is "No registration"
    And pro webinar is live
    When pro webinar is recorded
    And I visit the "View booking page"
    Then I should see the "viewer access" with text matching "No registration"

  @failingduetoIN-8313
  Scenario: Webinar with BrightTALK screenshare is recorded
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar is live
    When I visit the "pro-webinar-presenter-brighttalk-screenshare-page" page
    And I add BrightTALK screenshare
    And I end pro webinar
    Then pro webinar is recorded