Feature: Syndicated content
  As a channel owner
  I want to be able to share content between channels
  So that I can leverage viewings and/or content count

#  TODO create core channel owner syndication scenarios here.

  @nightly
  Scenario: Syndicate content outside of the content plan
    Given I am logged in
    And I own a channel
    And the portal publishing for channel configuration is set as included
    And the following channel features are set
      | key                          | value    |
      | content plan for syndication | disabled |
      | self syndication             | enabled  |
    And a webcast exists
    And I syndicate that webcast into another channel I own
    And the syndication is approved
    When I visit the Manage Webcast Syndication page
    Then I should see the "Manage Syndication Table"
    And I should see the following rows in "Manage Syndication Table"
      | manage syndication channel column | manage syndication pre-reg column | manage syndication views column | manage syndication viewed for column | manage syndication syndication column |
      | {consumer_channel.title}          | 0                                 | 0                               | < 2 min                              | Active                                |
    And I should see 1 "Manage Syndication Manage Button"