Feature: Feature image on the view or edit booking page
  As a channel owner
  I want to be able see the feature image on the booking pages
  So that is accessible for the audience

  @nightly
  Scenario: Upload powerpoint for upcoming webinar
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    And a pro webinar starts in 28 minutes
    And I visit the "presenter" page
    And I select PPT file "SeleniumTest.ppt" to begin upload
    And temporal step
    And PPT slides are uploaded
    And I visit the "view-booking-page" page
    And first feature image is uploaded
    And I visit the "presenter" page
    When I click a button to remove slides
    And I click on the "presenter-screen-remove-slides-confirmation-button"
    And I select PPT file "spinningsnowflakes.pptx" to begin upload
    And PPT slides are uploaded
    And I visit the "view-booking-page" page
    And feature image is uploaded
    Then feature image is replaced

  @nightly
  Scenario: The warning message is shown if the feature file is too large on the Edit booking form
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    And a pro webinar exists
    When I visit the "edit-booking-page" page
    And for element "audio-and-slides-webinar-booking-form-timeminutes" I select value "15"
    And I select feature image that is too large
    And I click Proceed button on the Edit booking form
    Then I should see the "pro-webinar-booking-form-image-too-large-error" with text matching "Image size should be 640 x 360 pixels [Max size 1Mb]"

  @nightly
  Scenario: The new feature image appears on the view booking page
    Given I am logged in
    And I own a channel
    And temporal step
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    And a pro webinar exists
    When I visit the "edit-booking-page" page
    And for element "audio-and-slides-webinar-booking-form-timeminutes" I select value "15"
    And I select jpeg feature image
    And I click Proceed button on the Edit booking form
    Then the feature image appears on the view booking page

  @nightly
  Scenario: The new feature image appears on the view booking page
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | Pro Webinars                  | enabled |
      | Max Communications Per Period | 5       |
    And a pro webinar exists
    When I visit the "edit-booking-page" page
    And for element "audio-and-slides-webinar-booking-form-timeminutes" I select value "15"
    And I select png feature image
    And I click Proceed button on the Edit booking form
    Then the feature image appears on the view booking page