Feature: Reports
  As an audience of the pro webinar
  I want to be able to generate activity on the webinar
  So that the channel owner can access my audience activity data

  @nightly
  Scenario: Audience viewing information appears on the channel reports
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
      | registration level            | Minimum |
    And a pro webinar exists
    And a pro webinar starts in 5 minutes
    And no resource is needed
    And I visit the "presenter" page
    When I open new browser window 2
    And I visit the "audience" page
    And I wait for 120 secs
    And I activate browser window 1
    Then I visit the "webcast-reports" page
    And viewers total number 1 and 1 live and 0 recorded is generated on the viewings board
    And I visit the "webcast-viewings-report" page
    And the number of viewers on report is 1
    And viewer details on viewing report are shown
    And CSV report link is present

  @nightly
  Scenario: Audience viewing information appears on the channel owner webasts listing page
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
      | registration level            | Minimum |
    And a pro webinar exists
    And a pro webinar starts in 5 minutes
    And no resource is needed
    When I visit the "audience" page
    And I wait for 120 secs
    And I visit the "manage-channel-page" page
    Then webinar details are correct on webcast listing page
    And webinar rating is "0" on the webcast listing page

  @nightly
  Scenario: Anonymous viewer is shown on the viewings report
    Given I am logged in as manager
    And a pro webinar exists
    And a channel has Anonymous realm
    And a pro webinar viewer access is "No registration"
    And a pro webinar starts in 5 minutes
    And I wait for 300 secs
    When I delete all cookies
    And I visit the "audience" page
    And I should see the flash player
    And I delete all cookies
    And I wait for 60 secs
    And I am logged in as manager
    And I wait for 10 secs
    And I visit the "webcast-viewings-report" page
    Then anonymous viewer details are shown on viewings report

  @nightly
  Scenario: User containing specific and international characters appears on viewings report
    Given a webinar exists
    And a pro webinar starts in 6 minutes
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Moderate |
    When I visit webinar audience play page
    And I wait for 5 secs
    And I fill in the following form details
      | field                     | value                                                                           |
      | Reg-form first name field | èàùéâêîôûëïçäöüß€áéíñóúü¿¡思想的指導者の毎日何千人も積極생각                         |
      | Reg-form last name field  | 매일수천àÀáÁãÃçÇéÉêÊÍíóÓúÚüÜёъяшертыуиопющэасдфгчйкл                              |
      | Reg-form email field      | random                                                                          |
      | Reg-form job title field  | ьжзхцвбнм每天都有思想領袖正在積極分享他們的見解!@£$%^&*()                             |
      | reg-form company field    | _+±{}?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆  |
      | Reg-form password field   | password                                                                        |
    And I click on the "Reg-form proceed button"
    And I wait for 90 secs
    And I delete all cookies
    And I am logged in as BrightTALK manager
    And I visit the "webcast-viewings-report" page
    And viewer first name èàùéâêîôûëïçäöüß€áéíñóúü¿¡思想的指導者の毎日何千人も積極생각, last name 매일수천àÀáÁãÃçÇéÉêÊÍíóÓúÚüÜёъяшертыуиопющэасдфгчйкл, job title ьжзхцвбнм每天都有思想領袖正在積極分享他們的見解!@£$%^&*(), company _+±{}?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆ appears on viewings report