Feature: Reports
  As an audience of the pro webinar
  I want to be able to generate activity on the webinar
  So that the channel owner can access my audience activity data

  @nightly
  Scenario: Reports are generated for vote containing international and specific characters
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar contains vote with the following details
      |èàùéâêîôûëïçäöüß€áéíñóúü¿¡思想的指導|者の毎日何千人も積極생각지도자의 매일|수천àÀáÁãÃçÇéÉêÊÍíóÓúÚüÜёъяшер|ьжзхцвбнм每天都有思想領袖正在積極分享他們的見解|!@£$%^&*()_+±{}:|?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆|
    And pro webinar is live
    And the vote is open
    And audience has voted
    When I visit the "Webcast Reports" page
    Then I see the vote on the vote board with the following details
      |èàùéâêîôûëïçäöüß€áéíñóúü¿¡思想的指導|1|
    And I see the vote report for the vote "èàùéâêîôûëïçäöüß€áéíñóúü¿¡思想的指導"
      |者の毎日何千人も積極생각지도자의 매일|1|100|
      |수천àÀáÁãÃçÇéÉêÊÍíóÓúÚüÜёъяшер|0|0|
      |ьжзхцвбнм每天都有思想領袖正在積極分享他們的見解|0|0|
      |!@£$%^&*()_+±{}:|0|0|
      |?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆|0|0|

  @nightly
  Scenario: Attachment download report is generated for channel owner
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
      | registration level            | Minimum |
    And a pro webinar exists
    And pro webinar contains "link to a file" attachment
    And pro webinar contains "link to a web page" attachment
    And pro webinar is live
    When I visit the "audience" page
    And I wait for 30 secs
    And I generate attachments downloads
    And I visit the "webcast-reports" page
    And 2 attachments views is generated
    And I visit the "webcast-attachments-report" page
    And the attachment viewers details are shown
    And I visit the "manage-channel-page"
    And webinar details are shown on the webcasts listing page

  @nightly
  Scenario: Audience ratings appear on the channel reports
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
      | registration level            | Minimum |
    And a pro webinar exists
    And a pro webinar starts in 5 minutes
    And no resource is needed
    And I visit the "presenter" page
    When I open new browser window 2
    And I visit the "audience" page
    And I click on the "audience-page-ratings-tab" in the "audience-tabs-frame" iframe
    And I select 3 rating stars
    And I enter rating text "Selenium Feedback Text"
    And click "Send Rating" button
    And I activate browser window 1
    Then I visit the "webcast-reports" page
    And feedback number 1 is generated

  @nightly
  Scenario: Audience questions appear on the channel reports
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
      | registration level            | Minimum |
    And a pro webinar exists
    And a pro webinar starts in 5 minutes
    And no resource is needed
    And I visit the "presenter" page
    When I open new browser window 2
    And I visit the "audience" page
    And I click on the "audience-page-questions-tab" in the "audience-tabs-frame" iframe
    And I switch to audience page tab frame
    And for element "audience-page-question-textarea" I enter value "my Question"
    And I click on the "audience-page-send-question-button"
    And I activate browser window 1
    Then I visit the "webcast-reports" page
    And questions number 1 is generated on the questions board
    And I visit the "webcast-questions-report" page
    And question "my Question" appears on the report

  Scenario: As an audience I to generate audience activity that includes special characters
    Given the channel is created
    When the pro webinar is scheduled to start in less than 5 minutes
    And audience sends rating including special and international characters
    And audience sends question including special and international characters
    And audience downloads attachment
    Then the channel owner viewings report is generated
    And the channel owner feedback report is generated
    And the channel owner questions report is generated
    And the channel owner attachment downloads report is generated

  Scenario: As channel owner I want to see poll results
    Given the pro webinar is live
    And vote contains international and specific characters
    And vote is open
    When the audience polls
    And viewer details contain international and specific characters
    Then the vote appears on the channel owner votes report showing details correctly

  Scenario: as an unregistered audience I want to view the pro webinar so the channel owner can see the details on viewing reports
    Given the channel has anonymous realm assigned
    When the anonymous viewer views the channel content
    Then the viewings report shows the anonymous viewing data.

  Scenario: as a viewer I want to register to pro webinar so that channel owner can see my details on the pre-registration report
    Given the channel contains upcoming pro webinar
    When audience viewer contains international / specific characters in its profile data
    And registers to this pro webinar
    Then the channel owner pre-registration report shows the details correctly

  Scenario: as a viewer with international profile data I want to view the pro webinar so that channel owner can see my details on the pre-registration report
    Given the channel contains upcoming pro webinar
    When audience viewer contains international / specific characters in its profile data
    And viewer views this pro webinar
    Then the channel owner viewing report shows the details correctly



