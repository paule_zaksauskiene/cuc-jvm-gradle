Feature: Reports
  As an audience of the pro webinar
  I want to be able to generate activity on the webinar
  So that the channel owner can access my audience activity data

  @nightly
  Scenario: User containing specific and international characters appears on pre-registration report
    Given a webinar exists
    And no resource is needed
    And the following channel features are set
      | key                | value    |
      | registration level | Moderate |
    When I visit webinar audience play page
    And I wait for 5 secs
    And I fill in the following form details
      | field                     | value                                                                           |
      | Reg-form first name field | èàùéâêîôûëïçäöüß€áéíñóúü¿¡思想的指導者の毎日何千人も積極생각                         |
      | Reg-form last name field  | 매일수천àÀáÁãÃçÇéÉêÊÍíóÓúÚüÜёъяшертыуиопющэасдфгчйкл                              |
      | Reg-form email field      | random                                                                          |
      | Reg-form job title field  | ьжзхцвбнм每天都有思想領袖正在積極分享他們的見解!@£$%^&*()                             |
      | reg-form company field    | _+±{}?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆  |
      | Reg-form password field   | password                                                                        |
    And I click on the "Reg-form proceed button"
    And I should see the "audience-page-your-place-confirmed" with text matching "Your place is confirmed"
    And I delete all cookies
    And I am logged in as BrightTALK manager
    And I visit the "webcast-reports" page
    Then number of pre-registrations is 1
    And I visit the "webcast-preregistrations-report" page
    And I should see the "webcast-reports-registrations-report-first-name" with text matching "èàùéâêîôûëïçäöüß€áéíñóúü¿¡思想的指導者の毎日何千人も積極생각"
    And I should see the "webcast-reports-registrations-report-second-name" with text matching "매일수천àÀáÁãÃçÇéÉêÊÍíóÓúÚüÜёъяшертыуиопющэасдфгчйкл"
    And I should see the "webcast-reports-registrations-reports-job-title" with text matching "ьжзхцвбнм每天都有思想領袖正在積極分享他們的見解!@£$%^&*()"
    And I should see the "webcast-reports-registrations-reports-company" with text matching "_+±{}?~¡¡€#¢∞§¶•ªº–≠“‘…æ«≤≥÷,.[]-=§`å∑´®†¥¨^øπ¬˚∆˙©ƒ∂ßåΩ≈ç√∫~µœ§`å∑´®†¥¨^øπ¬˚∆"