Feature: Booking pro webinar
  As a channel owner
  I want to be able fill the pro webinar booking form
  So that I can create upcoming pro webinar

  @testtbd
  Scenario: Channel owner can not change the custom url if its been disabled on channel settings
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value   |
      | enableCommunicationCustomURL  | false   |
    And a pro webinar exists
    When I visit the "edit-booking-page" page
    Then pro webinar custom url can not be changed on the edit booking page

  @nightly
  Scenario: Channel owner can change the custom url if its been enabled on channel settings
    Given I am logged in
    And I own a channel
    And temporal step
    And the following channel features are set
      | key                           | value     |
      | enableCommunicationCustomURL  | enabled   |
    And a pro webinar exists
    When I visit the "edit-booking-page" page
    And for element "audio-and-slides-webinar-booking-form-timeminutes" I select value "15"
    And I click on the "edit-booking-page-change-webcast-url-link"
    And for element "edit-booking-page-edit-webcast-url" I enter value "http://www.telegraph.co.uk"
    And I click on the "edit-booking-page-edit-next"
    Then the "edit-booking-page-edit-webcast-url" is not present
    And I should see the "view-booking-page-webast-url" with text matching "http://www.telegraph.co.uk"

  @testtbd
  Scenario: Channel owner can revert the custom url
    Given I am logged in
    And I own a channel
    And the following channel features are set
      | key                           | value     |
      | enableCommunicationCustomURL  | enabled   |
    And a pro webinar exists
    When I visit the "edit-booking-page" page
    And for element "audio-and-slides-webinar-booking-form-timeminutes" I select value "15"
    And I click on the "edit-booking-page-change-webcast-url-link"
    And for element "edit-booking-page-edit-webcast-url" I enter value "http://www.telegraph.co.uk"
    And I click on the "edit-booking-page-edit-next"
    Then the "edit-booking-page-edit-webcast-url" is not present
    And I should see the "view-booking-page-webast-url" with text matching "http://www.telegraph.co.uk"