Feature: Pro webinar reupload
  As a channel owner of a pro webinar
  I want to be able to open the preview and prepare page
  So that I can reupload new video

  @nightly @firefox
  Scenario: As a channel owner when I navigate away from the page when transcoding is in progress and return back, the transcoding continues
    Given I am logged in as manager
    And I visit the "pro-webinar-prepare-and-present" page for pro webinar 1510274949 in channel 2000248877
    And I start reuploading "SampleVideo_1280x720_20mb.mp4" video file
    And the element "on-demand-video-transcoding-in-progress" is present
    And I visit the "brighttalk-home" page
    And I wait for 5 secs
    And I visit the "pro-webinar-prepare-and-present" page for pro webinar 1510274949 in channel 2000248877
    Then the element "on-demand-video-transcoding-in-progress" is present