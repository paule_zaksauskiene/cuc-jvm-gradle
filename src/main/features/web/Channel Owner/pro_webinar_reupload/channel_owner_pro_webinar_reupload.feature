Feature: Pro webinar reupload
  As a channel owner of a pro webinar
  I want to be able to open the preview and prepare page
  So that I can reupload new video

  @nightly @firefox
  Scenario: As a channel owner using a no flash browser when I visit the prepare and present section of a recorded pro webinar I will see a button allowing me to Re-upload a video
    Given I am logged in as manager
    When I visit the "pro-webinar-prepare-and-present" page for pro webinar 1510272071 in channel 2000248877
    Then I should see the "pro-webinar-prepare-and-present-reupload-button" with text matching "Re-upload"

  @nightly @firefox
  Scenario: As a channel owner when using a no flash browser I click to re-upload a video for a pro webinar I am taken to a page allowing me to upload a video
    Given I am logged in as manager
    When I visit the "pro-webinar-prepare-and-present" page for pro webinar 1510272071 in channel 2000248877
    And I click on the "pro-webinar-prepare-and-present-reupload-button"
    Then I should see the "upload-video-page-title" with text matching "Upload video"

  @nightly @firefox
  Scenario: As a channel owner when using a no flash browser when I visit the page for re-uploading a video for a pro webinar I will see a button titled browse
    Given I am logged in as manager
    When I visit the "pro-webinar-prepare-and-present" page for pro webinar 1510272071 in channel 2000248877
    And I click on the "pro-webinar-prepare-and-present-reupload-button"
    Then I should see the "upload-video-page-browse-button" with text matching "Browse"

  @nightly @firefox
  Scenario: As a channel owner when using a no flash browser on the page for re-uploading a video for a pro webinar when I click to browse I can select an acceptable file to upload
    Given I am logged in as manager
    When I visit the "pro-webinar-prepare-and-present" page for pro webinar 1510272071 in channel 2000248877
    And I click on the "pro-webinar-prepare-and-present-reupload-button"
    And I have selected a "SampleVideo_1280x720_5mb.mp4" video file
    And I should see the "upload-video-page-selected-file" with text matching "SampleVideo_1280x720_5mb.mp4"

  @nightly @firefox
  Scenario: As a channel owner using a no flash browser when then video upload stage for a video being uploaded to a Pro Webinar is finished I will then progress to the transcoding phase
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar is live
    When pro webinar is recorded
    When I visit the "pro-webinar-prepare-and-present" page
    And I start reuploading "SampleVideo_1280x720_5mb.mp4" video file
    Then the element "on-demand-video-transcoding-in-progress" is present
    And I should see the "on-demand-video-transcoding-status" with text matching "Processing"

  @nightly @firefox
  Scenario: As a channel owner using a no flash browser when the transcoding stage for a video being uploaded to a Pro Webinar is finished then I should be told the video has been uploaded
    Given I am logged in as manager
    And a pro webinar exists
    And pro webinar is live
    And pro webinar is recorded
    When I visit the "pro-webinar-prepare-and-present" page
    And I start reuploading "SampleVideo_1280x720_20mb.mp4" video file
    Then the element "prepare-and-present-page-title-text" appears in "600" secs
    And I should see the "prepare-and-present-webinar-available" with text matching "This Pro Webinar is now available to be viewed"