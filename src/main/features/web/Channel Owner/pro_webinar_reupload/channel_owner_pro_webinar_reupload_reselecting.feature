Feature: Pro webinar reupload
  As a channel owner of a recorded pro webinar
  I want to be able reselect another videofile
  So that I can reupload new video

  @nightly @firefox
  Scenario: As a channel owner when using a no flash browser on the page for re-uploading a video for a pro webinar when I click to browse I can select an acceptable file to upload
    Given the "SampleVideo_1280x720_5mb.mp4" video file is selected for existing pro webinar on the prepare and present page
    When I click on the "upload-video-page-remove-video-file-button"
    Then the element "upload-video-page-file-not-selected" is present

  @nightly @firefox
  Scenario: As a channel owner using a no flash browser who has selected a video to upload when I click browse and re select another video the new video filename will be shown
    Given the "SampleVideo_1280x720_5mb.mp4" video file is selected for existing pro webinar on the prepare and present page
    When I select a "SampleVideo_1280x720_20mb.mp4" video file
    Then I should see the "upload-video-page-selected-file" with text matching "SampleVideo_1280x720_20mb.mp4"

  @nightly @firefox
  Scenario: As a channel owner using a no flash browser who has selected a video to upload when I click browse and re select another video the new video filesize will be shown
    Given the "SampleVideo_1280x720_5mb.mp4" video file is selected for existing pro webinar on the prepare and present page
    When I select a "SampleVideo_1280x720_20mb.mp4" video file
    Then I should see the "upload-video-page-selected-file" with text matching "(20.09MB)"