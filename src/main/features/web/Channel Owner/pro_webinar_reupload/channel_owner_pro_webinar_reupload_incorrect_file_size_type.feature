Feature: Pro webinar reupload
  As a channel owner of a recorded pro webinar
  I want to be able see the warning messages when reuploading the video
  So that I can when file type and size are not correct

  @nightly @firefox
  Scenario: As a channel owner who has created Pro Webinar when I try to upload a file that is not the accepted file type I will not be able to select it
    Given I am logged in as manager
    When I visit the "pro-webinar-prepare-and-present" page for pro webinar 1510272071 in channel 2000248877
    And I click on the "pro-webinar-prepare-and-present-reupload-button"
    And I have selected a "SampleVideo_1280x720_5mb.mp3" video file
    Then I should see the "upload-video-page-upload-failed-message" with text matching "Upload of this video file failed"

  @nightly @firefox
  Scenario: As a channel owner who has created Pro Webinar when I try to upload a file that is not valid I am shown an error message that the file cannot be transcoded
    Given I am logged in as manager
    When I visit the "pro-webinar-prepare-and-present" page for pro webinar 1510272071 in channel 2000248877
    When I start reuploading "invalid_video_file.mp4" video file
    Then I should see the "upload-video-page-upload-failed-message" with text matching "Your file could not be transcoded."

  @nightly @firefox
  Scenario: As a channel owner who has created Pro Webinar when I try to upload a file that is too small I am shown an error message that the file cannot be transcoded
    Given I am logged in as manager
    When I visit the "pro-webinar-prepare-and-present" page for pro webinar 1510272071 in channel 2000248877
    And I click on the "pro-webinar-prepare-and-present-reupload-button"
    And I have selected a "image_test1.mp4" video file
    Then I should see the "upload-video-page-upload-failed-message" with text matching "Video file too small"