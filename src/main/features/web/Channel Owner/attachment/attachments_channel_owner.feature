Feature: Attachments
  As a owner of the pro webinar
  I want to be able add attachments
  So that audience can download them

  @nightly
  Scenario: Warning message is shown if no file is selected for the file type attachment
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    When I visit the "webcast-attachments-page" page
    And for element "attachment-type-locator" I select text "File or link to a file"
    And I click on the "add-attachment-button"
    And for element "attachment-file-title-field" I enter value "test attachment title"
    And for element "attachment-file-description-field" I enter value "test attachment description"
    And for element "attachment-upload-location" I select text "Europe"
    And I click on the "attachment-upload-button"
    Then I should see the "attachment-file-not-selected-error" with text matching "Please select a file"

  @nightly
  Scenario: Warning message is shown if virus file is selected for attachment upload
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    When I visit the "webcast-attachments-page" page
    And for element "attachment-type-locator" I select text "File or link to a file"
    And I click on the "add-attachment-button"
    And for element "attachment-file-title-field" I enter value "test attachment title"
    And for element "attachment-file-description-field" I enter value "test attachment description"
    And I select eicar-test-virus.ppt file for attachment upload
    And for element "attachment-upload-location" I select text "Europe"
    And I click on the "attachment-upload-button"
    Then the element "attachment-file-virus-detected-error" appears in "240" secs

  @nightly
  Scenario: Warning message is shown if compressed virus file is selected for attachment upload
    Given I am logged in as manager
    And the following channel features are set
      | key                           | value   |
      | Pro Webinar Slides            | enabled |
    And a pro webinar exists
    When I visit the "webcast-attachments-page" page
    And for element "attachment-type-locator" I select text "File or link to a file"
    And I click on the "add-attachment-button"
    And for element "attachment-file-title-field" I enter value "test attachment title"
    And for element "attachment-file-description-field" I enter value "test attachment description"
    And I select eicar-test-virus.ppt.zip file for attachment upload
    And for element "attachment-upload-location" I select text "Europe"
    And I click on the "attachment-upload-button"
    Then the element "attachment-file-virus-detected-error" appears in "240" secs