Feature: I should be on the right page after login from demand central
  As a BrightTALK user with different roles
  I should be redirected to the correct page after login from demand central 

@test
  Scenario Outline: It should show correct weekly sync settings as selected
	Given I log in as a user with only mandatory fields mapped for Marketo connector
	When I click on "connector-manage-on-homepage"
	And I update the sync settings as following
    |lead partition       |how often        		|email notification     |
    |US Leads    		  |Weekly,Thursday,03:00    |all	                |	
	Then I should see the "<field>" with text matching "<text>"
	Examples:
      | field                                       | text                             |
      | sync-settings-partition-field			    | US Leads                         |
      | sync-settings-howoften-field              	| Weekly                           |      
      | sync-settings-day-field              		| Thursday                         |
      | sync-settings-hour-field         	   	    | 03:00                            |

@test
  Scenario Outline: It should show correct daily sync settings as selected
	Given I log in as a user with only mandatory fields mapped for Marketo connector
	When I click on "connector-manage-on-homepage"
	And I update the sync settings as following
    |lead partition       |how often        		|email notification     |
    |US Leads    		  |Daily,Thursday,04:00     |all	                |	
	Then I should see the "<field>" with text matching "<text>"
	Examples:
      | field                                       | text                             |
      | sync-settings-partition-field			    | US Leads                         |
      | sync-settings-howoften-field              	| Daily                            |      
      | sync-settings-hour-field         	   	    | 04:00                            |

@test
  Scenario Outline: It should show error only email notification checked
	Given I log in as a user with only mandatory fields mapped for Marketo connector
	When I click on "connector-manage-on-homepage"
	And I update the sync settings as following
    |lead partition       |how often        		|email notification     |
    |US Leads    		  |Daily,Thursday,04:00     |all	                |	
	Then I should see "<checkbox>" is "<status>"      
	Examples:	
      | checkbox                                    | status                             |
      | error-completion-checkbox			        | checked                          	 |	
      | error-only-checkbox			   			    | unchecked                          |	     

@test
  Scenario Outline: It should show error and completion email notification checked
	Given I log in as a user with only mandatory fields mapped for Marketo connector
	When I click on "connector-manage-on-homepage"
	And I update the sync settings as following
    |lead partition       |how often        		|email notification     |
    |US Leads    		  |Daily,Thursday,04:00     |error	                |	
	Then I should see "<checkbox>" is "<status>"      
	Examples:	
      | checkbox                                    | status                             |
      | error-completion-checkbox			        | unchecked                          |	
      | error-only-checkbox			   			    | checked                            |	    

