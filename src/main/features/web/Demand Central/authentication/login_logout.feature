Feature: I should be on the right page after login from demand central
  As a BrightTALK user with different roles
  I should be redirected to the correct page after login from demand central 

@test
  Scenario Outline: I should be on the right page login from demand central
	Given I am a "<role>"
	And I visit the "demand central login" page
	When I log in with email and password
	Then I should be on the "<page>" page
	Examples:
      | role                                       	| page                             |
      | BrightTALK manager with accounts            | demand central home              |
      | BrightTALK manager without accounts         | channels I own                   |     
      | BrightTALK user					                    | knowledge feed                   |
      | channel owner with accounts		              | demand central home              |    
      | channel owner without accounts            	| channels I own                   |

