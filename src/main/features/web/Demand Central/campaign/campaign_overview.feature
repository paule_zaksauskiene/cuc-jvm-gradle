Feature: It should show correct on campaign overview page
  As a channel owner
  I should be able to see the correct information on campaign overview page

@test
  Scenario Outline: It should show right on campaign overview page
	Given I am logged in as channel owner
	And I own a channel
	And a pro webinar exists
	And I have a dc campaign with following details
      | key                                       	| value                            |
      | displayName        							            | Fan original campaign            |
      | status                               		    | Active  						             |
      | startDate                           	      | 2017-06-16					             | 
      | target									                    | 1000 							               |
      | leadsFulfilled         					            | 200   						               |
	    | budget           							              | 95000                            | 
      | CPL           							               	| 95                               | 
      | earlyDelivery         					            | true                             |
	    | community 									                | Information Technology 		       |	  
      | companySizeFilter       				            | 2500-5000                        |
      | jobLevelFilter        						          | Director+                        |
      | countries           						            | United States,Canada,China,Japan |
	    | partnerEx									                  | Oracle,Cloudendure 			         |
      | companyEx              					            | Google,Amazon               	   |
  When I visit the "brighttalk home" page     
  And I visit the "demand central campaign overview" page
	Then I should see the "<field>" with text matching "<text>"
	Examples:
      | field                                       | text                             |
      | dc-campaign-overview-budget-field           | 95,000                           |
      | dc-campaign-overview-cpl-field              | 95                               |
      | dc-campaign-overview-target-field           | 1,000                            |
      | dc-campaign-overview-startdate-field        | JUN 16 2017                      |
      | dc-campaign-overview-delivery-field         | As quickly as possible           |
      | dc-campaign-overview-community-field        | Information Technology           |      
      | dc-campaign-overview-companysize-field      | 2500-5000                        |   
      | dc-campaign-overview-geography-field        | NORTH AMERICA, China, Japan      |  
      | dc-campaign-overview-partnerex-field        | Oracle, Cloudendure              |   
      | dc-campaign-overview-companyex-field        | Google,Amazon                    | 