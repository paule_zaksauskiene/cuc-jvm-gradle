Feature: I should be able to update campaign displayed name 
  As a channel owner
  I should be able to update campaign displayed name on campaign listing page

@test
  Scenario: Update the campaign title as channel owner
	Given I am logged in as channel owner
	And I own a channel
	And a pro webinar exists
	And I have a dc campaign with following details
      | key                                       	| value                            |
      | status                               		    | Active  						             |
      | startDate                           	      | 2017-06-16					             | 
      | target									                    | 1000 							               |
      | leadsFulfilled         					            | 200   						               |
	    | budget           							              | 95000                            | 
      | CPL           							               	| 95                               | 
      | earlyDelivery         					            | true                             |
	    | community 									                | Information Technology 		       |	  
      | companySizeFilter       				            | 2500-5000                        |
      | jobLevelFilter        						          | Director+                        |
      | countries           						            | United States,Canada,China,Japan |
	    | partnerEx									                  | Oracle,Cloudendure 			         |
      | companyEx              					            | Google,Amazon               	   |  
  When I visit the "brighttalk home" page    
  And I visit the "demand central campaign listing" page
  Then I should see the "dc-campaign-listing-displayname-field" with text matching "1706_Content Lead Campaign"
  And I edit the dccampaign name as "Fan campaign name change"
  Then I should see the "dc-campaign-listing-displayname-field" with text matching "Fan campaign name change"

@test
  Scenario: Try to edit campaign title as empty
  Given I am logged in as demand central channel owner
  And I own a dcchannel
  And a pro webinar exists
  And I have a dc campaign with following details
      | key                                         | value                            |
      | status                                      | Active                           |
      | startDate                                   | 2017-06-16                       | 
      | target                                      | 1000                             |
      | leadsFulfilled                              | 200                              |
      | budget                                      | 95000                            | 
      | CPL                                         | 95                               | 
      | earlyDelivery                               | true                             |
      | community                                   | Information Technology           |    
      | companySizeFilter                           | 2500-5000                        |
      | jobLevelFilter                              | Director+                        |
      | countries                                   | United States,Canada,China,Japan |
      | partnerEx                                   | Oracle,Cloudendure               |
      | companyEx                                   | Google,Amazon                    |
  When I visit the "brighttalk home" page     
  And I visit the "demand central campaign listing" page
  And I edit the dccampaign name as ""
  Then I should see the "dc-campaign-listing-displayname-field" with text matching "1706_Content Lead Campaign"

@test
  Scenario: Try to cancel in the middle of editing
  Given I am logged in as demand central channel owner
  And I own a channel
  And a pro webinar exists
  And I have a dc campaign with following details
      | key                                         | value                            |
      | status                                      | Active                           |
      | startDate                                   | 2017-06-16                       | 
      | target                                      | 1000                             |
      | leadsFulfilled                              | 200                              |
      | budget                                      | 95000                            | 
      | CPL                                         | 95                               | 
      | earlyDelivery                               | true                             |
      | community                                   | Information Technology           |    
      | companySizeFilter                           | 2500-5000                        |
      | jobLevelFilter                              | Director+                        |
      | countries                                   | United States,Canada,China,Japan |
      | partnerEx                                   | Oracle,Cloudendure               |
      | companyEx                                   | Google,Amazon                    |
  When I visit the "brighttalk home" page     
  And I visit the "demand central campaign listing" page
  And I cancel after editing the dccampaign name as "Fan campaign name change"
  Then I should see the "dc-campaign-listing-displayname-field" with text matching "1706_Content Lead Campaign"



