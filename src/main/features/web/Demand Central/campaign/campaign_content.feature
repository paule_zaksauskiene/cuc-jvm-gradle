Feature: It should show correct on campaign content page
  As a channel owner
  I should be able to see the correct information on campaign content page

@test
  Scenario Outline: It should show right on campaign content page
	Given I am logged in as channel owner
	And I own a channel
	And I have a dc campaign with following webcasts
	 |date time 		    |description	|presenter	 |status   |tags			    | visibility  | published |
     |2021-08-24 08:17:00   |Jerry's   	    |CEO Jerry   |upcoming |Cloud Computing     | public	  | Publish  | 
     |2017-05-22 16:22:00   |Lin's webcast  |Lin     	 |recorded |IT   	            | public      | Publish   |
  When I visit the "brighttalk home" page 
  And I visit the "demand central campaign content" page
  Then I should see the "<field>" with text matching "<text>"
  Examples:
      | field                                       | text                       |
      | dc-campaign-content-datetime-line1          | Aug 24 2021                |
      | dc-campaign-content-status-line1            | Upcoming                   |            
      | dc-campaign-content-datetime-line2          | May 22 2017                |
      | dc-campaign-content-status-line2            | Recorded                   |      