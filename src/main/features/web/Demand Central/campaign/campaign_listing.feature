Feature: It should show correct on campaign listing page
  As a channel owner
  I should be able to see the correct information on campaign listing page

@test
  Scenario Outline: It should show right on campaign listing page
	Given I am logged in as channel owner
	And I own a channel
	And a pro webinar exists
	And I have multiple dc campaigns with following details
    |campaign name        |start date     |status     |target   |fulfillment  |
    |My First Campaign    |2017-07-05     |Inactive   |1000     |500          |
    |My Second Campaign   |2017-08-08     |Active     |1500     |750          |
    |My Third Campaign    |2017-09-06     |Pending    |2000     |0            |
    |My Fourth Campaign   |2017-06-12     |Complete   |2500     |2500         |   
  When I visit the "brighttalk home" page       
  And I visit the "demand central campaign listing" page
  Then I should see the "<field>" with text matching "<text>"
  Examples:
      | field                                       | text                       |
      | dc-campaign-listing-displayname-line1       | My Third Campaign          |
      | dc-campaign-listing-displayname-line2       | My Second Campaign         |
      | dc-campaign-listing-displayname-line3       | My First Campaign          |
      | dc-campaign-listing-displayname-line4       | My Fourth Campaign         |
      | dc-campaign-listing-startdate-line1         | Sep 06 2017                |
      | dc-campaign-listing-startdate-line2         | Aug 08 2017                |
      | dc-campaign-listing-startdate-line3         | Jul 05 2017                |
      | dc-campaign-listing-startdate-line4         | Jun 12 2017                |
      | dc-campaign-listing-status-line1            | Pending                	 |
      | dc-campaign-listing-status-line2            | Active                	 |
      | dc-campaign-listing-status-line3            | Inactive                	 |
      | dc-campaign-listing-status-line4            | Complete                	 |
      | dc-campaign-listing-target-line1            | 2,000                		 |
      | dc-campaign-listing-target-line2            | 1,500                		 |
      | dc-campaign-listing-target-line3            | 1,000                		 |
      | dc-campaign-listing-target-line4            | 2,500                		 |
      | dc-campaign-listing-fulfillment-line1       | 0                			 |
      | dc-campaign-listing-fulfillment-line2       | 750               		 |
      | dc-campaign-listing-fulfillment-line3       | 500               		 |
      | dc-campaign-listing-fulfillment-line4       | 2,500            		     |
 

















