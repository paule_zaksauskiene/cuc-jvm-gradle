Feature: It should show correct leads delivered on campaign audience page
  As a channel owner
  I should be able to see correct leads information on campaign audience page

@test
  Scenario Outline: It should show right on campaign audience page
  Given I am logged in as channel owner
  And I own a channel
  And a pro webinar exists
  And I have a dc campaign with following leads delivered
   |delivery date     |first name  |last name  |job title           |company  |job level     | company size | industry          | 
     |2017-08-21 00:00:00   |Jerry     |Lin      |Software Engineer     |Google   |Other       | 5000+    | Technology - Other    |
     |2017-05-20 16:20:11   |Steve     |Ken        |Engineering Manager   |Amazon   |Manager/Team lead | 2501-4999  | Media, Advertising, & PR| 
  When I visit the "brighttalk home" page 
  And I visit the "demand central campaign audience" page
  Then I should see "<field>" with data "<text>" show up
  Examples:
      | field                                       | text                       |
      | dc-campaign-audience-deliverydate-line1     | Aug 21 2017                |
      | dc-campaign-audience-name-line1             | Jerry Lin                  |
      | dc-campaign-audience-jobtitle-line1         | Software Engineer          |
      | dc-campaign-audience-company-line1          | Google                     |
      | dc-campaign-audience-deliverydate-line2     | May 20 2017                |
      | dc-campaign-audience-name-line2             | Steve Ken                  |
      | dc-campaign-audience-jobtitle-line2         | Engineering Manager        |
      | dc-campaign-audience-company-line2          | Amazon                     |  