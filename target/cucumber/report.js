$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Channel Owner/content/feature_image.feature");
formatter.feature({
  "line": 1,
  "name": "Feature image on the view or edit booking page",
  "description": "As a channel owner\nI want to be able see the feature image on the booking pages\nSo that is accessible for the audience",
  "id": "feature-image-on-the-view-or-edit-booking-page",
  "keyword": "Feature"
});
formatter.before({
  "duration": 3913885670,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "Upload powerpoint for upcoming webinar",
  "description": "",
  "id": "feature-image-on-the-view-or-edit-booking-page;upload-powerpoint-for-upcoming-webinar",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 6,
      "name": "@test"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "I am logged in as manager",
  "keyword": "Given "
});
formatter.step({
  "line": 9,
  "name": "the following channel features are set",
  "rows": [
    {
      "cells": [
        "key",
        "value"
      ],
      "line": 10
    },
    {
      "cells": [
        "Pro Webinar Slides",
        "enabled"
      ],
      "line": 11
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "a pro webinar exists",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "a pro webinar starts in 28 minutes",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I visit the \"presenter\" page",
  "keyword": "And "
});
formatter.step({
  "line": 15,
  "name": "I select PPT file \"SeleniumTest.ppt\" to begin upload",
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "temporal step",
  "keyword": "And "
});
formatter.step({
  "line": 17,
  "name": "PPT slides are uploaded",
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "I visit the \"view-booking-page\" page",
  "keyword": "And "
});
formatter.step({
  "line": 19,
  "name": "first feature image is uploaded",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "I visit the \"presenter\" page",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "I click a button to remove slides",
  "keyword": "When "
});
formatter.step({
  "line": 22,
  "name": "I click on the \"presenter-screen-remove-slides-confirmation-button\"",
  "keyword": "And "
});
formatter.step({
  "line": 23,
  "name": "I select PPT file \"spinningsnowflakes.pptx\" to begin upload",
  "keyword": "And "
});
formatter.step({
  "line": 24,
  "name": "PPT slides are uploaded",
  "keyword": "And "
});
formatter.step({
  "line": 25,
  "name": "I visit the \"view-booking-page\" page",
  "keyword": "And "
});
formatter.step({
  "line": 26,
  "name": "feature image is uploaded",
  "keyword": "And "
});
formatter.step({
  "line": 27,
  "name": "feature image is replaced",
  "keyword": "Then "
});
formatter.match({
  "location": "BrowserPreconditionSteps.i_am_authenticated_as_manager()"
});
formatter.result({
  "duration": 16092400299,
  "status": "passed"
});
formatter.match({
  "location": "ChannelPreconditionSteps.set_feature(KeyValuePair\u003e)"
});
formatter.result({
  "duration": 7390193228,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "a",
      "offset": 0
    },
    {},
    {
      "val": "pro",
      "offset": 2
    }
  ],
  "location": "WebcastPreconditionSteps.a_webcast_exists(String,String,String)"
});
formatter.result({
  "duration": 2875378149,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "28",
      "offset": 24
    }
  ],
  "location": "WebcastPreconditionSteps.a_pro_webinar_starts_in_minutes(String)"
});
formatter.result({
  "duration": 33167143488,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "presenter",
      "offset": 13
    }
  ],
  "location": "BrowserActionSteps.i_visit_page(String)"
});
formatter.result({
  "duration": 37824294242,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "SeleniumTest.ppt",
      "offset": 19
    }
  ],
  "location": "PresenterActionSteps.uploadPowerpointSlides(String)"
});
formatter.result({
  "duration": 5193457814,
  "status": "passed"
});
formatter.match({
  "location": "BrowserAssertionSteps.temporal_step()"
});
formatter.result({
  "duration": 193861,
  "status": "passed"
});
formatter.match({
  "location": "PresenterAssertionSteps.ppt_slides_are_uploaded()"
});
formatter.result({
  "duration": 101437417853,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "view-booking-page",
      "offset": 13
    }
  ],
  "location": "BrowserActionSteps.i_visit_page(String)"
});
formatter.result({
  "duration": 5363283536,
  "status": "passed"
});
formatter.match({
  "location": "ChannelOwnerAssertionSteps.featureImageIsShown()"
});
formatter.result({
  "duration": 62555767718,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "presenter",
      "offset": 13
    }
  ],
  "location": "BrowserActionSteps.i_visit_page(String)"
});
formatter.result({
  "duration": 42588602823,
  "status": "passed"
});
formatter.match({
  "location": "PresenterActionSteps.clickRemoveSlides()"
});
formatter.result({
  "duration": 128596844,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "presenter-screen-remove-slides-confirmation-button",
      "offset": 16
    },
    {},
    {}
  ],
  "location": "BrowserActionSteps.i_click(String,String,String)"
});
formatter.result({
  "duration": 6992350805,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "spinningsnowflakes.pptx",
      "offset": 19
    }
  ],
  "location": "PresenterActionSteps.uploadPowerpointSlides(String)"
});
formatter.result({
  "duration": 6998819186,
  "status": "passed"
});
formatter.match({
  "location": "PresenterAssertionSteps.ppt_slides_are_uploaded()"
});
formatter.result({
  "duration": 120776613063,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "view-booking-page",
      "offset": 13
    }
  ],
  "location": "BrowserActionSteps.i_visit_page(String)"
});
formatter.result({
  "duration": 2872996893,
  "status": "passed"
});
formatter.match({
  "location": "ChannelOwnerAssertionSteps.verifyFeatureImage()"
});
formatter.result({
  "duration": 62995896232,
  "status": "passed"
});
formatter.match({
  "location": "ChannelOwnerAssertionSteps.feature_image_is_replaced()"
});
formatter.result({
  "duration": 17512273386,
  "status": "passed"
});
formatter.after({
  "duration": 619319396427,
  "status": "passed"
});
});